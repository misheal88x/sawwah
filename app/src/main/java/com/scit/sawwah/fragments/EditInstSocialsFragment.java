package com.scit.sawwah.fragments;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.widget.ButtonBarLayout;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.google.gson.Gson;
import com.scit.sawwah.APIClasses.InstitutionsAPIsClass;
import com.scit.sawwah.Models.ContactObject;
import com.scit.sawwah.Models.InstitutionObject;
import com.scit.sawwah.R;
import com.scit.sawwah.activities.AddSocialActivity;
import com.scit.sawwah.adapters.EditInstSocialsAdapter;
import com.scit.sawwah.interfaces.IFailure;
import com.scit.sawwah.interfaces.IMove;
import com.scit.sawwah.interfaces.IResponse;
import com.scit.sawwah.tools.BaseFragment;

import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Misheal on 8/28/2019.
 */

public class EditInstSocialsFragment extends BaseFragment {

    private RecyclerView recyclerView;
    private List<ContactObject> contact_list;
    private EditInstSocialsAdapter adapter;
    private LinearLayoutManager layoutManager;
    private Button btn_add;
    private LinearLayout empty_layout;
    private int facility_id = 0;
    private RelativeLayout root;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_edit_inst_socials, container, false);
    }
    @Override
    public void init_events() {
        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                facility_id = base.sharedPreferences.getInt("update_inst_id",0);
                Intent intent = new Intent(base,AddSocialActivity.class);
                intent.putExtra("facility_id",facility_id);
                startActivityForResult(intent,100);
            }
        });
    }

    @Override
    public void init_fragment(Bundle savedInstanceState) {
        init_recycler();
        //btn_add.setVisibility(View.GONE);
    }

    @Override
    public void init_views() {
        super.init_views();
        btn_add = base.findViewById(R.id.edit_inst_socials_add_btn);
        recyclerView = base.findViewById(R.id.edit_inst_socials_recycler);
        empty_layout = base.findViewById(R.id.empty_layout);
        root = base.findViewById(R.id.edit_inst_socials_layout);
        //btn_add.setVisibility(View.GONE);
    }

    private void init_recycler(){
        init_list();
        adapter = new EditInstSocialsAdapter(base,contact_list, new IMove() {
            @Override
            public void move(int position) {

            }
        });
        layoutManager = new LinearLayoutManager(base,LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    private void init_list(){
        contact_list = new ArrayList<>();
        ContactObject[] contact_array = new Gson().fromJson(base.sharedPreferences.getString("update_inst_contacts","[]"),ContactObject[].class);
        if (contact_array.length>0){
            empty_layout.setVisibility(View.GONE);
            //btn_add.setVisibility(View.VISIBLE);
            for (ContactObject co : contact_array){
                contact_list.add(co);
            }
        }else {
            empty_layout.setVisibility(View.VISIBLE);
            //btn_add.setVisibility(View.GONE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 100){
            if (resultCode == RESULT_OK){
                empty_layout.setVisibility(View.GONE);
                callAPI();
            }
        }
    }

    private void callAPI(){
        InstitutionsAPIsClass.details(base.sharedPreferences,
                base,
                base.sharedPreferences.getString("language", "ar"),
                base.sharedPreferences.getString("service_token", "")
                ,String.valueOf(facility_id),
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json !=null){
                            String json1 = new Gson().toJson(json);
                            InstitutionObject success = new Gson().fromJson(json1,InstitutionObject.class);


                            //Contacts
                            if (success.getContacts()!=null){
                                if (success.getContacts().size()>0){
                                    List<ContactObject> list = new ArrayList<>();
                                    for (ContactObject co : success.getContacts()){
                                        list.add(co);
                                    }
                                    String co_json = new Gson().toJson(list);
                                    base.editor.putString("update_inst_contacts",co_json);
                                }
                            }
                            base.editor.commit();
                            init_recycler();
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callAPI();
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });
    }
}
