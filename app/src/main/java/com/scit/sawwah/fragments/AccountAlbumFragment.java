package com.scit.sawwah.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.snackbar.Snackbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.scit.sawwah.APIClasses.MemorialAPIsClass;
import com.scit.sawwah.Models.MemorialImageObject;
import com.scit.sawwah.Models.MemorialImagesResponse;
import com.scit.sawwah.R;
import com.scit.sawwah.activities.AddMemorialImageActivity;
import com.scit.sawwah.adapters.AccountAlbumAdapter;
import com.scit.sawwah.interfaces.IFailure;
import com.scit.sawwah.interfaces.IMove;
import com.scit.sawwah.interfaces.IResponse;
import com.scit.sawwah.tools.BaseFragment;
import com.scit.sawwah.tools.EndlessRecyclerViewGridScrollListener;

import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Misheal on 8/9/2019.
 */

public class AccountAlbumFragment extends BaseFragment {
    private RecyclerView rv_images;
    private List<MemorialImageObject> list_images;
    private AccountAlbumAdapter adapter;
    private GridLayoutManager layoutManager;
    private RelativeLayout root;
    private int currentPage = 1;
    private LinearLayout empty_layout;
    private Button btn_add;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_account_album, container, false);
    }

    @Override
    public void init_events() {
        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(base, AddMemorialImageActivity.class);
                intent.putExtra("type","customer");
                startActivityForResult(intent,100);
            }
        });
    }

    @Override
    public void init_fragment(Bundle savedInstanceState) {
        list_images = new ArrayList<>();
        adapter = new AccountAlbumAdapter(base,1 ,list_images,new IMove() {
            @Override
            public void move(int position) {
                callDeleteAPI(String.valueOf(list_images.get(position).getId()),position);
            }
        });
        layoutManager = new GridLayoutManager(base,3);
        rv_images.setLayoutManager(layoutManager);
        rv_images.setAdapter(adapter);
        rv_images.addOnScrollListener(new EndlessRecyclerViewGridScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if (list_images.size()>=20){
                    Snackbar.make(root,getResources().getString(R.string.get_more),Snackbar.LENGTH_SHORT).show();
                    currentPage++;
                    callAPI(currentPage,1);
                }
            }
        });
        callAPI(currentPage,0);
    }

    @Override
    public void init_views() {
        rv_images = base.findViewById(R.id.account_album_recycler);
        root = base.findViewById(R.id.account_album_layout);
        empty_layout = base.findViewById(R.id.empty_layout);
        btn_add = base.findViewById(R.id.account_album_add_btn);
    }

    private void callAPI(final int page, final int type){
        MemorialAPIsClass.getCustomerImages(base.sharedPreferences,
                base,
                base.sharedPreferences.getString("language", "ar"),
                base.sharedPreferences.getString("service_token", ""),
                type,
                page,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json !=null){
                            String json1 = new Gson().toJson(json);
                            MemorialImagesResponse success = new Gson().fromJson(json1,MemorialImagesResponse.class);
                            if (success.getData()!=null){
                                if (success.getData().size() > 0){
                                    empty_layout.setVisibility(View.GONE);
                                    for (MemorialImageObject mio : success.getData()){
                                        list_images.add(mio);
                                        adapter.notifyDataSetChanged();
                                    }
                                    if (type == 0){
                                        runAnimation(rv_images,0,adapter);
                                    }
                                }else {
                                    if (type == 1){
                                        Snackbar.make(root,getResources().getString(R.string.no_more),Snackbar.LENGTH_SHORT).show();
                                    }else {
                                        empty_layout.setVisibility(View.VISIBLE);
                                        Toast.makeText(base, getResources().getString(R.string.no_data), Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }else {
                                empty_layout.setVisibility(View.VISIBLE);
                            }
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callAPI(page,type);
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });
    }

    private void callDeleteAPI(final String image_id, final int position){
        MemorialAPIsClass.removeCustomerImage(base.sharedPreferences,
                base,
                base.sharedPreferences.getString("language", "ar"),
                base.sharedPreferences.getString("service_token", ""),
                image_id,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json !=null){
                            Toast.makeText(base, base.getResources().getString(R.string.memorial_image_delete_success), Toast.LENGTH_SHORT).show();
                            list_images.remove(position);
                            currentPage = 1;
                            adapter.notifyDataSetChanged();
                            list_images.clear();
                            rv_images.setAdapter(new AccountAlbumAdapter(base, 1, new ArrayList<MemorialImageObject>(), new IMove() {
                                @Override
                                public void move(int position) {
                                    callDeleteAPI(String.valueOf(list_images.get(position).getId()),position);
                                }
                            }));
                            callAPI(currentPage,0);
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callDeleteAPI(image_id,position);
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });
    }
    public void runAnimation(RecyclerView recyclerView,int type,AccountAlbumAdapter adapter){
        Context context = recyclerView.getContext();
        LayoutAnimationController controller = null;
        if (type == 0){ // Fall down animation
            controller = AnimationUtils.loadLayoutAnimation(context,R.anim.layout_fall_down);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutAnimation(controller);
            recyclerView.getAdapter().notifyDataSetChanged();
            recyclerView.scheduleLayoutAnimation();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 100){
            if (resultCode == RESULT_OK){
                list_images.clear();
                currentPage = 1;
                rv_images.setAdapter(new AccountAlbumAdapter(base, 1, new ArrayList<MemorialImageObject>(), new IMove() {
                    @Override
                    public void move(int position) {
                        callDeleteAPI(String.valueOf(list_images.get(position).getId()),position);
                    }
                }));
                callAPI(currentPage,0);
            }
        }

    }
}
