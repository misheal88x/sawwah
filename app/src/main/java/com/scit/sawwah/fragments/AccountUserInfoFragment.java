package com.scit.sawwah.fragments;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.scit.sawwah.R;
import com.scit.sawwah.activities.EditPasswordActivity;
import com.scit.sawwah.tools.BaseFragment;
import com.scit.sawwah.tools.BaseFunctions;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Misheal on 8/12/2019.
 */

public class AccountUserInfoFragment extends BaseFragment {
    private TextView btn_edit_password;
    private CircleImageView img_profile;
    private TextView tv_name;
    private TextView tv_activate;
    private ImageView img_activate;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_account_user_info, container, false);
    }

    @Override
    public void init_events() {
        btn_edit_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(base, EditPasswordActivity.class));
            }
        });
    }

    @Override
    public void init_fragment(Bundle savedInstanceState) {
        //Image
        BaseFunctions.setGlideImage(base,img_profile,base.sharedPreferences.getString("account_image",""));
        //Name
        tv_name.setText(base.sharedPreferences.getString("account_name",""));
        //Activate
        int active = base.sharedPreferences.getInt("account_status",0);
        if (active == 1||active == 2){
            tv_activate.setText(getResources().getString(R.string.settings_active));
            img_activate.setImageResource(R.drawable.ic_circle_green_filled);
        }else {
            tv_activate.setText(getResources().getString(R.string.settings_dis_active));
            img_activate.setImageResource(R.drawable.ic_circle_red_filled);
        }
    }

    @Override
    public void init_views() {
        btn_edit_password = base.findViewById(R.id.account_user_info_edit_password);
        img_profile = base.findViewById(R.id.account_user_info_image);
        tv_name = base.findViewById(R.id.account_user_info_user_name);
        tv_activate = base.findViewById(R.id.account_user_info_active_txt);
        img_activate = base.findViewById(R.id.account_user_info_active_light);
    }
}
