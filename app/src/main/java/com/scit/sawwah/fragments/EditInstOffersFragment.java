package com.scit.sawwah.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.google.gson.Gson;
import com.scit.sawwah.Models.OfferObject;
import com.scit.sawwah.R;
import com.scit.sawwah.activities.AddOfferActivity;
import com.scit.sawwah.adapters.EditInstOffersAdapter;
import com.scit.sawwah.interfaces.IMove;
import com.scit.sawwah.tools.BaseFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Misheal on 8/28/2019.
 */

public class EditInstOffersFragment extends BaseFragment {
    private Button btn_add;
    private RecyclerView rv_ads;
    private List<OfferObject> list_offfers;
    private EditInstOffersAdapter adapter;
    private LinearLayoutManager layoutManager;
    private RelativeLayout root;
    private LinearLayout empty_layout;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_edit_inst_offers, container, false);
    }
    @Override
    public void init_events() {
        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(base, AddOfferActivity.class));
            }
        });
    }

    @Override
    public void init_fragment(Bundle savedInstanceState) {
        init_recycler();
    }

    @Override
    public void init_views() {
        super.init_views();
        btn_add = base.findViewById(R.id.edit_inst_offers_add_btn);
        rv_ads = base.findViewById(R.id.edit_inst_offers_recycler);
        root = base.findViewById(R.id.edit_inst_offers_layout);
        empty_layout = base.findViewById(R.id.empty_layout);
    }

    private void init_recycler(){
        list_offfers = new ArrayList<>();
        init_list();
        adapter = new EditInstOffersAdapter(base,list_offfers,root, new IMove() {
            @Override
            public void move(int position) {

            }
        });
        layoutManager = new LinearLayoutManager(base,LinearLayoutManager.VERTICAL,false);
        rv_ads.setLayoutManager(layoutManager);
        runAnimation(rv_ads,0,adapter);
        rv_ads.setAdapter(adapter);
    }

    private void init_list(){
        OfferObject[] array_offers = new Gson().fromJson(base.sharedPreferences.getString("update_inst_offers","[]"),OfferObject[].class);
        if (array_offers.length > 0){
            empty_layout.setVisibility(View.GONE);
            for (OfferObject oo : array_offers){
                list_offfers.add(oo);
            }
        }
    }
    public void runAnimation(RecyclerView recyclerView,int type,EditInstOffersAdapter adapter){
        Context context = recyclerView.getContext();
        LayoutAnimationController controller = null;
        if (type == 0){ // Fall down animation
            controller = AnimationUtils.loadLayoutAnimation(context,R.anim.layout_fall_down_vertical);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutAnimation(controller);
            recyclerView.getAdapter().notifyDataSetChanged();
            recyclerView.scheduleLayoutAnimation();
        }
    }
}
