package com.scit.sawwah.fragments;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.snackbar.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.rm.rmswitch.RMSwitch;
import com.scit.sawwah.APIClasses.CardAPIsClass;
import com.scit.sawwah.Models.MyCardObject;
import com.scit.sawwah.R;
import com.scit.sawwah.activities.GetCardActivity;
import com.scit.sawwah.interfaces.IFailure;
import com.scit.sawwah.interfaces.IResponse;
import com.scit.sawwah.tools.BaseFragment;
import com.scit.sawwah.tools.BaseFunctions;

/**
 * Created by Misheal on 8/13/2019.
 */

public class AccountMyCardFragment extends BaseFragment {

    private RMSwitch sw_status;
    private TextView tv_get_card;
    private ImageView img_front,img_back;
    private TextView tv_expire_date;
    private LinearLayout root;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_account_my_card, container, false);
    }

    @Override
    public void init_events() {
        tv_get_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(base, GetCardActivity.class));
            }
        });
    }

    @Override
    public void init_fragment(Bundle savedInstanceState) {
        setActiveSwitchColors();
        sw_status.setClickable(false);
        callAPI();

    }

    @Override
    public void init_views() {
        sw_status = base.findViewById(R.id.account_my_card_status_switch);
        tv_get_card = base.findViewById(R.id.account_my_card_how_get_card_txt);
        img_front = base.findViewById(R.id.account_my_card_front_image);
        img_back = base.findViewById(R.id.account_my_card_back_image);
        tv_expire_date = base.findViewById(R.id.account_my_card_expire_date);
        root = base.findViewById(R.id.account_my_card_layout);
        root.setVisibility(View.GONE);
    }

    private void setActiveSwitchColors() {
        sw_status.setSwitchBkgCheckedColor(getResources().getColor(R.color.colorTextSecobdary));
        sw_status.setSwitchToggleCheckedColor(Color.WHITE);
        sw_status.setSwitchToggleNotCheckedColor(Color.WHITE);
    }
    private void callAPI(){
        CardAPIsClass.getCardInfo(base.sharedPreferences,
                base,
                base.sharedPreferences.getString("language", "ar"),
                base.sharedPreferences.getString("service_token", ""),
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json !=null){
                            String json1 = new Gson().toJson(json);
                            MyCardObject success = new Gson().fromJson(json1,MyCardObject.class);
                            //Status
                            if (success.getStatus() == 0){
                                sw_status.setChecked(false);
                            }else {
                                sw_status.setChecked(true);
                            }
                            //Front Image
                            if (success.getFrontside_image()!=null){
                                BaseFunctions.setGlideImage(base,img_front,success.getFrontside_image());
                            }
                            //Back Image
                            if (success.getBackside_image()!=null){
                                BaseFunctions.setGlideImage(base,img_back,success.getBackside_image());
                            }
                            //Expire Date
                            if (success.getExpired_at()!=null){
                                tv_expire_date.setText(BaseFunctions.dateExtractor(success.getExpired_at()));
                            }
                            root.setVisibility(View.VISIBLE);
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callAPI();
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });
    }

}
