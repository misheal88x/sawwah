package com.scit.sawwah.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.snackbar.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.scit.sawwah.APIClasses.StatisticsAPIsClass;
import com.scit.sawwah.Models.StatisticObject;
import com.scit.sawwah.R;
import com.scit.sawwah.interfaces.IFailure;
import com.scit.sawwah.interfaces.IResponse;
import com.scit.sawwah.tools.BaseFragment;

/**
 * Created by Misheal on 8/13/2019.
 */

public class AccountStatisticsFragment extends BaseFragment {
    private TextView tv_favourite,tv_search,tv_packages,tv_card;
    private LinearLayout root;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_account_statistics, container, false);
    }

    @Override
    public void init_events() {

    }

    @Override
    public void init_fragment(Bundle savedInstanceState) {
        callAPI();
    }

    @Override
    public void init_views() {
        tv_favourite = base.findViewById(R.id.account_statistics_fav_txt);
        tv_card = base.findViewById(R.id.account_statistics_card_txt);
        tv_packages = base.findViewById(R.id.account_statistics_packages_txt);
        tv_search = base.findViewById(R.id.account_statistics_search_txt);
        root = base.findViewById(R.id.account_statistics_layout);
    }

    private void callAPI(){
        StatisticsAPIsClass.getSta(base.sharedPreferences,
                base,
                base.sharedPreferences.getString("language", "ar"),
                base.sharedPreferences.getString("service_token", ""),
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json != null){
                            String json1 = new Gson().toJson(json);
                            StatisticObject success = new Gson().fromJson(json1,StatisticObject.class);
                            tv_search.setText(String.valueOf(success.getSearch_count()));
                            tv_favourite.setText(String.valueOf(success.getFav_facilities_count()));
                            tv_card.setText(String.valueOf(success.getCard_usage()));
                            tv_packages.setText(String.valueOf(success.getPack_req_count()));
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callAPI();
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });
    }

}
