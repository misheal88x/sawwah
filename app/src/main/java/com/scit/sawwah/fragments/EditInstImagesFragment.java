package com.scit.sawwah.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.widget.ButtonBarLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.scit.sawwah.APIClasses.InstitutionsAPIsClass;
import com.scit.sawwah.APIClasses.MemorialAPIsClass;
import com.scit.sawwah.Models.InstitutionObject;
import com.scit.sawwah.Models.MemorialImageObject;
import com.scit.sawwah.R;
import com.scit.sawwah.activities.AddMemorialImageActivity;
import com.scit.sawwah.adapters.AccountAlbumAdapter;
import com.scit.sawwah.interfaces.IFailure;
import com.scit.sawwah.interfaces.IMove;
import com.scit.sawwah.interfaces.IResponse;
import com.scit.sawwah.tools.BaseFragment;

import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Misheal on 8/28/2019.
 */

public class EditInstImagesFragment extends BaseFragment {

    private RecyclerView recyclerView;
    private AccountAlbumAdapter adapter;
    private GridLayoutManager layoutManager;
    private List<MemorialImageObject> images_list;
    private Button btn_add;
    private LinearLayout empty_layout;
    private String inst_id = "";
    private RelativeLayout root;
    @Override
    public void init_events() {
        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InstitutionObject inst = new Gson().fromJson(base.sharedPreferences.getString("update_inst_institution_object","{}"),InstitutionObject.class);
                if (inst!=null){
                    if (inst.getId()!=0){
                        inst_id = String.valueOf(inst.getId());
                        Intent intent = new Intent(base, AddMemorialImageActivity.class);
                        intent.putExtra("institution_id",inst.getId());
                        intent.putExtra("type","facility");
                        startActivityForResult(intent,100);
                    }
                }
            }
        });
    }

    @Override
    public void init_fragment(Bundle savedInstanceState) {
        init_recycler();
    }

    @Override
    public void init_views() {
        super.init_views();
        recyclerView = base.findViewById(R.id.edit_inst_images_recycler);
        btn_add = base.findViewById(R.id.edit_inst_images_add_btn);
        empty_layout = base.findViewById(R.id.empty_layout);
        root = base.findViewById(R.id.edit_inst_image_layout);
        //btn_add.setVisibility(View.GONE);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_edit_inst_images, container, false);
    }

    private void init_recycler(){
        init_list();
        adapter = new AccountAlbumAdapter(base, 1, images_list,new IMove() {
            @Override
            public void move(int position) {
                callDeleteAPI(String.valueOf(images_list.get(position).getId()),position);
            }
        });
        layoutManager = new GridLayoutManager(base,3);
        recyclerView.setLayoutManager(layoutManager);
        runAnimation(recyclerView,0,adapter);
        recyclerView.setAdapter(adapter);
    }

    private void init_list(){
        images_list = new ArrayList<>();
        MemorialImageObject[] images_array = new Gson().fromJson(base.sharedPreferences.getString("update_inst_images","[]"),MemorialImageObject[].class);
        if (images_array.length > 0){
            empty_layout.setVisibility(View.GONE);
            //btn_add.setVisibility(View.VISIBLE);
            for (MemorialImageObject meo : images_array){
                images_list.add(meo);
            }
        }else {
            empty_layout.setVisibility(View.VISIBLE);
            //btn_add.setVisibility(View.GONE);
        }
    }
    public void runAnimation(RecyclerView recyclerView,int type,AccountAlbumAdapter adapter){
        Context context = recyclerView.getContext();
        LayoutAnimationController controller = null;
        if (type == 0){ // Fall down animation
            controller = AnimationUtils.loadLayoutAnimation(context,R.anim.layout_fall_down);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutAnimation(controller);
            recyclerView.getAdapter().notifyDataSetChanged();
            recyclerView.scheduleLayoutAnimation();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 100){
            if (resultCode == RESULT_OK){
                callAPI();
            }
        }

    }

    private void callAPI(){
        InstitutionsAPIsClass.details(base.sharedPreferences,
                base,
                base.sharedPreferences.getString("language", "ar"),
                base.sharedPreferences.getString("service_token", ""),
                inst_id
                ,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json !=null){
                            String json1 = new Gson().toJson(json);
                            InstitutionObject success = new Gson().fromJson(json1,InstitutionObject.class);
                            //Memorial images
                            if (success.getMemorial_images()!=null){
                                if (success.getMemorial_images().size()>0){
                                    List<MemorialImageObject> list = new ArrayList<>();
                                    for (MemorialImageObject meo : success.getMemorial_images()){
                                        list.add(meo);
                                    }
                                    String meo_json = new Gson().toJson(list);
                                    base.editor.putString("update_inst_images",meo_json);
                                }else {
                                    base.editor.putString("update_inst_images","[]");
                                }
                            }else {
                                base.editor.putString("update_inst_images","[]");
                            }
                            base.editor.commit();
                            init_recycler();
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callAPI();
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });
    }

    private void callDeleteAPI(final String image_id, final int position){
        MemorialAPIsClass.removeImage(base.sharedPreferences,
                base,
                base.sharedPreferences.getString("language", "ar"),
                base.sharedPreferences.getString("service_token", ""),
                image_id,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json !=null){
                            Toast.makeText(base, base.getResources().getString(R.string.memorial_image_delete_success), Toast.LENGTH_SHORT).show();
                            images_list.remove(position);
                            adapter.notifyDataSetChanged();
                            callAPI();
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callDeleteAPI(image_id,position);
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });
    }
}
