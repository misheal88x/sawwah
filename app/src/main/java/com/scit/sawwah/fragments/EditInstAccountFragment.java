package com.scit.sawwah.fragments;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;
import com.scit.sawwah.APIClasses.InstitutionsAPIsClass;
import com.scit.sawwah.Models.CategoryObject;
import com.scit.sawwah.Models.CityObject;
import com.scit.sawwah.Models.ContactInfoObject;
import com.scit.sawwah.Models.CountryObject;
import com.scit.sawwah.Models.InstitutionObject;
import com.scit.sawwah.R;
import com.scit.sawwah.activities.MapActivity;
import com.scit.sawwah.adapters.CustomSpinnerAdapter;
import com.scit.sawwah.adapters.RegisterCategoriesAdapter;
import com.scit.sawwah.adapters.RegisterCategoriesRowsAdapter;
import com.scit.sawwah.interfaces.IFailure;
import com.scit.sawwah.interfaces.IMove;
import com.scit.sawwah.interfaces.IResponse;
import com.scit.sawwah.tools.BaseFragment;
import com.scit.sawwah.tools.BaseFunctions;
import com.soundcloud.android.crop.Crop;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Misheal on 8/28/2019.
 */

public class EditInstAccountFragment extends BaseFragment {

    private RecyclerView categories_recycler;
    private List<CategoryObject> categories_list;
    private List<CategoryObject> input_categories_list;
    private RegisterCategoriesAdapter categories_adapter;
    private GridLayoutManager categories_layout_manager;
    private Spinner countries_spinner,cities_spinner;
    private CustomSpinnerAdapter countries_adapter,cities_adapter;
    private List<String> countries_list_string,cities_list_string;
    private List<CityObject> cities_list;
    private List<CountryObject> countries_list;
    private ImageView map;
    private TextView tv_choose_category;
    private SimpleRatingBar rb_rate;
    private EditText edt_name,edt_desc,edt_address,edt_phone,edt_land_line;
    private CircleImageView img_icon;
    private ImageView img_cover,img_map;
    private LinearLayout root,btn_save;
    private int category_id = 0;
    private int country_id = 0;
    private int city_id = 0;
    private String lat = "";
    private String lng = "";
    private String choosed_rate = "";
    private static final String TEMP_PHOTO_FILE1 = "temporary_holder1.jpg";
    private static final String TEMP_PHOTO_FILE2 = "temporary_holder2.jpg";
    private String iconPath = "",coverPath = "";
    private Uri iconUri = null,coverUri = null;
    private boolean icon_chosen = false;
    private boolean cover_chosen = false;
    private int image_clicked = -1;
    private String id = "";
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_edit_inst_account, container, false);
    }
    @Override
    public void init_events() {
        map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(base, MapActivity.class);
                i.putExtra("type","with_edit");
                i.putExtra("lat",lat);
                i.putExtra("lng",lng);
                startActivityForResult(i, 1);
            }
        });
        tv_choose_category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder=new AlertDialog.Builder(base);
                View view=getLayoutInflater().inflate(R.layout.dialog_register_categories,null);
                input_categories_list = new ArrayList<>();
                CategoryObject[] input_category_array = new Gson().fromJson(base.sharedPreferences.getString("categories","[]"),CategoryObject[].class);
                if (input_category_array.length>0){
                    for (CategoryObject co : input_category_array){
                        input_categories_list.add(co);
                    }
                }
                RecyclerView recycler = view.findViewById(R.id.register_categories_dialog_recycler);
                builder.setView(view);
                final AlertDialog dialog = builder.create();
                LinearLayoutManager layoutManager = new LinearLayoutManager(base,LinearLayoutManager.VERTICAL,false);
                RegisterCategoriesRowsAdapter adapter = new RegisterCategoriesRowsAdapter(base, input_categories_list, new IMove() {
                    @Override
                    public void move(int position) {
                        if (categories_list.size()==0){
                            categories_list.add(input_categories_list.get(position));
                            categories_adapter.notifyDataSetChanged();
                            dialog.cancel();
                        }else {
                            Toast.makeText(base, getResources().getString(R.string.register_inst_no_more_category), Toast.LENGTH_SHORT).show();
                            dialog.cancel();
                        }
                    }
                });
                recycler.setLayoutManager(layoutManager);
                recycler.setAdapter(adapter);
                dialog.show();
            }
        });
        countries_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int i, long id) {
                long pos = parent.getItemIdAtPosition(i);
                int position = Integer.valueOf(String.valueOf(pos));
                country_id = countries_list.get(position).getId();
                CountryObject co = countries_list.get(position);
                if (co.getCities()!=null){
                    if (co.getCities().size() > 0){
                        cities_list.clear();
                        cities_list_string.clear();
                        cities_adapter.notifyDataSetChanged();
                        for (CityObject coo : co.getCities()){
                            cities_list.add(coo);
                            cities_list_string.add(coo.getName());
                            cities_adapter.notifyDataSetChanged();
                        }
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        cities_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int i, long id) {
                long pos = parent.getItemIdAtPosition(i);
                int position = Integer.valueOf(String.valueOf(pos));
                city_id = cities_list.get(position).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        rb_rate.setOnRatingBarChangeListener(new SimpleRatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(SimpleRatingBar simpleRatingBar, float rating, boolean fromUser) {
                choosed_rate = String.valueOf(rating);
            }
        });
        img_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestPermissions(new String[]
                        {
                                Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE
                        },1);
                image_clicked = 1;
            }
        });
        img_cover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestPermissions(new String[]
                        {
                                Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE
                        },1);
                image_clicked = 2;
            }
        });

        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(categories_list.size() == 0){
                    Toast.makeText(base, getResources().getString(R.string.register_inst_no_category), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (country_id == 0){
                    Toast.makeText(base, getResources().getString(R.string.register_inst_no_country), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (city_id == 0){
                    Toast.makeText(base, getResources().getString(R.string.register_inst_no_city), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (choosed_rate.equals("")){
                    Toast.makeText(base, getResources().getString(R.string.register_inst_no_rate), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (edt_name.getText().toString().equals("")){
                    Toast.makeText(base, getResources().getString(R.string.register_inst_no_name), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (edt_desc.getText().toString().equals("")){
                    Toast.makeText(base, getResources().getString(R.string.register_inst_no_desc), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (edt_address.getText().toString().equals("")){
                    Toast.makeText(base, getResources().getString(R.string.register_inst_no_address), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (lat.equals("")||lng.equals("")){
                    Toast.makeText(base, getResources().getString(R.string.register_inst_no_location), Toast.LENGTH_SHORT).show();
                    return;
                }
                callAPI();
            }
        });
    }

    @Override
    public void init_fragment(Bundle savedInstanceState) {
        init_spinner();
        init_recycler();
        restore_views();
    }

    @Override
    public void init_views() {
        super.init_views();
        categories_recycler = base.findViewById(R.id.edit_inst_account_recycler);
        countries_spinner = base.findViewById(R.id.edit_inst_account_country_spinner);
        cities_spinner = base.findViewById(R.id.edit_inst_account_city_spinner);
        map = base.findViewById(R.id.edit_inst_account_map);
        tv_choose_category = base.findViewById(R.id.edit_inst_account_category_arrow);
        rb_rate = base.findViewById(R.id.edit_inst_account_rate);
        edt_name = base.findViewById(R.id.edit_inst_account_name);
        edt_desc = base.findViewById(R.id.edit_inst_account_desc);
        edt_address = base.findViewById(R.id.edit_inst_account_address);
        img_icon = base.findViewById(R.id.edit_inst_account_icon);
        img_cover = base.findViewById(R.id.edit_inst_account_cover);
        img_map = base.findViewById(R.id.edit_inst_account_map);
        root = base.findViewById(R.id.edit_inst_account_layout);
        btn_save = base.findViewById(R.id.edit_inst_account_save_btn);
        edt_phone = base.findViewById(R.id.register_inst_phone_number);
        edt_land_line = base.findViewById(R.id.register_inst_land_line);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case 1:
            {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    Crop.pickImage(base,this);
                }else {
                    Toast.makeText(base, getResources().getString(R.string.add_photos_permission_denied), Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }

    private Uri getTempUri() {
        if (image_clicked == 1){
            return Uri.fromFile(getTempFile(TEMP_PHOTO_FILE1));
        }else {
            return Uri.fromFile(getTempFile(TEMP_PHOTO_FILE2));
        }
    }

    private File getTempFile(String temp) {
        if (isSDCARDMounted()) {

            File f = new File(Environment.getExternalStorageDirectory(),temp);
            try {
                f.createNewFile();
            } catch (IOException e) {

            }
            return f;
        } else {
            return null;
        }
    }

    private boolean isSDCARDMounted(){
        String status = Environment.getExternalStorageState();
        if (status.equals(Environment.MEDIA_MOUNTED))
            return true;
        return false;
    }

    @Override
    public void onActivityResult(int requestCode, final int resultCode, final Intent data) {
        if (resultCode==RESULT_OK){
            if (requestCode == Crop.REQUEST_PICK) {
                base.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Uri source_uri = data.getData();
                        if (image_clicked == 1){
                            Uri destination_uri = Uri.fromFile(getTempFile(TEMP_PHOTO_FILE1));
                            Crop.of(source_uri,destination_uri).asSquare().start(base,EditInstAccountFragment.this);
                            //img_icon.setImageURI(Crop.getOutput(data));
                            String filePath= Environment.getExternalStorageDirectory()
                                    +"/"+ TEMP_PHOTO_FILE1;
                            iconPath = filePath;
                            icon_chosen = true;

                        }else {
                            Uri destination_uri = Uri.fromFile(getTempFile(TEMP_PHOTO_FILE2));
                            Crop.of(source_uri,destination_uri).withAspect(16,9).start(getContext(),EditInstAccountFragment.this);
                            //img_cover.setImageURI(Crop.getOutput(data));
                            String filePath= Environment.getExternalStorageDirectory()
                                    + "/"+TEMP_PHOTO_FILE2;
                            coverPath = filePath;
                            cover_chosen = true;

                        }
                    }
                });
            }else if (requestCode == Crop.REQUEST_CROP){
                handle_crop(resultCode,data);
            }
        }
        if (requestCode == 1){
            if(resultCode == Activity.RESULT_OK){
                lat=data.getStringExtra("latitude");
                lng = data.getStringExtra("longitude");
            }
        }
    }

    private void handle_crop(int code, final Intent result){
        if (code == RESULT_OK){
            base.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (image_clicked == 1){
                        img_icon.setImageResource(0);
                        img_icon.setImageURI(Crop.getOutput(result));
                        String filePath= Environment.getExternalStorageDirectory()
                                + "/"+TEMP_PHOTO_FILE1;
                        iconPath = filePath;
                        icon_chosen = true;
                    }else {
                        img_cover.setImageResource(0);
                        img_cover.setImageURI(Crop.getOutput(result));
                        String filePath= Environment.getExternalStorageDirectory()
                                +"/"+ TEMP_PHOTO_FILE2;
                        coverPath = filePath;
                        cover_chosen = true;
                    }
                }
            });

        }else if (code == Crop.RESULT_ERROR){
            Toast.makeText(base, "An error occurred", Toast.LENGTH_SHORT).show();
        }
    }

    private void init_spinner(){
        countries_list = new ArrayList<>();
        countries_list_string = new ArrayList<>();
        cities_list = new ArrayList<>();
        cities_list_string = new ArrayList<>();

        CountryObject[] countries_array = new Gson().fromJson(base.sharedPreferences.getString("countries","[]"),CountryObject[].class);
        if (countries_array.length > 0){
            for (CountryObject co : countries_array){
                countries_list.add(co);
                countries_list_string.add(co.getName());
            }
        }


        countries_adapter = new CustomSpinnerAdapter(base,R.layout.spinner_white_arrow_item,countries_list_string);
        countries_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        countries_spinner.setAdapter(countries_adapter);

        cities_adapter = new CustomSpinnerAdapter(base,R.layout.spinner_white_arrow_item,cities_list_string);
        cities_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cities_spinner.setAdapter(cities_adapter);
    }

    private void init_recycler(){
        categories_list = new ArrayList<>();
        categories_adapter = new RegisterCategoriesAdapter(base, categories_list, new IMove() {
            @Override
            public void move(int position) {
                //categories_list.remove(position);
            }
        });
        categories_layout_manager = new GridLayoutManager(base,2);
        categories_recycler.setLayoutManager(categories_layout_manager);
        categories_recycler.setAdapter(categories_adapter);
    }

    private void restore_views(){
        InstitutionObject io = new Gson().fromJson(base.sharedPreferences.getString("update_inst_institution_object","{}"),InstitutionObject.class);
        //Id
        id = String.valueOf(io.getId());
        //Category
        CategoryObject[] categories_array = new Gson().fromJson(base.sharedPreferences.getString("categories","[]"),CategoryObject[].class);
        if (categories_array.length > 0){
            for (CategoryObject co : categories_array){
                if (co.getId() == io.getCategory_id()){
                    categories_list.add(co);
                    categories_adapter.notifyDataSetChanged();
                    break;
                }
            }
        }
        //Country
        int storedCountryId = io.getCountry_id();
        if (countries_list.size() > 0){
            for (int i = 0; i < countries_list.size() ; i++) {
                if (countries_list.get(i).getId() == storedCountryId){
                    country_id = storedCountryId;
                    countries_spinner.setSelection(i);
                    cities_list.clear();
                    cities_list_string.clear();
                    cities_spinner.setAdapter(null);
                    for (CityObject co : countries_list.get(i).getCities()){
                        cities_list.add(co);
                        cities_list_string.add(co.getName());
                    }
                    cities_adapter = new CustomSpinnerAdapter(base,R.layout.spinner_white_arrow_item,cities_list_string);
                    cities_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    cities_spinner.setAdapter(cities_adapter);
                    break;
                }
            }
        }
        //City
        int storedCityId = io.getCity_id();
        if (cities_list.size() > 0){
            for (int i = 0; i < cities_list.size(); i++) {
                if (cities_list.get(i).getId() == storedCityId){
                    city_id = storedCityId;
                    cities_spinner.setSelection(i);
                    break;
                }
            }
        }
        //Rate
        rb_rate.setRating(io.getRating());
        choosed_rate = String.valueOf(io.getRating());
        //Name
        if (io.getName()!=null){
            edt_name.setText(io.getName());
        }
        //Descriptions
        if (io.getDetails()!=null){
            edt_desc.setText(io.getDetails());
        }
        //Address
        if (io.getAddress()!=null){
            edt_address.setText(io.getAddress());
        }
        //Icon
        if (io.getImage_icon()!=null){
            BaseFunctions.setGlideImage(base,img_icon,io.getImage_icon());
        }
        //Cover
        if (io.getImage_cover()!=null){
            BaseFunctions.setGlideImage(base,img_cover,io.getImage_cover());
        }
        //Lat Lng
        lat = String.valueOf(io.getLat());
        lng = String.valueOf(io.getLng());
        //ContactInfo
        if (!base.sharedPreferences.getString("contact_info_object","").equals("")&&
                !base.sharedPreferences.getString("contact_info_object","").equals("{}")){
            ContactInfoObject cio = new Gson().fromJson(base.sharedPreferences.getString("contact_info_object",""),ContactInfoObject.class);
            if (!cio.getMobile_number().equals("")&&!cio.getMobile_number().equals("0")){
                edt_phone.setText(cio.getMobile_number());
            }
            if (!cio.getPhone_number().equals("")&&!cio.getPhone_number().equals("0")){
                edt_land_line.setText(cio.getPhone_number());
            }
        }
    }

    private void callAPI(){
        String phonetxt = edt_phone.getText().toString();
        String landtxt = edt_land_line.getText().toString();
        ContactInfoObject cio = new ContactInfoObject();
        if (phonetxt.equals("")){
            phonetxt = "0";
        }
        if (landtxt.equals("")){
            landtxt = "0";
        }
        cio.setMobile_number(phonetxt);
        cio.setPhone_number(landtxt);
        InstitutionsAPIsClass.editInstitution(
                base.sharedPreferences,
                base,
                base.sharedPreferences.getString("language", "ar"),
                base.sharedPreferences.getString("service_token", ""),
                id,
                String.valueOf(categories_list.get(0).getId()),
                String.valueOf(city_id),
                lat,
                lng,
                edt_name.getText().toString(),
                edt_address.getText().toString(),
                edt_desc.getText().toString(),
                choosed_rate,
                iconPath,
                coverPath,
                new Gson().toJson(cio),
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null){
                            Toast.makeText(base, getResources().getString(R.string.register_inst_edited_success), Toast.LENGTH_SHORT).show();

                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callAPI();
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });
    }
}
