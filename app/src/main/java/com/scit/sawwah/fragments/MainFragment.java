package com.scit.sawwah.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.azoft.carousellayoutmanager.CarouselLayoutManager;
import com.azoft.carousellayoutmanager.CarouselZoomPostLayoutListener;
import com.azoft.carousellayoutmanager.CenterScrollListener;
import com.baoyz.widget.PullRefreshLayout;
import com.google.gson.Gson;
import com.rm.rmswitch.RMSwitch;
import com.scit.sawwah.APIClasses.BasicAPIsClass;
import com.scit.sawwah.Models.AdvertisementObject;
import com.scit.sawwah.Models.MemorialImageObject;
import com.scit.sawwah.Models.OfferObject;
import com.scit.sawwah.Models.SliderObject;
import com.scit.sawwah.Models.StartupResponse;
import com.scit.sawwah.R;
import com.scit.sawwah.activities.AllAdvertisementsActivity;
import com.scit.sawwah.activities.AllOffersActivity;
import com.scit.sawwah.activities.MemorialImagesActivity;
import com.scit.sawwah.adapters.HomeAdvertisementsAdapter;
import com.scit.sawwah.adapters.HomeMemorialPhotosAdapter;
import com.scit.sawwah.adapters.HomeOffersAdapter;
import com.scit.sawwah.adapters.HomeSpecialOffersAdapter;
import com.scit.sawwah.dialoges.VisitorDialog;
import com.scit.sawwah.interfaces.IFailure;
import com.scit.sawwah.interfaces.IMove;
import com.scit.sawwah.interfaces.IResponse;
import com.scit.sawwah.tools.BaseFragment;

import java.util.ArrayList;
import java.util.List;

public class MainFragment extends BaseFragment {
    private OnFragmentInteractionListener mListener;
    private RecyclerView rv_special_offers,rv_advertisements,rv_offers,rv_memorial;
    private List<SliderObject> list_special_offers;
    private List<MemorialImageObject> list_memorial;
    private List<OfferObject> list_offers;
    private List<AdvertisementObject> list_ads;
    private HomeSpecialOffersAdapter special_offers_adapter;
    private HomeAdvertisementsAdapter advertisements_adapter;
    private HomeOffersAdapter offersAdapter;
    private HomeMemorialPhotosAdapter memorialAdapter;
    private CarouselLayoutManager special_offers_layout_manager;
    private LinearLayoutManager advertisements_layout_manager,offers_layout_manager,memorial_layout_manager;
    private RMSwitch sw_active;
    private TextView tv_all_offers,tv_all_ads;
    private TextView tv_all_memorial;
    private RelativeLayout memorial_layout,offers_layout,ads_layout;
    private RelativeLayout lyt_card_status;
    private TextView tv_active;
    private boolean is_visitor;
    private PullRefreshLayout refreshLayout;



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void init_views() {
        super.init_views();
        sw_active = base.findViewById(R.id.home_active_switch);
        rv_special_offers = base.findViewById(R.id.home_special_offer_recycler);
        rv_advertisements = base.findViewById(R.id.home_advertisements_recycler);
        rv_offers = base.findViewById(R.id.home_offer_recycler);
        rv_memorial = base.findViewById(R.id.home_memorial_recycler);
        tv_all_offers = base.findViewById(R.id.home_show_all_offers);
        memorial_layout = base.findViewById(R.id.home_memorial_layout);
        offers_layout = base.findViewById(R.id.home_offers_layout);
        ads_layout = base.findViewById(R.id.home_ads_layout);
        tv_all_ads = base.findViewById(R.id.home_show_all_ads);
        tv_all_memorial = base.findViewById(R.id.home_all_memorial);
        lyt_card_status = base.findViewById(R.id.home_card_status_layout);
        tv_active = base.findViewById(R.id.home_active_text);
        refreshLayout = base.findViewById(R.id.swipeRefreshLayout);
        refreshLayout.setRefreshStyle(PullRefreshLayout.STYLE_SMARTISAN);
        refreshLayout.setColor(getResources().getColor(R.color.colorPrimary));
        sw_active.setClickable(false);
        is_visitor = base.sharedPreferences.getBoolean("is_visitor",false);
    }

    @Override
    public void init_events() {
        refreshLayout.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (is_visitor){
                    refreshLayout.setRefreshing(false);
                }else {
                    callAPI();
                }
            }
        });
        tv_all_offers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(base, AllOffersActivity.class);
                intent.putExtra("filter","-1");
                intent.putExtra("with",false);
                startActivity(intent);
            }
        });
        tv_all_memorial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (base.sharedPreferences.getBoolean("is_visitor",false)){
                    VisitorDialog dialog = new VisitorDialog(base);
                    dialog.show();
                }else {
                    Intent intent = new Intent(base, MemorialImagesActivity.class);
                    intent.putExtra("type","from_home");
                    startActivity(intent);
                }

            }
        });
        tv_all_ads.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(base, AllAdvertisementsActivity.class);
                intent.putExtra("filter","-1");
                intent.putExtra("with",false);
                startActivity(intent);
            }
        });
    }

    @Override
    public void init_fragment(Bundle savedInstanceState) {
        mListener.onOpenFragment("main","");
        init_lists();
        setActiveSwitchColors();
        //Special Offers Recycler
        special_offers_adapter = new HomeSpecialOffersAdapter(base,list_special_offers, new IMove() {
            @Override
            public void move(int position) {

            }
        });
        special_offers_layout_manager = new CarouselLayoutManager(CarouselLayoutManager.HORIZONTAL,true);
        special_offers_layout_manager.setPostLayoutListener(new CarouselZoomPostLayoutListener());
        rv_special_offers.setLayoutManager(special_offers_layout_manager);
        runAnimationSpecialOffers(rv_special_offers,0,special_offers_adapter);
        rv_special_offers.hasFixedSize();
        rv_special_offers.setAdapter(special_offers_adapter);
        rv_special_offers.addOnScrollListener(new CenterScrollListener());
        //Advertisements
        advertisements_adapter = new HomeAdvertisementsAdapter(base, list_ads,new IMove() {
            @Override
            public void move(int position) {

            }
        });
        advertisements_layout_manager = new LinearLayoutManager(base,LinearLayoutManager.HORIZONTAL,false);
        rv_advertisements.setLayoutManager(advertisements_layout_manager);
        runAnimationAdvertisements(rv_advertisements,0,advertisements_adapter);
        rv_advertisements.setAdapter(advertisements_adapter);
        //Offers
        offersAdapter = new HomeOffersAdapter(base, list_offers,new IMove() {
            @Override
            public void move(int position) {
                //startActivity(new Intent(base, OfferDetailsActivity.class));
            }
        });
        offers_layout_manager = new LinearLayoutManager(base,LinearLayoutManager.HORIZONTAL,false);
        rv_offers.setLayoutManager(offers_layout_manager);
        runAnimationOffers(rv_offers,0,offersAdapter);
        rv_offers.setAdapter(offersAdapter);
        //Memorial Photos
        memorialAdapter = new HomeMemorialPhotosAdapter(base,list_memorial, new IMove() {
            @Override
            public void move(int position) {

            }
        });
        memorial_layout_manager = new LinearLayoutManager(base,LinearLayoutManager.HORIZONTAL,false);
        rv_memorial.setLayoutManager(memorial_layout_manager);
        runAnimationMemorial(rv_memorial,0,memorialAdapter);
        rv_memorial.setAdapter(memorialAdapter);
        if (is_visitor||base.sharedPreferences.getInt("account_status",0) == 2||base.sharedPreferences.getString("account_type","customer").equals("customer")){
            lyt_card_status.setVisibility(View.GONE);
        }else {
           if (base.sharedPreferences.getString("account_type","customer").equals("owner")){
               tv_active.setText(getResources().getString(R.string.home_activation_owner_text));
               sw_active.setVisibility(View.GONE);
           }
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    private void setActiveSwitchColors() {
        sw_active.setSwitchBkgCheckedColor(getResources().getColor(R.color.colorTextSecobdary));
        sw_active.setSwitchToggleCheckedColor(Color.WHITE);
        sw_active.setSwitchToggleNotCheckedColor(Color.WHITE);
    }

    private void init_lists(){
        list_special_offers = new ArrayList<>();
        list_memorial = new ArrayList<>();
        list_offers = new ArrayList<>();
        list_ads = new ArrayList<>();

        //Slider
        SliderObject[] array_special_offers = new Gson().fromJson(base.sharedPreferences.getString("sliders","[]"),SliderObject[].class);
        if (array_special_offers.length>0){
            for (SliderObject so:array_special_offers){
                list_special_offers.add(so);
            }
        }else {
            rv_special_offers.setVisibility(View.GONE);
        }

        //Memorial images
        MemorialImageObject[] array_memorial = new Gson().fromJson(base.sharedPreferences.getString("memorial_images","[]"),MemorialImageObject[].class);
        if (array_memorial.length>0){
            if (array_memorial.length>=20){
                for (int i = 0; i <20 ; i++) {
                    list_memorial.add(array_memorial[i]);
                }
            }else {
                for (MemorialImageObject meo : array_memorial){
                    list_memorial.add(meo);
                }
            }
        }else {
            memorial_layout.setVisibility(View.GONE);
            rv_memorial.setVisibility(View.GONE);
        }

        //Offers
        OfferObject[] array_offers = new Gson().fromJson(base.sharedPreferences.getString("offers","[]"),OfferObject[].class);
        Log.i("home_offers", "init_lists: "+String.valueOf(array_offers.length));
        if (array_offers.length>0){
            if (array_offers.length>=20){
                for (int i = 0; i <20 ; i++) {
                    list_offers.add(array_offers[i]);
                }
            }else {
                for (OfferObject oo : array_offers){
                    list_offers.add(oo);
                }
            }
        }else {
            offers_layout.setVisibility(View.GONE);
            rv_offers.setVisibility(View.GONE);
        }
        //Advertisements
        AdvertisementObject[] array_ads = new Gson().fromJson(base.sharedPreferences.getString("advertisements","[]"),AdvertisementObject[].class);
        if (array_ads.length>0){
            if (array_ads.length>=20){
                for (int i = 0; i <20 ; i++) {
                    list_ads.add(array_ads[i]);
                }
            }else {
                for (int i = 0; i <array_ads.length ; i++) {
                    list_ads.add(array_ads[i]);
                }
            }
        }else {
            ads_layout.setVisibility(View.GONE);
            rv_advertisements.setVisibility(View.GONE);
        }
    }

    public void runAnimationSpecialOffers(RecyclerView recyclerView,int type,HomeSpecialOffersAdapter adapter){
        Context context = recyclerView.getContext();
        LayoutAnimationController controller = null;
        if (type == 0){ // Fall down animation
            controller = AnimationUtils.loadLayoutAnimation(context,R.anim.layout_fall_down_vertical);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutAnimation(controller);
            recyclerView.getAdapter().notifyDataSetChanged();
            recyclerView.scheduleLayoutAnimation();
        }
    }
    public void runAnimationOffers(RecyclerView recyclerView,int type,HomeOffersAdapter adapter){
        Context context = recyclerView.getContext();
        LayoutAnimationController controller = null;
        if (type == 0){ // Fall down animation
            controller = AnimationUtils.loadLayoutAnimation(context,R.anim.layout_fall_down);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutAnimation(controller);
            recyclerView.getAdapter().notifyDataSetChanged();
            recyclerView.scheduleLayoutAnimation();
        }
    }
    public void runAnimationAdvertisements(RecyclerView recyclerView,int type,HomeAdvertisementsAdapter adapter){
        Context context = recyclerView.getContext();
        LayoutAnimationController controller = null;
        if (type == 0){ // Fall down animation
            controller = AnimationUtils.loadLayoutAnimation(context,R.anim.layout_fall_down);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutAnimation(controller);
            recyclerView.getAdapter().notifyDataSetChanged();
            recyclerView.scheduleLayoutAnimation();
        }
    }
    public void runAnimationMemorial(RecyclerView recyclerView,int type,HomeMemorialPhotosAdapter adapter){
        Context context = recyclerView.getContext();
        LayoutAnimationController controller = null;
        if (type == 0){ // Fall down animation
            controller = AnimationUtils.loadLayoutAnimation(context,R.anim.layout_fall_down);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutAnimation(controller);
            recyclerView.getAdapter().notifyDataSetChanged();
            recyclerView.scheduleLayoutAnimation();
        }
    }

    private void callAPI(){
        refreshLayout.setRefreshing(true);
        //Toast.makeText(this, String.valueOf(10/3)+" "+String.valueOf(10%3), Toast.LENGTH_SHORT).show();
        String android_id = Settings.Secure.getString(base.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        Log.i("device_id", "callAPI: "+android_id);
        BasicAPIsClass.startup(base.sharedPreferences, base, base.sharedPreferences.getString("language", "ar"),
                android_id, new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json != null){
                            String json1 = new Gson().toJson(json);
                            StartupResponse success = new Gson().fromJson(json1,StartupResponse.class);
                            //Service token
                            if (success.getService_token()!=null){
                                Log.i("service_token", "onResponse: "+success.getService_token());
                                base.editor.putString("service_token",success.getService_token());
                            }
                            //Offers
                            if (success.getOffers()!=null){
                                if (success.getOffers().size()>0){
                                    String offers_json = new Gson().toJson(success.getOffers());
                                    base.editor.putString("offers",offers_json);
                                }
                            }
                            //Advertisements
                            if (success.getPosters()!=null){
                                if (success.getPosters().size()>0){
                                    String ads_json = new Gson().toJson(success.getPosters());
                                    base.editor.putString("advertisements",ads_json);
                                }
                            }
                            //Memorial images
                            if (success.getMemorial_images()!=null){
                                if (success.getMemorial_images().size()>0){
                                    String memorial_json = new Gson().toJson(success.getMemorial_images());
                                    base.editor.putString("memorial_images",memorial_json);
                                }
                            }
                            //Sliders
                            if (success.getSliders()!=null){
                                if (success.getSliders().size()>0){
                                    String sliders_json = new Gson().toJson(success.getSliders());
                                    base.editor.putString("sliders",sliders_json);
                                }
                            }
                            //Countries
                            if (success.getCountries()!=null){
                                if (success.getCountries().size()>0){
                                    String countries_json = new Gson().toJson(success.getCountries());
                                    base.editor.putString("countries",countries_json);
                                }
                            }
                            //Categories
                            if (success.getCategories()!=null){
                                if (success.getCategories().size()>0){
                                    String categories_json = new Gson().toJson(success.getCategories());
                                    base.editor.putString("categories",categories_json);
                                }
                            }
                            //Reactions
                            if (success.getReactions_types()!=null){
                                if (success.getReactions_types().size()>0){
                                    String reactions_json = new Gson().toJson(success.getReactions_types());
                                    base.editor.putString("reactions",reactions_json);
                                }
                            }
                            //Payment types
                            if (success.getPayments_types()!=null){
                                if (success.getPayments_types().size()>0){
                                    String payments_json = new Gson().toJson(success.getPayments_types());
                                    base.editor.putString("payments_types",payments_json);
                                }
                            }
                            //My Institutions
                            if (success.getMy_facilities()!=null){
                                if (success.getMy_facilities().size() > 0){
                                    String insts_json = new Gson().toJson(success.getMy_facilities());
                                    base.editor.putString("my_insts",insts_json);
                                }
                            }
                            //My Packages
                            if(success.getMy_packages()!=null){
                                if (success.getMy_facilities().size()>0){
                                    String packages_json = new Gson().toJson(success.getMy_packages());
                                    base.editor.putString("my_packages",packages_json);
                                }
                            }
                            base.editor.commit();
                            list_ads.clear();
                            list_memorial.clear();
                            list_offers.clear();
                            list_special_offers.clear();
                            init_lists();
                            rv_advertisements.setAdapter(new HomeAdvertisementsAdapter(base, list_ads, new IMove() {
                                @Override
                                public void move(int position) {

                                }
                            }));
                            rv_memorial.setAdapter(new HomeMemorialPhotosAdapter(base, list_memorial, new IMove() {
                                @Override
                                public void move(int position) {

                                }
                            }));
                            rv_offers.setAdapter(new HomeOffersAdapter(base, list_offers, new IMove() {
                                @Override
                                public void move(int position) {

                                }
                            }));
                            rv_special_offers.setAdapter(new HomeSpecialOffersAdapter(base, list_special_offers, new IMove() {
                                @Override
                                public void move(int position) {

                                }
                            }));
                            refreshLayout.setRefreshing(false);
                        }else {
                            refreshLayout.setRefreshing(false);
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        refreshLayout.setRefreshing(false);
                        Snackbar.make(base.findViewById(R.id.home_layout), getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callAPI();
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });
    }
}
