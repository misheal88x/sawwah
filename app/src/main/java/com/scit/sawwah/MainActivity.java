package com.scit.sawwah;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.view.menu.MenuBuilder;
import androidx.appcompat.view.menu.MenuPopupHelper;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;
import com.scit.sawwah.APIClasses.AccountsAPIsClass;
import com.scit.sawwah.APIClasses.BasicAPIsClass;
import com.scit.sawwah.Models.IconPowerMenuItem;
import com.scit.sawwah.Models.InstitutionObject;
import com.scit.sawwah.activities.AboutAppActivity;
import com.scit.sawwah.activities.AccountOwnerSettingsActivity;
import com.scit.sawwah.activities.AccountSettingsActivity;
import com.scit.sawwah.activities.AdvancedSearchActivity;
import com.scit.sawwah.activities.ComplaintsAndSuggestionsActivity;
import com.scit.sawwah.activities.NotificationsActivity;
import com.scit.sawwah.activities.OffersAndAdvertisementsActivity;
import com.scit.sawwah.activities.PreferredActivity;
import com.scit.sawwah.activities.PricesPackagesActivity;
import com.scit.sawwah.activities.PrivacyActivity;
import com.scit.sawwah.activities.ProfileActivity;
import com.scit.sawwah.activities.QuickSearchActivity;
import com.scit.sawwah.activities.SettingsActivity;
import com.scit.sawwah.activities.SplashActivity;
import com.scit.sawwah.adapters.IconMenuAdapter;
import com.scit.sawwah.dialoges.CategoriesDialog;
import com.scit.sawwah.activities.LoginActivity;
import com.scit.sawwah.dialoges.VisitorDialog;
import com.scit.sawwah.fragments.MainFragment;
import com.scit.sawwah.interfaces.IFailure;
import com.scit.sawwah.interfaces.IResponse;
import com.scit.sawwah.tools.BaseActivity;
import com.scit.sawwah.tools.BaseFragment;
import com.skydoves.powermenu.CustomPowerMenu;
import com.skydoves.powermenu.MenuAnimation;
import com.skydoves.powermenu.OnMenuItemClickListener;

public class MainActivity extends BaseActivity implements BaseFragment.OnFragmentInteractionListener {

//CTRL+Click on BaseActivity to open the class and see onCreate method
    //And changing the font and onBackPressed
    private RelativeLayout drawerLayout;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    private NavigationView navigationView;
    private Toolbar toolbar;
    private MainFragment mainFragment;
    private ImageView btn_categories, btn_menu, btn_user,btn_facility ,btn_notifications;
    private TabLayout bottom_tab_layout, top_tab_layout;
    private TextView tv_title, tv_notifications, btn_search;
    private int numBackPressed = 0;
    private int myFacilityId = 0;
    CustomPowerMenu customPowerMenu;

    @Override
    public void set_layout() {
        setContentView(R.layout.activity_main);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        mainFragment = new MainFragment();
        open_fragment(mainFragment);
        setSupportActionBar(toolbar);
        if (sharedPreferences.getBoolean("is_visitor",false) == false){
            //if (sharedPreferences.getBoolean("is_fcm_token_updated",false) == false){
            FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( MainActivity.this,  new OnSuccessListener<InstanceIdResult>() {
                @Override
                public void onSuccess(InstanceIdResult instanceIdResult) {
                    String newToken = instanceIdResult.getToken();
                    callUpdateFCMTokenAPI(newToken);
                }
            });
            //}
            if (sharedPreferences.getString("account_type","customer").equals("owner")){
                if (!sharedPreferences.getString("my_insts","[]").equals("[]")&&
                        sharedPreferences.getString("my_insts","[]")!= null){
                    InstitutionObject[] myFacilities = new Gson().fromJson(
                            sharedPreferences.getString("my_insts","[]"),
                            InstitutionObject[].class);
                    if (myFacilities.length>0){
                        btn_facility.setVisibility(View.VISIBLE);
                        myFacilityId = myFacilities[0].getId();
                    }
                }

            }
        }

    }


    @Override
    public void init_views() {
        toolbar = findViewById(R.id.toolbar1);
        drawerLayout = findViewById(R.id.drawerLayout);
        btn_categories = findViewById(R.id.btn_categories);
        btn_menu = findViewById(R.id.btn_menu);
        btn_notifications = findViewById(R.id.btn_notifications);
        btn_user = findViewById(R.id.btn_user);
        btn_facility = findViewById(R.id.btn_facility);
        bottom_tab_layout = findViewById(R.id.tab_layout);
        top_tab_layout = findViewById(R.id.top_tab_layout);
        tv_title = findViewById(R.id.tv_fragment_name);
        tv_notifications = findViewById(R.id.tv_notifications);
        btn_search = findViewById(R.id.btn_search);
    }

    @Override
    public void init_events() {

        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sharedPreferences.getBoolean("is_visitor",false)){
                    VisitorDialog dialog = new VisitorDialog(MainActivity.this);
                    dialog.show();
                }else {
                    startActivity(new Intent(MainActivity.this, QuickSearchActivity.class));
                }
                /*
                AdvancedSearchFragment advancedSearchFragment = new AdvancedSearchFragment();
                advancedSearchFragment.set_previous(mainFragment);
                open_fragment(advancedSearchFragment);
                **/
            }
        });

        btn_notifications.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*
                NotificationsFragment notificationsFragment = new NotificationsFragment();
                notificationsFragment.set_previous(mainFragment);
                open_fragment(notificationsFragment);
                **/
                startActivity(new Intent(MainActivity.this, NotificationsActivity.class));
            }
        });

        btn_categories.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CategoriesDialog categoriesDialog = new CategoriesDialog(MainActivity.this);
                categoriesDialog.show();
            }
        });

        btn_menu.setOnClickListener(new View.OnClickListener() {

            @SuppressLint("RestrictedApi")
            @Override
            public void onClick(View v) {

                if (sharedPreferences.getBoolean("is_visitor",false)){
                    showCustomerMenu(v,true);
                }else {
                    if (sharedPreferences.getString("account_type","customer").equals("customer")){
                        showCustomerMenu(v,false);
                    }else {
                        showOwnerMenu(v);
                    }
                }

            }
        });

        setTabLayoutListener();

        btn_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sharedPreferences.getBoolean("is_visitor",false)){
                    startActivity(new Intent(MainActivity.this,LoginActivity.class));
                    editor.putBoolean("is_visitor",false);
                    editor.commit();
                    startActivity(new Intent(MainActivity.this,LoginActivity.class));
                }else {
                    if (sharedPreferences.getString("account_type","customer").equals("customer")){
                        startActivity(new Intent(MainActivity.this, AccountSettingsActivity.class));
                    }else {
                        startActivity(new Intent(MainActivity.this, AccountOwnerSettingsActivity.class));
                    }
                }
            }
        });
        btn_facility.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (myFacilityId!=0){
                    Intent intent = new Intent(MainActivity.this, ProfileActivity.class);
                    intent.putExtra("institution_id",myFacilityId);
                    startActivity(intent);
                }
            }
        });
    }

    private void showOwnerMenu(View v){
        customPowerMenu = new CustomPowerMenu.Builder<>(MainActivity.this, new IconMenuAdapter())
                    .addItem(new IconPowerMenuItem(ContextCompat.getDrawable(MainActivity.this, R.drawable.ic_main_menu_about_app), getResources().getString(R.string.main_menu_about_app)))
                    .addItem(new IconPowerMenuItem(ContextCompat.getDrawable(MainActivity.this, R.drawable.ic_main_menu_privacy), getResources().getString(R.string.main_menu_privacy_policy)))
                    .addItem(new IconPowerMenuItem(ContextCompat.getDrawable(MainActivity.this, R.drawable.ic_main_menu_settings), getResources().getString(R.string.main_menu_settings)))
                    .addItem(new IconPowerMenuItem(ContextCompat.getDrawable(MainActivity.this, R.drawable.ic_main_menu_complaints), getResources().getString(R.string.main_menu_complaints)))
                    .addItem(new IconPowerMenuItem(ContextCompat.getDrawable(MainActivity.this, R.drawable.ic_main_menu_prices_packages), getResources().getString(R.string.main_menu_prices_packages)))
                    .addItem(new IconPowerMenuItem(ContextCompat.getDrawable(MainActivity.this, R.drawable.ic_main_menu_sign_out), getResources().getString(R.string.main_menu_sign_out)))
                    .setAnimation(MenuAnimation.SHOWUP_TOP_LEFT)
                    .setMenuRadius(10f)
                    .setMenuShadow(10f)
                    .setOnMenuItemClickListener(onIconMenuItemClickListener)
                    .build();
        customPowerMenu.showAsDropDown(v);
    }
    private void showCustomerMenu(View v,boolean is_visitor){
        if (is_visitor){
            customPowerMenu = new CustomPowerMenu.Builder<>(MainActivity.this, new IconMenuAdapter())
                    .addItem(new IconPowerMenuItem(ContextCompat.getDrawable(MainActivity.this, R.drawable.ic_main_menu_about_app), getResources().getString(R.string.main_menu_about_app)))
                    .addItem(new IconPowerMenuItem(ContextCompat.getDrawable(MainActivity.this, R.drawable.ic_main_menu_privacy), getResources().getString(R.string.main_menu_privacy_policy)))
                    .addItem(new IconPowerMenuItem(ContextCompat.getDrawable(MainActivity.this, R.drawable.ic_main_menu_settings), getResources().getString(R.string.main_menu_settings)))
                    .addItem(new IconPowerMenuItem(ContextCompat.getDrawable(MainActivity.this, R.drawable.ic_main_menu_complaints), getResources().getString(R.string.main_menu_complaints)))
                    .addItem(new IconPowerMenuItem(ContextCompat.getDrawable(MainActivity.this, R.drawable.ic_main_menu_sign_out), getResources().getString(R.string.main_menu_sign_out)))
                    .setAnimation(MenuAnimation.SHOWUP_TOP_LEFT)
                    .setMenuRadius(10f)
                    .setMenuShadow(10f)
                    .setOnMenuItemClickListener(onIconMenuItemClickListener)
                    .build();
        }else {
            customPowerMenu = new CustomPowerMenu.Builder<>(MainActivity.this, new IconMenuAdapter())
                    .addItem(new IconPowerMenuItem(ContextCompat.getDrawable(MainActivity.this, R.drawable.ic_main_menu_about_app), getResources().getString(R.string.main_menu_about_app)))
                    .addItem(new IconPowerMenuItem(ContextCompat.getDrawable(MainActivity.this, R.drawable.ic_main_menu_privacy), getResources().getString(R.string.main_menu_privacy_policy)))
                    .addItem(new IconPowerMenuItem(ContextCompat.getDrawable(MainActivity.this, R.drawable.ic_main_menu_settings), getResources().getString(R.string.main_menu_settings)))
                    .addItem(new IconPowerMenuItem(ContextCompat.getDrawable(MainActivity.this, R.drawable.ic_main_menu_complaints), getResources().getString(R.string.main_menu_complaints)))
                    .addItem(new IconPowerMenuItem(ContextCompat.getDrawable(MainActivity.this, R.drawable.ic_main_menu_sign_out), getResources().getString(R.string.main_menu_sign_out)))
                    .setAnimation(MenuAnimation.SHOWUP_TOP_LEFT)
                    .setMenuRadius(10f)
                    .setMenuShadow(10f)
                    .setOnMenuItemClickListener(onIconMenuItemClickListener)
                    .build();
        }

        customPowerMenu.showAsDropDown(v);
    }

    private OnMenuItemClickListener<IconPowerMenuItem> onIconMenuItemClickListener = new OnMenuItemClickListener<IconPowerMenuItem>() {
        @Override
        public void onItemClick(int position, IconPowerMenuItem item) {
            if (!sharedPreferences.getBoolean("is_visitor",false)){
                if (sharedPreferences.getString("account_type","customer").equals("customer")){
                    switch (position){
                        //About App
                        case 0:{
                            startActivity(new Intent(MainActivity.this, AboutAppActivity.class));
                        }break;
                        //Privacy Policy
                        case 1:{
                            startActivity(new Intent(MainActivity.this, PrivacyActivity.class));
                        }break;
                        //Settings
                        case 2:{
                            Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
                            startActivity(intent);
                        }break;
                        //Complaints
                        case 3:{
                            startActivity(new Intent(MainActivity.this, ComplaintsAndSuggestionsActivity.class));
                        }break;
                        //Log out
                        case 4:{
                            callLogoutAPI();
                        }break;
                    }
                } else {
                    switch (position){
                        //About App
                        case 0:{
                            startActivity(new Intent(MainActivity.this, AboutAppActivity.class));
                        }break;
                        //Privacy Policy
                        case 1:{
                            startActivity(new Intent(MainActivity.this, PrivacyActivity.class));
                        }break;
                        //Settings
                        case 2:{
                            Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
                            startActivity(intent);
                        }break;
                        //Complaints
                        case 3:{
                            startActivity(new Intent(MainActivity.this, ComplaintsAndSuggestionsActivity.class));
                        }break;
                        //Prices Packages
                        case 4:{
                            startActivity(new Intent(MainActivity.this, PricesPackagesActivity.class));
                        }break;
                        //Log out
                        case 5:{
                            callLogoutAPI();
                        }break;
                    }
                }
            }else {
                switch (position){
                    //About App
                    case 0:{
                        startActivity(new Intent(MainActivity.this, AboutAppActivity.class));
                    }break;
                    //Privacy Policy
                    case 1:{}break;
                    //Settings
                    case 2:{
                        Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
                        startActivity(intent);
                    }break;
                    //Complaints
                    case 3:{
                        VisitorDialog dialog = new VisitorDialog(MainActivity.this);
                        dialog.show();
                    }break;
                    //Log In
                    case 4:{
                        startActivity(new Intent(MainActivity.this,LoginActivity.class));
                        editor.putBoolean("is_visitor",false);
                        editor.commit();
                    }break;
                }
            }
            customPowerMenu.dismiss();
        }
    };


    @Override
    public void set_fragment_place() {
        this.fragment_place = (ViewGroup) findViewById(R.id.fragment_place);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onBackPressed() {
        if (numBackPressed==1){
            stopService(new Intent(MainActivity.this, MainActivity.class));
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }else {
            Toast.makeText(this,getResources().getString(R.string.back_button), Toast.LENGTH_SHORT).show();
            numBackPressed++;
        }
    }

    @Override
    public void onOpenFragment(String tag, String title) {
        if (tag.equals("main")) {
            top_tab_layout.setVisibility(View.GONE);
            tv_title.setVisibility(View.GONE);
            bottom_tab_layout.setVisibility(View.VISIBLE);
            btn_categories.setVisibility(View.VISIBLE);
            tv_notifications.setVisibility(View.GONE);
        } else {
            top_tab_layout.setVisibility(View.VISIBLE);
            tv_title.setVisibility(View.VISIBLE);
            bottom_tab_layout.setVisibility(View.GONE);
            btn_categories.setVisibility(View.GONE);
            tv_notifications.setVisibility(View.GONE);
        }

        tv_title.setText(title);
    }


 /*   @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_settings:
                SettingsFragment settingsFragment = new SettingsFragment();
                settingsFragment.set_previous(mainFragment);
                open_fragment(settingsFragment);
                break;

            case R.id.action_about:
                startActivity(new Intent(MainActivity.this, AboutAppActivity.class));
                break;
            case R.id.action_prices:
                PricesPackagesFragment pricesPackagesFragment = new PricesPackagesFragment();
                pricesPackagesFragment.set_previous(mainFragment);
                open_fragment(pricesPackagesFragment);
                break;
            case R.id.action_login:
                startActivity(new Intent(MainActivity.this, CreateAccountActivity.class));
                break;

        }
        return super.onOptionsItemSelected(item);
    }*/

    private void setTabLayoutListener() {
        bottom_tab_layout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                switch (tab.getPosition()) {
                    case 0:
                        startActivity(new Intent(MainActivity.this, OffersAndAdvertisementsActivity.class));
                        /*
                        PendingPhotosFragment pendingPhotosFragment = new PendingPhotosFragment();
                        pendingPhotosFragment.set_previous(mainFragment);
                        open_fragment(pendingPhotosFragment);
                        **/
                        break;

                    case 1:
                        startActivity(new Intent(MainActivity.this, PreferredActivity.class));
                        /*
                        FavoriteFragment favoriteFragment = new FavoriteFragment();
                        favoriteFragment.set_previous(mainFragment);
                        open_fragment(favoriteFragment);
                        **/
                        break;

                    case 3:
                        /*
                        SearchFragment searchFragment = new SearchFragment();
                        searchFragment.set_previous(mainFragment);
                        open_fragment(searchFragment);
                        **/
                    {
                        if (sharedPreferences.getBoolean("is_visitor", false)) {
                            VisitorDialog dialog = new VisitorDialog(MainActivity.this);
                            dialog.show();
                        } else {
                            startActivity(new Intent(MainActivity.this, AdvancedSearchActivity.class));
                        }
                    }
                        break;

                    case 4:
                        open_fragment(mainFragment);
                        break;

                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0:
                        startActivity(new Intent(MainActivity.this, OffersAndAdvertisementsActivity.class));
                        /*
                        PendingPhotosFragment pendingPhotosFragment = new PendingPhotosFragment();
                        pendingPhotosFragment.set_previous(mainFragment);
                        open_fragment(pendingPhotosFragment);
                        **/
                        break;

                    case 1:
                        startActivity(new Intent(MainActivity.this, PreferredActivity.class));
                        /*
                        FavoriteFragment favoriteFragment = new FavoriteFragment();
                        favoriteFragment.set_previous(mainFragment);
                        open_fragment(favoriteFragment);
                        **/
                        break;

                    case 3:
                        /*
                        SearchFragment searchFragment = new SearchFragment();
                        searchFragment.set_previous(mainFragment);
                        open_fragment(searchFragment);
                        **/
                    {
                        if (sharedPreferences.getBoolean("is_visitor", false)) {
                            VisitorDialog dialog = new VisitorDialog(MainActivity.this);
                            dialog.show();
                        } else {
                            startActivity(new Intent(MainActivity.this, AdvancedSearchActivity.class));
                        }
                    }
                        break;

                    case 4:
                        open_fragment(mainFragment);
                        break;

                }
            }
        });

        top_tab_layout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                switch (tab.getPosition()) {
                    case 0:
                        /*
                        PendingPhotosFragment pendingPhotosFragment = new PendingPhotosFragment();
                        pendingPhotosFragment.set_previous(mainFragment);
                        open_fragment(pendingPhotosFragment);
                        **/
                        break;

                    case 1:
                        /*
                        FavoriteFragment favoriteFragment = new FavoriteFragment();
                        favoriteFragment.set_previous(mainFragment);
                        open_fragment(favoriteFragment);
                        **/
                        break;

                    case 2:
                        /*
                        SearchFragment searchFragment = new SearchFragment();
                        searchFragment.set_previous(mainFragment);
                        open_fragment(searchFragment);
                        **/
                        break;

                    case 3:
                        open_fragment(mainFragment);
                        break;

                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0:
                        /*
                        PendingPhotosFragment pendingPhotosFragment = new PendingPhotosFragment();
                        pendingPhotosFragment.set_previous(mainFragment);
                        open_fragment(pendingPhotosFragment);
                        **/
                        break;

                    case 1:
                        /*
                        FavoriteFragment favoriteFragment = new FavoriteFragment();
                        favoriteFragment.set_previous(mainFragment);
                        open_fragment(favoriteFragment);
                        **/
                        break;

                    case 2:
                        /*
                        SearchFragment searchFragment = new SearchFragment();
                        searchFragment.set_previous(mainFragment);
                        open_fragment(searchFragment);
                        **/
                        break;

                    case 3:
                        open_fragment(mainFragment);
                        break;

                }
            }
        });
    }

    private void callLogoutAPI(){
        BasicAPIsClass.logout(sharedPreferences,
                MainActivity.this,
                sharedPreferences.getString("language", "ar"),
                sharedPreferences.getString("service_token", ""),
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json != null){
                            editor.putString("access_token","");
                            editor.putString("service_token","");
                            editor.putBoolean("is_visitor",false);
                            editor.putInt("account_status",0);
                            editor.putString("offers","[]");
                            editor.putString("memorial_images","[]");
                            editor.putString("sliders","[]");
                            editor.putString("countries","[]");
                            editor.putString("categories","[]");
                            editor.putString("reactions","[]");
                            editor.putString("payments_types","[]");
                            editor.putString("my_insts","[]");
                            editor.putString("my_packages","[]");
                            editor.putInt("account_id",0);
                            editor.putString("account_image","");
                            editor.putString("account_name","");
                            editor.putString("account_user_name","");
                            editor.putString("account_type","");
                            editor.commit();
                            startActivity(new Intent(MainActivity.this,SplashActivity.class));
                            finish();
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(drawerLayout, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callLogoutAPI();
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });
    }

    private void callUpdateFCMTokenAPI(String token) {
        AccountsAPIsClass.updateFirebaseToken(sharedPreferences, getApplication(),
                sharedPreferences.getString("language", "ar"),
                sharedPreferences.getString("service_token", ""),
                token, new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json != null){
                            editor.putBoolean("is_fcm_token_updated",true);
                            editor.commit();
                        }else {
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                    }
                });
    }
}
