package com.scit.sawwah.APIClasses;

import android.content.Context;
import android.content.SharedPreferences;

import com.scit.sawwah.API.OffersAPIs;
import com.scit.sawwah.API.PackagesAPIs;
import com.scit.sawwah.Models.BaseResponse;
import com.scit.sawwah.dialoges.NewProgressBar;
import com.scit.sawwah.interfaces.IFailure;
import com.scit.sawwah.interfaces.IResponse;
import com.scit.sawwah.tools.BaseFunctions;
import com.scit.sawwah.tools.BaseRetrofit;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Misheal on 9/15/2019.
 */

public class PackagesAPIsClass extends BaseRetrofit {
    private static NewProgressBar dialog;

    public static void get_all_packages(SharedPreferences sharedPreferences,
                                   final Context context,
                                   String language,
                                   String service_token,
                                   String filter,
                                   IResponse onResponse1,
                                   IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog = new NewProgressBar(context);
        dialog.show();
        Retrofit retrofit = configureRetrofitWithBearer(sharedPreferences);
        PackagesAPIs api = retrofit.create(PackagesAPIs.class);
        Call<BaseResponse> call = api.getAllPackages(BaseFunctions.languageToNumberConverter(language),
                service_token,filter);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });
    }

    public static void request(SharedPreferences sharedPreferences,
                                        final Context context,
                                        String language,
                                        String service_token,
                                        String package_id,
                                        String payment_type_id,
                                        String contactInfo,
                                        String image,
                                        IResponse onResponse1,
                                        IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog = new NewProgressBar(context);
        dialog.show();

        File file = new File(image);
        final RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"),file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("image",file.getName(),requestBody);

        //Package id
        RequestBody packageIdRequest = RequestBody.create(MultipartBody.FORM,package_id);
        //Payment type id
        RequestBody paymentTypeIdRequest = RequestBody.create(MultipartBody.FORM,payment_type_id);
        //Contact Info
        RequestBody contactInfoRequest = RequestBody.create(MultipartBody.FORM,contactInfo);
        Retrofit retrofit = configureRetrofitWithBearer(sharedPreferences);
        PackagesAPIs api = retrofit.create(PackagesAPIs.class);
        Call<BaseResponse> call = api.request(BaseFunctions.languageToNumberConverter(language),
                service_token,packageIdRequest,paymentTypeIdRequest,contactInfoRequest,body);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });
    }

    public static void renew(SharedPreferences sharedPreferences,
                                        final Context context,
                                        String language,
                                        String service_token,
                                        String package_id,
                                        IResponse onResponse1,
                                        IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog = new NewProgressBar(context);
        dialog.show();
        Retrofit retrofit = configureRetrofitWithBearer(sharedPreferences);
        PackagesAPIs api = retrofit.create(PackagesAPIs.class);
        Call<BaseResponse> call = api.renew(BaseFunctions.languageToNumberConverter(language),
                service_token,package_id);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });
    }
}
