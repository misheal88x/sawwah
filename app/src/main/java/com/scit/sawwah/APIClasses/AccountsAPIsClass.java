package com.scit.sawwah.APIClasses;

import android.content.Context;
import android.content.SharedPreferences;
import android.provider.Settings;
import android.widget.Toast;

import com.scit.sawwah.API.AccountsAPIs;
import com.scit.sawwah.Models.BaseResponse;
import com.scit.sawwah.R;
import com.scit.sawwah.activities.SplashActivity;
import com.scit.sawwah.dialoges.NewProgressBar;
import com.scit.sawwah.interfaces.IFailure;
import com.scit.sawwah.interfaces.IResponse;
import com.scit.sawwah.tools.BaseFunctions;
import com.scit.sawwah.tools.BaseRetrofit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Misheal on 9/14/2019.
 */

public class AccountsAPIsClass extends BaseRetrofit {
    private static NewProgressBar dialog;

    public static void reactOnImage(SharedPreferences sharedPreferences,
                                    final Context context,
                                    String language,
                                    String service_token,
                                    int reaction_id,
                                    int memorial_image_id,
                                    IResponse onResponse1,
                                    IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog = new NewProgressBar(context);
        dialog.show();
        Retrofit retrofit = configureRetrofitWithBearer(sharedPreferences);
        AccountsAPIs api = retrofit.create(AccountsAPIs.class);
        Call<BaseResponse> call = api.reactOnImage(BaseFunctions.languageToNumberConverter(language),
                service_token,reaction_id,memorial_image_id);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });
    }

    public static void getNotifications(SharedPreferences sharedPreferences,
                                        final Context context,
                                        String language,
                                        String service_token,
                                        final int type,
                                        int page,
                                        IResponse onResponse1,
                                        IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog = new NewProgressBar(context);
        if (type == 0){
            dialog.show();
        }
        Retrofit retrofit = configureRetrofitWithBearer(sharedPreferences);
        AccountsAPIs api = retrofit.create(AccountsAPIs.class);
        Call<BaseResponse> call = api.getNotifications(BaseFunctions.languageToNumberConverter(language),
                service_token,page);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (type == 0){
                    dialog.cancel();
                }
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                if (type == 0){
                    dialog.cancel();
                }
                onFailure.onFailure();
            }
        });
    }

    public static void editPassword(SharedPreferences sharedPreferences,
                                    final Context context,
                                    String language,
                                    String service_token,
                                    String password,
                                    String oldPassword,
                                    IResponse onResponse1,
                                    IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog = new NewProgressBar(context);
        dialog.show();
        Retrofit retrofit = configureRetrofitWithBearer(sharedPreferences);
        AccountsAPIs api = retrofit.create(AccountsAPIs.class);
        Call<BaseResponse> call = api.editPassword(BaseFunctions.languageToNumberConverter(language),
                service_token,password,oldPassword);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });
    }

    public static void resetPasswordRequest(SharedPreferences sharedPreferences,
                                     final Context context,
                                     String language,
                                     String service_token,
                                     String username,
                                     IResponse onResponse1,
                                     IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog = new NewProgressBar(context);
        dialog.show();
        Retrofit retrofit = configureRetrofitWithoutBearer();
        AccountsAPIs api = retrofit.create(AccountsAPIs.class);
        Call<BaseResponse> call = api.resetPasswordRequest(BaseFunctions.languageToNumberConverter(language),
                service_token,username);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });
    }

    public static void resetPassword(SharedPreferences sharedPreferences,
                                     final Context context,
                                     String language,
                                     String service_token,
                                     String code,
                                     String password,
                                     IResponse onResponse1,
                                     IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog = new NewProgressBar(context);
        dialog.show();
        String android_id = Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        Retrofit retrofit = configureRetrofitWithoutBearer();
        AccountsAPIs api = retrofit.create(AccountsAPIs.class);
        Call<BaseResponse> call = api.resetPassword(BaseFunctions.languageToNumberConverter(language),
                service_token,android_id,code,password);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });
    }

    public static void updateDeviceSettings(SharedPreferences sharedPreferences,
                                            final Context context,
                                            String language,
                                            String service_token,
                                            String city_id,
                                            String notifications,
                                            String language_id,
                                            IResponse onResponse1,
                                            IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog = new NewProgressBar(context);
        dialog.show();
        Retrofit retrofit = configureRetrofitWithBearer(sharedPreferences);
        AccountsAPIs api = retrofit.create(AccountsAPIs.class);
        Call<BaseResponse> call = api.updateDeviceSettings(BaseFunctions.languageToNumberConverter(language),
                service_token,language_id,notifications,city_id);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });
    }

    public static void updateFirebaseToken(SharedPreferences sharedPreferences,
                                            final Context context,
                                            String language,
                                            String service_token,
                                            String token,
                                            IResponse onResponse1,
                                            IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        Retrofit retrofit = configureRetrofitWithBearer(sharedPreferences);
        AccountsAPIs api = retrofit.create(AccountsAPIs.class);
        Call<BaseResponse> call = api.updateFirebaseToken(BaseFunctions.languageToNumberConverter(language),
                service_token,token);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.body()!=null){
                    if (response.body().getData()!=null){
                        if (response.body().getStatus_code()==200){
                            onResponse.onResponse(response.body().getData());
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }

    public static void profile(SharedPreferences sharedPreferences,
                                    final Context context,
                                    String language,
                                    String service_token,
                                    IResponse onResponse1,
                                    IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog = new NewProgressBar(context);
        dialog.show();
        Retrofit retrofit = configureRetrofitWithBearer(sharedPreferences);
        AccountsAPIs api = retrofit.create(AccountsAPIs.class);
        Call<BaseResponse> call = api.profile(BaseFunctions.languageToNumberConverter(language),
                service_token);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });
    }

}
