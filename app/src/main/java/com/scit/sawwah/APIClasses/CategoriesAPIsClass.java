package com.scit.sawwah.APIClasses;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.scit.sawwah.API.CategoriesAPIs;
import com.scit.sawwah.Models.BaseResponse;
import com.scit.sawwah.R;
import com.scit.sawwah.dialoges.NewProgressBar;
import com.scit.sawwah.interfaces.IFailure;
import com.scit.sawwah.interfaces.IResponse;
import com.scit.sawwah.tools.BaseFunctions;
import com.scit.sawwah.tools.BaseRetrofit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Misheal on 9/3/2019.
 */

public class CategoriesAPIsClass extends BaseRetrofit {
    private static NewProgressBar dialog;

    public static void getFacilities(SharedPreferences sharedPreferences,
                                     final Context context,
                                     String language,
                                     String service_token,
                                     String id,
                                     String order,
                                     IResponse onResponse1,
                                     IFailure onFailure1
                                     ){
        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog = new NewProgressBar(context);
        dialog.show();
        Retrofit retrofit = configureRetrofitWithBearer(sharedPreferences);
        CategoriesAPIs api = retrofit.create(CategoriesAPIs.class);
        Call<BaseResponse> call = api.getFacilities(BaseFunctions.languageToNumberConverter(language),service_token,
                sharedPreferences.getString("my_lat","0"),
                sharedPreferences.getString("my_lng","0"),
                id,order);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });
    }

    public static void getCategories(SharedPreferences sharedPreferences,
                                     final Context context,
                                     String language,
                                     String service_token,
                                     int type,
                                     int page,
                                     IResponse onResponse1,
                                     IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        Retrofit retrofit = configureRetrofitWithBearer(sharedPreferences);
        CategoriesAPIs api = retrofit.create(CategoriesAPIs.class);
        Call<BaseResponse> call = api.getCategories(BaseFunctions.languageToNumberConverter(language),
                service_token,page);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }
}
