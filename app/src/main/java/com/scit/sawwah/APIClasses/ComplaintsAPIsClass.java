package com.scit.sawwah.APIClasses;

import android.content.Context;
import android.content.SharedPreferences;

import com.scit.sawwah.API.ComplaintsAPIs;
import com.scit.sawwah.Models.BaseResponse;
import com.scit.sawwah.dialoges.NewProgressBar;
import com.scit.sawwah.interfaces.IFailure;
import com.scit.sawwah.interfaces.IResponse;
import com.scit.sawwah.tools.BaseFunctions;
import com.scit.sawwah.tools.BaseRetrofit;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Misheal on 9/12/2019.
 */

public class ComplaintsAPIsClass extends BaseRetrofit {
    private static NewProgressBar dialog;

    public static void add(SharedPreferences sharedPreferences,
                           final Context context,
                           String language,
                           String service_token,
                           String image_icon,
                           String name,
                           String username,
                           String title,
                           String content,
                           String type,
                           final IResponse onResponse1,
                           IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog = new NewProgressBar(context);
        dialog.show();

        //Image Icon
        File file = new File(image_icon);
        final RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"),file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("image_icon",file.getName(),requestBody);
        //Name
        RequestBody nameRequest = RequestBody.create(MultipartBody.FORM,name);
        //User Name
        RequestBody userNameRequest = RequestBody.create(MultipartBody.FORM,username);
        //Title
        RequestBody titleRequest = RequestBody.create(MultipartBody.FORM,title);
        //Content
        RequestBody contentRequest = RequestBody.create(MultipartBody.FORM,content);
        //Type
        RequestBody typeRequest = RequestBody.create(MultipartBody.FORM,type);
        Retrofit retrofit = configureRetrofitWithBearer(sharedPreferences);
        ComplaintsAPIs api = retrofit.create(ComplaintsAPIs.class);
        Call<BaseResponse> call = api.add(BaseFunctions.languageToNumberConverter(language),
                service_token,body,nameRequest,userNameRequest,contentRequest,typeRequest,titleRequest);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });
    }
}
