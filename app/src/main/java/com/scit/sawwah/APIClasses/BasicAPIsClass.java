package com.scit.sawwah.APIClasses;

import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.google.gson.Gson;
import com.scit.sawwah.API.BasicAPIs;
import com.scit.sawwah.Models.BaseResponse;
import com.scit.sawwah.R;
import com.scit.sawwah.dialoges.NewProgressBar;
import com.scit.sawwah.interfaces.IFailure;
import com.scit.sawwah.interfaces.IResponse;
import com.scit.sawwah.tools.BaseFunctions;
import com.scit.sawwah.tools.BaseRetrofit;

import java.io.File;
import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Misheal on 8/30/2019.
 */

public class BasicAPIsClass extends BaseRetrofit {
    private static NewProgressBar dialog;

    public static void startup(SharedPreferences sharedPreferences,
                               final Context context,
                               String language,
                               String device_id,
                               IResponse onRespose1,
                               IFailure onFailure1){
        onResponse = onRespose1;
        onFailure = onFailure1;
        Retrofit retrofit = configureRetrofitWithoutBearer();
        BasicAPIs api = retrofit.create(BasicAPIs.class);
        Call<BaseResponse> call;
        call = api.startup(language,device_id,"android",sharedPreferences.getString("service_token",""));
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }

    public static void register(SharedPreferences sharedPreferences,
                                final Context context,
                                String language,
                                String service_token,
                                String name,
                                String username,
                                String password,
                                String type,
                                String image,
                                IResponse onResponse1,
                                IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog = new NewProgressBar(context);
        dialog.show();
        File file = new File(image);
        final RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"),file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("image",file.getName(),requestBody);

        //Name
        RequestBody nameRequest = RequestBody.create(MultipartBody.FORM,name);
        //Username
        RequestBody userNameRequest = RequestBody.create(MultipartBody.FORM,username);
        //Password
        RequestBody passwordRequest = RequestBody.create(MultipartBody.FORM,password);
        //Type
        RequestBody typeRequest = RequestBody.create(MultipartBody.FORM,type);

        Retrofit retrofit = configureRetrofitWithoutBearer();
        BasicAPIs api = retrofit.create(BasicAPIs.class);
        Call<BaseResponse> call = api.register(BaseFunctions.languageToNumberConverter(language),
                service_token,
                nameRequest,
                userNameRequest,
                passwordRequest,
                typeRequest,
                body);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });
    }

    public static void login(SharedPreferences sharedPreferences,
                             final Context context,
                             String language,
                             String service_token,
                             String android_id,
                             String userName,
                             String password,
                             IResponse onRespone1,
                             IFailure onFailure1){
        onResponse = onRespone1;
        onFailure = onFailure1;
        dialog = new NewProgressBar(context);
        dialog.show();
        Retrofit retrofit = configureRetrofitWithoutBearer();
        BasicAPIs api = retrofit.create(BasicAPIs.class);
        Call<BaseResponse> call = api.login(BaseFunctions.languageToNumberConverter(language),
                service_token,android_id,userName,password);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });
    }

    public static void activeAccount(SharedPreferences sharedPreferences,
                                     final Context context,
                                     String language,
                                     String service_token,
                                     String code,
                                     IResponse onResponse1,
                                     IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog = new NewProgressBar(context);
        dialog.show();
        Retrofit retrofit = configureRetrofitWithBearer(sharedPreferences);
        BasicAPIs api = retrofit.create(BasicAPIs.class);
        Call<BaseResponse> call = api.activeAccount(BaseFunctions.languageToNumberConverter(language),
                service_token,code);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });
    }

    public static void resendCode(SharedPreferences sharedPreferences,
                             final Context context,
                             String language,
                             String service_token,
                             IResponse onRespone1,
                             IFailure onFailure1){
        onResponse = onRespone1;
        onFailure = onFailure1;
        dialog = new NewProgressBar(context);
        dialog.show();
        Retrofit retrofit = configureRetrofitWithBearer(sharedPreferences);
        BasicAPIs api = retrofit.create(BasicAPIs.class);
        Call<BaseResponse> call = api.resendActiveCode(BaseFunctions.languageToNumberConverter(language),
                service_token);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });
    }

    public static void logout(SharedPreferences sharedPreferences,
                                  final Context context,
                                  String language,
                                  String service_token,
                                  IResponse onRespone1,
                                  IFailure onFailure1){
        onResponse = onRespone1;
        onFailure = onFailure1;
        dialog = new NewProgressBar(context);
        dialog.show();
        Retrofit retrofit = configureRetrofitWithBearer(sharedPreferences);
        BasicAPIs api = retrofit.create(BasicAPIs.class);
        Call<BaseResponse> call = api.logout(BaseFunctions.languageToNumberConverter(language),
                service_token);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });
    }

    public static void facebookRegister(SharedPreferences sharedPreferences,
                                      final Context context,
                                      String language,
                                      String service_token,
                                      String android_id,
                                      String account_type,
                                      String image,
                                      String access_token,
                                      IResponse onResponse1,
                                      IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        Retrofit retrofit = configureRetrofitWithoutBearer();
        BasicAPIs api = retrofit.create(BasicAPIs.class);
        Call<BaseResponse> call = api.facebookRegister(BaseFunctions.languageToNumberConverter(language),
                service_token,android_id,account_type,image,access_token);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {

                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {

                onFailure.onFailure();
            }
        });
    }
    public static void googleRegister(SharedPreferences sharedPreferences,
                                        final Context context,
                                        String language,
                                        String service_token,
                                        String android_id,
                                        String account_type,
                                        String image,
                                        String access_token,
                                        IResponse onResponse1,
                                        IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        Retrofit retrofit = configureRetrofitWithoutBearer();
        BasicAPIs api = retrofit.create(BasicAPIs.class);
        Call<BaseResponse> call = api.googleRegister(BaseFunctions.languageToNumberConverter(language),
                service_token,android_id,account_type,image,access_token);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {

                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {

                onFailure.onFailure();
            }
        });
    }

    public static void facebookCheck(SharedPreferences sharedPreferences,
                             final Context context,
                             String language,
                             String service_token,
                             String access_token,
                             IResponse onRespone1,
                             IFailure onFailure1){
        onResponse = onRespone1;
        onFailure = onFailure1;
        Retrofit retrofit = configureRetrofitWithoutBearer();
        BasicAPIs api = retrofit.create(BasicAPIs.class);
        Call<BaseResponse> call = api.facebookCheck(BaseFunctions.languageToNumberConverter(language),
                service_token,access_token);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }

    public static void googleCheck(SharedPreferences sharedPreferences,
                                     final Context context,
                                     String language,
                                     String service_token,
                                     String access_token,
                                     IResponse onRespone1,
                                     IFailure onFailure1){
        onResponse = onRespone1;
        onFailure = onFailure1;
        Retrofit retrofit = configureRetrofitWithoutBearer();
        BasicAPIs api = retrofit.create(BasicAPIs.class);
        Call<BaseResponse> call = api.googleCheck(BaseFunctions.languageToNumberConverter(language),
                service_token,access_token);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }

    public static void policy(SharedPreferences sharedPreferences,
                              final Context context,
                              String language,
                              String service_token,
                              String type,
                              IResponse onResponse1,
                              IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog =new NewProgressBar(context);
        dialog.show();
        Retrofit retrofit = configureRetrofitWithoutBearer();
        BasicAPIs api = retrofit.create(BasicAPIs.class);
        Call<BaseResponse> call = api.policy(BaseFunctions.languageToNumberConverter(language),
                service_token,type);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });
    }
}
