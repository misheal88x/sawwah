package com.scit.sawwah.APIClasses;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.scit.sawwah.API.SearchAPIs;
import com.scit.sawwah.Models.BaseResponse;
import com.scit.sawwah.Models.ReactionTypeObject;
import com.scit.sawwah.dialoges.NewProgressBar;
import com.scit.sawwah.interfaces.IFailure;
import com.scit.sawwah.interfaces.IResponse;
import com.scit.sawwah.tools.BaseFunctions;
import com.scit.sawwah.tools.BaseRetrofit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Misheal on 9/11/2019.
 */

public class SearchAPIsClass extends BaseRetrofit {
    private static NewProgressBar dialog;

    public static void searchByWord(SharedPreferences sharedPreferences,
                                    final Context context,
                                    String language,
                                    String service_token,
                                    String word,
                                    final int type,
                                    int page,
                                    IResponse onResponse1,
                                    IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog = new NewProgressBar(context);
        if (type == 0){
            dialog.show();
        }
        Retrofit retrofit = configureRetrofitWithBearer(sharedPreferences);
        SearchAPIs api = retrofit.create(SearchAPIs.class);
        Call<BaseResponse> call = api.searchByWord(BaseFunctions.languageToNumberConverter(language),
                service_token,word,page);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (type == 0){
                    dialog.cancel();
                }
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                if (type == 0){
                    dialog.cancel();
                }
                onFailure.onFailure();
            }
        });
    }

    public static void search(SharedPreferences sharedPreferences,
                              final Context context,
                              String language,
                              String service_token,
                              String lat,
                              String lng,
                              String city_id,
                              String searchType,
                              String country_id,
                              String category_id,
                              String rating,
                              String radius,
                              String word,
                              final int type,
                              int page,
                              IResponse onResponse1,
                              IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog = new NewProgressBar(context);
        if (type == 0){
            dialog.show();
        }
        Retrofit retrofit = configureRetrofitWithBearer(sharedPreferences);
        SearchAPIs api = retrofit.create(SearchAPIs.class);
        Call<BaseResponse> call = api.search(BaseFunctions.languageToNumberConverter(language),
                service_token,
                lat,lng,city_id,searchType,country_id,category_id,rating,Integer.valueOf(radius),
                word,page);

        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (type == 0){
                    dialog.cancel();
                }
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                if (type == 0){
                    dialog.cancel();
                }
                onFailure.onFailure();
            }
        });
    }
}
