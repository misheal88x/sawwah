package com.scit.sawwah.APIClasses;

import android.content.Context;
import android.widget.Toast;

import com.scit.sawwah.API.WeatherAPI;
import com.scit.sawwah.Models.BaseResponse;
import com.scit.sawwah.Models.WeatherResponse;
import com.scit.sawwah.R;
import com.scit.sawwah.dialoges.NewProgressBar;
import com.scit.sawwah.interfaces.IFailure;
import com.scit.sawwah.interfaces.IResponse;
import com.scit.sawwah.tools.BaseRetrofit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Misheal on 9/10/2019.
 */

public class WeatherAPIClass extends BaseRetrofit {
    private static NewProgressBar dialog;

    public static void getWeather(final Context context,
                                  String lat,
                                  String lng,
                                  IResponse onResponse1,
                                  final IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog = new NewProgressBar(context);
        dialog.show();
        Retrofit retrofit = configureRetrofitWithoutBearer();
        WeatherAPI api = retrofit.create(WeatherAPI.class);
        Call<WeatherResponse> call = api.getWeather(lat,lng,"4eeeab57b3f8291b5b9968dd54fea58d","metric");
        call.enqueue(new Callback<WeatherResponse>() {
            @Override
            public void onResponse(Call<WeatherResponse> call, Response<WeatherResponse> response) {
                dialog.cancel();
                if (response.body()!=null){
                    if (response.body().getCode()==200){
                        onResponse.onResponse(response.body());
                    }else {
                        Toast.makeText(context, context.getResources().getString(R.string.error_occurred), Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(context, context.getResources().getString(R.string.error_occurred), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<WeatherResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });
    }
}
