package com.scit.sawwah.APIClasses;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.scit.sawwah.API.InstitutionsAPIs;
import com.scit.sawwah.API.OffersAPIs;
import com.scit.sawwah.Models.BaseResponse;
import com.scit.sawwah.R;
import com.scit.sawwah.dialoges.NewProgressBar;
import com.scit.sawwah.interfaces.IFailure;
import com.scit.sawwah.interfaces.IResponse;
import com.scit.sawwah.tools.BaseFunctions;
import com.scit.sawwah.tools.BaseRetrofit;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Misheal on 9/3/2019.
 */

public class InstitutionsAPIsClass extends BaseRetrofit {
    private static NewProgressBar dialog;

    public static void getRatings(SharedPreferences sharedPreferences,
                                  final Context context,
                                  String language,
                                  String service_token,
                                  float lat,
                                  float lng,
                                  String id,
                                  int page,
                                  final int type,
                                  IResponse onResponse1,
                                  final IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog = new NewProgressBar(context);
        if (type == 0){
            dialog.show();
        }
        Retrofit retrofit = configureRetrofitWithBearer(sharedPreferences);
        InstitutionsAPIs api = retrofit.create(InstitutionsAPIs.class);
        Call<BaseResponse> call = api.getRatings(BaseFunctions.languageToNumberConverter(language),
                lat,lng,service_token,id,page);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (type == 0){
                    dialog.cancel();
                }
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                if (type == 0){
                    dialog.cancel();
                }
                onFailure.onFailure();
            }
        });
    }

    public static void details(SharedPreferences sharedPreferences,
                               final Context context,
                               String language,
                               String service_token,
                               String id,
                               IResponse onResponse1,
                               final IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog = new NewProgressBar(context);
        dialog.show();
        Retrofit retrofit = configureRetrofitWithBearer(sharedPreferences);
        InstitutionsAPIs api = retrofit.create(InstitutionsAPIs.class);
        Call<BaseResponse> call = api.details(BaseFunctions.languageToNumberConverter(language),
                service_token,id);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });
    }

    public static void add(SharedPreferences sharedPreferences,
                           final Context context,
                           String language,
                           String service_token,
                           String category_id,
                           String city_id,
                           String lat,
                           String lng,
                           String name,
                           String address,
                           String details,
                           String rating,
                           String icon,
                           String cover,
                           String contact_info,
                           IResponse onResponse1,
                           IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog = new NewProgressBar(context);
        dialog.show();

        //Icon
        File file1 = new File(icon);
        final RequestBody requestBody1 = RequestBody.create(MediaType.parse("multipart/form-data"),file1);
        MultipartBody.Part body1 = MultipartBody.Part.createFormData("icon",file1.getName(),requestBody1);
        //Cover
        File file2 = new File(cover);
        final RequestBody requestBody2 = RequestBody.create(MediaType.parse("multipart/form-data"),file2);
        MultipartBody.Part body2 = MultipartBody.Part.createFormData("cover",file2.getName(),requestBody2);

        //Category id
        RequestBody categoryRequest = RequestBody.create(MultipartBody.FORM,category_id);
        //City id
        RequestBody cityRequest = RequestBody.create(MultipartBody.FORM,city_id);
        //Latitude
        RequestBody latRequest = RequestBody.create(MultipartBody.FORM,lat);
        //Longitude
        RequestBody lngRequest = RequestBody.create(MultipartBody.FORM,lng);
        //Name
        RequestBody nameRequest = RequestBody.create(MultipartBody.FORM,name);
        //Address
        RequestBody addressRequest = RequestBody.create(MultipartBody.FORM,address);
        //Details
        RequestBody detailsRequest = RequestBody.create(MultipartBody.FORM,details);
        //Ratings
        RequestBody ratingRequest = RequestBody.create(MultipartBody.FORM,rating);
        //Contact Info
        RequestBody contactRequest = RequestBody.create(MultipartBody.FORM,contact_info);

        Retrofit retrofit = configureRetrofitWithBearer(sharedPreferences);
        InstitutionsAPIs api = retrofit.create(InstitutionsAPIs.class);
        Call<BaseResponse> call = api.addInstitution(BaseFunctions.languageToNumberConverter(language),
                service_token,categoryRequest,cityRequest,latRequest,lngRequest,nameRequest,addressRequest,
                detailsRequest,ratingRequest,contactRequest,body1,body2);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });
    }

    public static void editInstitution(SharedPreferences sharedPreferences,
                                        final Context context,
                                        String language,
                                        String service_token,
                                        String id,
                                        String category_id,
                                        String city_id,
                                        String lat,
                                        String lng,
                                        String name,
                                        String address,
                                        String details,
                                        String rating,
                                        String icon,
                                        String cover,
                                        String contact_info,
                                        IResponse onResponse1,
                                        IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog = new NewProgressBar(context);
        dialog.show();

        MultipartBody.Part body1,body2;
        //Icon
        if (icon.equals("")){
            body1 = null;
        }else {
            File file1 = new File(icon);
            final RequestBody requestBody1 = RequestBody.create(MediaType.parse("multipart/form-data"),file1);
            body1 = MultipartBody.Part.createFormData("icon",file1.getName(),requestBody1);
        }
        //Cover
        if (cover.equals("")){
            body2 = null;
        }else {
            File file2 = new File(cover);
            final RequestBody requestBody2 = RequestBody.create(MediaType.parse("multipart/form-data"),file2);
            body2 = MultipartBody.Part.createFormData("cover",file2.getName(),requestBody2);
        }
        //Category id
        RequestBody categoryRequest = RequestBody.create(MultipartBody.FORM,category_id);
        //City id
        RequestBody cityRequest = RequestBody.create(MultipartBody.FORM,city_id);
        //Latitude
        RequestBody latRequest = RequestBody.create(MultipartBody.FORM,lat);
        //Longitude
        RequestBody lngRequest = RequestBody.create(MultipartBody.FORM,lng);
        //Name
        RequestBody nameRequest = RequestBody.create(MultipartBody.FORM,name);
        //Address
        RequestBody addressRequest = RequestBody.create(MultipartBody.FORM,address);
        //Details
        RequestBody detailsRequest = RequestBody.create(MultipartBody.FORM,details);
        //Ratings
        RequestBody ratingRequest = RequestBody.create(MultipartBody.FORM,rating);
        //Contact Info
        RequestBody contactRequest = RequestBody.create(MultipartBody.FORM,contact_info);
        Retrofit retrofit = configureRetrofitWithBearer(sharedPreferences);
        InstitutionsAPIs api = retrofit.create(InstitutionsAPIs.class);
        Call<BaseResponse> call = api.editInstitution(BaseFunctions.languageToNumberConverter(language),
                service_token,id,categoryRequest,cityRequest,latRequest,lngRequest,nameRequest,addressRequest,
                detailsRequest,ratingRequest,contactRequest,body1,body2);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });
    }

    public static void getAllFacilities(SharedPreferences sharedPreferences,
                               final Context context,
                               String language,
                               String service_token,
                               IResponse onResponse1,
                               final IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog = new NewProgressBar(context);
        dialog.show();
        Retrofit retrofit = configureRetrofitWithBearer(sharedPreferences);
        InstitutionsAPIs api = retrofit.create(InstitutionsAPIs.class);
        Call<BaseResponse> call = api.getAllFacilities(BaseFunctions.languageToNumberConverter(language),
                service_token);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });
    }

    public static void rate(SharedPreferences sharedPreferences,
                               final Context context,
                               String language,
                               String service_token,
                               String id,
                               String value,
                               String note,
                               IResponse onResponse1,
                               final IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog = new NewProgressBar(context);
        dialog.show();
        Retrofit retrofit = configureRetrofitWithBearer(sharedPreferences);
        InstitutionsAPIs api = retrofit.create(InstitutionsAPIs.class);
        Call<BaseResponse> call = api.rate(BaseFunctions.languageToNumberConverter(language),
                service_token,id,value,note);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });
    }

    public static void getAllInsts(SharedPreferences sharedPreferences,
                                    final Context context,
                                    String language,
                                    String service_token,
                                    final int page,
                                    final int type,
                                    IResponse onResponse1,
                                    IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog = new NewProgressBar(context);
        if (type == 0){
            dialog.show();
        }
        Log.i("my_lng", "getAllInsts: "+sharedPreferences.getString("my_lng","0"));
        Retrofit retrofit = configureRetrofitWithBearer(sharedPreferences);
        InstitutionsAPIs api = retrofit.create(InstitutionsAPIs.class);
        Call<BaseResponse> call;
        call =api.get_insts(BaseFunctions.languageToNumberConverter(language),
                service_token,
                sharedPreferences.getString("my_lat","0"),
                sharedPreferences.getString("my_lng","0"),
                page);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (type == 0){
                    dialog.cancel();
                }
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                if (type == 0){
                    dialog.cancel();
                }
                onFailure.onFailure();
            }
        });
    }

    public static void addSocial(SharedPreferences sharedPreferences,
                                       final Context context,
                                       String language,
                                       String service_token,
                                       String name,
                                       String value,
                                       String type,
                                       String facility_id,
                                       String icon_file,
                                       IResponse onResponse1,
                                       IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog = new NewProgressBar(context);
        dialog.show();

        Log.i("social_infos", "addSocial: "+"name : "+name);
        Log.i("social_infos", "addSocial: "+"value : "+value);
        Log.i("social_infos", "addSocial: "+"type : "+type);
        Log.i("social_infos", "addSocial: "+"facility_id : "+facility_id);
        Log.i("social_infos", "addSocial: "+"icon_file : "+icon_file);
        //Image
        File file = new File(icon_file);
        final RequestBody requestBody1 = RequestBody.create(MediaType.parse("multipart/form-data"),file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("icon_file",file.getName(),requestBody1);

        //Name
        RequestBody nameRequest = RequestBody.create(MultipartBody.FORM,name);
        //Value
        RequestBody valueRequest = RequestBody.create(MultipartBody.FORM,value);
        //Type
        RequestBody typeRequest = RequestBody.create(MultipartBody.FORM,type);
        //Facility id
        RequestBody idRequest = RequestBody.create(MultipartBody.FORM,facility_id);

        Retrofit retrofit = configureRetrofitWithBearer(sharedPreferences);
        InstitutionsAPIs api = retrofit.create(InstitutionsAPIs.class);
        Call<BaseResponse> call = api.addSocial(BaseFunctions.languageToNumberConverter(language),
                service_token,nameRequest,valueRequest,typeRequest,idRequest,body);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                Log.i("social_error", "onFailure: "+t.getMessage());
                onFailure.onFailure();
            }
        });
    }
}
