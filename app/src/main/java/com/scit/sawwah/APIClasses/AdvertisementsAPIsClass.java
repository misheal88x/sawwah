package com.scit.sawwah.APIClasses;

import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.scit.sawwah.API.AdvertisementsAPIs;
import com.scit.sawwah.API.OffersAPIs;
import com.scit.sawwah.Models.BaseResponse;
import com.scit.sawwah.R;
import com.scit.sawwah.dialoges.NewProgressBar;
import com.scit.sawwah.interfaces.IFailure;
import com.scit.sawwah.interfaces.IResponse;
import com.scit.sawwah.tools.BaseFunctions;
import com.scit.sawwah.tools.BaseRetrofit;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Misheal on 9/2/2019.
 */

public class AdvertisementsAPIsClass extends BaseRetrofit {
    private static NewProgressBar dialog;

    public static void getAllAds(SharedPreferences sharedPreferences,
                                 final Context context,
                                 String service_token,
                                 String language,
                                 float lat,
                                 float lng,
                                 String filter,
                                 final int type,
                                 int page,
                                 IResponse onResponse1,
                                 final IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog = new NewProgressBar(context);
        if (type == 0){
            dialog.show();
        }
        Retrofit retrofit = configureRetrofitWithBearer(sharedPreferences);
        AdvertisementsAPIs api = retrofit.create(AdvertisementsAPIs.class);
        Call<BaseResponse> call = api.getAllAds(BaseFunctions.languageToNumberConverter(language),lat,lng,service_token,filter,page);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (type == 0){
                    dialog.cancel();
                }
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                if (type == 0){
                    dialog.cancel();
                }
                onFailure.onFailure();
            }
        });
    }

    public static void details(SharedPreferences sharedPreferences,
                               final Context context,
                               String language,
                               String service_token,
                               float lat,
                               float lng,
                               String id,
                               IResponse onResponse1,
                               IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog = new NewProgressBar(context);
        dialog.show();
        Retrofit retrofit = configureRetrofitWithBearer(sharedPreferences);
        AdvertisementsAPIs api = retrofit.create(AdvertisementsAPIs.class);
        Call<BaseResponse> call = api.details(BaseFunctions.languageToNumberConverter(language),
                lat,
                lng,
                service_token,
                id);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });
    }

    public static void addAdvertisement(SharedPreferences sharedPreferences,
                                final Context context,
                                String language,
                                String service_token,
                                String title,
                                String details,
                                String package_id,
                                String facility_id,
                                String start_date,
                                String end_date,
                                IResponse onResponse1,
                                IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog = new NewProgressBar(context);
        dialog.show();
        Retrofit retrofit = configureRetrofitWithBearer(sharedPreferences);
        AdvertisementsAPIs api = retrofit.create(AdvertisementsAPIs.class);
        Call<BaseResponse> call = api.addAdvertisement(BaseFunctions.languageToNumberConverter(language),
                service_token,title,details,package_id,facility_id,start_date,end_date);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });

    }

    public static void deleteAd(SharedPreferences sharedPreferences,
                                   final Context context,
                                   String language,
                                   String service_token,
                                   String ad_id,
                                   IResponse onResponse1,
                                   IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog = new NewProgressBar(context);
        dialog.show();
        Retrofit retrofit = configureRetrofitWithBearer(sharedPreferences);
        AdvertisementsAPIs api = retrofit.create(AdvertisementsAPIs.class);
        Call<BaseResponse> call = api.deleteAds(BaseFunctions.languageToNumberConverter(language),
                service_token,ad_id);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });
    }

    public static void updateAdvertisement(SharedPreferences sharedPreferences,
                                        final Context context,
                                        String language,
                                        String service_token,
                                        String id,
                                        String title,
                                        String details,
                                        String package_id,
                                        String facility_id,
                                        String start_date,
                                        String end_date,
                                        IResponse onResponse1,
                                        IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog = new NewProgressBar(context);
        dialog.show();
        Retrofit retrofit = configureRetrofitWithBearer(sharedPreferences);
        AdvertisementsAPIs api = retrofit.create(AdvertisementsAPIs.class);
        Call<BaseResponse> call = api.updateAdvertisement(BaseFunctions.languageToNumberConverter(language),
                service_token,id,title,details,package_id,facility_id,start_date,end_date);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });

    }
}
