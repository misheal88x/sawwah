package com.scit.sawwah.APIClasses;

import android.content.Context;
import android.content.SharedPreferences;

import com.scit.sawwah.API.CardAPIs;
import com.scit.sawwah.Models.BaseResponse;
import com.scit.sawwah.dialoges.NewProgressBar;
import com.scit.sawwah.interfaces.IFailure;
import com.scit.sawwah.interfaces.IResponse;
import com.scit.sawwah.tools.BaseFunctions;
import com.scit.sawwah.tools.BaseRetrofit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Misheal on 9/9/2019.
 */

public class CardAPIsClass extends BaseRetrofit {
    private static NewProgressBar dialog ;

    public static void add_benefit(SharedPreferences sharedPreferences,
                                   final Context context,
                                   String language,
                                   String service_token,
                                   String barcode,
                                   String facility_id,
                                   IResponse onResponse1,
                                   IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog = new NewProgressBar(context);
        dialog.show();
        Retrofit retrofit = configureRetrofitWithBearer(sharedPreferences);
        CardAPIs api = retrofit.create(CardAPIs.class);
        Call<BaseResponse> call = api.add_benefit(BaseFunctions.languageToNumberConverter(language),
                service_token,barcode,facility_id);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });
    }

    public static void get_benefits(SharedPreferences sharedPreferences,
                                    final Context context,
                                    String language,
                                    String service_token,
                                    String facility_id,
                                    final int type,
                                    int page,
                                    IResponse onResponse1,
                                    IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog = new NewProgressBar(context);
        if (type == 0){
            dialog.show();
        }
        Retrofit retrofit = configureRetrofitWithBearer(sharedPreferences);
        CardAPIs api = retrofit.create(CardAPIs.class);
        Call<BaseResponse> call = api.get_benifits(BaseFunctions.languageToNumberConverter(language),
                service_token,facility_id,page);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (type == 0){
                    dialog.cancel();
                }
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                if (type == 0){
                    dialog.cancel();
                }
                onFailure.onFailure();
            }
        });
    }

    public static void getCardInfo(SharedPreferences sharedPreferences,
                                   final Context context,
                                   String language,
                                   String service_token,
                                   IResponse onResponse1,
                                   final IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog = new NewProgressBar(context);
        dialog.show();
        Retrofit retrofit = configureRetrofitWithBearer(sharedPreferences);
        CardAPIs api = retrofit.create(CardAPIs.class);
        Call<BaseResponse> call = api.getCardInfo(BaseFunctions.languageToNumberConverter(language),
                service_token);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });

    }
}
