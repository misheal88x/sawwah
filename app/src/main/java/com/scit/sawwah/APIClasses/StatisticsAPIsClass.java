package com.scit.sawwah.APIClasses;

import android.content.Context;
import android.content.SharedPreferences;

import com.scit.sawwah.API.OffersAPIs;
import com.scit.sawwah.API.StatisticsAPIs;
import com.scit.sawwah.Models.BaseResponse;
import com.scit.sawwah.dialoges.NewProgressBar;
import com.scit.sawwah.interfaces.IFailure;
import com.scit.sawwah.interfaces.IResponse;
import com.scit.sawwah.tools.BaseFunctions;
import com.scit.sawwah.tools.BaseRetrofit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Misheal on 9/22/2019.
 */

public class StatisticsAPIsClass extends BaseRetrofit{
    private static NewProgressBar dialog;

    public static void getSta(SharedPreferences sharedPreferences,
                                   final Context context,
                                   String language,
                                   String service_token,
                                   IResponse onResponse1,
                                   IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog = new NewProgressBar(context);
        dialog.show();
        Retrofit retrofit = configureRetrofitWithBearer(sharedPreferences);
        StatisticsAPIs api = retrofit.create(StatisticsAPIs.class);
        Call<BaseResponse> call = api.getSta(BaseFunctions.languageToNumberConverter(language),
                service_token);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });
    }
}
