package com.scit.sawwah.APIClasses;

import android.content.Context;
import android.content.SharedPreferences;

import com.scit.sawwah.API.FavouriteAPIs;
import com.scit.sawwah.Models.BaseResponse;
import com.scit.sawwah.dialoges.NewProgressBar;
import com.scit.sawwah.interfaces.IFailure;
import com.scit.sawwah.interfaces.IResponse;
import com.scit.sawwah.tools.BaseFunctions;
import com.scit.sawwah.tools.BaseRetrofit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Misheal on 9/5/2019.
 */

public class FavouriteAPIsClass extends BaseRetrofit {
    private static NewProgressBar dialog;

    public static void getFavourite(SharedPreferences sharedPreferences,
                                    final Context context,
                                    String language,
                                    String service_token,
                                    IResponse onResponse1,
                                    IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog = new NewProgressBar(context);
        dialog.show();
        Retrofit retrofit = configureRetrofitWithBearer(sharedPreferences);
        FavouriteAPIs api = retrofit.create(FavouriteAPIs.class);
        Call<BaseResponse> call = api.getFavourites(BaseFunctions.languageToNumberConverter(language),
                service_token,
                sharedPreferences.getString("my_lat","0"),
                sharedPreferences.getString("my_lng","0"));
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });
    }

    public static void toggle_item(SharedPreferences sharedPreferences,
                                   final Context context,
                                   String language,
                                   String service_token,
                                   String id ,
                                   String type,
                                   IResponse onResponse1,
                                   IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog = new NewProgressBar(context);
        dialog.show();
        Retrofit retrofit = configureRetrofitWithBearer(sharedPreferences);
        FavouriteAPIs api = retrofit.create(FavouriteAPIs.class);
        Call<BaseResponse> call = api.toggle_item(BaseFunctions.languageToNumberConverter(language),
                service_token,id,type);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });

    }
}
