package com.scit.sawwah.APIClasses;

import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.scit.sawwah.API.BasicAPIs;
import com.scit.sawwah.API.OffersAPIs;
import com.scit.sawwah.Models.BaseResponse;
import com.scit.sawwah.R;
import com.scit.sawwah.dialoges.NewProgressBar;
import com.scit.sawwah.interfaces.IFailure;
import com.scit.sawwah.interfaces.IResponse;
import com.scit.sawwah.tools.BaseFunctions;
import com.scit.sawwah.tools.BaseRetrofit;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Misheal on 9/2/2019.
 */

public class OffersAPIsClass extends BaseRetrofit {
    private static NewProgressBar dialog;

    public static void getAllOffers(SharedPreferences sharedPreferences,
                                    final Context context,
                                    String language,
                                    String service_token,
                                    float lat,
                                    float lng,
                                    String filter,
                                    final int page,
                                    final int type,
                                    IResponse onResponse1,
                                    IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog = new NewProgressBar(context);
        if (type == 0){
            dialog.show();
        }
        Retrofit retrofit = configureRetrofitWithBearer(sharedPreferences);
        OffersAPIs api = retrofit.create(OffersAPIs.class);
        Call<BaseResponse> call;
        call =api.getAllOffers(BaseFunctions.languageToNumberConverter(language),
                lat,
                lng,
                service_token,
                filter,
                page);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (type == 0){
                    dialog.cancel();
                }
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                if (type == 0){
                    dialog.cancel();
                }
                onFailure.onFailure();
            }
        });
    }

    public static void get_details(SharedPreferences sharedPreferences,
                                   final Context context,
                                   String language,
                                   String service_token,
                                   String id,
                                   IResponse onResponse1,
                                   IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog = new NewProgressBar(context);
        dialog.show();
        Retrofit retrofit = configureRetrofitWithBearer(sharedPreferences);
        OffersAPIs api = retrofit.create(OffersAPIs.class);
        Call<BaseResponse> call = api.details(BaseFunctions.languageToNumberConverter(language),
                service_token,id);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });
    }

    public static void addOffer(SharedPreferences sharedPreferences,
                                final Context context,
                                String language,
                                String service_token,
                                String title,
                                String details,
                                String package_id,
                                String facility_id,
                                String start_date,
                                String end_date,
                                String image,
                                IResponse onResponse1,
                                IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog = new NewProgressBar(context);
        dialog.show();

        File file = new File(image);
        final RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"),file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("image",file.getName(),requestBody);

        //Title
        RequestBody titleRequest = RequestBody.create(MultipartBody.FORM,title);
        //Details
        RequestBody detailsRequest = RequestBody.create(MultipartBody.FORM,details);
        //Package
        RequestBody packageRequest = RequestBody.create(MultipartBody.FORM,package_id);
        //Institution
        RequestBody instRequest = RequestBody.create(MultipartBody.FORM,facility_id);
        //Start date
        RequestBody startDateRequest = RequestBody.create(MultipartBody.FORM,start_date);
        //End date
        RequestBody endDateRequest = RequestBody.create(MultipartBody.FORM,end_date);
        Retrofit retrofit = configureRetrofitWithBearer(sharedPreferences);
        OffersAPIs api = retrofit.create(OffersAPIs.class);
        Call<BaseResponse> call = api.addOffer(BaseFunctions.languageToNumberConverter(language),
                service_token,titleRequest,detailsRequest,packageRequest,instRequest,startDateRequest,endDateRequest,body);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });

    }

    public static void deleteOffer(SharedPreferences sharedPreferences,
                                   final Context context,
                                   String language,
                                   String service_token,
                                   String id,
                                   IResponse onResponse1,
                                   IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog = new NewProgressBar(context);
        dialog.show();
        Retrofit retrofit = configureRetrofitWithBearer(sharedPreferences);
        OffersAPIs api = retrofit.create(OffersAPIs.class);
        Call<BaseResponse> call = api.deleteOffer(BaseFunctions.languageToNumberConverter(language),
                service_token,id);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });
    }

    public static void updateOffer(SharedPreferences sharedPreferences,
                                final Context context,
                                String language,
                                String service_token,
                                String id,
                                String title,
                                String details,
                                String package_id,
                                String facility_id,
                                String start_date,
                                String end_date,
                                String is_hidden,
                                IResponse onResponse1,
                                IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog = new NewProgressBar(context);
        dialog.show();

        //Title
        RequestBody titleRequest = RequestBody.create(MultipartBody.FORM,title);
        //Details
        RequestBody detailsRequest = RequestBody.create(MultipartBody.FORM,details);
        //Package
        RequestBody packageRequest = RequestBody.create(MultipartBody.FORM,package_id);
        //Institution
        RequestBody instRequest = RequestBody.create(MultipartBody.FORM,facility_id);
        //Start date
        RequestBody startDateRequest = RequestBody.create(MultipartBody.FORM,start_date);
        //End date
        RequestBody endDateRequest = RequestBody.create(MultipartBody.FORM,end_date);
        //Is hidden
        RequestBody isHiddenRequest = RequestBody.create(MultipartBody.FORM,is_hidden);
        Retrofit retrofit = configureRetrofitWithBearer(sharedPreferences);
        OffersAPIs api = retrofit.create(OffersAPIs.class);
        Call<BaseResponse> call = api.updateOffer(BaseFunctions.languageToNumberConverter(language),
                service_token,id,titleRequest,detailsRequest,packageRequest,instRequest,startDateRequest,endDateRequest,null,isHiddenRequest);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });

    }
}
