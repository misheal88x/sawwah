package com.scit.sawwah.APIClasses;

import android.content.Context;
import android.content.SharedPreferences;

import com.scit.sawwah.API.MemorialAPIs;
import com.scit.sawwah.API.OffersAPIs;
import com.scit.sawwah.Models.BaseResponse;
import com.scit.sawwah.dialoges.NewProgressBar;
import com.scit.sawwah.interfaces.IFailure;
import com.scit.sawwah.interfaces.IResponse;
import com.scit.sawwah.tools.BaseFunctions;
import com.scit.sawwah.tools.BaseRetrofit;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Misheal on 9/22/2019.
 */

public class MemorialAPIsClass extends BaseRetrofit {
    private static NewProgressBar dialog;

    public static void getImages(SharedPreferences sharedPreferences,
                                 final Context context,
                                 String language,
                                 String service_token,
                                 String filter,
                                 final int type,
                                 int page,
                                 IResponse onResponse1,
                                 IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog = new NewProgressBar(context);
        if (type == 0){
            dialog.show();
        }
        Retrofit retrofit = configureRetrofitWithoutBearer();
        MemorialAPIs api = retrofit.create(MemorialAPIs.class);
        Call<BaseResponse> call;
        call =api.getImages(BaseFunctions.languageToNumberConverter(language),
                service_token,
                filter,
                page);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (type == 0){
                    dialog.cancel();
                }
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                if (type == 0){
                    dialog.cancel();
                }
                onFailure.onFailure();
            }
        });
    }

    public static void removeImage(SharedPreferences sharedPreferences,
                                   final Context context,
                                   String language,
                                   String service_token,
                                   String id,
                                   IResponse onResponse1,
                                   IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog = new NewProgressBar(context);
        dialog.show();
        Retrofit retrofit = configureRetrofitWithBearer(sharedPreferences);
        MemorialAPIs api = retrofit.create(MemorialAPIs.class);
        Call<BaseResponse> call = api.removeImage(BaseFunctions.languageToNumberConverter(language),
                service_token,id);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });
    }

    public static void removeCustomerImage(SharedPreferences sharedPreferences,
                                   final Context context,
                                   String language,
                                   String service_token,
                                   String id,
                                   IResponse onResponse1,
                                   IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog = new NewProgressBar(context);
        dialog.show();
        Retrofit retrofit = configureRetrofitWithBearer(sharedPreferences);
        MemorialAPIs api = retrofit.create(MemorialAPIs.class);
        Call<BaseResponse> call = api.removeCustomerImage(BaseFunctions.languageToNumberConverter(language),
                service_token,id);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });
    }

    public static void addImage(SharedPreferences sharedPreferences,
                                final Context context,
                                String language,
                                String service_token,
                                String facility_id,
                                String comment,
                                String image,
                                IResponse onResponse1,
                                IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog = new NewProgressBar(context);
        dialog.show();

        File file = new File(image);
        final RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"),file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("image",file.getName(),requestBody);

        //Facility id
        RequestBody facilityidRequest = RequestBody.create(MultipartBody.FORM,facility_id);
        //Comment
        RequestBody commentRequest = RequestBody.create(MultipartBody.FORM,comment);

        Retrofit retrofit = configureRetrofitWithBearer(sharedPreferences);
        MemorialAPIs api = retrofit.create(MemorialAPIs.class);
        Call<BaseResponse> call = api.addImage(BaseFunctions.languageToNumberConverter(language),
                service_token,facilityidRequest,commentRequest,body);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });
    }

    public static void getCustomerImages(SharedPreferences sharedPreferences,
                                 final Context context,
                                 String language,
                                 String service_token,
                                 final int type,
                                 int page,
                                 IResponse onResponse1,
                                 IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog = new NewProgressBar(context);
        if (type == 0){
            dialog.show();
        }
        Retrofit retrofit = configureRetrofitWithBearer(sharedPreferences);
        MemorialAPIs api = retrofit.create(MemorialAPIs.class);
        Call<BaseResponse> call;
        call =api.getCustomerImages(BaseFunctions.languageToNumberConverter(language),
                service_token,
                page);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (type == 0){
                    dialog.cancel();
                }
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                if (type == 0){
                    dialog.cancel();
                }
                onFailure.onFailure();
            }
        });
    }

    public static void addCustomerImage(SharedPreferences sharedPreferences,
                                final Context context,
                                String language,
                                String service_token,
                                String image,
                                IResponse onResponse1,
                                IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog = new NewProgressBar(context);
        dialog.show();

        File file = new File(image);
        final RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"),file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("image",file.getName(),requestBody);

        Retrofit retrofit = configureRetrofitWithBearer(sharedPreferences);
        MemorialAPIs api = retrofit.create(MemorialAPIs.class);
        Call<BaseResponse> call = api.addCustomerImage(BaseFunctions.languageToNumberConverter(language),
                service_token,body);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });
    }
}
