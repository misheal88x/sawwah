package com.scit.sawwah.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Misheal on 8/30/2019.
 */

public class BaseResponse {
    @SerializedName("status_code") private int status_code = 0;
    @SerializedName("data") private Object data = null;

    public BaseResponse() {}

    public BaseResponse(int status_code, Object data) {
        this.status_code = status_code;
        this.data = data;
    }

    public int getStatus_code() {
        return status_code;
    }

    public void setStatus_code(int status_code) {
        this.status_code = status_code;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
