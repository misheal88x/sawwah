package com.scit.sawwah.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Misheal on 9/10/2019.
 */

public class WeatherMainObject {
    @SerializedName("temp") private float temp = 0;
    @SerializedName("temp_min") private float temp_min = 0;
    @SerializedName("temp_max") private float temp_max = 0;

    public WeatherMainObject() {}

    public WeatherMainObject(float temp, float temp_min, float temp_max) {
        this.temp = temp;
        this.temp_min = temp_min;
        this.temp_max = temp_max;
    }

    public float getTemp() {
        return temp;
    }

    public void setTemp(float temp) {
        this.temp = temp;
    }

    public float getTemp_min() {
        return temp_min;
    }

    public void setTemp_min(float temp_min) {
        this.temp_min = temp_min;
    }

    public float getTemp_max() {
        return temp_max;
    }

    public void setTemp_max(float temp_max) {
        this.temp_max = temp_max;
    }
}
