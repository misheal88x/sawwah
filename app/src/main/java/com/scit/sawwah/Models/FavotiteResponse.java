package com.scit.sawwah.Models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Misheal on 9/6/2019.
 */

public class FavotiteResponse {
    @SerializedName("offers") private List<OfferObject> offers = new ArrayList<>();
    @SerializedName("facilities") private List<InstitutionObject> facilities = new ArrayList<>();

    public FavotiteResponse() {}

    public FavotiteResponse(List<OfferObject> offers, List<InstitutionObject> facilities) {
        this.offers = offers;
        this.facilities = facilities;
    }

    public List<OfferObject> getOffers() {
        return offers;
    }

    public void setOffers(List<OfferObject> offers) {
        this.offers = offers;
    }

    public List<InstitutionObject> getFacilities() {
        return facilities;
    }

    public void setFacilities(List<InstitutionObject> facilities) {
        this.facilities = facilities;
    }
}
