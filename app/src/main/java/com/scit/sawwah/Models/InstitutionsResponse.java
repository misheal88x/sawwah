package com.scit.sawwah.Models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Misheal on 10/5/2019.
 */

public class InstitutionsResponse {
    @SerializedName("current_page") int current_page = 0;
    @SerializedName("data")
    List<InstitutionObject> data = new ArrayList<>();
    @SerializedName("first_page_url") String first_page_url = "";
    @SerializedName("from") int from = 0;
    @SerializedName("last_page") int last_page = 0;
    @SerializedName("last_page_url") String last_page_url = "";
    @SerializedName("next_page_url") String next_page_url = "";
    @SerializedName("path") String path = "";
    @SerializedName("per_page") int per_page = 0;
    @SerializedName("prev_page_url") String prev_page_url = "";
    @SerializedName("to") int to = 0;
    @SerializedName("total") int total = 0;

    public InstitutionsResponse() {}

    public InstitutionsResponse(int current_page,
                          List<InstitutionObject> data,
                          String first_page_url,
                          int from,
                          int last_page,
                          String last_page_url,
                          String next_page_url,
                          String path,
                          int per_page,
                          String prev_page_url,
                          int to,
                          int total) {
        this.current_page = current_page;
        this.data = data;
        this.first_page_url = first_page_url;
        this.from = from;
        this.last_page = last_page;
        this.last_page_url = last_page_url;
        this.next_page_url = next_page_url;
        this.path = path;
        this.per_page = per_page;
        this.prev_page_url = prev_page_url;
        this.to = to;
        this.total = total;
    }

    public int getCurrent_page() {
        return current_page;
    }

    public void setCurrent_page(int current_page) {
        this.current_page = current_page;
    }

    public List<InstitutionObject> getData() {
        return data;
    }

    public void setData(List<InstitutionObject> data) {
        this.data = data;
    }

    public String getFirst_page_url() {
        return first_page_url;
    }

    public void setFirst_page_url(String first_page_url) {
        this.first_page_url = first_page_url;
    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public int getLast_page() {
        return last_page;
    }

    public void setLast_page(int last_page) {
        this.last_page = last_page;
    }

    public String getLast_page_url() {
        return last_page_url;
    }

    public void setLast_page_url(String last_page_url) {
        this.last_page_url = last_page_url;
    }

    public String getNext_page_url() {
        return next_page_url;
    }

    public void setNext_page_url(String next_page_url) {
        this.next_page_url = next_page_url;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public int getPer_page() {
        return per_page;
    }

    public void setPer_page(int per_page) {
        this.per_page = per_page;
    }

    public String getPrev_page_url() {
        return prev_page_url;
    }

    public void setPrev_page_url(String prev_page_url) {
        this.prev_page_url = prev_page_url;
    }

    public int getTo() {
        return to;
    }

    public void setTo(int to) {
        this.to = to;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
