package com.scit.sawwah.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Misheal on 9/3/2019.
 */

public class RateObject {
    @SerializedName("user_name") private String user_name = "";
    @SerializedName("rate_value") private float rate_value = 0;
    @SerializedName("note") private String note = "";
    @SerializedName("created_at") private String created_at = "";

    public RateObject() {}

    public RateObject(String user_name, float rate_value, String note, String created_at) {
        this.user_name = user_name;
        this.rate_value = rate_value;
        this.note = note;
        this.created_at = created_at;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public float getRate_value() {
        return rate_value;
    }

    public void setRate_value(float rate_value) {
        this.rate_value = rate_value;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }
}
