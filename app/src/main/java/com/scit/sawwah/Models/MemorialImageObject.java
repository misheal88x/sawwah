package com.scit.sawwah.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Misheal on 8/30/2019.
 */

public class MemorialImageObject {
    @SerializedName("id") private int id = 0;
    @SerializedName("language_id") private int language_id = 0;
    @SerializedName("comment") private String comment = "";
    @SerializedName("owner_id") private int owner_id = 0;
    @SerializedName("image_icon") private String image_icon = "";
    @SerializedName("created_at") private String created_at = "";
    @SerializedName("facility_name") private String facility_name = "";
    @SerializedName("city_name") private String city_name = "";
    @SerializedName("country_name") private String country_name = "";
    @SerializedName("facility_image") private String facility_image = "";
    @SerializedName("owner_name") private String owner_name = "";
    @SerializedName("image") private String image = "";
    @SerializedName("user_reaction") private int user_reaction = 0;


    public MemorialImageObject() {}

    public MemorialImageObject(int id,
                               int language_id,
                               String comment,
                               int owner_id,
                               String image_icon,
                               String created_at,
                               String facility_name,
                               String city_name,
                               String country_name,
                               String facility_image,
                               String owner_name,
                               String image) {
        this.id = id;
        this.language_id = language_id;
        this.comment = comment;
        this.owner_id = owner_id;
        this.image_icon = image_icon;
        this.created_at = created_at;
        this.facility_name = facility_name;
        this.city_name = city_name;
        this.country_name = country_name;
        this.facility_image = facility_image;
        this.owner_name = owner_name;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getLanguage_id() {
        return language_id;
    }

    public void setLanguage_id(int language_id) {
        this.language_id = language_id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getOwner_id() {
        return owner_id;
    }

    public void setOwner_id(int owner_id) {
        this.owner_id = owner_id;
    }

    public String getImage_icon() {
        return image_icon;
    }

    public void setImage_icon(String image_icon) {
        this.image_icon = image_icon;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getFacility_name() {
        return facility_name;
    }

    public void setFacility_name(String facility_name) {
        this.facility_name = facility_name;
    }

    public String getCity_name() {
        return city_name;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }

    public String getCountry_name() {
        return country_name;
    }

    public void setCountry_name(String country_name) {
        this.country_name = country_name;
    }

    public String getFacility_image() {
        return facility_image;
    }

    public void setFacility_image(String facility_image) {
        this.facility_image = facility_image;
    }

    public String getOwner_name() {
        return owner_name;
    }

    public void setOwner_name(String owner_name) {
        this.owner_name = owner_name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getUser_reaction() {
        return user_reaction;
    }

    public void setUser_reaction(int user_reaction) {
        this.user_reaction = user_reaction;
    }
}
