package com.scit.sawwah.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Misheal on 8/31/2019.
 */

public class SliderObject {
    @SerializedName("value") private String vlaue = "";
    @SerializedName("type") private int type = 0;
    @SerializedName("discount") private float discount = 0;
    @SerializedName("data") private String data = "";

    public SliderObject() {}

    public SliderObject(String vlaue, int type) {
        this.vlaue = vlaue;
        this.type = type;
    }

    public String getVlaue() {
        return vlaue;
    }

    public void setVlaue(String vlaue) {
        this.vlaue = vlaue;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public float getDiscount() {
        return discount;
    }

    public void setDiscount(float discount) {
        this.discount = discount;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
