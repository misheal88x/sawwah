package com.scit.sawwah.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Misheal on 8/31/2019.
 */

public class ReactionTypeObject {
    @SerializedName("id") private int id= 0;
    @SerializedName("title") private String title = "";
    @SerializedName("image_icon") private String image_icon = "";

    public ReactionTypeObject() {}

    public ReactionTypeObject(int id, String title, String image_icon) {
        this.id = id;
        this.title = title;
        this.image_icon = image_icon;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage_icon() {
        return image_icon;
    }

    public void setImage_icon(String image_icon) {
        this.image_icon = image_icon;
    }
}
