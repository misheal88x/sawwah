package com.scit.sawwah.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Misheal on 9/3/2019.
 */

public class ImageObject {
    @SerializedName("id") private int id = 0;
    @SerializedName("src") private String src = "";

    public ImageObject() {}

    public ImageObject(int id, String src) {
        this.id = id;
        this.src = src;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }
}
