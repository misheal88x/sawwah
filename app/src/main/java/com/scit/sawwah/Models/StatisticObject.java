package com.scit.sawwah.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Misheal on 9/22/2019.
 */

public class StatisticObject {
    @SerializedName("search_count") private int search_count = 0;
    @SerializedName("fav_facilities_count") private int fav_facilities_count = 0;
    @SerializedName("pack_req_count") private int pack_req_count = 0;
    @SerializedName("card_usage") private int card_usage = 0;

    public StatisticObject() {}

    public StatisticObject(int search_count, int fav_facilities_count, int pack_req_count, int card_usage) {
        this.search_count = search_count;
        this.fav_facilities_count = fav_facilities_count;
        this.pack_req_count = pack_req_count;
        this.card_usage = card_usage;
    }

    public int getSearch_count() {
        return search_count;
    }

    public void setSearch_count(int search_count) {
        this.search_count = search_count;
    }

    public int getFav_facilities_count() {
        return fav_facilities_count;
    }

    public void setFav_facilities_count(int fav_facilities_count) {
        this.fav_facilities_count = fav_facilities_count;
    }

    public int getPack_req_count() {
        return pack_req_count;
    }

    public void setPack_req_count(int pack_req_count) {
        this.pack_req_count = pack_req_count;
    }

    public int getCard_usage() {
        return card_usage;
    }

    public void setCard_usage(int card_usage) {
        this.card_usage = card_usage;
    }
}
