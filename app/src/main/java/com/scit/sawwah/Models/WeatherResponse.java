package com.scit.sawwah.Models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Misheal on 9/10/2019.
 */

public class WeatherResponse {
    @SerializedName("cod") private int code = 0;
    @SerializedName("list") private List<WeatherListObject> list = new ArrayList<>();

    public WeatherResponse() {}

    public WeatherResponse(List<WeatherListObject> list) {
        this.list = list;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public List<WeatherListObject> getList() {
        return list;
    }

    public void setList(List<WeatherListObject> list) {
        this.list = list;
    }
}
