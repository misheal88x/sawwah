package com.scit.sawwah.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Misheal on 9/4/2019.
 */

public class RegisterResponse {
    @SerializedName("id") private int id = 0;
    @SerializedName("role_id") private int role_id = 0;
    @SerializedName("image") private String image = "";
    @SerializedName("name") private String name = "";
    @SerializedName("username") private String username = "";
    @SerializedName("created_at") private String created_at = "";
    @SerializedName("updated_at") private String updated_at = "";
    @SerializedName("account_type_id") private int account_type_id = 0;
    @SerializedName("status") private int status = 0;
    @SerializedName("varified_at") private String varified_at = "";
    @SerializedName("authenticated_at") private String authenticated_at = "";
    @SerializedName("deleted_at") private String deleted_at = "";
    @SerializedName("is_deleted") private int is_deleted = 0;
    @SerializedName("access_token") private String access_token = "";
    @SerializedName("service_token") private String service_token = "";
    @SerializedName("device_setting") private DeviceSettingsObject device_setting = new DeviceSettingsObject();
    @SerializedName("account_settings") private AccountSettingsObject account_settings = new AccountSettingsObject();

    public RegisterResponse() {}

    public RegisterResponse(int id,
                            int role_id,
                            String image,
                            String name,
                            String username,
                            String created_at,
                            String updated_at,
                            int account_type_id,
                            int status,
                            String varified_at,
                            String authenticated_at,
                            String deleted_at,
                            int is_deleted,
                            String access_token) {
        this.id = id;
        this.role_id = role_id;
        this.image = image;
        this.name = name;
        this.username = username;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.account_type_id = account_type_id;
        this.status = status;
        this.varified_at = varified_at;
        this.authenticated_at = authenticated_at;
        this.deleted_at = deleted_at;
        this.is_deleted = is_deleted;
        this.access_token = access_token;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRole_id() {
        return role_id;
    }

    public void setRole_id(int role_id) {
        this.role_id = role_id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public int getAccount_type_id() {
        return account_type_id;
    }

    public void setAccount_type_id(int account_type_id) {
        this.account_type_id = account_type_id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getVarified_at() {
        return varified_at;
    }

    public void setVarified_at(String varified_at) {
        this.varified_at = varified_at;
    }

    public String getAuthenticated_at() {
        return authenticated_at;
    }

    public void setAuthenticated_at(String authenticated_at) {
        this.authenticated_at = authenticated_at;
    }

    public String getDeleted_at() {
        return deleted_at;
    }

    public void setDeleted_at(String deleted_at) {
        this.deleted_at = deleted_at;
    }

    public int getIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(int is_deleted) {
        this.is_deleted = is_deleted;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public DeviceSettingsObject getDevice_setting() {
        return device_setting;
    }

    public void setDevice_setting(DeviceSettingsObject device_setting) {
        this.device_setting = device_setting;
    }

    public AccountSettingsObject getAccount_settings() {
        return account_settings;
    }

    public void setAccount_settings(AccountSettingsObject account_settings) {
        this.account_settings = account_settings;
    }

    public String getService_token() {
        return service_token;
    }

    public void setService_token(String service_token) {
        this.service_token = service_token;
    }
}
