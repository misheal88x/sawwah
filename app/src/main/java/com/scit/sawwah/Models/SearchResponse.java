package com.scit.sawwah.Models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Misheal on 9/11/2019.
 */

public class SearchResponse {
    @SerializedName("facilities") private List<InstitutionObject> facilities = new ArrayList<>();
    @SerializedName("offers") private OffersResponse offers = new OffersResponse();
    @SerializedName("posters") private AdvertisementsResponse posters = new AdvertisementsResponse();

    public SearchResponse() {}

    public SearchResponse(List<InstitutionObject> facilities, OffersResponse offers, AdvertisementsResponse posters) {
        this.facilities = facilities;
        this.offers = offers;
        this.posters = posters;
    }

    public List<InstitutionObject> getFacilities() {
        return facilities;
    }

    public void setFacilities(List<InstitutionObject> facilities) {
        this.facilities = facilities;
    }

    public OffersResponse getOffers() {
        return offers;
    }

    public void setOffers(OffersResponse offers) {
        this.offers = offers;
    }

    public AdvertisementsResponse getPosters() {
        return posters;
    }

    public void setPosters(AdvertisementsResponse posters) {
        this.posters = posters;
    }
}
