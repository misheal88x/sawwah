package com.scit.sawwah.Models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Misheal on 9/10/2019.
 */

public class WeatherListObject {
    @SerializedName("main") private WeatherMainObject main = new WeatherMainObject();
    @SerializedName("weather") private List<WeatherWeatherObject> weather = new ArrayList<>();
    @SerializedName("dt_txt") private String dt_txt = "";

    public WeatherListObject() {}

    public WeatherListObject(WeatherMainObject main, List<WeatherWeatherObject> weather, String dt_txt) {
        this.main = main;
        this.weather = weather;
        this.dt_txt = dt_txt;
    }

    public WeatherMainObject getMain() {
        return main;
    }

    public void setMain(WeatherMainObject main) {
        this.main = main;
    }

    public List<WeatherWeatherObject> getWeather() {
        return weather;
    }

    public void setWeather(List<WeatherWeatherObject> weather) {
        this.weather = weather;
    }

    public String getDt_txt() {
        return dt_txt;
    }

    public void setDt_txt(String dt_txt) {
        this.dt_txt = dt_txt;
    }
}
