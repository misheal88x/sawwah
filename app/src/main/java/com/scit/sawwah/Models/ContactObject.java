package com.scit.sawwah.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Misheal on 9/3/2019.
 */

public class ContactObject {
    @SerializedName("name") private String name = "";
    @SerializedName("value") private String value = "";
    @SerializedName("icon") private String icon = "";
    @SerializedName("type") private int type = 0;

    public ContactObject() {}

    public ContactObject(String name, String value, String icon, int type) {
        this.name = name;
        this.value = value;
        this.icon = icon;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
