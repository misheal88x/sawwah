package com.scit.sawwah.Models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Misheal on 8/31/2019.
 */

public class CountryObject {
    @SerializedName("id") private int id = 0;
    @SerializedName("name") private String name = "";
    @SerializedName("cities") private List<CityObject> cities = new ArrayList<>();

    public CountryObject() {}

    public CountryObject(int id, String name, List<CityObject> cities) {
        this.id = id;
        this.name = name;
        this.cities = cities;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<CityObject> getCities() {
        return cities;
    }

    public void setCities(List<CityObject> cities) {
        this.cities = cities;
    }
}
