package com.scit.sawwah.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Misheal on 9/14/2019.
 */

public class NotificationObject {
    @SerializedName("id") private int id = 0;
    @SerializedName("image") private String image = "";
    @SerializedName("target_id") private int target_id = 0;
    @SerializedName("target") private int target = 0;
    @SerializedName("flag") private int flag = 0;
    @SerializedName("read_at") private String read_at = "";
    @SerializedName("created_at") private String created_at = "";
    @SerializedName("title") private String title = "";
    @SerializedName("payload") private String payload = "";

    public NotificationObject() {}

    public NotificationObject(int id,
                              int target_id,
                              int target,
                              int flag,
                              String read_at,
                              String created_at,
                              String title,
                              String payload) {
        this.id = id;
        this.target_id = target_id;
        this.target = target;
        this.flag = flag;
        this.read_at = read_at;
        this.created_at = created_at;
        this.title = title;
        this.payload = payload;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTarget_id() {
        return target_id;
    }

    public void setTarget_id(int target_id) {
        this.target_id = target_id;
    }

    public int getTarget() {
        return target;
    }

    public void setTarget(int target) {
        this.target = target;
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public String getRead_at() {
        return read_at;
    }

    public void setRead_at(String read_at) {
        this.read_at = read_at;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
