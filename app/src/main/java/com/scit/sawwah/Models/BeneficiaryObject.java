package com.scit.sawwah.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Misheal on 9/9/2019.
 */

public class BeneficiaryObject {
    @SerializedName("user_id") private int user_id = 0;
    @SerializedName("user_name") private String user_name = "";
    @SerializedName("username") private String username = "";
    @SerializedName("user_image") private String user_image = "";
    @SerializedName("scan_time") private String scan_time = "";

    public BeneficiaryObject() {}

    public BeneficiaryObject(int user_id, String user_name, String username, String user_image, String scan_time) {
        this.user_id = user_id;
        this.user_name = user_name;
        this.username = username;
        this.user_image = user_image;
        this.scan_time = scan_time;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUser_image() {
        return user_image;
    }

    public void setUser_image(String user_image) {
        this.user_image = user_image;
    }

    public String getScan_time() {
        return scan_time;
    }

    public void setScan_time(String scan_time) {
        this.scan_time = scan_time;
    }
}
