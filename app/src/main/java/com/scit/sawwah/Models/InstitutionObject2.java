package com.scit.sawwah.Models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Misheal on 9/20/2019.
 */

public class InstitutionObject2 {
    @SerializedName("id") private int id = 0;
    @SerializedName("image_icon") private String image_icon = "";
    @SerializedName("image_cover") private String image_cover = "";
    @SerializedName("rating") private float rating = 0;
    @SerializedName("name") private String name = "";
    @SerializedName("visit_views") private int visit_views = 0;
    @SerializedName("lat") private float lat = 0;
    @SerializedName("lng") private float lng = 0;
    @SerializedName("marked_as_favourite") private int marked_as_favourite = 0;
    @SerializedName("distance") private float distance = 0;
    @SerializedName("city_name") private String city_name = "";
    @SerializedName("country_name") private String country_name = "";
    @SerializedName("address") private String address = "";
    @SerializedName("details") private String details = "";
    @SerializedName("search_view") private int search_view = 0;
    @SerializedName("category_id") private int category_id = 0;
    @SerializedName("country_id") private int country_id = 0;
    @SerializedName("city_id") private int city_id = 0;
    @SerializedName("rating_1_num") private int rating_1_num = 0;
    @SerializedName("rating_2_num") private int rating_2_num = 0;
    @SerializedName("rating_3_num") private int rating_3_num = 0;
    @SerializedName("rating_4_num") private int rating_4_num = 0;
    @SerializedName("rating_5_num") private int rating_5_num = 0;
    @SerializedName("reaction_ratio") private int reaction_ratio = 0;
    @SerializedName("offers_count") private int offers_count = 0;
    @SerializedName("posters_count") private int posters_count = 0;
    @SerializedName("users_rating") private float users_rating = 0;
    @SerializedName("users_rating_list") private List<RateObject> users_rating_list = new ArrayList<>();
    @SerializedName("favourite_count") private int favourite_count = 0;
    @SerializedName("rating_count") private int rating_count = 0;
    @SerializedName("offers") private List<OfferObject> offers = new ArrayList<>();
    @SerializedName("posters") private List<AdvertisementObject> posters = new ArrayList<>();
    @SerializedName("images") private List<ImageObject> images = new ArrayList<>();
    @SerializedName("related_facilities") private List<InstitutionObject> related_facilities = new ArrayList<>();
    @SerializedName("contacts") private List<ContactObject> contacts = new ArrayList<>();
    @SerializedName("memorial_images") private List<MemorialImageObject> memorial_images = new ArrayList<>();

    public InstitutionObject2() {}

    public InstitutionObject2(int id,
                             String image_icon,
                             float rating,
                             String name,
                             int visit_views,
                             float lat,
                             float lng,
                             float distance) {
        this.id = id;
        this.image_icon = image_icon;
        this.rating = rating;
        this.name = name;
        this.visit_views = visit_views;
        this.lat = lat;
        this.lng = lng;
        this.distance = distance;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImage_icon() {
        return image_icon;
    }

    public void setImage_icon(String image_icon) {
        this.image_icon = image_icon;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getVisit_views() {
        return visit_views;
    }

    public void setVisit_views(int visit_views) {
        this.visit_views = visit_views;
    }

    public float getLat() {
        return lat;
    }

    public void setLat(float lat) {
        this.lat = lat;
    }

    public float getLng() {
        return lng;
    }

    public void setLng(float lng) {
        this.lng = lng;
    }

    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }

    public String getImage_cover() {
        return image_cover;
    }

    public void setImage_cover(String image_cover) {
        this.image_cover = image_cover;
    }

    public String getCity_name() {
        return city_name;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }

    public String getCountry_name() {
        return country_name;
    }

    public void setCountry_name(String country_name) {
        this.country_name = country_name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public int getSearch_view() {
        return search_view;
    }

    public void setSearch_view(int search_view) {
        this.search_view = search_view;
    }

    public int getCategory_id() {
        return category_id;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }

    public int getCountry_id() {
        return country_id;
    }

    public void setCountry_id(int country_id) {
        this.country_id = country_id;
    }

    public int getCity_id() {
        return city_id;
    }

    public void setCity_id(int city_id) {
        this.city_id = city_id;
    }

    public int getRating_1_num() {
        return rating_1_num;
    }

    public void setRating_1_num(int rating_1_num) {
        this.rating_1_num = rating_1_num;
    }

    public int getRating_2_num() {
        return rating_2_num;
    }

    public void setRating_2_num(int rating_2_num) {
        this.rating_2_num = rating_2_num;
    }

    public int getRating_3_num() {
        return rating_3_num;
    }

    public void setRating_3_num(int rating_3_num) {
        this.rating_3_num = rating_3_num;
    }

    public int getRating_4_num() {
        return rating_4_num;
    }

    public void setRating_4_num(int rating_4_num) {
        this.rating_4_num = rating_4_num;
    }

    public int getRating_5_num() {
        return rating_5_num;
    }

    public void setRating_5_num(int rating_5_num) {
        this.rating_5_num = rating_5_num;
    }

    public int getReaction_ratio() {
        return reaction_ratio;
    }

    public void setReaction_ratio(int reaction_ratio) {
        this.reaction_ratio = reaction_ratio;
    }

    public int getOffers_count() {
        return offers_count;
    }

    public void setOffers_count(int offers_count) {
        this.offers_count = offers_count;
    }

    public int getPosters_count() {
        return posters_count;
    }

    public void setPosters_count(int posters_count) {
        this.posters_count = posters_count;
    }

    public float getUsers_rating() {
        return users_rating;
    }

    public void setUsers_rating(float users_rating) {
        this.users_rating = users_rating;
    }

    public List<RateObject> getUsers_rating_list() {
        return users_rating_list;
    }

    public void setUsers_rating_list(List<RateObject> users_rating_list) {
        this.users_rating_list = users_rating_list;
    }

    public int getFavourite_count() {
        return favourite_count;
    }

    public void setFavourite_count(int favourite_count) {
        this.favourite_count = favourite_count;
    }

    public int getRating_count() {
        return rating_count;
    }

    public void setRating_count(int rating_count) {
        this.rating_count = rating_count;
    }

    public List<OfferObject> getOffers() {
        return offers;
    }

    public void setOffers(List<OfferObject> offers) {
        this.offers = offers;
    }

    public List<AdvertisementObject> getPosters() {
        return posters;
    }

    public void setPosters(List<AdvertisementObject> posters) {
        this.posters = posters;
    }

    public List<ImageObject> getImages() {
        return images;
    }

    public void setImages(List<ImageObject> images) {
        this.images = images;
    }

    public List<InstitutionObject> getRelated_facilities() {
        return related_facilities;
    }

    public void setRelated_facilities(List<InstitutionObject> related_facilities) {
        this.related_facilities = related_facilities;
    }

    public List<ContactObject> getContacts() {
        return contacts;
    }

    public void setContacts(List<ContactObject> contacts) {
        this.contacts = contacts;
    }

    public List<MemorialImageObject> getMemorial_images() {
        return memorial_images;
    }

    public void setMemorial_images(List<MemorialImageObject> memorial_images) {
        this.memorial_images = memorial_images;
    }

    public int getMarked_as_favourite() {
        return marked_as_favourite;
    }

    public void setMarked_as_favourite(int marked_as_favourite) {
        this.marked_as_favourite = marked_as_favourite;
    }
}
