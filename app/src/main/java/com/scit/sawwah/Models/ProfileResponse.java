package com.scit.sawwah.Models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Misheal on 9/23/2019.
 */

public class ProfileResponse {
    @SerializedName("id") private int id = 0;
    @SerializedName("name") private String name = "";
    @SerializedName("username") private String username = "";
    @SerializedName("image") private String image = "";
    @SerializedName("status") private int status = 0;
    @SerializedName("account_type_id") private int account_type_id = 0;
    @SerializedName("facilities") private List<InstitutionObject> facilities = new ArrayList<>();
    @SerializedName("memporial_images") private List<MemorialImageObject> memporial_images = new ArrayList<>();
    @SerializedName("active_packages") private List<PricePackageObject> active_packages = new ArrayList<>();
    @SerializedName("packages") private List<PricePackageObject> packages = new ArrayList<>();

    public ProfileResponse() {}

    public ProfileResponse(int id,
                           String name,
                           String username,
                           String image,
                           int status,
                           int account_type_id,
                           List<InstitutionObject> facilities,
                           List<MemorialImageObject> memporial_images,
                           List<PricePackageObject> active_packages,
                           List<PricePackageObject> packages) {
        this.id = id;
        this.name = name;
        this.username = username;
        this.image = image;
        this.status = status;
        this.account_type_id = account_type_id;
        this.facilities = facilities;
        this.memporial_images = memporial_images;
        this.active_packages = active_packages;
        this.packages = packages;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getAccount_type_id() {
        return account_type_id;
    }

    public void setAccount_type_id(int account_type_id) {
        this.account_type_id = account_type_id;
    }

    public List<InstitutionObject> getFacilities() {
        return facilities;
    }

    public void setFacilities(List<InstitutionObject> facilities) {
        this.facilities = facilities;
    }

    public List<MemorialImageObject> getMemporial_images() {
        return memporial_images;
    }

    public void setMemporial_images(List<MemorialImageObject> memporial_images) {
        this.memporial_images = memporial_images;
    }

    public List<PricePackageObject> getActive_packages() {
        return active_packages;
    }

    public void setActive_packages(List<PricePackageObject> active_packages) {
        this.active_packages = active_packages;
    }

    public List<PricePackageObject> getPackages() {
        return packages;
    }

    public void setPackages(List<PricePackageObject> packages) {
        this.packages = packages;
    }
}
