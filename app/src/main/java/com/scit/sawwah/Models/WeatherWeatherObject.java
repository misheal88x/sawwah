package com.scit.sawwah.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Misheal on 9/10/2019.
 */

public class WeatherWeatherObject {
    @SerializedName("main") private String main = "";
    @SerializedName("description") private String description = "";

    public WeatherWeatherObject() {}

    public WeatherWeatherObject(String main, String description) {
        this.main = main;
        this.description = description;
    }

    public String getMain() {
        return main;
    }

    public void setMain(String main) {
        this.main = main;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
