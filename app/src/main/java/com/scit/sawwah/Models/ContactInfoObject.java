package com.scit.sawwah.Models;

import com.google.gson.annotations.SerializedName;

public class ContactInfoObject {
    @SerializedName("mobile_number") private String mobile_number = "";
    @SerializedName("phone_number") private String phone_number = "";

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getMobile_number() {
        return mobile_number;
    }

    public void setMobile_number(String mobile_number) {
        this.mobile_number = mobile_number;
    }
}
