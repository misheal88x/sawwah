package com.scit.sawwah.Models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Misheal on 8/31/2019.
 */

public class OfferObject {
    @SerializedName("id") private int id = 0;
    @SerializedName("visit_views") private int visit_views = 0;
    @SerializedName("facility_id") private int facility_id = 0;
    @SerializedName("package_id") private int package_id = 0;
    @SerializedName("created_at") private String created_at = "";
    @SerializedName("title") private String title = "";
    @SerializedName("image") private String image = "";
    @SerializedName("details") private String details = "";
    @SerializedName("start_date") private String start_date = "";
    @SerializedName("end_date") private String end_date = "";
    @SerializedName("facility_name") private String facility_name = "";
    @SerializedName("facility_image") private String facility_image = "";
    @SerializedName("favourite_count") private int favourite_count = 0;
    @SerializedName("marked_as_favourite") private boolean marked_as_favourite = false;
    @SerializedName("is_hidden") private int is_hidden = 0;
    @SerializedName("similar_offers") private List<OfferObject> similar_offers = new ArrayList<>();


    public OfferObject() {}

    public OfferObject(int id,
                       String title,
                       String image,
                       String start_date,
                       String end_date,
                       String facility_name,
                       String facility_image,
                       int favourite_count,
                       boolean marked_as_favourite) {
        this.id = id;
        this.title = title;
        this.image = image;
        this.start_date = start_date;
        this.end_date = end_date;
        this.facility_name = facility_name;
        this.facility_image = facility_image;
        this.favourite_count = favourite_count;
        this.marked_as_favourite = marked_as_favourite;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getFacility_name() {
        return facility_name;
    }

    public void setFacility_name(String facility_name) {
        this.facility_name = facility_name;
    }

    public String getFacility_image() {
        return facility_image;
    }

    public void setFacility_image(String facility_image) {
        this.facility_image = facility_image;
    }

    public int getFavourite_count() {
        return favourite_count;
    }

    public void setFavourite_count(int favourite_count) {
        this.favourite_count = favourite_count;
    }

    public boolean isMarked_as_favourite() {
        return marked_as_favourite;
    }

    public void setMarked_as_favourite(boolean marked_as_favourite) {
        this.marked_as_favourite = marked_as_favourite;
    }

    public int getIs_hidden() {
        return is_hidden;
    }

    public void setIs_hidden(int is_hidden) {
        this.is_hidden = is_hidden;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public int getVisit_views() {
        return visit_views;
    }

    public void setVisit_views(int visit_views) {
        this.visit_views = visit_views;
    }

    public int getFacility_id() {
        return facility_id;
    }

    public void setFacility_id(int facility_id) {
        this.facility_id = facility_id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public List<OfferObject> getSimilar_offers() {
        return similar_offers;
    }

    public void setSimilar_offers(List<OfferObject> similar_offers) {
        this.similar_offers = similar_offers;
    }

    public int getPackage_id() {
        return package_id;
    }

    public void setPackage_id(int package_id) {
        this.package_id = package_id;
    }
}
