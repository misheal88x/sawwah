package com.scit.sawwah.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Misheal on 9/20/2019.
 */

public class DeviceSettingsObject {
    @SerializedName("allow_global_notification") private int allow_global_notification = 0;
    @SerializedName("view_language_id") private int view_language_id = 0;
    @SerializedName("city_id") private int city_id = 0;
    @SerializedName("country_id") private int country_id = 0;
    @SerializedName("need_set") private int need_set = 0;

    public DeviceSettingsObject() {}

    public DeviceSettingsObject(int allow_global_notification, int view_language_id, int city_id, int need_set) {
        this.allow_global_notification = allow_global_notification;
        this.view_language_id = view_language_id;
        this.city_id = city_id;
        this.need_set = need_set;
    }

    public int getAllow_global_notification() {
        return allow_global_notification;
    }

    public void setAllow_global_notification(int allow_global_notification) {
        this.allow_global_notification = allow_global_notification;
    }

    public int getView_language_id() {
        return view_language_id;
    }

    public void setView_language_id(int view_language_id) {
        this.view_language_id = view_language_id;
    }

    public int getCity_id() {
        return city_id;
    }

    public void setCity_id(int city_id) {
        this.city_id = city_id;
    }

    public int getNeed_set() {
        return need_set;
    }

    public void setNeed_set(int need_set) {
        this.need_set = need_set;
    }

    public int getCountry_id() {
        return country_id;
    }

    public void setCountry_id(int country_id) {
        this.country_id = country_id;
    }
}
