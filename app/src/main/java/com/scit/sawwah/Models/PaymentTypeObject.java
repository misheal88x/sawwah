package com.scit.sawwah.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Misheal on 8/31/2019.
 */

public class PaymentTypeObject {
    @SerializedName("id") private int id = 0;
    @SerializedName("type") private String type = "";

    public PaymentTypeObject() {}

    public PaymentTypeObject(int id, String type) {
        this.id = id;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
