package com.scit.sawwah.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Misheal on 9/15/2019.
 */

public class PricePackageObject {
    @SerializedName("id") private int id = 0;
    @SerializedName("type") private int type = 0;
    @SerializedName("cost") private String cost = "";
    @SerializedName("duration") private int duration = 0;
    @SerializedName("state") private int status = 0;
    @SerializedName("image_icon") private String image_icon = "";
    @SerializedName("created_at") private String created_at = "";
    @SerializedName("name") private String name = "";
    @SerializedName("op_offer_duration") private int op_offer_duration = 0;
    @SerializedName("op_offer_num") private int op_offer_num = 0;
    @SerializedName("aop_offer_duration") private int iaop_offer_durationd = 0;
    @SerializedName("aop_offer_num") private int aop_offer_num = 0;
    @SerializedName("aop_ads_duration") private int aop_ads_duration = 0;
    @SerializedName("aop_ads_num") private int aop_ads_num = 0;
    @SerializedName("ap_ads_num") private int ap_ads_num = 0;
    @SerializedName("ap_ads_duration") private int ap_ads_duration = 0;

    public PricePackageObject() {}

    public PricePackageObject(int id,
                              int type,
                              String cost,
                              int duration,
                              String image_icon,
                              String created_at,
                              String name,
                              int op_offer_duration,
                              int op_offer_num,
                              int iaop_offer_durationd,
                              int aop_offer_num,
                              int aop_ads_duration,
                              int aop_ads_num,
                              int ap_ads_num,
                              int ap_ads_duration) {
        this.id = id;
        this.type = type;
        this.cost = cost;
        this.duration = duration;
        this.image_icon = image_icon;
        this.created_at = created_at;
        this.name = name;
        this.op_offer_duration = op_offer_duration;
        this.op_offer_num = op_offer_num;
        this.iaop_offer_durationd = iaop_offer_durationd;
        this.aop_offer_num = aop_offer_num;
        this.aop_ads_duration = aop_ads_duration;
        this.aop_ads_num = aop_ads_num;
        this.ap_ads_num = ap_ads_num;
        this.ap_ads_duration = ap_ads_duration;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getImage_icon() {
        return image_icon;
    }

    public void setImage_icon(String image_icon) {
        this.image_icon = image_icon;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getOp_offer_duration() {
        return op_offer_duration;
    }

    public void setOp_offer_duration(int op_offer_duration) {
        this.op_offer_duration = op_offer_duration;
    }

    public int getOp_offer_num() {
        return op_offer_num;
    }

    public void setOp_offer_num(int op_offer_num) {
        this.op_offer_num = op_offer_num;
    }

    public int getIaop_offer_durationd() {
        return iaop_offer_durationd;
    }

    public void setIaop_offer_durationd(int iaop_offer_durationd) {
        this.iaop_offer_durationd = iaop_offer_durationd;
    }

    public int getAop_offer_num() {
        return aop_offer_num;
    }

    public void setAop_offer_num(int aop_offer_num) {
        this.aop_offer_num = aop_offer_num;
    }

    public int getAop_ads_duration() {
        return aop_ads_duration;
    }

    public void setAop_ads_duration(int aop_ads_duration) {
        this.aop_ads_duration = aop_ads_duration;
    }

    public int getAop_ads_num() {
        return aop_ads_num;
    }

    public void setAop_ads_num(int aop_ads_num) {
        this.aop_ads_num = aop_ads_num;
    }

    public int getAp_ads_num() {
        return ap_ads_num;
    }

    public void setAp_ads_num(int ap_ads_num) {
        this.ap_ads_num = ap_ads_num;
    }

    public int getAp_ads_duration() {
        return ap_ads_duration;
    }

    public void setAp_ads_duration(int ap_ads_duration) {
        this.ap_ads_duration = ap_ads_duration;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
