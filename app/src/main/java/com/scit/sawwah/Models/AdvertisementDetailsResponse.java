package com.scit.sawwah.Models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Misheal on 9/12/2019.
 */

public class AdvertisementDetailsResponse {
    @SerializedName("poster") private AdvertisementObject poster = new AdvertisementObject();
    @SerializedName("related_posters") private List<AdvertisementObject> related_posters = new ArrayList<>();

    public AdvertisementDetailsResponse() {}

    public AdvertisementDetailsResponse(AdvertisementObject poster, List<AdvertisementObject> related_posters) {
        this.poster = poster;
        this.related_posters = related_posters;
    }

    public AdvertisementObject getPoster() {
        return poster;
    }

    public void setPoster(AdvertisementObject poster) {
        this.poster = poster;
    }

    public List<AdvertisementObject> getRelated_posters() {
        return related_posters;
    }

    public void setRelated_posters(List<AdvertisementObject> related_posters) {
        this.related_posters = related_posters;
    }
}
