package com.scit.sawwah.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Misheal on 9/9/2019.
 */

public class MyCardObject {
    @SerializedName("id") private int id = 0;
    @SerializedName("status") private int status = 0;
    @SerializedName("expired_at") private String expired_at = "";
    @SerializedName("frontside_image") private String frontside_image = "";
    @SerializedName("backside_image") private String backside_image = "";

    public MyCardObject() {}

    public MyCardObject(int id, int status, String expired_at, String frontside_image, String backside_image) {
        this.id = id;
        this.status = status;
        this.expired_at = expired_at;
        this.frontside_image = frontside_image;
        this.backside_image = backside_image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getExpired_at() {
        return expired_at;
    }

    public void setExpired_at(String expired_at) {
        this.expired_at = expired_at;
    }

    public String getFrontside_image() {
        return frontside_image;
    }

    public void setFrontside_image(String frontside_image) {
        this.frontside_image = frontside_image;
    }

    public String getBackside_image() {
        return backside_image;
    }

    public void setBackside_image(String backside_image) {
        this.backside_image = backside_image;
    }
}
