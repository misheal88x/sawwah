package com.scit.sawwah.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Misheal on 9/9/2019.
 */

public class AccountSettingsObject {
    @SerializedName("need_login") private int need_login = 0;
    @SerializedName("user_type") private int user_type = 0;
    @SerializedName("created_as") private int created_as = 0;
    @SerializedName("account_status") private int account_status = 0;
    @SerializedName("is_profile_completed") private int is_profile_completed = 0;

    public AccountSettingsObject() {}

    public AccountSettingsObject(int need_login, int user_type, int is_profile_completed) {
        this.need_login = need_login;
        this.user_type = user_type;
        this.is_profile_completed = is_profile_completed;
    }

    public int getNeed_login() {
        return need_login;
    }

    public void setNeed_login(int need_login) {
        this.need_login = need_login;
    }

    public int getUser_type() {
        return user_type;
    }

    public void setUser_type(int user_type) {
        this.user_type = user_type;
    }

    public int getIs_profile_completed() {
        return is_profile_completed;
    }

    public void setIs_profile_completed(int is_profile_completed) {
        this.is_profile_completed = is_profile_completed;
    }

    public int getCreated_as() {
        return created_as;
    }

    public void setCreated_as(int created_as) {
        this.created_as = created_as;
    }

    public int getAccount_status() {
        return account_status;
    }

    public void setAccount_status(int account_status) {
        this.account_status = account_status;
    }
}
