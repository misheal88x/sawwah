package com.scit.sawwah.Models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Misheal on 8/30/2019.
 */

public class StartupResponse {
    @SerializedName("offers") private List<OfferObject> offers = new ArrayList<>();
    @SerializedName("posters") private List<AdvertisementObject> posters = new ArrayList<>();
    @SerializedName("memorial_images") private List<MemorialImageObject> memorial_images = new ArrayList<>();
    @SerializedName("account_status") private int account_status = 0;
    @SerializedName("text_posters") private List<AdvertisementObject> text_posters = new ArrayList<>();
    @SerializedName("sliders") private List<SliderObject> sliders = new ArrayList<>();
    @SerializedName("countries") private List<CountryObject> countries = new ArrayList<>();
    @SerializedName("categories") private List<CategoryObject> categories = new ArrayList<>();
    @SerializedName("reactions_types") private List<ReactionTypeObject> reactions_types = new ArrayList<>();
    @SerializedName("payments_types") private List<PaymentTypeObject> payments_types = new ArrayList<>();
    @SerializedName("service_token") private String service_token = "";
    @SerializedName("my_facilities") private List<InstitutionObject> my_facilities = new ArrayList<>();
    @SerializedName("my_packages") private List<PricePackageObject> my_packages = new ArrayList<>();
    @SerializedName("device_setting") private DeviceSettingsObject device_setting = new DeviceSettingsObject();
    @SerializedName("account_settings") private AccountSettingsObject account_settings = new AccountSettingsObject();

    public StartupResponse() {}

    public StartupResponse(List<OfferObject> offers,
                           List<MemorialImageObject> memorial_images,
                           int account_status,
                           List<AdvertisementObject> text_posters,
                           List<SliderObject> sliders,
                           List<CountryObject> countries,
                           List<CategoryObject> categories,
                           List<ReactionTypeObject> reactions_types,
                           String service_token) {
        this.offers = offers;
        this.memorial_images = memorial_images;
        this.account_status = account_status;
        this.text_posters = text_posters;
        this.sliders = sliders;
        this.countries = countries;
        this.categories = categories;
        this.reactions_types = reactions_types;
        this.service_token = service_token;
    }

    public List<OfferObject> getOffers() {
        return offers;
    }

    public void setOffers(List<OfferObject> offers) {
        this.offers = offers;
    }

    public List<MemorialImageObject> getMemorial_images() {
        return memorial_images;
    }

    public void setMemorial_images(List<MemorialImageObject> memorial_images) {
        this.memorial_images = memorial_images;
    }

    public int getAccount_status() {
        return account_status;
    }

    public void setAccount_status(int account_status) {
        this.account_status = account_status;
    }

    public List<AdvertisementObject> getText_posters() {
        return text_posters;
    }

    public void setText_posters(List<AdvertisementObject> text_posters) {
        this.text_posters = text_posters;
    }

    public List<SliderObject> getSliders() {
        return sliders;
    }

    public void setSliders(List<SliderObject> sliders) {
        this.sliders = sliders;
    }

    public List<CountryObject> getCountries() {
        return countries;
    }

    public void setCountries(List<CountryObject> countries) {
        this.countries = countries;
    }

    public List<CategoryObject> getCategories() {
        return categories;
    }

    public void setCategories(List<CategoryObject> categories) {
        this.categories = categories;
    }

    public List<ReactionTypeObject> getReactions_types() {
        return reactions_types;
    }

    public void setReactions_types(List<ReactionTypeObject> reactions_types) {
        this.reactions_types = reactions_types;
    }

    public List<PaymentTypeObject> getPayments_types() {
        return payments_types;
    }

    public void setPayments_types(List<PaymentTypeObject> payments_types) {
        this.payments_types = payments_types;
    }

    public String getService_token() {
        return service_token;
    }

    public void setService_token(String service_token) {
        this.service_token = service_token;
    }

    public AccountSettingsObject getAccount_settings() {
        return account_settings;
    }

    public void setAccount_settings(AccountSettingsObject account_settings) {
        this.account_settings = account_settings;
    }

    public List<AdvertisementObject> getPosters() {
        return posters;
    }

    public void setPosters(List<AdvertisementObject> posters) {
        this.posters = posters;
    }

    public DeviceSettingsObject getDevice_setting() {
        return device_setting;
    }

    public void setDevice_setting(DeviceSettingsObject device_setting) {
        this.device_setting = device_setting;
    }

    public List<InstitutionObject> getMy_facilities() {
        return my_facilities;
    }

    public void setMy_facilities(List<InstitutionObject> my_facilities) {
        this.my_facilities = my_facilities;
    }

    public List<PricePackageObject> getMy_packages() {
        return my_packages;
    }

    public void setMy_packages(List<PricePackageObject> my_packages) {
        this.my_packages = my_packages;
    }
}
