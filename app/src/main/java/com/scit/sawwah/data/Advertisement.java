package com.scit.sawwah.data;

public class Advertisement {

    private String advertisement_id;
    private String adver_name;
    private String type;
    private String start_date;
    private String end_date;
    private String description;
    private String visitor_number;
    private String status;


    public Advertisement(String advertisement_id, String adver_name, String type, String start_date,
                         String end_date, String description, String visitor_number, String status) {
        this.advertisement_id = advertisement_id;
        this.adver_name = adver_name;
        this.type = type;
        this.start_date = start_date;
        this.end_date = end_date;
        this.description = description;
        this.visitor_number = visitor_number;
        this.status = status;
    }

    public String getAdvertisement_id() {
        return advertisement_id;
    }

    public String getAdver_name() {
        return adver_name;
    }

    public String getType() {
        return type;
    }

    public String getStart_date() {
        return start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public String getDescription() {
        return description;
    }

    public String getVisitor_number() {
        return visitor_number;
    }

    public String getStatus() {
        return status;
    }
}
