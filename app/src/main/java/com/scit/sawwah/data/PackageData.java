package com.scit.sawwah.data;

public class PackageData {

    private String id;
    private String general_type;
    private String type;
    private String advert_total_number;
    private String show_total_number;
    private String adver_general_number;
    private String show_city_number;
    private String show_country_number;
    private String cost_package;
    private String package_duration;
    private String show_duration;
    private String adver_duration;


    public PackageData(String id, String general_type, String type, String advert_total_number,
                       String show_total_number, String adver_general_number, String show_city_number,
                       String show_country_number, String cost_package, String package_duration, String show_duration,
                       String adver_duration) {
        this.id = id;
        this.general_type = general_type;
        this.type = type;
        this.advert_total_number = advert_total_number;
        this.show_total_number = show_total_number;
        this.adver_general_number = adver_general_number;
        this.show_city_number = show_city_number;
        this.show_country_number = show_country_number;
        this.cost_package = cost_package;
        this.package_duration = package_duration;
        this.show_duration = show_duration;
        this.adver_duration = adver_duration;
    }


    public PackageData(String general_type, String advert_total_number, String show_total_number,
                       String cost_package, String package_duration, String show_duration, String adver_duration) {
        this.general_type = general_type;
        this.advert_total_number = advert_total_number;
        this.show_total_number = show_total_number;
        this.cost_package = cost_package;
        this.package_duration = package_duration;
        this.show_duration = show_duration;
        this.adver_duration = adver_duration;
    }


    public String getId() {
        return id;
    }

    public String getGeneral_type() {
        return general_type;
    }

    public String getType() {
        return type;
    }

    public String getAdvert_total_number() {
        return advert_total_number;
    }

    public String getShow_total_number() {
        return show_total_number;
    }

    public String getAdver_general_number() {
        return adver_general_number;
    }

    public String getShow_city_number() {
        return show_city_number;
    }

    public String getShow_country_number() {
        return show_country_number;
    }

    public String getCost_package() {
        return cost_package;
    }

    public String getPackage_duration() {
        return package_duration;
    }

    public String getShow_duration() {
        return show_duration;
    }

    public String getAdver_duration() {
        return adver_duration;
    }
}
