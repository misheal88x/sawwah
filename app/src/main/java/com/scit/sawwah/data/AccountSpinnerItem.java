package com.scit.sawwah.data;

/**
 * Created by Misheal on 8/15/2018.
 */

public class AccountSpinnerItem {
    private float value;

    public AccountSpinnerItem(float value) {
        this.value = value;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }
}
