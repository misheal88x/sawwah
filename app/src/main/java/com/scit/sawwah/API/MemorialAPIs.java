package com.scit.sawwah.API;

import com.scit.sawwah.Models.BaseResponse;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Misheal on 9/22/2019.
 */

public interface MemorialAPIs {
    @GET("memorial_images")
    Call<BaseResponse> getImages(@Header("X-Language-View-ID") String language,
                                 @Header("serviceToken")String service_token,
                                 @Query("filter") String filter,
                                 @Query("page") int page);
    @POST("memorial_images/{id}/remove")
    Call<BaseResponse> removeImage(@Header("X-Language-View-ID") String language,
                                   @Header("serviceToken")String service_token,
                                   @Path("id") String id);
    @POST("users_memorial_images/{id}/remove")
    Call<BaseResponse> removeCustomerImage(@Header("X-Language-View-ID") String language,
                                   @Header("serviceToken")String service_token,
                                   @Path("id") String id);
    @Multipart
    @POST("memorial_images")
    Call<BaseResponse> addImage(@Header("X-Language-View-ID") String language,
                                @Header("serviceToken")String service_token,
                                @Part("facility_id") RequestBody facility_id,
                                @Part("comment") RequestBody comment,
                                @Part MultipartBody.Part image);

    @GET("users_memorial_images")
    Call<BaseResponse> getCustomerImages(@Header("X-Language-View-ID") String language,
                                 @Header("serviceToken")String service_token,
                                 @Query("page") int page);
    @Multipart
    @POST("users_memorial_images")
    Call<BaseResponse> addCustomerImage(@Header("X-Language-View-ID") String language,
                                @Header("serviceToken")String service_token,
                                @Part MultipartBody.Part image);
}
