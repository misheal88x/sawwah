package com.scit.sawwah.API;

import com.scit.sawwah.Models.BaseResponse;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Misheal on 9/3/2019.
 */

public interface InstitutionsAPIs {
    @GET("facilities/{id}?cmd=show_rating")
    Call<BaseResponse> getRatings(@Header("X-Language-View-ID") String language,
                                  @Header("lat") float lat,
                                  @Header("lng") float lng,
                                  @Header("serviceToken") String service_token,
                                  @Path("id") String id,
                                  @Query("page") int page);

    @GET("facilities/{id}")
    Call<BaseResponse> details(@Header("X-Language-View-ID") String language,
                               @Header("serviceToken") String service_token,
                               @Path("id") String id);
    @Multipart
    @POST("facilities")
    Call<BaseResponse> addInstitution(@Header("X-Language-View-ID") String language,
                                      @Header("serviceToken") String service_token,
                                      @Part("category_id") RequestBody category_id,
                                      @Part("city_id") RequestBody city_id,
                                      @Part("lat") RequestBody lat,
                                      @Part("lng") RequestBody lng,
                                      @Part("name") RequestBody name,
                                      @Part("address") RequestBody address,
                                      @Part("details") RequestBody details,
                                      @Part("rating") RequestBody rating,
                                      @Part("contact_info") RequestBody contact_info,
                                      @Part MultipartBody.Part icon,
                                      @Part MultipartBody.Part cover);
    @Multipart
    @POST("facilities/{id}/update")
    Call<BaseResponse> editInstitution(@Header("X-Language-View-ID") String language,
                                      @Header("serviceToken") String service_token,
                                      @Path("id") String id,
                                      @Part("category_id") RequestBody category_id,
                                      @Part("city_id") RequestBody city_id,
                                      @Part("lat") RequestBody lat,
                                      @Part("lng") RequestBody lng,
                                      @Part("name") RequestBody name,
                                      @Part("address") RequestBody address,
                                      @Part("details") RequestBody details,
                                      @Part("rating") RequestBody rating,
                                       @Part("contact_info") RequestBody contact_info,
                                      @Part MultipartBody.Part icon,
                                      @Part MultipartBody.Part cover);
    @GET("list-facilities")
    Call<BaseResponse> getAllFacilities(@Header("X-Language-View-ID") String language,
                                        @Header("serviceToken") String service_token);
    @FormUrlEncoded
    @POST("facilities/{id}/rate")
    Call<BaseResponse> rate(@Header("X-Language-View-ID") String language,
                            @Header("serviceToken") String service_token,
                            @Path("id") String id,
                            @Field("value") String value,
                            @Field("note") String note);

    @GET("facilities")
    Call<BaseResponse> get_insts(@Header("X-Language-View-ID") String language,
                                 @Header("serviceToken") String service_token,
                                 @Header("lat") String lat,
                                 @Header("lng") String lng,
                                 @Query("page") int page);
    @Multipart
    @POST("facilities_contacts")
    Call<BaseResponse> addSocial(
            @Header("X-Language-View-ID") String language,
            @Header("serviceToken") String service_token,
            @Part("name") RequestBody name,
            @Part("value") RequestBody value,
            @Part("type") RequestBody type,
            @Part("facility_id") RequestBody facility_id,
            @Part MultipartBody.Part icon_file
    );


}
