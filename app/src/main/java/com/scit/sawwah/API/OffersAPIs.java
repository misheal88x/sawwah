package com.scit.sawwah.API;

import com.scit.sawwah.Models.BaseResponse;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Misheal on 9/2/2019.
 */

public interface OffersAPIs {
    @GET("offers")
    Call<BaseResponse> getAllOffers(@Header("X-Language-View-ID") String language,
                                    @Header("lat") float lat,
                                    @Header("lng") float lng,
                                    @Header("serviceToken") String service_token,
                                    @Query("filter") String filter,
                                    @Query("page") int page);
    @GET("offers/{id}")
    Call<BaseResponse> details(@Header("X-Language-View-ID") String language,
                               @Header("serviceToken") String service_token,
                               @Path("id") String id);
    @Multipart
    @POST("offers")
    Call<BaseResponse> addOffer(@Header("X-Language-View-ID") String language,
                                @Header("serviceToken") String service_token,
                                @Part("title") RequestBody title,
                                @Part("details") RequestBody details,
                                @Part("package_id") RequestBody package_id,
                                @Part("facility_id") RequestBody facility_id,
                                @Part("start_date") RequestBody start_date,
                                @Part("end_date") RequestBody end_date,
                                @Part MultipartBody.Part image);
    @POST("offers/{id}/remove")
    Call<BaseResponse> deleteOffer(@Header("X-Language-View-ID") String language,
                                   @Header("serviceToken") String service_token,
                                   @Path("id") String offer_id);
    @Multipart
    @POST("offers/{id}/update")
    Call<BaseResponse> updateOffer(@Header("X-Language-View-ID") String language,
                                @Header("serviceToken") String service_token,
                                @Path("id") String id,
                                @Part("title") RequestBody title,
                                @Part("details") RequestBody details,
                                @Part("package_id") RequestBody package_id,
                                @Part("facility_id") RequestBody facility_id,
                                @Part("start_date") RequestBody start_date,
                                @Part("end_date") RequestBody end_date,
                                @Part MultipartBody.Part image,
                                   @Part("is_hidden") RequestBody is_hidden);

}
