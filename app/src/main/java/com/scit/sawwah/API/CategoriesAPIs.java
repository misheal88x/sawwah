package com.scit.sawwah.API;

import com.scit.sawwah.Models.BaseResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Misheal on 9/3/2019.
 */

public interface CategoriesAPIs {
    @GET("categories")
    Call<BaseResponse> getCategories(@Header("X-Language-View-ID") String language,
                                     @Header("serviceToken") String service_token,
                                     @Query("page") int page);

    @GET("categories/{id}")
    Call<BaseResponse> getFacilities(@Header("X-Language-View-ID") String language,
                                     @Header("serviceToken") String service_token,
                                     @Header("lat") String lat,
                                     @Header("lng") String lng,
                                     @Path("id") String id,
                                     @Query("order") String order);
}
