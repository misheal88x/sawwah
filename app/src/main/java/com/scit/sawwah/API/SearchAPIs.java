package com.scit.sawwah.API;

import com.scit.sawwah.Models.BaseResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Misheal on 9/11/2019.
 */

public interface SearchAPIs {
    @FormUrlEncoded
    @POST("search")
    Call<BaseResponse> searchByWord(@Header("X-Language-View-ID") String language,
                                    @Header("serviceToken") String serviceToken,
                                    @Field("words") String word,
                                    @Query("page") int page);
    @FormUrlEncoded
    @POST("search")
    Call<BaseResponse> search(@Header("X-Language-View-ID") String language,
                              @Header("serviceToken") String serviceToken,
                              @Header("lat")String lat,
                              @Header("lng") String lng,
                              @Field("city_id") String city_id,
                              @Field("type") String type,
                              @Field("country_id") String country_id,
                              @Field("category_id") String category_id,
                              @Field("rating") String rating,
                              @Field("radius") int radius,
                              @Field("words") String word,
                              @Query("page") int page);
}
