package com.scit.sawwah.API;

import com.scit.sawwah.Models.BaseResponse;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Misheal on 9/2/2019.
 */

public interface AdvertisementsAPIs {
    @GET("posters")
    Call<BaseResponse> getAllAds(@Header("X-Language-View-ID") String language,
                                 @Header("lat") float lat,
                                 @Header("lng") float lng,
                                 @Header("serviceToken") String service_token,
                                 @Query("filter") String filter,
                                 @Query("page") int page);
    @GET("posters/{id}")
    Call<BaseResponse> details(@Header("X-Language-View-ID") String language,
                               @Header("lat") float lat,
                               @Header("lng") float lng,
                               @Header("serviceToken") String service_token,
                               @Path("id") String id);
    @FormUrlEncoded
    @POST("posters")
    Call<BaseResponse> addAdvertisement(@Header("X-Language-View-ID") String language,
                                @Header("serviceToken") String service_token,
                                @Field("title") String title,
                                @Field("details") String details,
                                @Field("package_id") String  package_id,
                                @Field("facility_id") String facility_id,
                                @Field("start_date") String start_date,
                                @Field("end_date") String end_date);
    @POST("posters/{id}/remove")
    Call<BaseResponse> deleteAds(@Header("X-Language-View-ID") String language,
                                 @Header("serviceToken") String service_token,
                                 @Path("id") String ad_id);
    @FormUrlEncoded
    @POST("posters/{id}/update")
    Call<BaseResponse> updateAdvertisement(@Header("X-Language-View-ID") String language,
                                        @Header("serviceToken") String service_token,
                                        @Path("id") String id,
                                        @Field("title") String title,
                                        @Field("details") String details,
                                        @Field("package_id") String  package_id,
                                        @Field("facility_id") String facility_id,
                                        @Field("start_date") String start_date,
                                        @Field("end_date") String end_date);
}
