package com.scit.sawwah.API;

import com.scit.sawwah.Models.BaseResponse;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by Misheal on 9/12/2019.
 */

public interface ComplaintsAPIs {
    @Multipart
    @POST("complaints")
    Call<BaseResponse> add(@Header("X-Language-View-ID") String language,
                           @Header("serviceToken") String service_token,
                           @Part MultipartBody.Part image_icon,
                           @Part("name") RequestBody name,
                           @Part("username") RequestBody username,
                           @Part("content") RequestBody content,
                           @Part("type") RequestBody type,
                           @Part("title") RequestBody title);
}
