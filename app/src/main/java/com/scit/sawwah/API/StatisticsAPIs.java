package com.scit.sawwah.API;

import com.scit.sawwah.Models.BaseResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

/**
 * Created by Misheal on 9/22/2019.
 */

public interface StatisticsAPIs {
    @GET("statistics")
    Call<BaseResponse> getSta(@Header("X-Language-View-ID") String language,
                              @Header("serviceToken") String serviceToken);
}
