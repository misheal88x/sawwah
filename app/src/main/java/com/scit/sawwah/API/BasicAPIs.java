package com.scit.sawwah.API;

import com.scit.sawwah.Models.BaseResponse;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by Misheal on 8/30/2019.
 */

public interface BasicAPIs {
    @POST("startup")
    Call<BaseResponse> startup(@Header("X-Language-View-ID") String language,
                               @Header("DEVICE-ID") String device_id,
                               @Header("PLATFORM") String platform,
                               @Header("serviceToken") String service_token);
    @Multipart
    @POST("users/register")
    Call<BaseResponse> register(@Header("X-Language-View-ID") String language,
                                @Header("serviceToken") String service_token,
                                @Part("name") RequestBody name,
                                @Part("username") RequestBody username,
                                @Part("password") RequestBody password,
                                @Part("type") RequestBody type,
                                @Part MultipartBody.Part image);
    @FormUrlEncoded
    @POST("users/login")
    Call<BaseResponse> login(@Header("X-Language-View-ID") String language,
                             @Header("serviceToken") String service_token,
                             @Header("DEVICE-ID") String device_id,
                             @Field("username") String username,
                             @Field("password") String password);
    @FormUrlEncoded
    @POST("users/active")
    Call<BaseResponse> activeAccount(@Header("X-Language-View-ID") String language,
                                     @Header("serviceToken") String service_token,
                                     @Field("code") String code);
    @POST("users/resend-code")
    Call<BaseResponse> resendActiveCode(@Header("X-Language-View-ID") String language,
                                        @Header("serviceToken") String service_token);
    @POST("users/logout")
    Call<BaseResponse> logout(@Header("X-Language-View-ID") String language,
                              @Header("serviceToken") String service_token);
    @FormUrlEncoded
    @POST("users/register/facebook")
    Call<BaseResponse> facebookRegister(@Header("X-Language-View-ID") String language,
                                      @Header("serviceToken") String service_token,
                                        @Header("DEVICE-ID") String device_id,
                                      @Field("account_type") String account_type,
                                      @Field("image") String image,
                                      @Field("access_token") String access_token);
    @FormUrlEncoded
    @POST("users/register/google")
    Call<BaseResponse> googleRegister(@Header("X-Language-View-ID") String language,
                                        @Header("serviceToken") String service_token,
                                      @Header("DEVICE-ID") String device_id,
                                        @Field("account_type") String account_type,
                                        @Field("image") String image,
                                        @Field("access_token") String access_token);
    @FormUrlEncoded
    @POST("users/register/check/facebook")
    Call<BaseResponse> facebookCheck(@Header("X-Language-View-ID") String language,
                                     @Header("serviceToken") String service_token,
                                     @Field("access_token") String access_token);
    @FormUrlEncoded
    @POST("users/register/check/google")
    Call<BaseResponse> googleCheck(@Header("X-Language-View-ID") String language,
                                     @Header("serviceToken") String service_token,
                                     @Field("access_token") String access_token);
    @GET("static_pages/{type}/view")
    Call<BaseResponse> policy(@Header("X-Language-View-ID") String language,
                              @Header("serviceToken") String service_token,
                              @Path("type") String type);
}
