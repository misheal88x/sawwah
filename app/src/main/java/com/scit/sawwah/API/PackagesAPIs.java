package com.scit.sawwah.API;

import com.scit.sawwah.Models.BaseResponse;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

/**
 * Created by Misheal on 9/15/2019.
 */

public interface PackagesAPIs {
    @GET("packages")
    Call<BaseResponse> getAllPackages(@Header("X-Language-View-ID") String language,
                                      @Header("serviceToken") String service_token,
                                      @Query("filter") String filter);
    @Multipart
    @POST("packages/request")
    Call<BaseResponse> request(@Header("X-Language-View-ID") String language,
                               @Header("serviceToken") String service_token,
                               @Part("package_id") RequestBody package_id,
                               @Part("payment_type_id") RequestBody payment_type_id,
                               @Part("contact_info") RequestBody contact_info,
                               @Part MultipartBody.Part image);
    @FormUrlEncoded
    @POST("packages/request/renew")
    Call<BaseResponse> renew(@Header("X-Language-View-ID") String language,
                             @Header("serviceToken") String service_token,
                             @Field("package_id") String package_id);
}
