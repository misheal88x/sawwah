package com.scit.sawwah.API;

import com.scit.sawwah.Models.BaseResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Misheal on 9/9/2019.
 */

public interface CardAPIs {
    @FormUrlEncoded
    @POST("cards/beneficiaries")
    Call<BaseResponse> add_benefit(@Header("X-Language-View-ID") String language,
                                   @Header("serviceToken") String serviec_token,
                                   @Field("barcode") String barcode,
                                   @Field("facility_id") String facility_id);
    @GET("cards/beneficiaries/{facility_id}")
    Call<BaseResponse> get_benifits(@Header("X-Language-View-ID") String language,
                                    @Header("serviceToken") String serviec_token,
                                    @Path("facility_id")String facility_id,
                                    @Query("page") int page);
    @GET("cards/my/info")
    Call<BaseResponse> getCardInfo(@Header("X-Language-View-ID") String language,
                                   @Header("serviceToken") String service_token);
}
