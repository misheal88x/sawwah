package com.scit.sawwah.API;

import com.scit.sawwah.Models.BaseResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Misheal on 9/14/2019.
 */

public interface AccountsAPIs  {
    @FormUrlEncoded
    @POST("memorial_images/reactions")
    Call<BaseResponse> reactOnImage(@Header("X-Language-View-ID") String language,
                                    @Header("serviceToken")String service_token,
                                    @Field("reaction_type_id") int reaction_id,
                                    @Field("memorial_image_id") int memorial_image_id);
    @GET("notifications")
    Call<BaseResponse> getNotifications(@Header("X-Language-View-ID") String language,
                                        @Header("serviceToken")String service_token,
                                        @Query("page") int page);
    @FormUrlEncoded
    @POST("account/password/update")
    Call<BaseResponse> editPassword(@Header("X-Language-View-ID") String language,
                                    @Header("serviceToken")String service_token,
                                    @Field("password") String password,
                                    @Field("old_password") String old_password);
    @FormUrlEncoded
    @POST("account/password/reset/request")
    Call<BaseResponse> resetPasswordRequest(@Header("X-Language-View-ID") String language,
                                            @Header("serviceToken")String service_token,
                                            @Field("username") String username);

    @FormUrlEncoded
    @POST("account/password/reset")
    Call<BaseResponse> resetPassword(@Header("X-Language-View-ID") String language,
                                     @Header("serviceToken")String service_token,
                                     @Header("DEVICE-ID") String device_id,
                                     @Field("code") String code,
                                     @Field("password") String password);
    @FormUrlEncoded
    @POST("device/settings/update")
    Call<BaseResponse> updateDeviceSettings(@Header("X-Language-View-ID") String language,
                                            @Header("serviceToken")String service_token,
                                            @Field("language_id") String language_id,
                                            @Field("allow_global_notifications") String allow_global_notifications,
                                            @Field("city_id") String city_id);
    @FormUrlEncoded
    @POST("account/fcm/update")
    Call<BaseResponse> updateFirebaseToken(@Header("X-Language-View-ID") String language,
                                           @Header("serviceToken")String service_token,
                                           @Field("token") String token);

    @GET("profile")
    Call<BaseResponse> profile(@Header("X-Language-View-ID") String language,
                               @Header("serviceToken")String service_token);
}
