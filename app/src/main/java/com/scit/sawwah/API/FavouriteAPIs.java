package com.scit.sawwah.API;

import com.scit.sawwah.Models.BaseResponse;

import org.androidannotations.annotations.rest.Head;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by Misheal on 9/5/2019.
 */

public interface FavouriteAPIs {
    @GET("favorite")
    Call<BaseResponse> getFavourites(@Header("X-Language-View-ID") String language,
                                     @Header("serviceToken") String service_token,
                                     @Header("lat") String lat,
                                     @Header("lng") String lng);
    @POST("favorite/{id}/{type}/state")
    Call<BaseResponse> toggle_item(@Header("X-Language-View-ID") String language,
                                   @Header("serviceToken") String service_token,
                                   @Path("id") String id,
                                   @Path("type") String type);
}
