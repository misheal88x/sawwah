package com.scit.sawwah.API;

import com.scit.sawwah.Models.WeatherResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Misheal on 9/9/2019.
 */

public interface WeatherAPI {

    @GET("https://api.openweathermap.org/data/2.5/forecast")
    Call<WeatherResponse> getWeather(@Query("lat") String lat,
                                     @Query("lon") String lon,
                                     @Query("appid") String app_id,
                                     @Query("units") String units);
}
