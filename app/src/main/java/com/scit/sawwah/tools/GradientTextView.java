package com.scit.sawwah.tools;

import android.content.Context;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.util.AttributeSet;

import com.scit.sawwah.R;

public class GradientTextView extends androidx.appcompat.widget.AppCompatTextView {
    public GradientTextView(Context context) {
        super(context, null, -1);
    }

    public GradientTextView(Context context,
                            AttributeSet attrs) {
        super(context, attrs, -1);
    }

    public GradientTextView(Context context,
                            AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onLayout(boolean changed,
                            int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        if (changed)
            getPaint().setShader(getLinearGradient(getWidth()));
        /*if(changed)
        {
            getPaint().setShader( new LinearGradient(
                    0, 0, 0, getHeight(),
                    getContext().getResources().getColor(R.color.btnColor), getContext().getResources().getColor(R.color.colorPrimaryDark),
                    Shader.TileMode.CLAMP ) );
        }*/
    }

    private LinearGradient getLinearGradient(int height) {
        return new LinearGradient(0, 0, height, 0, getContext().getResources().getColor(R.color.colorPrimaryDark), getContext().getResources().getColor(R.color.btnColor), Shader.TileMode.CLAMP);
    }
}