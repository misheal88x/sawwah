package com.scit.sawwah.tools;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import androidx.core.app.NotificationCompat;

import android.util.Log;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.glide.slider.library.svg.GlideApp;
import com.scit.sawwah.Models.BaseResponse;
import com.scit.sawwah.R;
import com.scit.sawwah.interfaces.IResponse;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import retrofit2.Response;

/**
 * Created by Misheal on 8/28/2019.
 */

public class BaseFunctions {
    public static String dateExtractor(String datetime){
        String myDate = "";
        for (int i = 0; i <datetime.length() ; i++) {
            if (datetime.charAt(i)!=' '){
                myDate+=datetime.charAt(i);
            }else {
                break;
            }
        }
        return myDate;
    }

    public static String timeExtractor(String datetime){
        String myTime = "";
        boolean isTime = false;
        for (int i = 0; i <datetime.length() ; i++) {
            if (datetime.charAt(i)!=' '&&isTime){
                myTime+=datetime.charAt(i);
            }else if (datetime.charAt(i)==' '){
                isTime = true;
            }
        }
        return myTime;
    }

    public static String formatSeconds(long totalSecs) {
        long hours = (int) ((totalSecs) / 3600);
        long minutes = (int) ((totalSecs % 3600) / 60);
        long seconds = (int) (totalSecs % 60);
        return String.format("%02d:%02d:%02d", hours, minutes, seconds);
    }

    public static String languageToNumberConverter(String in){
        String out = "";
        if (in.equals("en")){
            out = "1";
        }else {
            out = "2";
        }
        return out;
    }

    public static String numberToLanguageConverter(String in){
        String out = "";
        if (in.equals("1")){
            out = "en";
        }else {
            out = "ar";
        }
        return out;
    }

    public static void setGlideImage(Context context,ImageView image, String url){
        try{
            GlideApp.with(context).load(url)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .centerCrop().into(image);
        }catch (Exception e){}
    }
    public static void setGlideDrawableImage(Context context, ImageView image, int drawable){
        try{
            GlideApp.with(context).load(drawable)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .centerCrop().into(image);
        }catch (Exception e){}
    }

    public static void processResponse(Response<BaseResponse> response, IResponse onResponse, Context context){
        if (response.body()!=null){
            if (response.body().getData()!=null){
                if (response.body().getStatus_code()==200){
                    onResponse.onResponse(response.body().getData());
                }
                else {
                    try {
                        Toast.makeText(context, String.valueOf(response.body().getData()), Toast.LENGTH_SHORT).show();
                    }catch (Exception e){
                        Toast.makeText(context, context.getResources().getString(R.string.error_exception), Toast.LENGTH_SHORT).show();
                    }
                }
            }else {
                Toast.makeText(context, context.getResources().getString(R.string.error_data), Toast.LENGTH_SHORT).show();
            }
        }else {
            if (response.code() == 405){
                Toast.makeText(context, context.getResources().getString(R.string.account_duplicated), Toast.LENGTH_SHORT).show();
            }else {
                Toast.makeText(context, context.getResources().getString(R.string.error_occurred), Toast.LENGTH_SHORT).show();
            }

        }
    }

    public static Date stringToDateConverter(String stringDate){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date d = sdf.parse(stringDate);
            return d;
        } catch (ParseException ex) {
            Log.v("Exception", ex.getLocalizedMessage());
            return null;
        }
    }
    public static String dateToDayConverter(Date date){
        SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
        String dayOfTheWeek = sdf.format(date);
        return dayOfTheWeek;
    }

    public static String todayDateString(){
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = df.format(c);
        return formattedDate;
    }

    public static String subString(String input,int start,int end){
        return input.substring(start,end);
    }

    public static String booleanToStringNumber(boolean in){
        String out = "";
        if (in){
            out = "1";
        }else {
            out = "0";
        }
        return out;
    }
    public static boolean stringNumberToBoolean(String in){
        boolean out = false;
        if (in.equals("1")){
            out = true;
        }else {
            out = false;
        }
        return out;
    }

    public static int getSpinnerPosition(AdapterView<?> parent,int i){
        long pos = parent.getItemIdAtPosition(i);
        int position = Integer.valueOf(String.valueOf(pos));
        return position;
    }

    public static void showNotification(Context context,
                                  PendingIntent pendingIntent,
                                  String message,
                                  NotificationCompat.Builder builder,
                                  NotificationManager notificationManager,
                                  String NOTIFICATION_CHANNEL_ID,
                                  int MY_NOTIFICATION_ID){
        if (pendingIntent!=null){
            builder = new NotificationCompat.Builder(context)
                    .setContentTitle(context.getResources().getString(R.string.app_name))
                    .setContentText(message)
                    .setStyle(new NotificationCompat.BigTextStyle()
                            .bigText(message))
                    .setTicker("new notification")
                    .setWhen(System.currentTimeMillis())
                    .setContentIntent(pendingIntent)
                    .setDefaults(Notification.DEFAULT_SOUND)
                    .setAutoCancel(true)
                    .setSmallIcon(R.drawable.logo)
                    .setVibrate(new long[]{500, 500, 500});
        }else {
            builder = new NotificationCompat.Builder(context)
                    .setContentTitle(context.getResources().getString(R.string.app_name))
                    .setContentText(message)
                    .setStyle(new NotificationCompat.BigTextStyle()
                            .bigText(message))
                    .setTicker("new notification")
                    .setWhen(System.currentTimeMillis())
                    .setDefaults(Notification.DEFAULT_SOUND)
                    .setAutoCancel(true)
                    .setSmallIcon(R.drawable.logo)
                    .setVibrate(new long[]{500, 500, 500});
        }
        notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        //notificationManager.notify(MY_NOTIFICATION_ID, myNotification);
        //Toast.makeText(getApplicationContext(), "", Toast.LENGTH_SHORT).show();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "NOTIFICATION_CHANNAEL", importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.BLUE);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{500, 500, 500});
            assert notificationManager != null;
            builder.setChannelId(NOTIFICATION_CHANNEL_ID);
            notificationManager.createNotificationChannel(notificationChannel);
        }
        assert notificationManager != null;
        notificationManager.notify(MY_NOTIFICATION_ID, builder.build());
    }

    public static void openBrowser(Context context,String url){
        Intent i = new Intent(Intent.ACTION_VIEW,
                Uri.parse(url));
        context.startActivity(i);
    }
    public static void openDialer(Context context,String phone_number){
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:"+phone_number));
        context.startActivity(intent);
    }
    public static void openEmail(Context context,String email){
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:")); // only email apps should handle this
        String[] addresses = new String[]{email};
        intent.putExtra(Intent.EXTRA_EMAIL, addresses);
        intent.putExtra(Intent.EXTRA_SUBJECT, "Sawwah");
        if (intent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivity(intent);
        }
    }

    public static void openFacebook(Context context,String url){
        context.startActivity(newFacebookIntent(context.getPackageManager(),url));
    }

    public static Intent newFacebookIntent(PackageManager pm, String url) {
        Uri uri = Uri.parse(url);
        try {
            ApplicationInfo applicationInfo = pm.getApplicationInfo("com.facebook.katana", 0);
            if (applicationInfo.enabled) {
                // http://stackoverflow.com/a/24547437/1048340
                uri = Uri.parse("fb://facewebmodal/f?href=" + url);
            }
        } catch (PackageManager.NameNotFoundException ignored) {
        }
        return new Intent(Intent.ACTION_VIEW, uri);
    }

    public static void openTwitter(Context context,String url){
        Intent intent = null;
        try {
            context.getPackageManager().getPackageInfo("com.twitter.android", 0);
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        } catch (PackageManager.NameNotFoundException e) {
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            context.startActivity(intent);
        }
    }
    public static void sendMessageBySMS(Context context,String message,String errorMessage){
        try {
            Intent sendIntent = new Intent(Intent.ACTION_VIEW);
            sendIntent.setData(Uri.parse("sms:"));
            sendIntent.putExtra("sms_body", message);
            context.startActivity(sendIntent);
        }catch (Exception e){
            Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
            return;
        }
    }

    public static void sendMessageByMessenger(Context context,String message,String errorMessage){
        try{
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, message);
            sendIntent.setType("text/plain");
            sendIntent.setPackage("com.facebook.orca");
            context.startActivity(sendIntent);
        }catch (Exception e){
            Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
            return;
        }
    }

    public static void sendMessageByWhatsapp(Context context,String message,String errorMessage){
        try{
            PackageManager pm = context.getPackageManager();
            Intent waIntent = new Intent(Intent.ACTION_SEND);
            waIntent.setType("text/plain");
            String text = message;
            PackageInfo info=pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA);
            waIntent.setPackage("com.whatsapp");
            waIntent.putExtra(Intent.EXTRA_TEXT, text);
            context.startActivity(Intent.createChooser(waIntent, "Share with"));

        }catch (Exception e){
            Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
            return;
        }
    }
}
