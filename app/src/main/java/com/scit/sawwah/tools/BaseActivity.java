package com.scit.sawwah.tools;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;

import android.view.ViewGroup;

import com.scit.sawwah.R;

import java.lang.reflect.Field;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public abstract class BaseActivity extends AppCompatActivity {


    protected ViewGroup fragment_place;
    public BaseFragment current_fragment;
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(LocaleHelper.onAttach(newBase)));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        //To make sure the app is taking the full screen
        /*
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            w.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        }
**/
        super.onCreate(savedInstanceState);
         //Change the activity font
        /*
        try {
            final Typeface regular = Typeface.createFromAsset(getAssets(), "fonts/STCRegular.ttf");
            replaceFont("MONOSPACE", regular);
        } catch (Exception e){
            e.printStackTrace();
        }
**/
        sharedPreferences = getSharedPreferences("Main",Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        LocaleHelper.setLocale(this, sharedPreferences.getString("language","ar"));
        set_layout();
        init_views();
        set_fragment_place();
        init_events();
        init_activity(savedInstanceState);
        //Thread.setDefaultUncaughtExceptionHandler(new DefaultExceptionHandler(this));
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

    }

    public abstract void set_layout();

    public abstract void init_activity(Bundle savedInstanceState);

    public abstract void init_views();

    public abstract void init_events();

    public abstract void set_fragment_place();


   /* public void send_data(DataMessage message){
        message.get_receiver().onReceive(message.get_extra());
    }

    public void send_data(ObjectMessage message){
        message.get_receiver().onReceive(message.get_object());
    }*/

         //To open specific fragment
    public void open_fragment(final BaseFragment fragment) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (current_fragment != fragment) {
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction transaction = fragmentManager.beginTransaction();
                    transaction.setCustomAnimations(R.anim.slide_up, R.anim.slide_down, R.anim.slide_up, R.anim.slide_down);
                    transaction.replace(fragment_place.getId(), fragment);
                    transaction.commit();
                    fragmentManager.executePendingTransactions();
                    current_fragment = fragment;
                }
            }
        };
        runOnUiThread(runnable);
    }

    protected static void replaceFont(String staticTypefaceFieldName,
                                      final Typeface newTypeface) {
        try {
            final Field staticField = Typeface.class
                    .getDeclaredField(staticTypefaceFieldName);
            staticField.setAccessible(true);
            staticField.set(null, newTypeface);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onBackPressed() {
            if (current_fragment != null && current_fragment.get_previous_fragment() != null)
                open_fragment(current_fragment.get_previous_fragment());
            else{
                super.onBackPressed();
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            }


    }
/*

    public Drawable buildBadgeCounterDrawable(int count, int backgroundImageId) {
        LayoutInflater inflater = LayoutInflater.from(this);
        View view = inflater.inflate(R.layout.badge_layout, null);
        view.setBackgroundResource(backgroundImageId);

        if (count == 0) {
            View counterTextPanel = view.findViewById(R.id.counterValuePanel);
            counterTextPanel.setVisibility(View.GONE);
        } else {
            TextView textView = (TextView) view.findViewById(R.id.count);
            textView.setText("" + count);
        }

        view.measure(
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());

        view.setDrawingCacheEnabled(true);
        view.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        Bitmap bitmap = Bitmap.createBitmap(view.getDrawingCache());
        view.setDrawingCacheEnabled(false);

        return new BitmapDrawable(getResources(), bitmap);
    }
*/
}
