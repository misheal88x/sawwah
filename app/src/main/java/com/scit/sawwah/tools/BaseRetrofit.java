package com.scit.sawwah.tools;

import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.scit.sawwah.interfaces.IFailure;
import com.scit.sawwah.interfaces.IResponse;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Misheal on 8/30/2019.
 */

public class BaseRetrofit  {
    public static final String BASE_URL = "https://sawwah-sy.com/____apis/public/api/v1/";
    public static final String BASE_ALI = "https://sawwah-sy.com/";
    //public static final String GOOGLE_BASE_URL = "https://www.googleapis.com/oauth2/v4/";
    public static Retrofit retrofit;
    public static IResponse onResponse;
    public static IFailure onFailure;
    public static Retrofit configureRetrofitWithoutBearer(){
        OkHttpClient client = new OkHttpClient.Builder().connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(25, TimeUnit.SECONDS)
                .writeTimeout(25, TimeUnit.SECONDS).build();
        retrofit = new Retrofit.Builder().client(client).baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        return retrofit;
    }

    public static Retrofit configureRetrofitWithBearer(SharedPreferences sharedPreferences){
        final String access_token = sharedPreferences.getString("access_token","");
        OkHttpClient client = new OkHttpClient.Builder().connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(25, TimeUnit.SECONDS)
                .writeTimeout(25, TimeUnit.SECONDS).addInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(Chain chain) throws IOException {
                        Request newRequest  = chain.request().newBuilder()
                                .addHeader("Authorization", "Bearer " + access_token)
                                .build();
                        return chain.proceed(newRequest);
                    }
                }).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        retrofit = new Retrofit.Builder().client(client).baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create(gson)).build();
        return retrofit;
    }
}
