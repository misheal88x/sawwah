package com.scit.sawwah.tools;

import android.app.Activity;
import android.net.Uri;
import androidx.fragment.app.Fragment;
import android.os.Bundle;
import androidx.annotation.Nullable;

public abstract class BaseFragment extends Fragment {

    protected BaseActivity base;
    protected BaseFragment previous;


    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
        void onOpenFragment(String tag, String title);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        base = (BaseActivity) activity;
    }



    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init(savedInstanceState);
    }

    public void init(Bundle savedInstanceState){
        init_views();
        init_events();
        init_fragment(savedInstanceState);
    }

    public void init_views(){
    }

    public abstract void init_events();

    public abstract void init_fragment(Bundle savedInstanceState);

    public void set_previous(BaseFragment fragment){
        previous = fragment;
    }


    public BaseFragment get_previous_fragment() {
        return previous;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

}
