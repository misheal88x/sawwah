package com.scit.sawwah.tools;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.scit.sawwah.App;
import com.scit.sawwah.activities.SplashActivity;

/**
 * Created by Misheal on 9/28/2019.
 */

public class DefaultExceptionHandler implements Thread.UncaughtExceptionHandler  {
    private Activity activity;

    public DefaultExceptionHandler(Activity activity) {
        this.activity = activity;
    }

    @Override
    public void uncaughtException(Thread thread, final Throwable ex) {
        Intent intent = new Intent(activity, SplashActivity.class);
        intent.putExtra("crash",true);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TASK
                | Intent.FLAG_ACTIVITY_NEW_TASK);
        Toast.makeText(activity, ex.getMessage(), Toast.LENGTH_SHORT).show();
        PendingIntent pendingIntent = PendingIntent.getActivity(App.getInstance().getBaseContext(),0,intent,PendingIntent.FLAG_ONE_SHOT);
        AlarmManager alarmManager = (AlarmManager) App.getInstance().getBaseContext().getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC,System.currentTimeMillis()+1000,pendingIntent);
        activity.finish();
        System.exit(2);
    }
}
