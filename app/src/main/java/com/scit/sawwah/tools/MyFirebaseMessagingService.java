package com.scit.sawwah.tools;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.scit.sawwah.APIClasses.AccountsAPIsClass;
import com.scit.sawwah.activities.AccountOwnerSettingsActivity;
import com.scit.sawwah.activities.AdvertisementDetailsActivity;
import com.scit.sawwah.activities.NotificationsActivity;
import com.scit.sawwah.activities.OfferDetailsActivity;
import com.scit.sawwah.activities.ProfileActivity;
import com.scit.sawwah.interfaces.IFailure;
import com.scit.sawwah.interfaces.IResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

/**
 * Created by Misheal on 9/17/2019.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    final int MY_NOTIFICATION_ID = 1;
    final String NOTIFICATION_CHANNEL_ID = "10001";
    NotificationManager notificationManager;
    //Notification myNotification;
    NotificationCompat.Builder builder;
    final Context context = this;
    public SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);
        try {
            sharedPreferences = getSharedPreferences("Main", Context.MODE_PRIVATE);
            editor = sharedPreferences.edit();
            String recent_token = FirebaseInstanceId.getInstance().getToken();
            editor.putString("fcm_token", recent_token);
            editor.commit();
            Log.i("refreshed_token", "the token: " + recent_token);
            if (!sharedPreferences.getBoolean("is_visitor",false)){
                callUpdateTokenAPI(recent_token);
            }
        }catch (Exception e){}
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        try {
            Map<String, String> params = remoteMessage.getData();
            JSONObject jsonObject = new JSONObject(params);
            Log.i("JSON_OBJECT", "the : " + jsonObject.toString());
            sendNotification(jsonObject);
        }catch (Exception e){}
    }
    private void sendNotification(JSONObject jsonObject) {
        String content = "";
        String target_type = "";
        String target_id = "";
        sharedPreferences = getSharedPreferences("Main", Context.MODE_PRIVATE);
        try {
            content = jsonObject.getString("payload");
            target_type = jsonObject.getString("target");
            target_id = jsonObject.getString("target_id");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        switch (target_type){
            //General
            case "0" : {
                Intent intent = new Intent(context, NotificationsActivity.class);
                PendingIntent pendingIntent = PendingIntent.getActivity(
                        context,
                        1,
                        intent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                                | PendingIntent.FLAG_ONE_SHOT);
                BaseFunctions.showNotification(context,pendingIntent,content,builder,notificationManager,NOTIFICATION_CHANNEL_ID,MY_NOTIFICATION_ID);
            }break;
            //Facility
            case "1" : {
                Intent intent = new Intent(context, ProfileActivity.class);
                intent.putExtra("institution_id",Integer.valueOf(target_id));
                PendingIntent pendingIntent = PendingIntent.getActivity(
                        context,
                        1,
                        intent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                                | PendingIntent.FLAG_ONE_SHOT);
                BaseFunctions.showNotification(context,pendingIntent,content,builder,notificationManager,NOTIFICATION_CHANNEL_ID,MY_NOTIFICATION_ID);
            }break;
            //Offer
            case "2" : {
                Intent intent = null;
                if(sharedPreferences.getBoolean("is_visitor",false)){
                    intent = new Intent(context, NotificationsActivity.class);
                }else{
                    intent = new Intent(context, OfferDetailsActivity.class);
                    intent.putExtra("offer_id",Integer.valueOf(target_id));
                }
                PendingIntent pendingIntent = PendingIntent.getActivity(
                        context,
                        1,
                        intent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                                | PendingIntent.FLAG_ONE_SHOT);
                BaseFunctions.showNotification(context,pendingIntent,content,builder,notificationManager,NOTIFICATION_CHANNEL_ID,MY_NOTIFICATION_ID);
            }break;
            //Poster
            case "3" : {
                Intent intent = null;
                if (sharedPreferences.getBoolean("is_visitor",false)){
                    intent = new Intent(context, NotificationsActivity.class);
                }else {
                    intent = new Intent(context, AdvertisementDetailsActivity.class);
                    intent.putExtra("advertisement_id",Integer.valueOf(target_id));
                }
                PendingIntent pendingIntent = PendingIntent.getActivity(
                        context,
                        1,
                        intent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                                | PendingIntent.FLAG_ONE_SHOT);
                BaseFunctions.showNotification(context,pendingIntent,content,builder,notificationManager,NOTIFICATION_CHANNEL_ID,MY_NOTIFICATION_ID);
            }break;
            //Active Price Package
            case "4" : {
                Intent intent = null;
                if (sharedPreferences.getBoolean("is_visitor",false)){
                    intent = new Intent(context, NotificationsActivity.class);
                }else {
                    if (sharedPreferences.getString("account_type","customer").equals("owner")){
                        intent = new Intent(context, AccountOwnerSettingsActivity.class);
                    }else {
                        intent = new Intent(context, NotificationsActivity.class);
                    }
                }
                PendingIntent pendingIntent = PendingIntent.getActivity(
                        context,
                        1,
                        intent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                                | PendingIntent.FLAG_ONE_SHOT);
                BaseFunctions.showNotification(context,pendingIntent,content,builder,notificationManager,NOTIFICATION_CHANNEL_ID,MY_NOTIFICATION_ID);
            }break;
        }
    }

    private void callUpdateTokenAPI(String token){
        AccountsAPIsClass.updateFirebaseToken(sharedPreferences, getApplication(),
                sharedPreferences.getString("language", ""),
                sharedPreferences.getString("service_token", ""),
                token, new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json != null){
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                    }
                });
    }
}
