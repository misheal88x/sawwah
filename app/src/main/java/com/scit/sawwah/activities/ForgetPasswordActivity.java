package com.scit.sawwah.activities;

import android.content.Intent;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.scit.sawwah.APIClasses.AccountsAPIsClass;
import com.scit.sawwah.R;
import com.scit.sawwah.interfaces.IFailure;
import com.scit.sawwah.interfaces.IResponse;
import com.scit.sawwah.tools.BaseActivity;
import com.scit.sawwah.tools.BaseFunctions;

public class ForgetPasswordActivity extends BaseActivity {
    private RelativeLayout toolbar,btn_confirm;
    private ImageButton btn_back;
    private EditText edt_phone;
    private LinearLayout root;

    @Override
    public void set_layout() {
        setContentView(R.layout.activity_forget_password);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {

    }

    @Override
    public void init_views() {
        toolbar = findViewById(R.id.forget_password_toolbar);
        btn_back = toolbar.findViewById(R.id.toolbar_back_btn);
        btn_confirm = findViewById(R.id.forget_password_confirm_btn);
        edt_phone = findViewById(R.id.forget_password_email_edt);
        root = findViewById(R.id.forget_password_layout);
    }

    @Override
    public void init_events() {
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btn_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt_phone.getText().toString().equals("")){
                    Toast.makeText(ForgetPasswordActivity.this, getResources().getString(R.string.forget_password_no_phone), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (edt_phone.getText().toString().length() < 10){
                    Toast.makeText(ForgetPasswordActivity.this, getResources().getString(R.string.register_error_mobile), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!BaseFunctions.subString(edt_phone.getText().toString(),0,2).equals("09")){
                    Toast.makeText(ForgetPasswordActivity.this, getResources().getString(R.string.register_error_mobile2), Toast.LENGTH_SHORT).show();
                    return;
                }
                callRequestCodeAPI();
            }
        });
    }

    @Override
    public void set_fragment_place() {

    }

    private void callRequestCodeAPI(){
        AccountsAPIsClass.resetPasswordRequest(sharedPreferences,
                ForgetPasswordActivity.this,
                sharedPreferences.getString("language", "ar"),
                sharedPreferences.getString("service_token", ""),
                edt_phone.getText().toString(),
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json != null){
                            Toast.makeText(ForgetPasswordActivity.this, getResources().getString(R.string.reset_password_wait), Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(ForgetPasswordActivity.this,ResetPasswordActivity.class);
                            intent.putExtra("phone_number",edt_phone.getText().toString());
                            startActivity(intent);
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callRequestCodeAPI();
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });
    }
}
