package com.scit.sawwah.activities;

import android.content.Intent;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.scit.sawwah.Models.PricePackageObject;
import com.scit.sawwah.R;
import com.scit.sawwah.adapters.PricesPackagesManageAdapter;
import com.scit.sawwah.tools.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import me.relex.circleindicator.CircleIndicator;

public class ManagePricesPackagesActivity extends BaseActivity {
    private RelativeLayout toolbar;
    private ImageButton btn_back;
    private TextView tv_title;
    private ViewPager viewPager;
    private CircleIndicator indicator;
    private PricesPackagesManageAdapter adapter;
    private List<PricePackageObject> list_packages;
    private Intent myIntent;
    private LinearLayout empty_layout;

    @Override
    public void set_layout() {
        setContentView(R.layout.activity_manage_prices_packages);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        tv_title.setText(getResources().getString(R.string.prices_packages_title));
        init_view_pager();
    }

    @Override
    public void init_views() {
        toolbar = findViewById(R.id.manage_prices_packages_toolbar);
        btn_back = toolbar.findViewById(R.id.toolbar_back_btn);
        tv_title = toolbar.findViewById(R.id.toolbar_title);
        viewPager = findViewById(R.id.manage_prices_packages_view_pager);
        indicator = findViewById(R.id.manage_prices_packages_indicator);
        empty_layout = findViewById(R.id.empty_layout);
        myIntent = getIntent();
    }

    @Override
    public void init_events() {
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void set_fragment_place() {

    }

    private void init_view_pager(){
        list_packages = new ArrayList<>();
        PricePackageObject[] array_packages = new Gson().fromJson(myIntent.getStringExtra("packages"),PricePackageObject[].class);
        if (array_packages.length > 0){
            empty_layout.setVisibility(View.GONE);
            for (PricePackageObject ppo : array_packages){
                list_packages.add(ppo);
            }
        }else {
            empty_layout.setVisibility(View.VISIBLE);
        }
        adapter = new PricesPackagesManageAdapter(ManagePricesPackagesActivity.this,list_packages);
        viewPager.setAdapter(adapter);
        indicator.setViewPager(viewPager);
    }

}
