package com.scit.sawwah.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.Handler;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.glide.slider.library.svg.GlideApp;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.gson.Gson;
import com.scit.sawwah.APIClasses.BasicAPIsClass;
import com.scit.sawwah.MainActivity;
import com.scit.sawwah.Models.AccountSettingsObject;
import com.scit.sawwah.Models.StartupResponse;
import com.scit.sawwah.R;
import com.scit.sawwah.interfaces.IFailure;
import com.scit.sawwah.interfaces.IResponse;
import com.scit.sawwah.tools.BaseActivity;

public class SplashActivity extends BaseActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener{
    private ImageView img_back;
    private LinearLayout root;
    private static int SPLASH_TIME_OUT=5000;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 10;
    private CountDownTimer cTimer = null;
    //---------------------------------------------------------------------------
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    PendingResult<LocationSettingsResult> result;
    final static int REQUEST_LOCATION = 199;
    //---------------------------------------------------------------------------

    @Override
    public void set_layout() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        FullScreencall();
        setContentView(R.layout.activity_splash);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {

        try{
            GlideApp.with(SplashActivity.this).load(R.drawable.splash_back)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .centerCrop().into(img_back);
        }catch (Exception e){}
        if (isOnline()){
            new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
            if (androidx.core.app.ActivityCompat.checkSelfPermission(SplashActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED&&
                    androidx.core.app.ActivityCompat.checkSelfPermission(SplashActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION)!= PackageManager.PERMISSION_GRANTED&&
                    androidx.core.app.ActivityCompat.checkSelfPermission(SplashActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED&&
                    androidx.core.app.ActivityCompat.checkSelfPermission(SplashActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED){
                requestRuntimePermission();
            }else {
                if (checkPlayServices()){
                    //---------------------------------------------------------------------------
                    mGoogleApiClient = new GoogleApiClient.Builder(SplashActivity.this)
                            .addApi(LocationServices.API)
                            .addConnectionCallbacks(SplashActivity.this)
                            .addOnConnectionFailedListener(SplashActivity.this).build();
                    mGoogleApiClient.connect();

                    //-----------------------------------------------------------------------------
                }
            }
            }
            },0);
        }else {
            AlertDialog.Builder builder = new AlertDialog.Builder(SplashActivity.this);
            builder.setMessage(getResources().getString(R.string.no_internet_dialog)).
                    setCancelable(false).
                    setPositiveButton(getResources().getString(R.string.no_internet_ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Intent intent = new Intent(Intent.ACTION_MAIN);
                            intent.addCategory(Intent.CATEGORY_HOME);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            finish();
                        }
                    }).show();
        }
    }

    private void startTimer() {
        cTimer = new CountDownTimer(15000, 1000) {
            public void onTick(long millisUntilFinished) {
            }
            public void onFinish() {
                Snackbar.make(root, getResources().getString(R.string.wait_message), Snackbar.LENGTH_INDEFINITE)
                        .setAction(getResources().getString(R.string.try_again), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                callAPI();
                            }
                        }).setActionTextColor(getResources().getColor(R.color.white)).show();
            }
        };
        cTimer.start();
    }

    void cancelTimer() {
        if(cTimer!=null)
            cTimer.cancel();
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode!= ConnectionResult.SUCCESS){
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)){
                GooglePlayServicesUtil.getErrorDialog(resultCode,this,PLAY_SERVICES_RESOLUTION_REQUEST).show();

            }else {
                Toast.makeText(this, getResources().getString(R.string.no_google_play_services), Toast.LENGTH_SHORT).show();
                finish();
            }
            return false;
        }
        return true;
    }

    public void FullScreencall() {
        if(Build.VERSION.SDK_INT > 11 && Build.VERSION.SDK_INT < 19) { // lower api
            View v = this.getWindow().getDecorView();
            v.setSystemUiVisibility(View.GONE);
        } else if(Build.VERSION.SDK_INT >= 19) {
            //for new api versions.
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
            decorView.setSystemUiVisibility(uiOptions);
        }
    }
    private void requestRuntimePermission() {
        androidx.core.app.ActivityCompat.requestPermissions(this,new String[]
                {
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                },11);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch(requestCode){
            case 11:{
                if (grantResults.length>0 && grantResults[0]== PackageManager.PERMISSION_GRANTED
                        && grantResults[1]== PackageManager.PERMISSION_GRANTED
                        && grantResults[2]== PackageManager.PERMISSION_GRANTED
                        && grantResults[3]== PackageManager.PERMISSION_GRANTED){
                    if (checkPlayServices()){
                        //---------------------------------------------------------------------------
                        mGoogleApiClient = new GoogleApiClient.Builder(SplashActivity.this)
                                .addApi(LocationServices.API)
                                .addConnectionCallbacks(SplashActivity.this)
                                .addOnConnectionFailedListener(SplashActivity.this).build();
                        mGoogleApiClient.connect();

                        //-----------------------------------------------------------------------------
                    }
                    /*
                    final LocationManager manager = (LocationManager) getSystemService( Context.LOCATION_SERVICE );
                    if ( !manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
                        buildAlertMessageNoGps();
                    }else {
                        callAPI();
                    }
                    **/
                }else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(SplashActivity.this);
                    builder.setMessage(getResources().getString(R.string.permission_denied)).setCancelable(false).
                            setPositiveButton(getResources().getString(R.string.no_internet_ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Intent intent = new Intent(Intent.ACTION_MAIN);
                                    intent.addCategory(Intent.CATEGORY_HOME);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                    finish();
                                }
                            }).show();
                }
                break;
            }
        }
    }

    public boolean isOnline(){
        try{
            ConnectivityManager conMgr =  (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
            if (conMgr != null){
                NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
                if (netInfo == null){
                    return false;
                }else {
                    return true;
                }
            }else {
                return false;
            }
        }catch (Exception e){
            Toast.makeText(this, "Crashed because of isOnline function", Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(30 * 1000);
        mLocationRequest.setFastestInterval(5 * 1000);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);

        result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                //final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        //startActivity(new Intent(SplashActivity.this,LoginActivity.class));
                        //finish();
                        callAPI();
                        startTimer();
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(
                                    SplashActivity.this,
                                    REQUEST_LOCATION);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        //...
                        break;
                        default:{
                            callAPI();
                            startTimer();
                        }
                }
            }
        });
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        Log.d("onActivityResult()", Integer.toString(resultCode));

        //final LocationSettingsStates states = LocationSettingsStates.fromIntent(data);
        switch (requestCode)
        {
            case REQUEST_LOCATION:
                switch (resultCode)
                {
                    case Activity.RESULT_OK:
                    {
                        // All required changes were successfully made
                        Toast.makeText(SplashActivity.this, getResources().getString(R.string.gps_on), Toast.LENGTH_LONG).show();
                        //startActivity(new Intent(SplashActivity.this,LoginActivity.class));
                        //finish();
                        callAPI();
                        startTimer();
                        break;
                    }
                    case Activity.RESULT_CANCELED:
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder(SplashActivity.this);
                        builder.setMessage(getResources().getString(R.string.gps_off)).setCancelable(false).
                                setPositiveButton(getResources().getString(R.string.no_internet_ok), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        Intent intent = new Intent(Intent.ACTION_MAIN);
                                        intent.addCategory(Intent.CATEGORY_HOME);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                        finish();
                                    }
                                }).show();
                        break;
                    }
                    default:
                    {
                        break;
                    }
                }
                break;
        }
    }

    @Override
    public void init_views() {
        img_back = findViewById(R.id.splash_back);
        root = findViewById(R.id.splash_root);
    }

    @Override
    public void init_events() {
    }

    @Override
    public void set_fragment_place() {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        cancelTimer();
    }

    @Override
    protected void onPause() {
        super.onPause();
        cancelTimer();
    }

    private void callAPI(){
        //Toast.makeText(this, String.valueOf(10/3)+" "+String.valueOf(10%3), Toast.LENGTH_SHORT).show();
        String android_id = Settings.Secure.getString(SplashActivity.this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        Log.i("device_id", "callAPI: "+android_id);
        BasicAPIsClass.startup(sharedPreferences, SplashActivity.this, sharedPreferences.getString("language", "ar"),
                android_id, new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json != null){
                            String json1 = new Gson().toJson(json);
                            StartupResponse success = new Gson().fromJson(json1,StartupResponse.class);
                            //Service token
                            if (success.getService_token()!=null){
                                Log.i("service_token", "onResponse: "+success.getService_token());
                                editor.putString("service_token",success.getService_token());
                            }
                            //Offers
                            if (success.getOffers()!=null){
                                if (success.getOffers().size()>0){
                                    String offers_json = new Gson().toJson(success.getOffers());
                                    editor.putString("offers",offers_json);
                                }
                            }
                            //Advertisements
                            if (success.getPosters()!=null){
                                if (success.getPosters().size()>0){
                                    String ads_json = new Gson().toJson(success.getPosters());
                                    editor.putString("advertisements",ads_json);
                                }
                            }
                            //Memorial images
                            if (success.getMemorial_images()!=null){
                                if (success.getMemorial_images().size()>0){
                                    String memorial_json = new Gson().toJson(success.getMemorial_images());
                                    editor.putString("memorial_images",memorial_json);
                                }
                            }
                            //Sliders
                            if (success.getSliders()!=null){
                                if (success.getSliders().size()>0){
                                    String sliders_json = new Gson().toJson(success.getSliders());
                                    editor.putString("sliders",sliders_json);
                                }
                            }
                            //Countries
                            if (success.getCountries()!=null){
                                if (success.getCountries().size()>0){
                                    String countries_json = new Gson().toJson(success.getCountries());
                                    editor.putString("countries",countries_json);
                                }
                            }
                            //Categories
                            if (success.getCategories()!=null){
                                if (success.getCategories().size()>0){
                                    String categories_json = new Gson().toJson(success.getCategories());
                                    editor.putString("categories",categories_json);
                                }
                            }
                            //Reactions
                            if (success.getReactions_types()!=null){
                                if (success.getReactions_types().size()>0){
                                    String reactions_json = new Gson().toJson(success.getReactions_types());
                                    editor.putString("reactions",reactions_json);
                                }
                            }
                            //Payment types
                            if (success.getPayments_types()!=null){
                                if (success.getPayments_types().size()>0){
                                    String payments_json = new Gson().toJson(success.getPayments_types());
                                    editor.putString("payments_types",payments_json);
                                }
                            }
                            //My Institutions
                            if (success.getMy_facilities()!=null){
                                if (success.getMy_facilities().size() > 0){
                                    String insts_json = new Gson().toJson(success.getMy_facilities());
                                    editor.putString("my_insts",insts_json);
                                }
                            }
                            //My Packages
                            if(success.getMy_packages()!=null){
                                if (success.getMy_facilities().size()>0){
                                    String packages_json = new Gson().toJson(success.getMy_packages());
                                    editor.putString("my_packages",packages_json);
                                }
                            }
                            editor.commit();
                            /*
                            //Device Settings
                            if (success.getDevice_setting()!=null){
                                DeviceSettingsObject dso = success.getDevice_setting();
                                //City Id
                                editor.putInt("settings_city_id",dso.getCity_id());
                                //Country Id
                                editor.putInt("settings_country_id",dso.getCity_id());
                                //Language
                                editor.putString("language", BaseFunctions.numberToLanguageConverter(String.valueOf(dso.getView_language_id())));
                                //Alert
                                editor.putBoolean("settings_alerts",BaseFunctions.stringNumberToBoolean(String.valueOf(dso.getAllow_global_notification())));
                                editor.commit();
                            }
                            **/

                            //Account Settings
                            // Need to login
                            if (success.getAccount_settings()!=null){
                                AccountSettingsObject aso = success.getAccount_settings();
                                if (aso.getNeed_login() == 1){
                                    cancelTimer();
                                    startActivity(new Intent(SplashActivity.this,LoginActivity.class));
                                    finish();
                                }else {
                                    //App new installed
                                    if (sharedPreferences.getString("access_token","").equals("")){
                                        cancelTimer();
                                        startActivity(new Intent(SplashActivity.this,LoginActivity.class));
                                        finish();
                                    }else {
                                        //Profile Completed
                                        if (aso.getIs_profile_completed() == 1){
                                            if (aso.getUser_type() == 1){
                                                editor.putString("account_type","customer");
                                            }else if (aso.getUser_type() == 2){
                                                editor.putString("account_type","owner");
                                            }
                                            editor.putInt("account_status",aso.getAccount_status());
                                            editor.putBoolean("is_visitor",false);
                                            editor.commit();
                                            cancelTimer();
                                            startActivity(new Intent(SplashActivity.this,MainActivity.class));
                                            finish();
                                        }else {
                                            //User type
                                            if (aso.getUser_type() == 0){
                                                if (aso.getCreated_as()!=1){
                                                    cancelTimer();
                                                    startActivity(new Intent(SplashActivity.this,LoginActivity.class));
                                                    finish();
                                                }else {
                                                    cancelTimer();
                                                    startActivity(new Intent(SplashActivity.this,RegisterActivity.class));
                                                    finish();
                                                }
                                            }else if (aso.getUser_type() == 1){
                                                editor.putString("account_type","customer");
                                                editor.putBoolean("is_visitor",false);
                                                editor.commit();
                                                cancelTimer();
                                                startActivity(new Intent(SplashActivity.this,MainActivity.class));
                                                finish();
                                            }else if (aso.getUser_type() == 2){
                                                editor.putString("account_type","owner");
                                                editor.putBoolean("is_visitor",false);
                                                editor.commit();
                                                cancelTimer();
                                                startActivity(new Intent(SplashActivity.this,RegisterInstitutionActivity.class));
                                                finish();
                                            }
                                        }
                                    }
                                }
                            }else {
                                Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                                cancelTimer();
                                startActivity(intent);
                                finish();
                            }
                        }else {
                            callAPI();
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callAPI();
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });
    }
}
