package com.scit.sawwah.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.scit.sawwah.R;
import com.scit.sawwah.tools.BaseActivity;

public class MapActivity extends BaseActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private GoogleMap map;
    private String currentLatitude = "", currentLongitude = "";
    private Button btn_choose_location;
    private FloatingActionButton location_fab,road_fab;
    private String choosedLatitude = "", choosedLongitude = "";
    private Marker marker;
    private Marker previous_marker;
    //---------------------------------------------------------------------------
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    PendingResult<LocationSettingsResult> result;
    final static int REQUEST_LOCATION = 199;
    private Intent myIntent;
    private float received_lat = 0.0f,received_lng = 0.0f;
    //---------------------------------------------------------------------------

    @Override
    public void set_layout() {
        setContentView(R.layout.activity_map);
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void init_activity(Bundle savedInstanceState) {
        initMap();
        if (myIntent.getStringExtra("type").equals("with_input")){
            btn_choose_location.setVisibility(View.GONE);
            road_fab.setVisibility(View.VISIBLE);
        }else if (myIntent.getStringExtra("type").equals("with_edit")){
            road_fab.setVisibility(View.GONE);
        }
    }

    @Override
    public void init_views() {
        btn_choose_location = findViewById(R.id.map_select_location);
        location_fab = findViewById(R.id.map_location_fab);
        road_fab = findViewById(R.id.map_road_fab);
        myIntent = getIntent();
    }

    @Override
    public void init_events() {
        btn_choose_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (choosedLatitude.equals("")||choosedLongitude.equals("")){
                    Toast.makeText(MapActivity.this, getResources().getString(R.string.map_no_selection), Toast.LENGTH_SHORT).show();
                    return;
                }else {
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("latitude", choosedLatitude);
                    returnIntent.putExtra("longitude", choosedLongitude);
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();
                }
            }
        });
        location_fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(MapActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(MapActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                Location currentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                if (currentLocation !=null){
                    currentLatitude = String.valueOf(currentLocation.getLatitude());
                    currentLongitude = String.valueOf(currentLocation.getLongitude());
                    //map.addMarker(new MarkerOptions().position(new LatLng(currentLocation.getLatitude(),currentLocation.getLongitude())).title(getResources().getString(R.string.map_my_location))).setDraggable(true);
                    map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()),15.0f));
                }
            }
        });
        road_fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri navigationIntentUri = Uri.parse("google.navigation:q=" +
                        String.valueOf(myIntent.getFloatExtra("lat",0)) +
                        "," + String.valueOf(myIntent.getFloatExtra("lng",0)));//creating intent with latlng
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, navigationIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);
            }
        });

    }

    @Override
    public void set_fragment_place() {

    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);


        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);

        result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                //final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS: {
                        if (ActivityCompat.checkSelfPermission(MapActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(MapActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            return;
                        }
                        Location currentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                        if (currentLocation != null) {
                            currentLatitude = String.valueOf(currentLocation.getLatitude());
                            currentLongitude = String.valueOf(currentLocation.getLongitude());
                            //map.addMarker(new MarkerOptions().position(new LatLng(currentLocation.getLatitude(),currentLocation.getLongitude())).title(getResources().getString(R.string.map_my_location))).setDraggable(true);
                            if (myIntent.getStringExtra("type").equals("with_input")){
                                float lat1 = 0,lng1 = 0;
                                if (received_lng==0.0f&&received_lng==0.0f){
                                    lat1 = Float.valueOf(myIntent.getStringExtra("lat"));
                                    lng1= Float.valueOf(myIntent.getStringExtra("lng"));
                                }else {
                                    lat1 = received_lat;
                                    lng1 = received_lng;
                                }

                                Marker myCurrent = map.addMarker(new MarkerOptions().position(new LatLng(lat1,lng1))
                                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_colored_location)).title("Location"));
                                //Move Camera to this location
                                map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat1,lng1),15.0f));
                            }else if (myIntent.getStringExtra("type").equals("without_input")){
                                map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()), 15.0f));
                            }else if (myIntent.getStringExtra("type").equals("with_edit")){
                                float lat = received_lat;
                                float lng = received_lng;
                                marker = map.addMarker(new MarkerOptions().position(new LatLng(lat,lng))
                                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_colored_location)).title("Location"));
                                //Move Camera to this location
                                map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat,lng),15.0f));
                            }
                        }
                    }
                    break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(
                                    MapActivity.this,
                                    REQUEST_LOCATION);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        //...
                        break;
                }
            }
        });
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.map = googleMap;
        if (myIntent.getStringExtra("type").equals("with_input")||myIntent.getStringExtra("type").equals("with_edit")) {
            try {
                received_lat = Float.valueOf(getIntent().getStringExtra("lat"));
                received_lng = Float.valueOf(getIntent().getStringExtra("lng"));
            }catch (Exception e){}
           // marker = map.addMarker(new MarkerOptions().position(new LatLng(received_lat,received_lng))
                 //   .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_one)).title("Location"));
            //Move Camera to this location
           // map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(received_lat,received_lng),15.0f));
        }
        mGoogleApiClient = new GoogleApiClient.Builder(MapActivity.this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(MapActivity.this)
                .addOnConnectionFailedListener(MapActivity.this).build();
        mGoogleApiClient.connect();
        this.map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                if (myIntent.getStringExtra("type").equals("without_input")||myIntent.getStringExtra("type").equals("with_edit")){
                    choosedLatitude = String.valueOf(latLng.latitude);
                    choosedLongitude = String.valueOf(latLng.longitude);
                    if (marker != null) {
                        marker.remove();
                        marker = map.addMarker(new MarkerOptions()
                                .position(latLng)
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_colored_location))
                                .title(getResources().getString(R.string.map_institution_location))
                                .draggable(true));
                    } else {
                        marker = map.addMarker(new MarkerOptions()
                                .position(latLng)
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_colored_location))
                                .title(getResources().getString(R.string.map_institution_location))
                                .draggable(true));
                    }
                }
            }
        });
        this.map.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) {

            }

            @Override
            public void onMarkerDrag(Marker marker) {

            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
                choosedLatitude = String.valueOf(marker.getPosition().latitude);
                choosedLatitude = String.valueOf(marker.getPosition().longitude);
            }
        });
    }



    private void initMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.activity_map_fragment);
        mapFragment.getMapAsync(MapActivity.this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("onActivityResult()", Integer.toString(resultCode));

        //final LocationSettingsStates states = LocationSettingsStates.fromIntent(data);
        switch (requestCode) {
            case REQUEST_LOCATION:
                switch (resultCode) {
                    case Activity.RESULT_OK: {
                        // All required changes were successfully made
                        Toast.makeText(MapActivity.this, getResources().getString(R.string.gps_on), Toast.LENGTH_LONG).show();
                        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            return;
                        }
                        Location currentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                        if (currentLocation !=null){
                            currentLatitude = String.valueOf(currentLocation.getLatitude());
                            currentLongitude = String.valueOf(currentLocation.getLongitude());
                            //map.addMarker(new MarkerOptions().position(new LatLng(currentLocation.getLatitude(),currentLocation.getLongitude())).title(getResources().getString(R.string.map_my_location))).setDraggable(true);
                            float lat1 = 0,lng1 = 0;
                            if (myIntent.getStringExtra("type").equals("with_input")){
                                if (received_lng==0.0f&&received_lng==0.0f){
                                    lat1 = Float.valueOf(myIntent.getStringExtra("lat"));
                                    lng1= Float.valueOf(myIntent.getStringExtra("lng"));
                                }else {
                                     lat1 = received_lat;
                                     lng1 = received_lng;
                                }

                                Marker myCurrent = map.addMarker(new MarkerOptions().position(new LatLng(lat1,lng1))
                                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_colored_location)).title("Location"));
                                //Move Camera to this location
                                map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat1,lng1),15.0f));
                            }else if (myIntent.getStringExtra("type").equals("without_input")){
                                map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()), 15.0f));
                            }else if (myIntent.getStringExtra("type").equals("with_edit")){
                                float lat = received_lat;
                                float lng = received_lng;
                                marker = map.addMarker(new MarkerOptions().position(new LatLng(lat,lng))
                                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_colored_location)).title("Location"));
                                //Move Camera to this location
                                map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat,lng),15.0f));
                            }
                        }
                        break;
                    }
                    case Activity.RESULT_CANCELED:
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder(MapActivity.this);
                        builder.setMessage(getResources().getString(R.string.gps_off)).setCancelable(false).
                                setPositiveButton(getResources().getString(R.string.no_internet_ok), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        finish();
                                    }
                                }).show();
                        break;
                    }
                    default:
                    {
                        break;
                    }
                }
                break;
        }
    }

    public Bitmap resizeMapIcons(String iconName, int width, int height){
        Bitmap imageBitmap = BitmapFactory.decodeResource(getResources(),getResources().getIdentifier(iconName, "drawable", getPackageName()));
        Bitmap resizedBitmap = Bitmap.createScaledBitmap(imageBitmap, width, height, false);
        return resizedBitmap;
    }
}
