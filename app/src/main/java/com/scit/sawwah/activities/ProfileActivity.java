package com.scit.sawwah.activities;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.azoft.carousellayoutmanager.CarouselLayoutManager;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;
import com.scit.sawwah.APIClasses.FavouriteAPIsClass;
import com.scit.sawwah.APIClasses.InstitutionsAPIsClass;
import com.scit.sawwah.Models.AdvertisementObject;
import com.scit.sawwah.Models.ContactInfoObject;
import com.scit.sawwah.Models.ContactObject;
import com.scit.sawwah.Models.ImageObject;
import com.scit.sawwah.Models.InstitutionObject;
import com.scit.sawwah.Models.MemorialImageObject;
import com.scit.sawwah.Models.OfferObject;
import com.scit.sawwah.Models.RateObject;
import com.scit.sawwah.R;
import com.scit.sawwah.adapters.AdvertisementsAdapter;
import com.scit.sawwah.adapters.BestTimesAdapter;
import com.scit.sawwah.adapters.FavoriteOffersAdapter;
import com.scit.sawwah.adapters.PhotoGalleryAdapter;
import com.scit.sawwah.adapters.ProfileContactingAdapter;
import com.scit.sawwah.adapters.SearchInstitutionsAdapter;
import com.scit.sawwah.adapters.UsersRatesAdapter;
import com.scit.sawwah.dialoges.VisitorDialog;
import com.scit.sawwah.dialoges.WeatherDialog;
import com.scit.sawwah.interfaces.IFailure;
import com.scit.sawwah.interfaces.IMove;
import com.scit.sawwah.interfaces.IResponse;
import com.scit.sawwah.tools.BaseActivity;
import com.scit.sawwah.tools.BaseFunctions;
import com.scit.sawwah.tools.BaseRetrofit;
import com.scit.sawwah.tools.NewScrollView;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileActivity extends BaseActivity implements OnMapReadyCallback {
    //Toolbar Section
    private ImageView btn_weather,btn_back;
    private String loc_lat = "",loc_lng = "";
    //Basic Info Section
    private ImageView img_cover;
    private CircleImageView img_logo;
    private TextView tv_title,tv_address,tv_desc,tv_opens;
    private ImageView btn_edit,btn_share;
    private SimpleRatingBar rb_rate;
    private RecyclerView rv_contactings;
    private List<ContactObject> list_contacts;
    private ProfileContactingAdapter adapter_contactings;
    private LinearLayoutManager layout_manager_contacting;
    private TextView tv_contacting;
    private TextView tv_phone;
    private String city = "";
    private int id = 0;
    //StatisticsAPIs Section
    private ImageView img_love;
    private TextView tv_lovers,tv_interaction,tv_search_count,tv_num_ads,tv_num_offers,tv_view_count;
    private CardView btn_invite_me;
    private TextView tv_rate_txt,tv_raters_num;
    private ProgressBar pb_rate_1,pb_rate_2,pb_rate_3,pb_rate_4,pb_rate_5,pb_raters;
    private ImageView img_emoji_1,img_emoji_2,img_emoji_3,img_emoji_4,img_emoji_5;
    private LinearLayout dialoge_layout;
    private EditText edt_invite_text;
    private ImageView img_invite_sms,img_invite,img_invite_messenger,img_invite_whatsapp;
    private int lovers = 0;
    private boolean love = false;
    //Users Rates Section
    private TextView tv_users_rates_txt;
    private RecyclerView rv_users_rates;
    private List<RateObject> list_rates;
    private UsersRatesAdapter usersRatesAdapter;
    private LinearLayoutManager ratesLayoutManager;
    private Button btn_all_rates,btn_rate;
    //Offers Section
    private RelativeLayout lyt_offers;
    private ImageView btn_add_offer;
    private TextView tv_all_offers;
    private RecyclerView rv_offers;
    private List<OfferObject> list_offers;
    private FavoriteOffersAdapter offersAdapter;
    //Advertisements Section
    private RelativeLayout lyt_ads;
    private ImageView btn_add_advertise;
    private TextView tv_all_ads;
    private RecyclerView rv_ads;
    private List<AdvertisementObject> list_ads;
    private AdvertisementsAdapter advertisementsAdapter;
    //Gallery Section
    private RelativeLayout lyt_gallery;
    private RecyclerView rv_photo_gallery;
    private List<ImageObject> list_gallery;
    private PhotoGalleryAdapter galleryAdapter;
    //Souvenir Section
    private RelativeLayout lyt_souvenir;
    private RecyclerView  rv_best_times;
    private List<MemorialImageObject> list_souvenir;
    private BestTimesAdapter bestTimesAdapter;
    //Location section
    private RelativeLayout lyt_location;
    private GoogleMap map;
    private RelativeLayout map_layout;
    private SupportMapFragment mapFragment;
    private float location_lat = 0,location_lng = 0;
    //Similar Institutions
    private TextView tv_similar_ints;
    private RecyclerView rv_similar_insts;
    private SearchInstitutionsAdapter similarInstsAdapter;
    private List<InstitutionObject> list_similar;
    private LinearLayoutManager similarInstsLayoutManager;
    //Extra
    private Intent myIntent;
    private RelativeLayout root;
    private FloatingActionButton btn_up;
    private NewScrollView scrollView;
    private CallbackManager callbackManager;
    private ShareDialog shareDialog;

    @Override
    public void set_layout() {
        setContentView(R.layout.activity_profile);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        list_contacts = new ArrayList<>();
        adapter_contactings = new ProfileContactingAdapter(ProfileActivity.this, list_contacts, new IMove() {
            @Override
            public void move(int position) {

            }
        });
        layout_manager_contacting = new LinearLayoutManager(ProfileActivity.this,LinearLayoutManager.HORIZONTAL,false);
        rv_contactings.setLayoutManager(layout_manager_contacting);
        rv_contactings.setAdapter(adapter_contactings);
        list_ads = new ArrayList<>();
        advertisementsAdapter = new AdvertisementsAdapter(ProfileActivity.this,list_ads, new IMove() {
            @Override
            public void move(int position) {

            }
        });
        list_souvenir = new ArrayList<>();
        bestTimesAdapter = new BestTimesAdapter(ProfileActivity.this, list_souvenir, new IMove() {
            @Override
            public void move(int position) {

            }
        });
        list_offers = new ArrayList<>();
        offersAdapter = new FavoriteOffersAdapter(ProfileActivity.this, list_offers,new IMove() {
            @Override
            public void move(int position) {

            }
        });
        list_gallery = new ArrayList<>();
        galleryAdapter = new PhotoGalleryAdapter(ProfileActivity.this, list_gallery, new IMove() {
            @Override
            public void move(int position) {

            }
        });
        rv_ads.setHasFixedSize(true);
        rv_ads.setNestedScrollingEnabled(false);
        rv_ads.setLayoutManager(new LinearLayoutManager(ProfileActivity.this));
        rv_ads.setAdapter(advertisementsAdapter);

        rv_best_times.setLayoutManager(new LinearLayoutManager(ProfileActivity.this, LinearLayoutManager.HORIZONTAL, false));
        rv_best_times.setAdapter(bestTimesAdapter);

        LinearLayoutManager offersLayoutManager = new LinearLayoutManager(ProfileActivity.this,CarouselLayoutManager.HORIZONTAL,false);
        rv_offers.setLayoutManager(offersLayoutManager);
        rv_offers.setAdapter(offersAdapter);

        rv_photo_gallery.setLayoutManager(new LinearLayoutManager(ProfileActivity.this,LinearLayoutManager.HORIZONTAL,false));
        rv_photo_gallery.setAdapter(galleryAdapter);

        list_rates = new ArrayList<>();
        usersRatesAdapter = new UsersRatesAdapter(ProfileActivity.this, list_rates,new IMove() {
            @Override
            public void move(int position) {

            }
        });
        ratesLayoutManager = new LinearLayoutManager(ProfileActivity.this,LinearLayoutManager.VERTICAL,false);
        rv_users_rates.setLayoutManager(ratesLayoutManager);
        rv_users_rates.setAdapter(usersRatesAdapter);

        list_similar = new ArrayList<>();
        similarInstsAdapter = new SearchInstitutionsAdapter(ProfileActivity.this,list_similar, new IMove() {
            @Override
            public void move(int position) {

            }
        });
        similarInstsLayoutManager = new LinearLayoutManager(ProfileActivity.this,LinearLayoutManager.HORIZONTAL,false);
        rv_similar_insts.setLayoutManager(similarInstsLayoutManager);
        rv_similar_insts.setAdapter(similarInstsAdapter);
        initMap();
        if (sharedPreferences.getString("account_type","customer").equals("customer")){
            btn_edit.setVisibility(View.GONE);
            btn_add_advertise.setVisibility(View.GONE);
            btn_add_offer.setVisibility(View.GONE);
        }
    }

    @Override
    public void init_views() {
        //Toolbar section
        btn_weather = findViewById(R.id.btn_weather);
        btn_back = findViewById(R.id.profile_back_btn);
        //Basic Info Section
        img_cover = findViewById(R.id.profile_cover);
        img_logo = findViewById(R.id.profile_icon);
        tv_title = findViewById(R.id.profile_name);
        tv_address = findViewById(R.id.profile_address);
        tv_desc = findViewById(R.id.profile_desc);
        tv_opens = findViewById(R.id.profile_opens);
        btn_edit = findViewById(R.id.profile_edit);
        btn_share = findViewById(R.id.profile_share_btn);
        rb_rate = findViewById(R.id.profile_rate_stars);
        tv_contacting = findViewById(R.id.profile_contacting_txt);
        rv_contactings = findViewById(R.id.profile_contacting_recycler);
        tv_phone = findViewById(R.id.profile_phone);

        //StatisticsAPIs Section
        img_love = findViewById(R.id.profile_love);
        tv_lovers = findViewById(R.id.profile_lovers);
        tv_interaction = findViewById(R.id.profile_interaction_rate);
        tv_search_count = findViewById(R.id.profile_search_count);
        tv_num_ads = findViewById(R.id.profile_ads_count);
        tv_num_offers = findViewById(R.id.profile_offers_count);
        tv_view_count = findViewById(R.id.profile_views_count);
        btn_invite_me = findViewById(R.id.card_invite_me);
        tv_rate_txt = findViewById(R.id.profile_rate_txt);
        tv_raters_num = findViewById(R.id.profile_raters_txt);
        pb_rate_1 = findViewById(R.id.profile_rate_progress_1);
        pb_rate_2 = findViewById(R.id.profile_rate_progress_2);
        pb_rate_3 = findViewById(R.id.profile_rate_progress_3);
        pb_rate_4 = findViewById(R.id.profile_rate_progress_4);
        pb_rate_5 = findViewById(R.id.profile_rate_progress_5);
        pb_raters = findViewById(R.id.profile_raters_progress);
        img_emoji_1 = findViewById(R.id.profile_emoji_1);
        img_emoji_2 = findViewById(R.id.profile_emoji_2);
        img_emoji_3 = findViewById(R.id.profile_emoji_3);
        img_emoji_4 = findViewById(R.id.profile_emoji_4);
        img_emoji_5 = findViewById(R.id.profile_emoji_5);
        dialoge_layout = findViewById(R.id.dialoge_layout);
        edt_invite_text = dialoge_layout.findViewById(R.id.invite_me_text);
        img_invite_sms = dialoge_layout.findViewById(R.id.invite_me_message);
        img_invite_whatsapp = dialoge_layout.findViewById(R.id.invite_me_whatsapp);
        img_invite_messenger = dialoge_layout.findViewById(R.id.invite_me_messenger);
        //Users Rates Section
        tv_users_rates_txt = findViewById(R.id.profile_users_rates_txt);
        rv_users_rates = findViewById(R.id.profile_user_rates_recycler);
        btn_all_rates = findViewById(R.id.profile_all_rates_btn);
        btn_rate = findViewById(R.id.profile_rate_institution_btn);
        //Offers Section
        lyt_offers = findViewById(R.id.profile_offers_layout);
        btn_add_offer = findViewById(R.id.btn_add_offer);
        tv_all_offers = findViewById(R.id.profile_all_offers);
        rv_offers = findViewById(R.id.rv_my_offers);
        //Advertisements Section
        lyt_ads = findViewById(R.id.profile_ads_layout);
        btn_add_advertise = findViewById(R.id.btn_add_advertise);
        tv_all_ads = findViewById(R.id.profile_all_ads);
        rv_ads = findViewById(R.id.rv_my_ads);
        //Gallery Section
        lyt_gallery = findViewById(R.id.profile_gallery_layout);
        rv_photo_gallery = findViewById(R.id.rv_photo_gallery);
        //Souvenir Section
        lyt_souvenir = findViewById(R.id.profile_souvenir_layout);
        rv_best_times = findViewById(R.id.rv_best_times);
        //Location section
        lyt_location = findViewById(R.id.profile_location_layout);
        map_layout = findViewById(R.id.map_layout);
        //Similar Institutions
        tv_similar_ints = findViewById(R.id.profile_similar_txt);
        rv_similar_insts = findViewById(R.id.profile_simlar_insts_recycler);
        //Extra
        root = findViewById(R.id.profile_layout);
        myIntent = getIntent();
        root.setVisibility(View.GONE);
        btn_up = findViewById(R.id.profile_up_fab);
        scrollView = findViewById(R.id.profile_scrollview);
    }

    @Override
    public void init_events() {
        tv_all_offers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProfileActivity.this,AllOffersActivity.class);
                intent.putExtra("filter",String.valueOf(id));
                intent.putExtra("with",true);
                startActivity(intent);
            }
        });
        tv_all_ads.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProfileActivity.this,AllAdvertisementsActivity.class);
                intent.putExtra("filter",String.valueOf(id));
                intent.putExtra("with",true);
                startActivity(intent);
            }
        });
        btn_add_offer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sharedPreferences.getBoolean("is_visitor",false)){
                    VisitorDialog dialog = new VisitorDialog(ProfileActivity.this);
                    dialog.show();
                }else {
                    startActivity(new Intent(ProfileActivity.this, AddOfferActivity.class));
                }
            }
        });

        btn_weather.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //startActivity(new Intent(ProfileActivity.this, WeatherActivity.class));
                WeatherDialog weatherDialog = new WeatherDialog(ProfileActivity.this,loc_lat,loc_lng,city);
                weatherDialog.show();
            }
        });

        btn_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FacebookSdk.sdkInitialize(ProfileActivity.this.getApplicationContext());
                callbackManager = CallbackManager.Factory.create();
                shareDialog = new ShareDialog(ProfileActivity.this);
                shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
                    @Override
                    public void onSuccess(Sharer.Result result) {
                        Toast.makeText(ProfileActivity.this, "Posted successfully", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onCancel() {
                        Toast.makeText(ProfileActivity.this, "Posted canceled", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(FacebookException error) {
                        Toast.makeText(ProfileActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
                ShareLinkContent linkContent = new ShareLinkContent.Builder().setQuote(getResources().getString(R.string.facebook_post_inst)).
                        setContentUrl(Uri.parse(BaseRetrofit.BASE_ALI+"facilities/"+String.valueOf(id))).build();
                if (shareDialog.canShow(ShareLinkContent.class)){
                    shareDialog.show(linkContent);
                }else {
                    Toast.makeText(ProfileActivity.this, "can't show", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btn_add_advertise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sharedPreferences.getBoolean("is_visitor",false)){
                    VisitorDialog dialog = new VisitorDialog(ProfileActivity.this);
                    dialog.show();
                }else {
                    startActivity(new Intent(ProfileActivity.this, AddAdvertiseActivity.class));
                }

            }
        });

        findViewById(R.id.btn_invite_me).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialoge_layout.getVisibility() == View.GONE)
                    dialoge_layout.setVisibility(View.VISIBLE);
                else if (dialoge_layout.getVisibility() == View.VISIBLE)
                    dialoge_layout.setVisibility(View.GONE);
            }
        });

        btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sharedPreferences.getBoolean("is_visitor",false)){
                    VisitorDialog dialog = new VisitorDialog(ProfileActivity.this);
                    dialog.show();
                }else {
                    Intent intent = new Intent(ProfileActivity.this,UpdateInstitutionDetailsActivity.class);
                    intent.putExtra("institution_id",id);
                    startActivity(intent);
                }
            }
        });
        btn_all_rates.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sharedPreferences.getBoolean("is_visitor",false)){
                    VisitorDialog dialog = new VisitorDialog(ProfileActivity.this);
                    dialog.show();
                }else {
                    Intent intent = new Intent(ProfileActivity.this,AllRatesActivity.class);
                    intent.putExtra("institution_id",id);
                    startActivity(intent);
                }
            }
        });

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btn_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scrollToTop();
            }
        });
        scrollView.setOnTouchListener(new View.OnTouchListener() {
            float y0 = 0;
            float y1 = 0;
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_MOVE) {
                    y0 = event.getY();
                    if (y1 - y0 > 0) {
                        btn_up.show();
                    } else if (y1 - y0 < 0) {
                        btn_up.hide();
                    }
                    y1 = event.getY();
                }
                return false;
            }
        });
        img_love.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (love == false){
                    toggle_item(String.valueOf(id),"1","1");
                }else {
                    toggle_item(String.valueOf(id),"1","0");
                }
            }
        });
        btn_rate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sharedPreferences.getBoolean("is_visitor",false)){
                    VisitorDialog dialog = new VisitorDialog(ProfileActivity.this);
                    dialog.show();
                }else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(ProfileActivity.this);
                    View view = getLayoutInflater().inflate(R.layout.dialog_rate_institution,null);
                    final String[] choosed_stars = {""};
                    SimpleRatingBar rb_stars = view.findViewById(R.id.dialog_rate_stars);
                    final EditText edt_comment = view.findViewById(R.id.dialog_rate_comment);
                    LinearLayout btn_rate = view.findViewById(R.id.dialog_rate_btn);
                    rb_stars.setOnRatingBarChangeListener(new SimpleRatingBar.OnRatingBarChangeListener() {
                        @Override
                        public void onRatingChanged(SimpleRatingBar simpleRatingBar, float rating, boolean fromUser) {
                            choosed_stars[0] = String.valueOf(rating);
                        }
                    });
                    builder.setView(view);
                    final AlertDialog dialog = builder.create();
                    btn_rate.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (choosed_stars[0].equals("")){
                                Toast.makeText(ProfileActivity.this, getResources().getString(R.string.rate_dialog_no_stars), Toast.LENGTH_SHORT).show();
                                return;
                            }
                            if (edt_comment.getText().toString().equals("")){
                                Toast.makeText(ProfileActivity.this, getResources().getString(R.string.rate_dialog_no_text), Toast.LENGTH_SHORT).show();
                                return;
                            }
                            InstitutionsAPIsClass.rate(sharedPreferences,
                                    ProfileActivity.this,
                                    sharedPreferences.getString("language", "ar"),
                                    sharedPreferences.getString("service_token", ""),
                                    String.valueOf(id),
                                    choosed_stars[0],
                                    edt_comment.getText().toString(),
                                    new IResponse() {
                                        @Override
                                        public void onResponse() {

                                        }

                                        @Override
                                        public void onResponse(Object json) {
                                            if (json !=null){
                                                Toast.makeText(ProfileActivity.this, getResources().getString(R.string.rate_dialog_success), Toast.LENGTH_SHORT).show();
                                                RateObject ro = new RateObject();
                                                ro.setNote(edt_comment.getText().toString());
                                                ro.setRate_value(Float.valueOf(choosed_stars[0]));
                                                list_rates.add(ro);
                                                usersRatesAdapter.notifyDataSetChanged();
                                                tv_users_rates_txt.setVisibility(View.VISIBLE);
                                                rv_users_rates.setVisibility(View.VISIBLE);
                                                dialog.cancel();
                                            }
                                        }
                                    }, new IFailure() {
                                        @Override
                                        public void onFailure() {
                                            Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                                    .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View v) {
                                                            callAPI();
                                                        }
                                                    }).setActionTextColor(getResources().getColor(R.color.white)).show();
                                        }
                                    });
                        }
                    });
                    dialog.show();
                }
            }
        });
        img_invite_sms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt_invite_text.getText().toString().equals("")){
                    Toast.makeText(ProfileActivity.this, getResources().getString(R.string.profile_no_text), Toast.LENGTH_SHORT).show();
                    return;
                }
                BaseFunctions.sendMessageBySMS(ProfileActivity.this,edt_invite_text.getText().toString()+"\n"+
                        getResources().getString(R.string.profile_you_can_visit)+"\n "+ BaseRetrofit.BASE_ALI+"facilities/"+
                        String.valueOf(id),getResources().getString(R.string.profile_no_sms));
            }
        });
        img_invite_messenger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt_invite_text.getText().toString().equals("")){
                    Toast.makeText(ProfileActivity.this, getResources().getString(R.string.profile_no_text), Toast.LENGTH_SHORT).show();
                    return;
                }
                BaseFunctions.sendMessageByMessenger(ProfileActivity.this,edt_invite_text.getText().toString()+"\n"+
                        getResources().getString(R.string.profile_you_can_visit)+"\n "+ BaseRetrofit.BASE_ALI+"facilities/"+
                        String.valueOf(id),getResources().getString(R.string.prfile_no_messenger));

            }
        });
        img_invite_whatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt_invite_text.getText().toString().equals("")){
                    Toast.makeText(ProfileActivity.this, getResources().getString(R.string.profile_no_text), Toast.LENGTH_SHORT).show();
                    return;
                }
                BaseFunctions.sendMessageByWhatsapp(ProfileActivity.this,edt_invite_text.getText().toString()+"\n"+
                        getResources().getString(R.string.profile_you_can_visit)+"\n "+ BaseRetrofit.BASE_ALI+"facilities/"+
                        String.valueOf(id),getResources().getString(R.string.profile_no_whatsapp));
            }
        });
    }

    public void scrollToTop() {
        int x = 0;
        int y = 0;

        ObjectAnimator xTranslate = ObjectAnimator.ofInt(scrollView, "scrollX", x);
        ObjectAnimator yTranslate = ObjectAnimator.ofInt(scrollView, "scrollY", y);

        AnimatorSet animators = new AnimatorSet();
        animators.setDuration(1000L);
        animators.playTogether(xTranslate, yTranslate);

        animators.addListener(new Animator.AnimatorListener() {

            @Override
            public void onAnimationStart(Animator arg0) {

            }

            @Override
            public void onAnimationRepeat(Animator arg0) {


            }

            @Override
            public void onAnimationEnd(Animator arg0) {


            }

            @Override
            public void onAnimationCancel(Animator arg0) {


            }
        });
        animators.start();
    }


    @Override
    public void set_fragment_place() {

    }

    public static void startAlphaAnimation(View v, long duration, int visibility) {
        AlphaAnimation alphaAnimation = (visibility == View.VISIBLE)
                ? new AlphaAnimation(0f, 1f)
                : new AlphaAnimation(1f, 0f);

        alphaAnimation.setDuration(duration);
        alphaAnimation.setFillAfter(true);
        v.startAnimation(alphaAnimation);
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                Intent intent = new Intent(ProfileActivity.this,MapActivity.class);
                intent.putExtra("type","with_input");
                intent.putExtra("lat",String.valueOf(location_lat));
                intent.putExtra("lng",String.valueOf(location_lng));
                startActivity(intent);
            }
        });
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                callAPI();
            }
        });
    }
    private void initMap(){
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.profile_map);
        mapFragment.getMapAsync(ProfileActivity.this);
    }

    private void callAPI(){
        InstitutionsAPIsClass.details(sharedPreferences,
                ProfileActivity.this,
                sharedPreferences.getString("language", "ar"),
                sharedPreferences.getString("service_token", ""),
                String.valueOf(myIntent.getIntExtra("institution_id", 0)),
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json != null){
                            String json1 = new Gson().toJson(json);
                            InstitutionObject success = new Gson().fromJson(json1,InstitutionObject.class);
                            //Toolbar
                            loc_lat = String.valueOf(success.getLat());
                            loc_lng = String.valueOf(success.getLng());
                            //Basic Info Section-----------------------------------------------------------
                            //ID
                            id = success.getId();
                            //Cover
                            if (success.getImage_cover()!=null){
                                BaseFunctions.setGlideImage(ProfileActivity.this,img_cover,success.getImage_cover());
                            }
                            //Icon
                            if (success.getImage_icon()!=null){
                                BaseFunctions.setGlideImage(ProfileActivity.this,img_logo,success.getImage_icon());
                            }
                            //Name
                            if (success.getName()!=null){
                                tv_title.setText(success.getName());
                            }
                            //Address
                            if (success.getAddress() != null){
                                tv_address.setText(success.getAddress());
                            }
                            //City
                            if (success.getCity_name()!=null){
                                city = success.getCity_name();
                            }
                            //Description
                            if (success.getDetails()!=null){
                                tv_desc.setText(success.getDetails());
                            }
                            //Rating
                            rb_rate.setRating(success.getRating());
                            //Contacting
                            if (success.getContacts()!=null){
                                if (success.getContacts().size()>0){
                                    for (ContactObject co : success.getContacts()){
                                        list_contacts.add(co);
                                        adapter_contactings.notifyDataSetChanged();
                                    }
                                }else {
                                    tv_contacting.setVisibility(View.GONE);
                                    rv_contactings.setVisibility(View.GONE);
                                }
                            }else {
                                tv_contacting.setVisibility(View.GONE);
                                rv_contactings.setVisibility(View.GONE);
                            }
                            //ContactInfo
                            if (success.getContact_info()!=null){
                                String phone = "";
                                String land = "";
                                ContactInfoObject cio = success.getContact_info();
                                if (cio.getMobile_number()!=null){
                                    if (!cio.getMobile_number().equals("")){
                                        if (!cio.getMobile_number().equals("0")){
                                            phone = cio.getMobile_number();
                                        }
                                    }
                                }
                                if (cio.getPhone_number()!=null){
                                    if (!cio.getPhone_number().equals("")){
                                        if (!cio.getPhone_number().equals("0")){
                                            land = cio.getPhone_number();
                                        }
                                    }
                                }
                                if (!phone.equals("")){
                                    if (!land.equals("")){
                                        tv_phone.setText(phone+" - "+land);
                                    }else {
                                        tv_phone.setText(phone);
                                    }
                                }else {
                                    if (!land.equals("")){
                                        tv_phone.setText(land);
                                    }
                                }
                            }
                            //StatisticsAPIs Section-------------------------------------------------------------
                            //Love
                            love = success.getMarked_as_favourite();
                            if(love == true){
                                img_love.setImageResource(R.drawable.ic_heart);
                            }else {
                                img_love.setImageResource(R.drawable.ic_grey_heart);
                            }
                            //Lovers
                            tv_lovers.setText(String.valueOf(success.getFavourite_count()));
                            lovers = success.getFavourite_count();
                            //Interaction
                            tv_interaction.setText(String.valueOf(success.getReaction_ratio())+"%");
                            //Search count
                            tv_search_count.setText(String.valueOf(success.getSearch_view()));
                            //Number of advertisements
                            tv_num_ads.setText(String.valueOf(success.getPosters_count()));
                            //Number of Offers
                            tv_num_offers.setText(String.valueOf(success.getOffers_count()));
                            //Views count
                            tv_view_count.setText(String.valueOf(success.getVisit_views()));
                            //Rating
                            tv_rate_txt.setText(String.valueOf(Math.round(success.getUsers_rating())));
                            //Raters
                            int num_raters = success.getRating_1_num()+success.getRating_2_num()+success.getRating_3_num()+
                                    success.getRating_4_num()+success.getRating_5_num();

                            tv_raters_num.setText(String.valueOf(num_raters));
                            pb_raters.setMax(1000);
                            int[] values = new int[]{success.getRating_1_num()
                                    ,success.getRating_2_num()
                                    ,success.getRating_3_num()
                                    ,success.getRating_4_num()
                                    ,success.getRating_5_num()};
                            int max = 0;
                            for (int i = 0; i <5 ; i++) {
                                if (values[i]>values[max]){
                                    max = i;
                                }
                            }
                            max++;
                            switch (max){
                                case 1: {
                                    pb_raters.setProgress(200);
                                    img_emoji_1.setVisibility(View.VISIBLE);
                                }break;
                                case 2: {
                                    pb_raters.setProgress(400);
                                    img_emoji_2.setVisibility(View.VISIBLE);
                                }break;
                                case 3: {
                                    pb_raters.setProgress(600);
                                    img_emoji_3.setVisibility(View.VISIBLE);
                                }break;
                                case 4: {
                                    pb_raters.setProgress(800);
                                    img_emoji_4.setVisibility(View.VISIBLE);
                                }break;
                                case 5: {
                                    pb_raters.setProgress(1000);
                                    img_emoji_5.setVisibility(View.VISIBLE);
                                }break;
                            }
                            //5 Stars Rating
                            pb_rate_5.setMax(num_raters);
                            pb_rate_5.setProgress(success.getRating_5_num());
                            //4 Stars Rating
                            pb_rate_4.setMax(num_raters);
                            pb_rate_4.setProgress(success.getRating_4_num());
                            //3 Stars Rating
                            pb_rate_3.setMax(num_raters);
                            pb_rate_3.setProgress(success.getRating_3_num());
                            //2 Stars Rating
                            pb_rate_2.setMax(num_raters);
                            pb_rate_2.setProgress(success.getRating_2_num());
                            //1 Stars Rating
                            pb_rate_1.setMax(num_raters);
                            pb_rate_1.setProgress(success.getRating_1_num());
                            //Users Rates Section---------------------------------------------------------
                            if (success.getUsers_rating_list()!=null){
                                if (success.getUsers_rating_list().size()>0){
                                    for (RateObject ro : success.getUsers_rating_list()){
                                        list_rates.add(ro);
                                        usersRatesAdapter.notifyDataSetChanged();
                                    }
                                }else {
                                    tv_users_rates_txt.setVisibility(View.GONE);
                                    rv_users_rates.setVisibility(View.GONE);
                                    btn_all_rates.setVisibility(View.GONE);
                                }
                            }else {
                                tv_users_rates_txt.setVisibility(View.GONE);
                                rv_users_rates.setVisibility(View.GONE);
                                btn_all_rates.setVisibility(View.GONE);
                            }
                            //Offers Section----------------------------------------------------------------
                            if (success.getOffers()!=null){
                                if (success.getOffers().size()>0){
                                    for (OfferObject oo : success.getOffers()){
                                        list_offers.add(oo);
                                        offersAdapter.notifyDataSetChanged();
                                    }
                                }else {
                                    lyt_offers.setVisibility(View.GONE);
                                    rv_offers.setVisibility(View.GONE);
                                }
                            }else {
                                lyt_offers.setVisibility(View.GONE);
                                rv_offers.setVisibility(View.GONE);
                            }
                            //Advertisements Section--------------------------------------------------------
                            if (success.getPosters()!=null){
                                if (success.getPosters().size()>0){
                                    for (AdvertisementObject ao : success.getPosters()){
                                        list_ads.add(ao);
                                        advertisementsAdapter.notifyDataSetChanged();
                                    }
                                }else {
                                    lyt_ads.setVisibility(View.GONE);
                                    rv_ads.setVisibility(View.GONE);
                                }
                            }else {
                                lyt_ads.setVisibility(View.GONE);
                                rv_ads.setVisibility(View.GONE);
                            }
                            //Gallery Section---------------------------------------------------------------
                            if (success.getImages()!=null){
                                if (success.getImages().size() > 0){
                                    for (ImageObject io : success.getImages()){
                                        list_gallery.add(io);
                                        galleryAdapter.notifyDataSetChanged();
                                    }
                                }else {
                                    lyt_gallery.setVisibility(View.GONE);
                                    rv_photo_gallery.setVisibility(View.GONE);
                                }
                            }else {
                                lyt_gallery.setVisibility(View.GONE);
                                rv_photo_gallery.setVisibility(View.GONE);
                            }
                            //Memorial Images Section---------------------------------------------------------
                            if (success.getMemorial_images()!=null){
                                if (success.getMemorial_images().size() > 0){
                                    for (MemorialImageObject mio : success.getMemorial_images()){
                                        list_souvenir.add(mio);
                                        bestTimesAdapter.notifyDataSetChanged();
                                    }
                                }else {
                                    lyt_souvenir.setVisibility(View.GONE);
                                    rv_best_times.setVisibility(View.GONE);
                                }
                            }else {
                                lyt_souvenir.setVisibility(View.GONE);
                                rv_best_times.setVisibility(View.GONE);
                            }
                            //Location Section---------------------------------------------------------------
                            if (success.getLat()==0||success.getLat()==0.0||success.getLng()==0||success.getLng()==0.0){
                                lyt_location.setVisibility(View.GONE);
                                map_layout.setVisibility(View.GONE);
                            }else {
                                float lat = success.getLat();
                                float lng = success.getLng();
                                location_lat = lat;
                                location_lng = lng;
                                Marker myCurrent = map.addMarker(new MarkerOptions().position(new LatLng(lat,lng))
                                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_one)).title("Location"));
                                //Move Camera to this location
                                map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat,lng),15.0f));
                            }
                            //Similar Institutions Section---------------------------------------------------
                            if (success.getRelated_facilities()!=null){
                                if (success.getRelated_facilities().size()>0){
                                    for (InstitutionObject io : success.getRelated_facilities()){
                                        list_similar.add(io);
                                        similarInstsAdapter.notifyDataSetChanged();
                                    }
                                }else {
                                    tv_similar_ints.setVisibility(View.GONE);
                                    rv_similar_insts.setVisibility(View.GONE);
                                }
                            }else {
                                tv_similar_ints.setVisibility(View.GONE);
                                rv_similar_insts.setVisibility(View.GONE);
                            }
                            root.setVisibility(View.VISIBLE);
                            //Hide non owner elements
                            if (sharedPreferences.getBoolean("is_visitor",false) == false){
                                if (sharedPreferences.getString("account_type","customer").equals("owner")){
                                    if (!sharedPreferences.getString("my_insts","[]").equals("[]")&&
                                            sharedPreferences.getString("my_insts","[]")!= null){
                                        InstitutionObject[] myFacilities = new Gson().fromJson(
                                                sharedPreferences.getString("my_insts","[]"),
                                                InstitutionObject[].class);
                                        if (myFacilities.length>0){
                                            int myFacilityId = myFacilities[0].getId();
                                            if (id == myFacilityId){
                                                btn_edit.setVisibility(View.VISIBLE);
                                                btn_rate.setVisibility(View.GONE);
                                                btn_add_advertise.setVisibility(View.VISIBLE);
                                                btn_add_offer.setVisibility(View.VISIBLE);
                                            }else {
                                                btn_edit.setVisibility(View.GONE);
                                                btn_rate.setVisibility(View.VISIBLE);
                                                btn_add_advertise.setVisibility(View.GONE);
                                                btn_add_offer.setVisibility(View.GONE);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                callAPI();
                                            }
                                        });
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });
    }

    private void toggle_item( String id, String type, final String state){
        FavouriteAPIsClass.toggle_item(sharedPreferences,
                ProfileActivity.this,
                sharedPreferences.getString("language", "ar"),
                sharedPreferences.getString("service_token", ""),
                id,
                type,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json !=null){
                            if (state.equals("0")){
                                img_love.setImageResource(R.drawable.ic_grey_heart);
                                lovers--;
                                tv_lovers.setText(String.valueOf(lovers));
                                Toast.makeText(ProfileActivity.this, getResources().getString(R.string.favourite_inst_remover), Toast.LENGTH_SHORT).show();
                            }else {
                                img_love.setImageResource(R.drawable.ic_heart);
                                lovers++;
                                tv_lovers.setText(String.valueOf(lovers));
                                Toast.makeText(ProfileActivity.this, getResources().getString(R.string.favourite_inst_add), Toast.LENGTH_SHORT).show();
                            }
                        }else {
                            Toast.makeText(ProfileActivity.this, getResources().getString(R.string.error_occurred), Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Toast.makeText(ProfileActivity.this, getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void callRateAPI(String id, String value, String note, final AlertDialog dialog){
        InstitutionsAPIsClass.rate(sharedPreferences,
                ProfileActivity.this,
                sharedPreferences.getString("language", "ar"),
                sharedPreferences.getString("service_token", ""),
                id,
                value,
                note,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json !=null){
                            Toast.makeText(ProfileActivity.this, getResources().getString(R.string.rate_dialog_success), Toast.LENGTH_SHORT).show();
                            dialog.cancel();
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callAPI();
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });
    }
}
