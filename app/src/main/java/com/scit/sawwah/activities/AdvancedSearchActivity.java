package com.scit.sawwah.activities;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.scit.sawwah.Models.CategoryObject;
import com.scit.sawwah.Models.CityObject;
import com.scit.sawwah.Models.CountryObject;
import com.scit.sawwah.R;
import com.scit.sawwah.adapters.CustomSpinnerAdapter;
import com.scit.sawwah.tools.BaseActivity;

import java.util.ArrayList;
import java.util.List;

public class AdvancedSearchActivity extends BaseActivity {
    private RelativeLayout toolbar;
    private ImageButton btn_back;
    private TextView tv_title;
    private Spinner country_spinner,city_spinner,category_spinner,stars_spinner;
    private CustomSpinnerAdapter country_adapter,city_adapter,category_adapter,stars_adapter;
    private List<CountryObject> country_list;
    private List<CityObject> city_list;
    private List<CategoryObject> category_list;
    private List<String> country_list_string,city_list_string,category_list_string,stars_list_string;
    private TextView btn_search_by_name;
    private CheckBox rb_map,rb_region;
    private SeekBar sb_radius;
    private TextView tv_radius;
    private ImageButton btn_search;
    private String choosed_country_id = "",choosed_city_id = "",choosed_category_id = "",choosed_rating = "";
    private boolean show_on_map = false;
    private boolean search_in_region = false;
    private int radius = -1;

    @Override
    public void set_layout() {
        setContentView(R.layout.activity_advanced_search);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        tv_title.setText(getResources().getString(R.string.advanced_search));
        init_spinners();

    }

    @Override
    public void init_views() {
        toolbar = findViewById(R.id.advanced_search_toolbar);
        btn_back = toolbar.findViewById(R.id.toolbar_back_btn);
        tv_title = toolbar.findViewById(R.id.toolbar_title);
        country_spinner = findViewById(R.id.advanced_search_country_spinner);
        city_spinner = findViewById(R.id.advanced_search_city_spinner);
        category_spinner = findViewById(R.id.advanced_search_category_spinner);
        stars_spinner = findViewById(R.id.advanced_search_rating_spinner);
        btn_search = findViewById(R.id.advanced_search_search_btn);
        btn_search_by_name = findViewById(R.id.advanced_search_name_search_btn);
        rb_map = findViewById(R.id.advanced_search_on_map_check);
        rb_region = findViewById(R.id.advanced_search_region_check);
        sb_radius = findViewById(R.id.advanced_search_radius_seek);
        tv_radius = findViewById(R.id.advanced_search_radius_txt);
        tv_radius.setText("0 "+getResources().getString(R.string.advanced_search_meter));
    }

    @Override
    public void init_events() {
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AdvancedSearchActivity.this,SearchResultsActivity.class));
            }
        });
        country_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int i, long id) {
                long pos = parent.getItemIdAtPosition(i);
                int position = Integer.valueOf(String.valueOf(pos));
                choosed_country_id = String.valueOf(country_list.get(position).getId());
                CountryObject co = country_list.get(position);
                if (co.getCities()!=null){
                    if (co.getCities().size() > 0){
                        city_list.clear();
                        city_list_string.clear();
                        city_adapter = new CustomSpinnerAdapter(AdvancedSearchActivity.this,android.R.layout.simple_spinner_item,city_list_string);
                        city_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        city_spinner.setAdapter(city_adapter);
                        CityObject dump_city = new CityObject();
                        dump_city.setId(-1);
                        dump_city.setName(getResources().getString(R.string.advanced_search_not_set));
                        city_list.add(dump_city);
                        city_list_string.add(dump_city.getName());
                        for (CityObject coo : co.getCities()){
                            city_list.add(coo);
                            city_list_string.add(coo.getName());
                            city_adapter.notifyDataSetChanged();
                        }
                    }else {
                        city_list.clear();
                        city_list_string.clear();
                        city_adapter = new CustomSpinnerAdapter(AdvancedSearchActivity.this,android.R.layout.simple_spinner_item,city_list_string);
                        city_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        city_spinner.setAdapter(city_adapter);
                        CityObject dump_city = new CityObject();
                        dump_city.setId(-1);
                        dump_city.setName(getResources().getString(R.string.advanced_search_not_set));
                        city_list.add(dump_city);
                        city_list_string.add(dump_city.getName());
                        city_adapter.notifyDataSetChanged();
                    }
                }else {
                    city_list.clear();
                    city_list_string.clear();
                    city_adapter = new CustomSpinnerAdapter(AdvancedSearchActivity.this,android.R.layout.simple_spinner_item,city_list_string);
                    city_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    city_spinner.setAdapter(city_adapter);
                    CityObject dump_city = new CityObject();
                    dump_city.setId(-1);
                    dump_city.setName(getResources().getString(R.string.advanced_search_not_set));
                    city_list.add(dump_city);
                    city_list_string.add(dump_city.getName());
                    city_adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        city_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int i, long id) {
                long pos = parent.getItemIdAtPosition(i);
                int position = Integer.valueOf(String.valueOf(pos));
                choosed_city_id = String.valueOf(city_list.get(position).getId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        category_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int i, long id) {
                long pos = parent.getItemIdAtPosition(i);
                int position = Integer.valueOf(String.valueOf(pos));
                choosed_category_id = String.valueOf(category_list.get(position).getId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        stars_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int i, long id) {
                long pos = parent.getItemIdAtPosition(i);
                int position = Integer.valueOf(String.valueOf(pos));
                switch (position){
                    case 0 : {
                        choosed_rating = "-1";
                    }break;
                    case 1 : {
                        choosed_rating = "1";
                    }break;
                    case 2 : {
                        choosed_rating = "2";
                    }break;
                    case 3 : {
                        choosed_rating = "3";
                    }break;
                    case 4 : {
                        choosed_rating = "4";
                    }break;
                    case 5 : {
                        choosed_rating = "5";
                    }break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btn_search_by_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AdvancedSearchActivity.this,QuickSearchActivity.class));
            }
        });
        rb_map.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                show_on_map = isChecked;
            }
        });
        rb_region.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                search_in_region = isChecked;
            }
        });
        sb_radius.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                String progressString = String.valueOf(progress * 100);
                radius = progress*100;
                tv_radius.setText(progressString+" "+getResources().getString(R.string.advanced_search_meter));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AdvancedSearchActivity.this,SearchResultsActivity.class);
                Intent intent1 = new Intent(AdvancedSearchActivity.this,SearchResultsMapActivity.class);
                if (search_in_region){
                    if (radius <= 0){
                        Toast.makeText(AdvancedSearchActivity.this, getResources().getString(R.string.advanced_search_no_radius), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (show_on_map){
                        intent1.putExtra("country_id",choosed_category_id);
                        intent1.putExtra("city_id",choosed_city_id);
                        intent1.putExtra("category_id",choosed_category_id);
                        intent1.putExtra("rating",choosed_rating);
                        intent1.putExtra("radius",radius);
                        startActivity(intent1);
                    }else {
                        intent.putExtra("type","advanced");
                        intent.putExtra("country_id",choosed_category_id);
                        intent.putExtra("city_id",choosed_city_id);
                        intent.putExtra("category_id",choosed_category_id);
                        intent.putExtra("rating",choosed_rating);
                        intent.putExtra("radius",radius);
                        startActivity(intent);
                    }
                }else {
                    if (show_on_map){
                        intent1.putExtra("country_id",choosed_category_id);
                        intent1.putExtra("city_id",choosed_city_id);
                        intent1.putExtra("category_id",choosed_category_id);
                        intent1.putExtra("rating",choosed_rating);
                        intent1.putExtra("radius",-1);
                        startActivity(intent1);
                    }else {
                        intent.putExtra("type","advanced");
                        intent.putExtra("country_id",choosed_category_id);
                        intent.putExtra("city_id",choosed_city_id);
                        intent.putExtra("category_id",choosed_category_id);
                        intent.putExtra("rating",choosed_rating);
                        intent.putExtra("radius",-1);
                        startActivity(intent);
                    }
                }
            }
        });
    }

    @Override
    public void set_fragment_place() {

    }

    private void init_lists(){
        country_list = new ArrayList<>();
        city_list = new ArrayList<>();
        category_list = new ArrayList<>();
        country_list_string = new ArrayList<>();
        city_list_string = new ArrayList<>();
        category_list_string = new ArrayList<>();
        stars_list_string = new ArrayList<>();

        CountryObject dump_country = new CountryObject();
        dump_country.setId(-1);
        dump_country.setName(getResources().getString(R.string.advanced_search_not_set));
        country_list.add(dump_country);
        country_list_string.add(dump_country.getName());

        CountryObject[] countries_array = new Gson().fromJson(sharedPreferences.getString("countries","[]"),CountryObject[].class);
        if (countries_array.length > 0){
            for (CountryObject co : countries_array){
                country_list.add(co);
                country_list_string.add(co.getName());
            }
        }

        CityObject dump_city = new CityObject();
        dump_city.setId(-1);
        dump_city.setName(getResources().getString(R.string.advanced_search_not_set));
        city_list.add(dump_city);
        city_list_string.add(dump_city.getName());

        CategoryObject dump_category = new CategoryObject();
        dump_category.setId(-1);
        dump_category.setName(getResources().getString(R.string.advanced_search_not_set));
        category_list.add(dump_category);
        category_list_string.add(dump_category.getName());

        CategoryObject[] categories_array = new Gson().fromJson(sharedPreferences.getString("categories","[]"),CategoryObject[].class);
        if (categories_array.length > 0){
            for (CategoryObject co : categories_array){
                category_list.add(co);
                category_list_string.add(co.getName());
            }
        }

        stars_list_string.add(getResources().getString(R.string.advanced_search_not_set));
        stars_list_string.add("1");
        stars_list_string.add("2");
        stars_list_string.add("3");
        stars_list_string.add("4");
        stars_list_string.add("5");
    }

    private void init_spinners(){
        init_lists();

        //Country
        country_adapter = new CustomSpinnerAdapter(AdvancedSearchActivity.this,android.R.layout.simple_spinner_item,country_list_string);
        country_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        country_spinner.setAdapter(country_adapter);

        //City
        city_adapter = new CustomSpinnerAdapter(AdvancedSearchActivity.this,android.R.layout.simple_spinner_item,city_list_string);
        city_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        city_spinner.setAdapter(city_adapter);

        //CategoryObject
        category_adapter = new CustomSpinnerAdapter(AdvancedSearchActivity.this,android.R.layout.simple_spinner_item,category_list_string);
        category_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        category_spinner.setAdapter(category_adapter);

        //Stars
        stars_adapter = new CustomSpinnerAdapter(AdvancedSearchActivity.this,android.R.layout.simple_spinner_item,stars_list_string);
        stars_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        stars_spinner.setAdapter(stars_adapter);
    }
}
