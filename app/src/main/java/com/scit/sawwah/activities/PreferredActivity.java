package com.scit.sawwah.activities;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.widget.NestedScrollView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.gson.Gson;
import com.scit.sawwah.APIClasses.FavouriteAPIsClass;
import com.scit.sawwah.Models.FavotiteResponse;
import com.scit.sawwah.Models.InstitutionObject;
import com.scit.sawwah.Models.OfferObject;
import com.scit.sawwah.R;
import com.scit.sawwah.adapters.PreferredOffersAdapter;
import com.scit.sawwah.adapters.PreferredRestsAdapter;
import com.scit.sawwah.interfaces.IFailure;
import com.scit.sawwah.interfaces.IMove;
import com.scit.sawwah.interfaces.IResponse;
import com.scit.sawwah.tools.BaseActivity;
import com.scit.sawwah.tools.BaseRetrofit;

import java.util.ArrayList;
import java.util.List;

public class PreferredActivity extends BaseActivity {
    private RelativeLayout toolbar;
    private TextView tv_title;
    private ImageButton btn_back;
    private Button btn_all_offers;
    private RecyclerView rv_offers,rv_rests;
    private PreferredOffersAdapter offers_adapter;
    private PreferredRestsAdapter rests_adapter;
    private List<OfferObject> offers_list;
    private List<InstitutionObject> rests_list;
    private LinearLayoutManager offers_layout_manager,rests_layout_manager;
    private RelativeLayout root;
    private NestedScrollView scrollView;
    private RelativeLayout lyt_offers,lyt_inst;
    private LinearLayout empty_layout;
    private CallbackManager callbackManager;
    private ShareDialog shareDialog;


    @Override
    public void set_layout() {
        setContentView(R.layout.activity_preferred);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        tv_title.setText(getResources().getString(R.string.preferred_title));
        init_recyclers();
        callAPI();
    }

    @Override
    public void init_views() {
        toolbar = findViewById(R.id.preferred_toolbar);
        tv_title = toolbar.findViewById(R.id.toolbar_title);
        btn_back = toolbar.findViewById(R.id.toolbar_back_btn);
        btn_all_offers = findViewById(R.id.preferred_all_offered);
        rv_offers = findViewById(R.id.preferred_offers_recycler);
        rv_rests = findViewById(R.id.preferred_restaurants_recycler);
        root = findViewById(R.id.preferred_layout);
        lyt_offers = findViewById(R.id.preferred_offers_layout);
        lyt_inst = findViewById(R.id.preferred_institutions_layout);
        scrollView = findViewById(R.id.preferred_scrollview);
        scrollView.setVisibility(View.GONE);
        empty_layout = findViewById(R.id.empty_layout);
    }

    @Override
    public void init_events() {
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btn_all_offers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PreferredActivity.this,AllOffersActivity.class);
                intent.putExtra("filter","-1");
                intent.putExtra("with",false);
                startActivity(intent);
            }
        });
    }

    @Override
    public void set_fragment_place() {

    }

    private void init_recyclers(){
        offers_list = new ArrayList<>();
        rests_list = new ArrayList<>();
        offers_adapter = new PreferredOffersAdapter(PreferredActivity.this, offers_list,new IMove() {
            @Override
            public void move(int position) {
                FacebookSdk.sdkInitialize(PreferredActivity.this.getApplicationContext());
                callbackManager = CallbackManager.Factory.create();
                shareDialog = new ShareDialog(PreferredActivity.this);
                shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
                    @Override
                    public void onSuccess(Sharer.Result result) {
                        Toast.makeText(PreferredActivity.this, "Posted successfully", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onCancel() {
                        Toast.makeText(PreferredActivity.this, "Posted canceled", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(FacebookException error) {
                        Toast.makeText(PreferredActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
                ShareLinkContent linkContent = new ShareLinkContent.Builder().setQuote(getResources().getString(R.string.facebook_post_offer)).
                        setContentUrl(Uri.parse(BaseRetrofit.BASE_ALI+"offers/"+String.valueOf(offers_list.get(position).getId()))).build();
                if (shareDialog.canShow(ShareLinkContent.class)){
                    shareDialog.show(linkContent);
                }else {
                    Toast.makeText(PreferredActivity.this, "can't show", Toast.LENGTH_SHORT).show();
                }
            }
        });
        offers_layout_manager = new LinearLayoutManager(PreferredActivity.this,LinearLayoutManager.HORIZONTAL,false);
        rv_offers.setLayoutManager(offers_layout_manager);
        rv_offers.setAdapter(offers_adapter);

        rests_adapter = new PreferredRestsAdapter(PreferredActivity.this, rests_list,new IMove() {
            @Override
            public void move(int position) {

            }
        });
        rests_layout_manager = new LinearLayoutManager(PreferredActivity.this,LinearLayoutManager.VERTICAL,false);
        rv_rests.setLayoutManager(rests_layout_manager);
        rv_rests.setAdapter(rests_adapter);
    }

    private void callAPI(){
        FavouriteAPIsClass.getFavourite(sharedPreferences,
                PreferredActivity.this,
                sharedPreferences.getString("language", "ar"),
                sharedPreferences.getString("service_token", ""),
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json != null){
                            String json1 = new Gson().toJson(json);
                            FavotiteResponse success = new Gson().fromJson(json1,FavotiteResponse.class);
                            //Offers
                            if (success.getOffers() != null){
                                if (success.getOffers().size() > 0){
                                    empty_layout.setVisibility(View.GONE);
                                    for (OfferObject oo : success.getOffers()){
                                        offers_list.add(oo);
                                        offers_adapter.notifyDataSetChanged();
                                    }
                                    runAnimationOffers(rv_offers,0,offers_adapter);
                                    scrollView.setVisibility(View.VISIBLE);
                                }else {
                                    Toast.makeText(PreferredActivity.this, getResources().getString(R.string.no_data), Toast.LENGTH_SHORT).show();
                                    lyt_offers.setVisibility(View.GONE);
                                    rv_offers.setVisibility(View.GONE);
                                }
                            }else {
                                Toast.makeText(PreferredActivity.this, getResources().getString(R.string.no_data), Toast.LENGTH_SHORT).show();
                                lyt_offers.setVisibility(View.GONE);
                                rv_offers.setVisibility(View.GONE);
                            }
                            //Institutions
                            if (success.getFacilities() != null){
                                if (success.getFacilities().size() > 0){
                                    empty_layout.setVisibility(View.GONE);
                                    for (InstitutionObject io : success.getFacilities()){
                                        rests_list.add(io);
                                        rests_adapter.notifyDataSetChanged();
                                    }
                                    runAnimationRests(rv_rests,0,rests_adapter);
                                }else {
                                    lyt_inst.setVisibility(View.GONE);
                                    rv_rests.setVisibility(View.GONE);
                                }
                            }else {
                                lyt_inst.setVisibility(View.GONE);
                                rv_rests.setVisibility(View.GONE);
                            }
                        }
                        scrollView.setVisibility(View.VISIBLE);
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callAPI();
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });
    }

    public void runAnimationOffers(RecyclerView recyclerView,int type,PreferredOffersAdapter adapter){
        Context context = recyclerView.getContext();
        LayoutAnimationController controller = null;
        if (type == 0){ // Fall down animation
            controller = AnimationUtils.loadLayoutAnimation(context,R.anim.layout_fall_down);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutAnimation(controller);
            recyclerView.getAdapter().notifyDataSetChanged();
            recyclerView.scheduleLayoutAnimation();
        }
    }
    public void runAnimationRests(RecyclerView recyclerView,int type,PreferredRestsAdapter adapter){
        Context context = recyclerView.getContext();
        LayoutAnimationController controller = null;
        if (type == 0){ // Fall down animation
            controller = AnimationUtils.loadLayoutAnimation(context,R.anim.layout_fall_down_vertical);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutAnimation(controller);
            recyclerView.getAdapter().notifyDataSetChanged();
            recyclerView.scheduleLayoutAnimation();
        }
    }
}
