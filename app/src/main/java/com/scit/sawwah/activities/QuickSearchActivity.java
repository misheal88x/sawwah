package com.scit.sawwah.activities;

import android.content.Intent;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.scit.sawwah.R;
import com.scit.sawwah.tools.BaseActivity;

public class QuickSearchActivity extends BaseActivity {
    private RelativeLayout toolbar;
    private ImageButton btn_back;
    private TextView tv_title;
    private EditText edt_word;
    private FloatingActionButton fab_search;

    @Override
    public void set_layout() {
        setContentView(R.layout.activity_quick_search);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        tv_title.setText(getResources().getString(R.string.quick_search_title));
    }

    @Override
    public void init_views() {
        toolbar = findViewById(R.id.quick_search_toolbar);
        btn_back = toolbar.findViewById(R.id.toolbar_back_btn);
        tv_title = toolbar.findViewById(R.id.toolbar_title);
        fab_search = findViewById(R.id.quick_search_btn);
        edt_word = findViewById(R.id.quick_search_word);
    }

    @Override
    public void init_events() {
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        fab_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt_word.getText().toString().equals("")){
                    Toast.makeText(QuickSearchActivity.this, getResources().getString(R.string.quick_search_no_word), Toast.LENGTH_SHORT).show();
                    return;
                }
                Intent intent = new Intent(QuickSearchActivity.this,SearchResultsActivity.class);
                intent.putExtra("type","quick");
                intent.putExtra("word",edt_word.getText().toString());
                startActivity(intent);
            }
        });
    }

    @Override
    public void set_fragment_place() {

    }
}
