package com.scit.sawwah.activities;

import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.scit.sawwah.APIClasses.AccountsAPIsClass;
import com.scit.sawwah.R;
import com.scit.sawwah.interfaces.IFailure;
import com.scit.sawwah.interfaces.IResponse;
import com.scit.sawwah.tools.BaseActivity;

public class EditPasswordActivity extends BaseActivity {
    private RelativeLayout toolbar;
    private ImageButton btn_back;
    private EditText edt_new_password,edt_old_password,edt_confirm;
    private ImageView img_new_password,img_old_password,img_confirm;
    private LinearLayout btn_save;
    private int newPasswordState = 0;
    private int oldPasswordState = 0;
    private int confirmPasswordState = 0;
    private LinearLayout root;


    @Override
    public void set_layout() {
        setContentView(R.layout.activity_edit_password);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {

    }

    @Override
    public void init_views() {
        toolbar = findViewById(R.id.edit_pass_toolbar);
        btn_back = toolbar.findViewById(R.id.toolbar_back_btn);
        edt_new_password = findViewById(R.id.edit_pass_new_password_edt);
        edt_old_password = findViewById(R.id.edit_pass_old_password_edt);
        edt_confirm = findViewById(R.id.edit_pass_confirm_edt);
        img_new_password = findViewById(R.id.edit_pass_new_password_eye);
        img_old_password = findViewById(R.id.edit_pass_old_password_eye);
        img_confirm = findViewById(R.id.edit_pass_confirm_eye);
        btn_save = findViewById(R.id.edit_pass_save_btn);
        root = findViewById(R.id.edit_pass_layout);
    }

    @Override
    public void init_events() {
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        img_new_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (newPasswordState == 0){
                    newPasswordState = 1;
                    img_new_password.setImageResource(R.drawable.ic_white_eye);
                    edt_new_password.setInputType(1);
                }else {
                    newPasswordState = 0;
                    img_new_password.setImageResource(R.drawable.ic_white_hidden_eye);
                    edt_new_password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                }
            }
        });
        img_old_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (oldPasswordState == 0){
                    oldPasswordState = 1;
                    img_old_password.setImageResource(R.drawable.ic_white_eye);
                    edt_old_password.setInputType(1);
                }else {
                    oldPasswordState = 0;
                    img_old_password.setImageResource(R.drawable.ic_white_hidden_eye);
                    edt_old_password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                }
            }
        });
        img_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (confirmPasswordState == 0){
                    confirmPasswordState = 1;
                    img_confirm.setImageResource(R.drawable.ic_white_eye);
                    edt_confirm.setInputType(1);
                }else {
                    confirmPasswordState = 0;
                    img_confirm.setImageResource(R.drawable.ic_white_hidden_eye);
                    edt_confirm.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                }
            }
        });
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt_new_password.getText().toString().equals("")){
                    Toast.makeText(EditPasswordActivity.this, getResources().getString(R.string.edit_pass_no_new_password), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (edt_old_password.getText().toString().equals("")){
                    Toast.makeText(EditPasswordActivity.this, getResources().getString(R.string.edit_pass_no_old_password), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (edt_confirm.getText().toString().equals("")){
                    Toast.makeText(EditPasswordActivity.this, getResources().getString(R.string.edit_pass_no_confirm), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!edt_confirm.getText().toString().equals(edt_new_password.getText().toString())){
                    Toast.makeText(EditPasswordActivity.this, getResources().getString(R.string.edit_pass_no_match), Toast.LENGTH_SHORT).show();
                    return;
                }
                callAPI();
            }
        });
    }

    @Override
    public void set_fragment_place() {

    }

    private void callAPI(){
        AccountsAPIsClass.editPassword(sharedPreferences,
                EditPasswordActivity.this,
                sharedPreferences.getString("language", "ar"),
                sharedPreferences.getString("service_token", ""),
                edt_new_password.getText().toString(),
                edt_old_password.getText().toString(),
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json != null){
                            Toast.makeText(EditPasswordActivity.this, getResources().getString(R.string.edit_pass_success), Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callAPI();
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });
    }
}
