package com.scit.sawwah.activities;

import android.content.Intent;
import android.graphics.Color;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.rm.rmswitch.RMSwitch;
import com.scit.sawwah.APIClasses.PackagesAPIsClass;
import com.scit.sawwah.R;
import com.scit.sawwah.interfaces.IFailure;
import com.scit.sawwah.interfaces.IResponse;
import com.scit.sawwah.tools.BaseActivity;

public class ActivePricePackageActivity extends BaseActivity {
    private RelativeLayout toolbar;
    private ImageButton btn_back;
    private TextView tv_title,tv_name,tv_active;
    private RMSwitch sw_active;
    private Button btn_back1;
    private LinearLayout root;
    private Intent myIntent;

    @Override
    public void set_layout() {
        setContentView(R.layout.activity_active_price_package);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        tv_title.setText(getResources().getString(R.string.prices_packages_title));
        setActiveSwitchColors();
        if (myIntent.getStringExtra("package_name")!=null){
            tv_name.setText(myIntent.getStringExtra("package_name"));
        }
        switch (myIntent.getIntExtra("package_status",0)){
            case -1:
            case 0:
            case 1:
            {
                tv_active.setText(getResources().getString(R.string.settings_dis_active));
                sw_active.setChecked(false);
            }break;
            case 2 : {
                tv_active.setText(getResources().getString(R.string.settings_active));
                sw_active.setChecked(true);
            }
        }
    }

    @Override
    public void init_views() {
        toolbar = findViewById(R.id.active_package_toolbar);
        btn_back = toolbar.findViewById(R.id.toolbar_back_btn);
        tv_title = toolbar.findViewById(R.id.toolbar_title);
        sw_active = findViewById(R.id.active_package_switch);
        tv_name = findViewById(R.id.active_package_name);
        tv_active = findViewById(R.id.active_package_active);
        btn_back1 = findViewById(R.id.active_package_back_btn);
        root = findViewById(R.id.active_package_layout);
        myIntent = getIntent();
    }

    @Override
    public void init_events() {
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btn_back1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        sw_active.addSwitchObserver(new RMSwitch.RMSwitchObserver() {
            @Override
            public void onCheckStateChange(RMSwitch switchView, boolean isChecked) {
                if (isChecked){
                    callRenewAPI();
                }
            }
        });
    }

    @Override
    public void set_fragment_place() {

    }

    private void setActiveSwitchColors() {
        sw_active.setSwitchBkgCheckedColor(getResources().getColor(R.color.colorTextSecobdary));
        sw_active.setSwitchToggleCheckedColor(Color.WHITE);
        sw_active.setSwitchToggleNotCheckedColor(Color.WHITE);
    }
    
    private void callRenewAPI(){
        PackagesAPIsClass.renew(sharedPreferences,
                ActivePricePackageActivity.this,
                sharedPreferences.getString("language", "ar"),
                sharedPreferences.getString("service_token", ""),
                String.valueOf(myIntent.getIntExtra("package_id", 0)),
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json != null){
                            String json1 = new Gson().toJson(json);
                            boolean success = new Gson().fromJson(json1,Boolean.class);
                            if (success){
                                Toast.makeText(ActivePricePackageActivity.this, getResources().getString(R.string.confirm_package_order_renew_success), Toast.LENGTH_SHORT).show();
                            }else if (!success){
                                Toast.makeText(ActivePricePackageActivity.this, getResources().getString(R.string.confirm_package_order_renew_failed), Toast.LENGTH_SHORT).show();
                            }
                            finish();
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callRenewAPI();
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });
    }
}
