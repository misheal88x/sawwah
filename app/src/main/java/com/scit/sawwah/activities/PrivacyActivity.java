package com.scit.sawwah.activities;

import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.scit.sawwah.APIClasses.BasicAPIsClass;
import com.scit.sawwah.R;
import com.scit.sawwah.interfaces.IFailure;
import com.scit.sawwah.interfaces.IResponse;
import com.scit.sawwah.tools.BaseActivity;

public class PrivacyActivity extends BaseActivity {
    private RelativeLayout toolbar;
    private ImageButton btn_back;
    private TextView tv_title;
    private WebView web;
    @Override
    public void set_layout() {
        setContentView(R.layout.activity_privecy);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        tv_title.setText(getResources().getString(R.string.privacy_policy));
        web.setWebViewClient(new WebViewClient());
        callAPI();
    }

    @Override
    public void init_views() {
        toolbar = findViewById(R.id.privacy_toolbar);
        btn_back = toolbar.findViewById(R.id.toolbar_back_btn);
        tv_title = toolbar.findViewById(R.id.toolbar_title);
        web = findViewById(R.id.privacy_web);

    }

    @Override
    public void init_events() {
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void set_fragment_place() {

    }
    private void callAPI(){
        BasicAPIsClass.policy(sharedPreferences,
                PrivacyActivity.this,
                sharedPreferences.getString("language", "ar"),
                sharedPreferences.getString("service_token", ""),
                "policy",
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json != null){
                            String json1 = new Gson().toJson(json);
                            String success = new Gson().fromJson(json1,String.class);
                            web.loadData(success, "text/html", "UTF-8");
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {

                    }
                });
    }
}
