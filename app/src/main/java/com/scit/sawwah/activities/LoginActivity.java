package com.scit.sawwah.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import androidx.annotation.NonNull;
import com.google.android.material.snackbar.Snackbar;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.gson.Gson;
import com.scit.sawwah.APIClasses.BasicAPIsClass;
import com.scit.sawwah.MainActivity;
import com.scit.sawwah.Models.AccountSettingsObject;
import com.scit.sawwah.Models.DeviceSettingsObject;
import com.scit.sawwah.Models.LoginResponse;
import com.scit.sawwah.R;
import com.scit.sawwah.interfaces.IFailure;
import com.scit.sawwah.interfaces.IResponse;
import com.scit.sawwah.tools.BaseActivity;
import com.scit.sawwah.tools.BaseFunctions;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

public class LoginActivity extends BaseActivity implements GoogleApiClient.OnConnectionFailedListener{
    private RelativeLayout toolbar,btn_google_register,btn_facebook_register,btn_visitor;
    private ImageButton btn_back;
    private TextView tv_title;
    private Button btn_register,btn_forget_password;
    private RelativeLayout btn_login;
    private EditText edt_name,edt_password;
    private ImageView img_password_eye,img_login_facebook,img_login_google;
    private int password_eye_state = 0;
    private LinearLayout root;

    private FirebaseAuth auth;
    private GoogleSignInOptions signInOptions;
    private static final int RC_SIGN_IN = 2;
    private GoogleApiClient mGoogleApiClient;
    private FirebaseAuth.AuthStateListener authStateListener;

    private CallbackManager callbackManager;

    @Override
    protected void onStart() {
        super.onStart();
        auth.addAuthStateListener(authStateListener);
    }

    @Override
    public void set_layout() {
        FacebookSdk.sdkInitialize(this.getApplicationContext());
        setContentView(R.layout.activity_login);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        facebook_init();
        google_init();
    }

    @Override
    public void init_views() {
        toolbar = findViewById(R.id.login_toolbar);
        btn_back = toolbar.findViewById(R.id.toolbar_back_btn);
        tv_title = toolbar.findViewById(R.id.toolbar_title);
        btn_register = findViewById(R.id.login_register);
        btn_forget_password = findViewById(R.id.login_forget_password);
        btn_facebook_register = findViewById(R.id.register_facebook_btn);
        btn_google_register = findViewById(R.id.register_google_btn);
        btn_login = findViewById(R.id.login_login_btn);
        btn_visitor = findViewById(R.id.register_visitor_btn);
        edt_name = findViewById(R.id.login_phone_edt);
        edt_password = findViewById(R.id.login_password_edt);
        img_password_eye = findViewById(R.id.login_password_icon);
        root = findViewById(R.id.login_layout);
        img_login_facebook = findViewById(R.id.login_facebook_icon);
        img_login_google = findViewById(R.id.login_google_icon);
    }

    @Override
    public void init_events() {
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btn_forget_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this,ForgetPasswordActivity.class));
            }
        });
        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this,RegisterActivity.class));
            }
        });
        btn_facebook_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this,
                        Arrays.asList("public_profile", "email", "user_birthday", "user_friends"));
            }
        });
        btn_google_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                googlesignInMethod();
                //startActivity(new Intent(LoginActivity.this,ChooseAccountTypeActivity.class));
            }
        });
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt_name.getText().toString().equals("")){
                    Toast.makeText(LoginActivity.this, getResources().getString(R.string.login_no_name), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (edt_name.getText().toString().length()<10){
                    Toast.makeText(LoginActivity.this, getResources().getString(R.string.register_error_mobile), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!BaseFunctions.subString(edt_name.getText().toString(),0,2).equals("09")){
                    Toast.makeText(LoginActivity.this, getResources().getString(R.string.register_error_mobile2), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (edt_password.getText().toString().equals("")){
                    Toast.makeText(LoginActivity.this, getResources().getString(R.string.register_no_password), Toast.LENGTH_SHORT).show();
                    return;
                }
                callLoginAPI();
            }
        });
        btn_visitor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.putBoolean("is_visitor",true);
                editor.commit();
                startActivity(new Intent(LoginActivity.this,MainActivity.class));
            }
        });

        img_password_eye.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (password_eye_state == 0){
                    password_eye_state = 1;
                    img_password_eye.setImageResource(R.drawable.ic_white_eye);
                    edt_password.setInputType(1);
                }else {
                    password_eye_state = 0;
                    img_password_eye.setImageResource(R.drawable.ic_white_hidden_eye);
                    edt_password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                }
            }
        });
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {}

    @Override
    public void set_fragment_place() {

    }

    private void facebook_init(){
        //FacebookSdk.sdkInitialize(this.getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {}

                    @Override
                    public void onCancel() {}

                    @Override
                    public void onError(FacebookException exception) {}});
    }

    private void google_init(){
        auth = FirebaseAuth.getInstance();

         signInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                 .requestIdToken(getString(R.string.default_web_client_id))
                 .requestServerAuthCode(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if (firebaseAuth.getCurrentUser()!= null){
                    //this is called when you already signed

                }else {

                }
            }
        };
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this,this)
                .addApi(Auth.GOOGLE_SIGN_IN_API,signInOptions).build();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode,resultCode,data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult task = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (task.isSuccess()){
                GoogleSignInAccount account = task.getSignInAccount();
                firebaseAuthWithGoogle(account);
                account.getServerAuthCode();
                Uri personPhoto = account.getPhotoUrl();
                String code = account.getIdToken();
                Log.i("google_token", "onActivityResult: "+code);
                checkGoogle(code,personPhoto.toString());
            }else {
                Toast.makeText(this, getResources().getString(R.string.login_proxy), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount account) {
        AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);
        auth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("TAG", "signInWithCredential:success");
                            FirebaseUser user = auth.getCurrentUser();
                            //updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w("TAG", "signInWithCredential:failure", task.getException());
                            //Toast.makeText(LoginActivity.this, "Authentication Failed.", Toast.LENGTH_SHORT).show();
                            //updateUI(null);
                        }
                    }
                });
    }

    private void googlesignInMethod() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    AccessTokenTracker tokenTracker = new AccessTokenTracker() {
        @Override
        protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
            if (currentAccessToken==null){
                Toast.makeText(LoginActivity.this, "User logged out", Toast.LENGTH_SHORT).show();
            }else {
                getFacebookInfos(currentAccessToken);
            }
        }
    };

    private void getFacebookInfos(final AccessToken accessToken){
        GraphRequest request = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                String FEmail = "";
                String FAccessToken = "";
                String FId = "";
                String FImage = "";
                try {
                    FEmail = object.getString("email");
                    FAccessToken = accessToken.getToken();
                    FId = object.getString("id");
                    FImage = "https://graph.facebook.com/"+FId+"/picture?type=normal";
                    Log.i("facebook_token", "onCompleted: "+FAccessToken);
                    checkFacebook(FAccessToken,FImage);
                    /*
                    Intent intent = new Intent(LoginActivity.this,ChooseAccountTypeActivity.class);
                    intent.putExtra("type","facebook");
                    intent.putExtra("image",FImage);
                    intent.putExtra("token",FAccessToken);
                    startActivity(intent);
                    **/
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields","email,id");
        request.setParameters(parameters);
        request.executeAsync();
    }


    private void callLoginAPI(){
        String android_id = Settings.Secure.getString(LoginActivity.this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        Log.i("service_token", "callLoginAPI: "+sharedPreferences.getString("service_token", ""));
        Log.i("device_id", "callLoginAPI: "+android_id);
        BasicAPIsClass.login(sharedPreferences,
                LoginActivity.this,
                sharedPreferences.getString("language", "ar"),
                sharedPreferences.getString("service_token", ""),
                android_id,
                edt_name.getText().toString(),
                edt_password.getText().toString(),
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json != null){
                            String json1 = new Gson().toJson(json);
                            LoginResponse success = new Gson().fromJson(json1,LoginResponse.class);
                            //Id
                            editor.putInt("account_id",success.getId());
                            //Service token
                            editor.putString("service_token",success.getService_token());
                            //Image
                            if (success.getImage()!=null){
                                editor.putString("account_image",success.getImage());
                            }
                            //name
                            if (success.getName()!=null){
                                editor.putString("account_name",success.getName());
                            }
                            //User name
                            if (success.getUsername()!=null){
                                editor.putString("account_user_name",success.getUsername());
                            }
                            //Account type
                            if (success.getAccount_type_id()!= 0){
                                if (success.getAccount_type_id() == 1){
                                    editor.putString("account_type","customer");
                                }else {
                                    editor.putString("account_type","owner");
                                }
                            }
                            //Account status
                            editor.putInt("account_status",success.getStatus());
                            //Access Token
                            if (success.getAccess_token()!=null){
                                editor.putString("access_token",success.getAccess_token());
                                Log.i("access_token", "onResponse: "+success.getAccess_token());
                            }
                            //My Institutions
                            if (success.getMy_facilities()!=null){
                                if (success.getMy_facilities().size() > 0){
                                    String insts_json = new Gson().toJson(success.getMy_facilities());
                                    editor.putString("my_insts",insts_json);
                                }
                            }
                            //My Packages
                            if(success.getMy_packages()!=null){
                                if (success.getMy_facilities().size()>0){
                                    String packages_json = new Gson().toJson(success.getMy_packages());
                                    editor.putString("my_packages",packages_json);
                                }
                            }
                            editor.commit();
                            //Device Settings
                            if (success.getDevice_setting()!=null){
                                DeviceSettingsObject dso = success.getDevice_setting();
                                //City Id
                                editor.putInt("settings_city_id",dso.getCity_id());
                                //Country Id
                                editor.putInt("settings_country_id",dso.getCity_id());
                                //Language
                                editor.putString("language", BaseFunctions.numberToLanguageConverter(String.valueOf(dso.getView_language_id())));
                                //Alert
                                editor.putBoolean("settings_alerts",BaseFunctions.stringNumberToBoolean(String.valueOf(dso.getAllow_global_notification())));
                                editor.commit();
                            }
                            //Account Settings
                            if (success.getAccount_settings()!=null){
                                AccountSettingsObject aso = success.getAccount_settings();
                                //Profile Completed
                                if (aso.getIs_profile_completed() == 1){
                                    if (aso.getUser_type() == 1){
                                        editor.putString("account_type","customer");
                                    }else if (aso.getUser_type() == 2){
                                        editor.putString("account_type","owner");
                                    }
                                    editor.putInt("account_status",aso.getAccount_status());
                                    editor.putBoolean("is_visitor",false);
                                    editor.commit();
                                    startActivity(new Intent(LoginActivity.this,MainActivity.class));
                                    finish();
                                }else {
                                    //User type
                                    if (aso.getUser_type() == 1){
                                        editor.putString("account_type","customer");
                                        editor.putBoolean("is_visitor",false);
                                        editor.commit();
                                        startActivity(new Intent(LoginActivity.this,MainActivity.class));
                                        finish();
                                    }else if (aso.getUser_type() == 2){
                                        editor.putString("account_type","owner");
                                        editor.putBoolean("is_visitor",false);
                                        editor.commit();
                                        startActivity(new Intent(LoginActivity.this,RegisterInstitutionActivity.class));
                                        finish();
                                    }
                                }
                            }
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callLoginAPI();
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });
    }

    private void checkFacebook(final String access_token, final String image){
        BaseFunctions.setGlideDrawableImage(LoginActivity.this,img_login_facebook,R.drawable.loading_indicator);
        BasicAPIsClass.facebookCheck(sharedPreferences,
                LoginActivity.this,
                sharedPreferences.getString("language", "ar"),
                sharedPreferences.getString("service_token", ""),
                access_token,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        img_login_facebook.setImageResource(R.drawable.ic_blue_facebook);
                        if (json != null){
                            String json1 = new Gson().toJson(json);
                            int success = new Gson().fromJson(json1,int.class);
                            if (success == -1){
                                Intent intent = new Intent(LoginActivity.this,ChooseAccountTypeActivity.class);
                                intent.putExtra("type","facebook");
                                intent.putExtra("image",image);
                                intent.putExtra("token",access_token);
                                startActivity(intent);
                                finish();
                            }else {
                                callFacebookAPI(String.valueOf(success),image,access_token);
                            }
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        img_login_facebook.setImageResource(R.drawable.ic_blue_facebook);
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        checkFacebook(access_token,image);
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });
    }

    private void checkGoogle(final String access_token, final String image){
        BaseFunctions.setGlideDrawableImage(LoginActivity.this,img_login_google,R.drawable.loading_indicator);
        BasicAPIsClass.googleCheck(sharedPreferences,
                LoginActivity.this,
                sharedPreferences.getString("language", "ar"),
                sharedPreferences.getString("service_token", ""),
                access_token,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        img_login_google.setImageResource(R.drawable.ic_red_google);
                        if (json != null){
                            String json1 = new Gson().toJson(json);
                            int success = new Gson().fromJson(json1,int.class);
                            if (success == -1){
                                Intent intent = new Intent(LoginActivity.this,ChooseAccountTypeActivity.class);
                                intent.putExtra("type","google");
                                intent.putExtra("image",image);
                                intent.putExtra("token",access_token);
                                startActivity(intent);
                                finish();
                            }else {
                                callGoogleAPI(String.valueOf(success),image,access_token);
                            }
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        img_login_google.setImageResource(R.drawable.ic_red_google);
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        checkGoogle(access_token,image);
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });
    }

    private void callFacebookAPI(final String account_type, final String image, final String access_token){
        BaseFunctions.setGlideDrawableImage(LoginActivity.this,img_login_facebook,R.drawable.loading_indicator);
        String android_id = Settings.Secure.getString(LoginActivity.this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        Log.i("infooo", "callFacebookAPI: "+sharedPreferences.getString("service_token", ""));
        BasicAPIsClass.facebookRegister(sharedPreferences,
                LoginActivity.this,
                sharedPreferences.getString("language", "ar"),
                sharedPreferences.getString("service_token", ""),
                android_id,
                account_type,
                image,
                access_token,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        img_login_facebook.setImageResource(R.drawable.ic_blue_facebook);
                        if (json != null){
                            try {
                                String json1 = new Gson().toJson(json);
                                LoginResponse success = new Gson().fromJson(json1,LoginResponse.class);
                                //Id
                                editor.putInt("account_id",success.getId());
                                //Service token
                                editor.putString("service_token",success.getService_token());
                                //Image
                                if (success.getImage()!=null){
                                    editor.putString("account_image",success.getImage());
                                }
                                //name
                                if (success.getName()!=null){
                                    editor.putString("account_name",success.getName());
                                }
                                //User name
                                if (success.getUsername()!=null){
                                    editor.putString("account_user_name",success.getUsername());
                                }
                                //Account type
                                if (success.getAccount_type_id()!= 0){
                                    if (success.getAccount_type_id() == 1){
                                        editor.putString("account_type","customer");
                                    }else {
                                        editor.putString("account_type","owner");
                                    }
                                }
                                //Account status
                                editor.putInt("account_status",success.getStatus());
                                //Access Token
                                if (success.getAccess_token()!=null){
                                    editor.putString("access_token",success.getAccess_token());
                                    Log.i("infooo", "callFacebookAPI: "+success.getAccess_token());
                                }
                                editor.commit();
                                //Device Settings
                                if (success.getDevice_setting()!=null){
                                    DeviceSettingsObject dso = success.getDevice_setting();
                                    //City Id
                                    editor.putInt("settings_city_id",dso.getCity_id());
                                    //Country Id
                                    editor.putInt("settings_country_id",dso.getCity_id());
                                    //Language
                                    editor.putString("language", BaseFunctions.numberToLanguageConverter(String.valueOf(dso.getView_language_id())));
                                    //Alert
                                    editor.putBoolean("settings_alerts",BaseFunctions.stringNumberToBoolean(String.valueOf(dso.getAllow_global_notification())));
                                    editor.commit();
                                }
                                //My Institutions
                                if (success.getMy_facilities()!=null){
                                    if (success.getMy_facilities().size() > 0){
                                        String insts_json = new Gson().toJson(success.getMy_facilities());
                                        editor.putString("my_insts",insts_json);
                                    }
                                }
                                //My Packages
                                if(success.getMy_packages()!=null){
                                    if (success.getMy_facilities().size()>0){
                                        String packages_json = new Gson().toJson(success.getMy_packages());
                                        editor.putString("my_packages",packages_json);
                                    }
                                }
                                //Account Settings
                                if (success.getAccount_settings()!=null){
                                    AccountSettingsObject aso = success.getAccount_settings();
                                    //Profile Completed
                                    if (aso.getIs_profile_completed() == 1){
                                        if (aso.getUser_type() == 1){
                                            editor.putString("account_type","customer");
                                        }else if (aso.getUser_type() == 2){
                                            editor.putString("account_type","owner");
                                        }
                                        editor.putInt("account_status",aso.getAccount_status());
                                        editor.putBoolean("is_visitor",false);
                                        editor.commit();
                                        startActivity(new Intent(LoginActivity.this,MainActivity.class));
                                        finish();
                                    }else {
                                        //User type
                                        if (aso.getUser_type() == 1){
                                            editor.putString("account_type","customer");
                                            editor.putBoolean("is_visitor",false);
                                            editor.commit();
                                            startActivity(new Intent(LoginActivity.this,MainActivity.class));
                                            finish();
                                        }else if (aso.getUser_type() == 2){
                                            editor.putString("account_type","owner");
                                            editor.putBoolean("is_visitor",false);
                                            editor.commit();
                                            startActivity(new Intent(LoginActivity.this,RegisterInstitutionActivity.class));
                                            finish();
                                        }
                                    }
                                }
                            }catch (Exception e){
                                Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                        .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                callFacebookAPI(account_type,image,access_token);
                                            }
                                        }).setActionTextColor(getResources().getColor(R.color.white)).show();
                            }

                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        img_login_facebook.setImageResource(R.drawable.ic_blue_facebook);
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callFacebookAPI(account_type,image,access_token);
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });
    }
    private void callGoogleAPI(final String account_type, final String image, final String access_token){
        BaseFunctions.setGlideDrawableImage(LoginActivity.this,img_login_google,R.drawable.loading_indicator);
        String android_id = Settings.Secure.getString(LoginActivity.this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        BasicAPIsClass.googleRegister(sharedPreferences,
                LoginActivity.this,
                sharedPreferences.getString("language", "ar"),
                sharedPreferences.getString("service_token", ""),
                android_id,
                account_type,
                image,
                access_token,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        img_login_google.setImageResource(R.drawable.ic_red_google);
                        if (json != null){
                            try{
                                String json1 = new Gson().toJson(json);
                                LoginResponse success = new Gson().fromJson(json1,LoginResponse.class);
                                //Id
                                editor.putInt("account_id",success.getId());
                                //Service token
                                editor.putString("service_token",success.getService_token());
                                //Image
                                if (success.getImage()!=null){
                                    editor.putString("account_image",success.getImage());
                                }
                                //name
                                if (success.getName()!=null){
                                    editor.putString("account_name",success.getName());
                                }
                                //User name
                                if (success.getUsername()!=null){
                                    editor.putString("account_user_name",success.getUsername());
                                }
                                //Account type
                                if (success.getAccount_type_id()!= 0){
                                    if (success.getAccount_type_id() == 1){
                                        editor.putString("account_type","customer");
                                    }else {
                                        editor.putString("account_type","owner");
                                    }
                                }
                                //Account status
                                editor.putInt("account_status",success.getStatus());
                                //Access Token
                                if (success.getAccess_token()!=null){
                                    editor.putString("access_token",success.getAccess_token());
                                }
                                editor.commit();
                                //Device Settings
                                if (success.getDevice_setting()!=null){
                                    DeviceSettingsObject dso = success.getDevice_setting();
                                    //City Id
                                    editor.putInt("settings_city_id",dso.getCity_id());
                                    //Country Id
                                    editor.putInt("settings_country_id",dso.getCity_id());
                                    //Language
                                    editor.putString("language", BaseFunctions.numberToLanguageConverter(String.valueOf(dso.getView_language_id())));
                                    //Alert
                                    editor.putBoolean("settings_alerts",BaseFunctions.stringNumberToBoolean(String.valueOf(dso.getAllow_global_notification())));
                                    editor.commit();
                                }
                                //My Institutions
                                if (success.getMy_facilities()!=null){
                                    if (success.getMy_facilities().size() > 0){
                                        String insts_json = new Gson().toJson(success.getMy_facilities());
                                        editor.putString("my_insts",insts_json);
                                    }
                                }
                                //My Packages
                                if(success.getMy_packages()!=null){
                                    if (success.getMy_facilities().size()>0){
                                        String packages_json = new Gson().toJson(success.getMy_packages());
                                        editor.putString("my_packages",packages_json);
                                    }
                                }
                                //Account Settings
                                if (success.getAccount_settings()!=null){
                                    AccountSettingsObject aso = success.getAccount_settings();
                                    //Profile Completed
                                    if (aso.getIs_profile_completed() == 1){
                                        if (aso.getUser_type() == 1){
                                            editor.putString("account_type","customer");
                                        }else if (aso.getUser_type() == 2){
                                            editor.putString("account_type","owner");
                                        }
                                        editor.putInt("account_status",aso.getAccount_status());
                                        editor.putBoolean("is_visitor",false);
                                        editor.commit();
                                        startActivity(new Intent(LoginActivity.this,MainActivity.class));
                                        finish();
                                    }else {
                                        //User type
                                        if (aso.getUser_type() == 1){
                                            editor.putString("account_type","customer");
                                            editor.putBoolean("is_visitor",false);
                                            editor.commit();
                                            startActivity(new Intent(LoginActivity.this,MainActivity.class));
                                            finish();
                                        }else if (aso.getUser_type() == 2){
                                            editor.putString("account_type","owner");
                                            editor.putBoolean("is_visitor",false);
                                            editor.commit();
                                            startActivity(new Intent(LoginActivity.this,RegisterInstitutionActivity.class));
                                            finish();
                                        }
                                    }
                                }
                            }catch (Exception e){
                                Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                        .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                callFacebookAPI(account_type,image,access_token);
                                            }
                                        }).setActionTextColor(getResources().getColor(R.color.white)).show();
                            }
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        img_login_google.setImageResource(R.drawable.ic_red_google);
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callFacebookAPI(account_type,image,access_token);
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });
    }

}
