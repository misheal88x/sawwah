package com.scit.sawwah.activities;

import android.app.DatePickerDialog;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import androidx.fragment.app.DialogFragment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.scit.sawwah.APIClasses.AccountsAPIsClass;
import com.scit.sawwah.APIClasses.AdvertisementsAPIsClass;
import com.scit.sawwah.Models.InstitutionObject;
import com.scit.sawwah.Models.PricePackageObject;
import com.scit.sawwah.Models.ProfileResponse;
import com.scit.sawwah.R;
import com.scit.sawwah.adapters.CustomSpinnerAdapter;
import com.scit.sawwah.fragments.DatePickerFragment;
import com.scit.sawwah.interfaces.IFailure;
import com.scit.sawwah.interfaces.IResponse;
import com.scit.sawwah.tools.BaseActivity;
import com.scit.sawwah.tools.BaseFunctions;

import java.util.ArrayList;
import java.util.List;

public class AddAdvertiseActivity extends BaseActivity implements DatePickerDialog.OnDateSetListener{
    private RelativeLayout toolbar,rl_add_btn;
    private ImageButton btn_back;
    private TextView tv_title;
    private EditText edt_title,edt_details;
    private Spinner package_spinner,institution_spinner;
    private CustomSpinnerAdapter package_adapter,institution_adapter;
    private List<String> package_list_string,institution_list_string;
    private List<PricePackageObject> package_list;
    private List<InstitutionObject> institution_list;
    private TextView tv_start_date,tv_end_date;
    private int clicked_date = 0;
    private String chosen_start_date = "",chosen_end_date = "";
    private int selected_inst_id = 0,selected_package_id = 0;
    private LinearLayout root;
    private int tries = 0;
    private int num_ads = 0;

    @Override
    public void set_layout() {
        setContentView(R.layout.activity_add_advertise);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        init_spinners();
        tv_title.setText(getResources().getString(R.string.add_advertisement_title));
    }

    @Override
    public void init_views() {
        toolbar = findViewById(R.id.add_advertisement_toolbar);
        rl_add_btn = findViewById(R.id.add_advertisement_add_btn);
        btn_back = toolbar.findViewById(R.id.toolbar_back_btn);
        tv_title = toolbar.findViewById(R.id.toolbar_title);
        edt_title = findViewById(R.id.add_advertisement_title);
        edt_details = findViewById(R.id.add_advertisement_details);
        package_spinner = findViewById(R.id.add_advertisement_package_spinner);
        institution_spinner = findViewById(R.id.add_advertisement_institution_spinner);
        tv_start_date = findViewById(R.id.add_advertisement_start_date_txt);
        tv_end_date = findViewById(R.id.add_advertisement_end_date_txt);
        root = findViewById(R.id.add_advertisement_layout);
    }

    @Override
    public void init_events() {
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        package_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int pos = BaseFunctions.getSpinnerPosition(parent,position);
                if (package_list.size()>0){
                    PricePackageObject ppo = package_list.get(pos);
                    selected_package_id = ppo.getId();
                    if (ppo.getType() == 0){
                        num_ads = ppo.getAp_ads_num();
                    }else if (ppo.getType() == 2){
                        num_ads = ppo.getAop_ads_num();
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        institution_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int pos = BaseFunctions.getSpinnerPosition(parent,position);
                if (institution_list.size()>0){
                    InstitutionObject io = institution_list.get(pos);
                    selected_inst_id = io.getId();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        tv_start_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clicked_date = 1;
                DialogFragment datePicker = new DatePickerFragment();
                datePicker.show(getSupportFragmentManager(),"date picker");
            }
        });
        tv_end_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clicked_date = 2;
                DialogFragment datePicker = new DatePickerFragment();
                datePicker.show(getSupportFragmentManager(),"date picker");
            }
        });
        rl_add_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt_title.getText().toString().equals("")){
                    Toast.makeText(AddAdvertiseActivity.this, getResources().getString(R.string.add_offer_no_title), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (edt_details.getText().toString().equals("")){
                    Toast.makeText(AddAdvertiseActivity.this, getResources().getString(R.string.add_offer_no_details), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (selected_package_id == 0){
                    Toast.makeText(AddAdvertiseActivity.this, getResources().getString(R.string.add_offer_no_package), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (chosen_start_date.equals("")){
                    Toast.makeText(AddAdvertiseActivity.this, getResources().getString(R.string.add_offer_no_start_date), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (chosen_end_date.equals("")){
                    Toast.makeText(AddAdvertiseActivity.this, getResources().getString(R.string.add_offer_no_end_date), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (selected_inst_id == 0){
                    Toast.makeText(AddAdvertiseActivity.this, getResources().getString(R.string.add_offer_no_inst), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (num_ads == 0){
                    Toast.makeText(AddAdvertiseActivity.this, getResources().getString(R.string.add_offer_not_enough_ads), Toast.LENGTH_SHORT).show();
                    return;
                }
                callAPI();
            }
        });
    }

    @Override
    public void set_fragment_place() {

    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
        String year_str = new String();
        String month_str = new String();
        String day_str = new String();
        year_str = String.valueOf(year);
        if (month+1<10){
            month_str = "0"+String.valueOf(month+1);
        }else {
            month_str = String.valueOf(month+1);
        }
        if (day<10){
            day_str = "0"+String.valueOf(day);
        }else {
            day_str = String.valueOf(day);
        }
        String desDate = year_str+"-"+month_str+"-"+day_str;
        if (clicked_date == 1){
            chosen_start_date = desDate;
            tv_start_date.setText(desDate);
        }else {
            chosen_end_date = desDate;
            tv_end_date.setText(desDate);
        }
    }

    private void init_spinners(){
        package_list = new ArrayList<>();
        institution_list = new ArrayList<>();
        package_list_string = new ArrayList<>();
        institution_list_string = new ArrayList<>();

        /*
        PricePackageObject[] package_array = new Gson().fromJson(sharedPreferences.getString("my_packages","[]"),PricePackageObject[].class);
        if (package_array.length > 0){
            for (PricePackageObject ppo : package_array){
                package_list.add(ppo);
                package_list_string.add(ppo.getName());
            }
        }
        **/
        InstitutionObject[] insts_array = new Gson().fromJson(sharedPreferences.getString("my_insts","[]"),InstitutionObject[].class);
        if (insts_array.length > 0){
            for (InstitutionObject io : insts_array){
                institution_list.add(io);
                institution_list_string.add(io.getName());
            }
        }

        package_adapter = new CustomSpinnerAdapter(getApplicationContext(), R.layout.new_spinner_item, package_list_string);
        institution_adapter = new CustomSpinnerAdapter(getApplicationContext(), R.layout.new_spinner_item, institution_list_string);

        package_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        institution_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        package_spinner.setAdapter(package_adapter);
        institution_spinner.setAdapter(institution_adapter);
        callProfileAPI();
    }

    private void callAPI(){
        AdvertisementsAPIsClass.addAdvertisement(sharedPreferences,
                AddAdvertiseActivity.this,
                sharedPreferences.getString("language", "ar"),
                sharedPreferences.getString("service_token", ""),
                edt_title.getText().toString(),
                edt_details.getText().toString(),
                String.valueOf(selected_package_id),
                String.valueOf(selected_inst_id),
                chosen_start_date,
                chosen_end_date,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json != null){
                            Toast.makeText(AddAdvertiseActivity.this, getResources().getString(R.string.add_offer_success), Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callAPI();
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });
    }

    private void callProfileAPI(){
        AccountsAPIsClass.profile(sharedPreferences,
                AddAdvertiseActivity.this,
                sharedPreferences.getString("language", "ar"),
                sharedPreferences.getString("service_token", ""),
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json != null){
                            String json1 = new Gson().toJson(json);
                            ProfileResponse success = new Gson().fromJson(json1,ProfileResponse.class);
                            //Active Packages
                            if (success.getActive_packages()!=null){
                                if (success.getActive_packages().size()>0){
                                    for (PricePackageObject ppo : success.getActive_packages()){
                                        if (ppo.getType()==0||ppo.getType()==2){
                                            package_list.add(ppo);
                                            package_list_string.add(ppo.getName());
                                            package_adapter.notifyDataSetChanged();
                                        }
                                    }
                                    if (package_list.size()==0){
                                        Toast.makeText(AddAdvertiseActivity.this, getResources().getString(R.string.prices_packages_no_packages), Toast.LENGTH_SHORT).show();
                                    }
                                }else {
                                    Toast.makeText(AddAdvertiseActivity.this, getResources().getString(R.string.prices_packages_no_packages), Toast.LENGTH_SHORT).show();
                                }
                            }else {
                                Toast.makeText(AddAdvertiseActivity.this, getResources().getString(R.string.prices_packages_no_packages), Toast.LENGTH_SHORT).show();
                            }
                        }

                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        if (tries<5){
                            tries++;
                            callProfileAPI();
                        }
                        /*
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callProfileAPI();
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                                **/
                    }
                });
    }
}
