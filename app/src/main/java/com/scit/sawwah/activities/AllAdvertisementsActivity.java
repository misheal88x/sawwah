package com.scit.sawwah.activities;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.widget.NestedScrollView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.gson.Gson;
import com.scit.sawwah.APIClasses.AdvertisementsAPIsClass;
import com.scit.sawwah.APIClasses.InstitutionsAPIsClass;
import com.scit.sawwah.Models.AdvertisementObject;
import com.scit.sawwah.Models.AdvertisementsResponse;
import com.scit.sawwah.Models.InstitutionObject;
import com.scit.sawwah.R;
import com.scit.sawwah.adapters.AllAdvertisementsAdapter;
import com.scit.sawwah.adapters.CustomSpinnerAdapter;
import com.scit.sawwah.interfaces.IFailure;
import com.scit.sawwah.interfaces.IMove;
import com.scit.sawwah.interfaces.IResponse;
import com.scit.sawwah.tools.BaseActivity;
import com.scit.sawwah.tools.BaseRetrofit;
import com.scit.sawwah.tools.EndlessRecyclerViewScrollListener;

import java.util.ArrayList;
import java.util.List;

public class AllAdvertisementsActivity extends BaseActivity {
    private RelativeLayout toolbar;
    private ImageButton btn_back;
    private TextView tv_title;
    private Button btn_view;
    private Spinner categories_spinner;
    private CustomSpinnerAdapter categories_adapter;
    private List<InstitutionObject> list_insts;
    private List<String> list_insts_string;
    private List<String> categories_list;
    private RecyclerView rv_recycler;
    private List<AdvertisementObject> ads_list;
    private AllAdvertisementsAdapter advertisements_adapter;
    private LinearLayoutManager advertisements_layout_manager;
    private NestedScrollView scrollView;
    private RelativeLayout root;
    private int currentPage = 1;
    private String filter = "-1";
    private boolean with = false;
    private LinearLayout empty_layout;
    private CallbackManager callbackManager;
    private ShareDialog shareDialog;
    private Intent myIntent;
    private RelativeLayout filter_layout;

    @Override
    public void set_layout() {
        setContentView(R.layout.activity_all_advertisements);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        with = myIntent.getBooleanExtra("with",false);
        filter = myIntent.getStringExtra("filter");
        scrollView.setFocusableInTouchMode(true);
        scrollView.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);
        tv_title.setText(getResources().getString(R.string.all_advertisements_title));
        init_spinner();
        init_recycler();
        if (with){
            filter_layout.setVisibility(View.GONE);
            callAPI(currentPage,0,myIntent.getStringExtra("filter"));
        }else {
            callFacilitiesAPI();
        }
    }

    @Override
    public void init_views() {
        toolbar = findViewById(R.id.all_advertisements_toolbar);
        btn_back = toolbar.findViewById(R.id.toolbar_back_btn);
        tv_title = toolbar.findViewById(R.id.toolbar_title);
        btn_view = findViewById(R.id.all_advertisements_show_btn);
        categories_spinner = findViewById(R.id.all_advertisements_categories_spinner);
        rv_recycler = findViewById(R.id.all_advertisements_recycler);
        scrollView = findViewById(R.id.all_advertisements_scroll);
        root = findViewById(R.id.all_ads_layout);
        empty_layout = findViewById(R.id.empty_layout);
        myIntent = getIntent();
        filter_layout = findViewById(R.id.filter_layout);
    }

    @Override
    public void init_events() {
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        categories_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int i, long id) {
                long pos = parent.getItemIdAtPosition(i);
                int position = Integer.valueOf(String.valueOf(pos));
                filter = String.valueOf(list_insts.get(position).getId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        btn_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ads_list.clear();
                advertisements_adapter = new AllAdvertisementsAdapter(AllAdvertisementsActivity.this, ads_list, new IMove() {
                    @Override
                    public void move(int position) {
                        FacebookSdk.sdkInitialize(AllAdvertisementsActivity.this.getApplicationContext());
                        callbackManager = CallbackManager.Factory.create();
                        shareDialog = new ShareDialog(AllAdvertisementsActivity.this);
                        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
                            @Override
                            public void onSuccess(Sharer.Result result) {
                                Toast.makeText(AllAdvertisementsActivity.this, "Posted successfully", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onCancel() {
                                Toast.makeText(AllAdvertisementsActivity.this, "Posted canceled", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onError(FacebookException error) {
                                Toast.makeText(AllAdvertisementsActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });
                        ShareLinkContent linkContent = new ShareLinkContent.Builder().setQuote(getResources().getString(R.string.facebook_post_ads)).
                                setContentUrl(Uri.parse(BaseRetrofit.BASE_ALI+"posters/"+String.valueOf(ads_list.get(position).getId()))).build();
                        if (shareDialog.canShow(ShareLinkContent.class)){
                            shareDialog.show(linkContent);
                        }else {
                            Toast.makeText(AllAdvertisementsActivity.this, "can't show", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                rv_recycler.setAdapter(advertisements_adapter);
                currentPage = 1;
                callAPI(currentPage,0,filter);
            }
        });
    }

    @Override
    public void set_fragment_place() {

    }

    private void init_list(){
        list_insts = new ArrayList<>();
        list_insts_string = new ArrayList<>();
        InstitutionObject io = new InstitutionObject();
        io.setId(-1);
        io.setName(getResources().getString(R.string.all_offers_all_categories));
        list_insts.add(io);
        list_insts_string.add(io.getName());
    }

    private void init_spinner(){
        init_list();
        categories_adapter = new CustomSpinnerAdapter(getApplicationContext(), R.layout.new_transparent_spinner_item, list_insts_string);
        categories_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        categories_spinner.setAdapter(categories_adapter);
    }

    private void init_recycler(){
        ads_list = new ArrayList<>();
        advertisements_adapter = new AllAdvertisementsAdapter(AllAdvertisementsActivity.this, ads_list,new IMove() {
            @Override
            public void move(int position) {
                FacebookSdk.sdkInitialize(AllAdvertisementsActivity.this.getApplicationContext());
                callbackManager = CallbackManager.Factory.create();
                shareDialog = new ShareDialog(AllAdvertisementsActivity.this);
                shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
                    @Override
                    public void onSuccess(Sharer.Result result) {
                        Toast.makeText(AllAdvertisementsActivity.this, "Posted successfully", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onCancel() {
                        Toast.makeText(AllAdvertisementsActivity.this, "Posted canceled", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(FacebookException error) {
                        Toast.makeText(AllAdvertisementsActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
                ShareLinkContent linkContent = new ShareLinkContent.Builder().setQuote(getResources().getString(R.string.facebook_post_ads)).
                        setContentUrl(Uri.parse(BaseRetrofit.BASE_ALI+"posters/"+String.valueOf(ads_list.get(position).getId()))).build();
                if (shareDialog.canShow(ShareLinkContent.class)){
                    shareDialog.show(linkContent);
                }else {
                    Toast.makeText(AllAdvertisementsActivity.this, "can't show", Toast.LENGTH_SHORT).show();
                }
            }
        });
        advertisements_layout_manager = new LinearLayoutManager(AllAdvertisementsActivity.this,LinearLayoutManager.VERTICAL,false);
        rv_recycler.setLayoutManager(advertisements_layout_manager);
        runAnimation(rv_recycler,0,advertisements_adapter);
        rv_recycler.setAdapter(advertisements_adapter);
        rv_recycler.addOnScrollListener(new EndlessRecyclerViewScrollListener(advertisements_layout_manager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if (ads_list.size()>=20){
                    Snackbar.make(root,getResources().getString(R.string.get_more),Snackbar.LENGTH_SHORT).show();
                    currentPage++;
                    callAPI(currentPage,1,filter);
                }
            }
        });
    }

    private void callAPI(final int page, final int type, final String filter){
        AdvertisementsAPIsClass.getAllAds(sharedPreferences,
                AllAdvertisementsActivity.this,
                sharedPreferences.getString("service_token", ""),
                sharedPreferences.getString("language", "ar"),
                0,
                0,
                filter,
                type,
                page,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json !=null){
                            String json1 = new Gson().toJson(json);
                            AdvertisementsResponse success = new Gson().fromJson(json1,AdvertisementsResponse.class);
                            if (success.getData()!=null){
                                if (success.getData().size() > 0){
                                    empty_layout.setVisibility(View.GONE);
                                    for (AdvertisementObject oo : success.getData()){
                                        ads_list.add(oo);
                                        advertisements_adapter.notifyDataSetChanged();
                                    }
                                    if (type == 0){
                                        runAnimation(rv_recycler,0,advertisements_adapter);
                                    }
                                }else {
                                    if (type == 1){
                                        Snackbar.make(root,getResources().getString(R.string.no_more),Snackbar.LENGTH_SHORT).show();
                                    }else {
                                        empty_layout.setVisibility(View.VISIBLE);
                                        Toast.makeText(AllAdvertisementsActivity.this, getResources().getString(R.string.no_data), Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callAPI(page,type,filter);
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });
    }

    private void callFacilitiesAPI(){
        InstitutionsAPIsClass.getAllFacilities(sharedPreferences,
                AllAdvertisementsActivity.this,
                sharedPreferences.getString("language", "ar"),
                sharedPreferences.getString("service_token", ""),
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json != null){
                            String json1 = new Gson().toJson(json);
                            InstitutionObject[] success = new Gson().fromJson(json1,InstitutionObject[].class);
                            if (success !=null){
                                if (success.length > 0){
                                    for (InstitutionObject io : success){
                                        list_insts.add(io);
                                        list_insts_string.add(io.getName());
                                        categories_adapter.notifyDataSetChanged();
                                    }
                                }
                            }
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callFacilitiesAPI();
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });
    }
    public void runAnimation(RecyclerView recyclerView,int type,AllAdvertisementsAdapter adapter){
        Context context = recyclerView.getContext();
        LayoutAnimationController controller = null;
        if (type == 0){ // Fall down animation
            controller = AnimationUtils.loadLayoutAnimation(context,R.anim.layout_fall_down_vertical);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutAnimation(controller);
            recyclerView.getAdapter().notifyDataSetChanged();
            recyclerView.scheduleLayoutAnimation();
        }
    }
}
