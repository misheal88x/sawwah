package com.scit.sawwah.activities;

import android.content.Intent;
import android.graphics.Color;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.rm.rmswitch.RMSwitch;
import com.scit.sawwah.APIClasses.AccountsAPIsClass;
import com.scit.sawwah.Models.CityObject;
import com.scit.sawwah.Models.CountryObject;
import com.scit.sawwah.R;
import com.scit.sawwah.adapters.CustomSpinnerAdapter;
import com.scit.sawwah.interfaces.IFailure;
import com.scit.sawwah.interfaces.IResponse;
import com.scit.sawwah.tools.BaseActivity;
import com.scit.sawwah.tools.BaseFunctions;

import java.util.ArrayList;
import java.util.List;

public class SettingsActivity extends BaseActivity {
    private RelativeLayout toolbar,lyt_ok;
    private ImageButton btn_back;
    private Spinner country_spinner,city_spinner,language_spinner;
    private CustomSpinnerAdapter country_adapter,city_adapter,language_adapter;
    private List<String> country_list_string,city_list_string,language_list;
    private List<CountryObject> country_list;
    private List<CityObject> city_list;
    private RMSwitch sw_alert;
    private TextView tv_title,tv_alert;
    private int city_id = 0,country_id = 0;
    private String language = "";
    private boolean alert_active = false;
    private LinearLayout root;

    @Override
    public void set_layout() {
        setContentView(R.layout.activity_settings);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        tv_title.setText(getResources().getString(R.string.settings_title));
        init_spinners();
        setActiveSwitchColors();
        restoreViews();
    }

    @Override
    public void init_views() {
        toolbar = findViewById(R.id.settings_toolbar);
        btn_back = toolbar.findViewById(R.id.toolbar_back_btn);
        tv_title = toolbar.findViewById(R.id.toolbar_title);
        lyt_ok = findViewById(R.id.settings_ok_btn);
        country_spinner = findViewById(R.id.settings_country_spinner);
        city_spinner = findViewById(R.id.settings_city_spinner);
        language_spinner = findViewById(R.id.settings_language_spinner);
        sw_alert = findViewById(R.id.settings_alerts_switch);
        tv_alert = findViewById(R.id.settings_alerts_text);
    }


    @Override
    public void init_events() {
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        country_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int i, long id) {
                long pos = parent.getItemIdAtPosition(i);
                int position = Integer.valueOf(String.valueOf(pos));
                country_id = country_list.get(position).getId();
                CountryObject co = country_list.get(position);
                if (co.getCities()!=null){
                    if (co.getCities().size() > 0){
                        city_list.clear();
                        city_list_string.clear();
                        city_adapter.notifyDataSetChanged();
                        for (CityObject coo : co.getCities()){
                            city_list.add(coo);
                            city_list_string.add(coo.getName());
                            city_adapter.notifyDataSetChanged();
                        }
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        city_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int i, long id) {
                long pos = parent.getItemIdAtPosition(i);
                int position = Integer.valueOf(String.valueOf(pos));
                city_id = city_list.get(position).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        language_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int i, long id) {
                long pos = parent.getItemIdAtPosition(i);
                int position = Integer.valueOf(String.valueOf(pos));
                if (position == 0){
                    language = "en";
                }else if (position == 1){
                    language = "ar";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        sw_alert.addSwitchObserver(new RMSwitch.RMSwitchObserver() {
            @Override
            public void onCheckStateChange(RMSwitch switchView, boolean isChecked) {
                if (isChecked){
                    tv_alert.setText(getResources().getString(R.string.settings_active));
                }else {
                    tv_alert.setText(getResources().getString(R.string.settings_dis_active));
                }
                alert_active = isChecked;
            }
        });
        lyt_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (country_id == 0){
                    Toast.makeText(SettingsActivity.this, getResources().getString(R.string.settings_no_country), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (city_id == 0){
                    Toast.makeText(SettingsActivity.this, getResources().getString(R.string.settings_no_city), Toast.LENGTH_SHORT).show();
                    return;
                }
                callAPI();
            }
        });
    }

    @Override
    public void set_fragment_place() {

    }

    private void restoreViews(){
        //Country
        int storedCountryId = sharedPreferences.getInt("settings_country_id",0);
        if (country_list.size() > 0){
            for (int i = 0; i < country_list.size() ; i++) {
                if (country_list.get(i).getId() == storedCountryId){
                    country_spinner.setSelection(i);
                    city_list.clear();
                    city_list_string.clear();
                    city_spinner.setAdapter(null);
                    for (CityObject co : country_list.get(i).getCities()){
                        city_list.add(co);
                        city_list_string.add(co.getName());
                    }
                    city_adapter = new CustomSpinnerAdapter(SettingsActivity.this,R.layout.new_spinner_item,city_list_string);
                    city_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    city_spinner.setAdapter(city_adapter);
                    break;
                }
            }
        }
        //City
        int storedCityId = sharedPreferences.getInt("settings_city_id",0);
        if (city_list.size() > 0){
            for (int i = 0; i < city_list.size(); i++) {
                if (city_list.get(i).getId() == storedCityId){
                    city_spinner.setSelection(i);
                    break;
                }
            }
        }
        //Language
        String storedLanguage = sharedPreferences.getString("language","ar");
        if (storedLanguage.equals("en")){
            language_spinner.setSelection(0);
        }else if (storedLanguage.equals("ar")){
            language_spinner.setSelection(1);
        }else {
            language_spinner.setSelection(0);
        }
        //Alerts
        boolean storedAlerts = sharedPreferences.getBoolean("settings_alerts",false);
        if (storedAlerts){
            tv_alert.setText(getResources().getString(R.string.settings_active));
            alert_active = storedAlerts;
            sw_alert.setChecked(true);
        }else {
            tv_alert.setText(getResources().getString(R.string.settings_dis_active));
            alert_active = storedAlerts;
            sw_alert.setChecked(false);
        }
    }

    private void initLists(){
        country_list = new ArrayList<>();
        city_list = new ArrayList<>();
        country_list_string = new ArrayList<>();
        city_list_string = new ArrayList<>();
        language_list = new ArrayList<>();

        CountryObject[] countries_array = new Gson().fromJson(sharedPreferences.getString("countries","[]"),CountryObject[].class);
        if (countries_array.length > 0){
            for (CountryObject co : countries_array){
                country_list.add(co);
                country_list_string.add(co.getName());
            }
        }
        language_list.add("English");
        language_list.add("العربية");

    }
    private void init_spinners(){
        initLists();
        //Country spinner
         country_adapter = new CustomSpinnerAdapter(SettingsActivity.this,R.layout.new_spinner_item,country_list_string);
         country_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
         country_spinner.setAdapter(country_adapter);
         //City spinner
        city_adapter = new CustomSpinnerAdapter(SettingsActivity.this,R.layout.new_spinner_item,city_list_string);
        city_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        city_spinner.setAdapter(city_adapter);
        //language spinner
        language_adapter = new CustomSpinnerAdapter(SettingsActivity.this,R.layout.new_spinner_item,language_list);
        language_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        language_spinner.setAdapter(language_adapter);
    }
    private void setActiveSwitchColors() {
        sw_alert.setSwitchBkgCheckedColor(getResources().getColor(R.color.colorTextSecobdary));
        sw_alert.setSwitchToggleCheckedColor(Color.WHITE);
        sw_alert.setSwitchToggleNotCheckedColor(Color.WHITE);
    }

    private void callAPI(){
        AccountsAPIsClass.updateDeviceSettings(sharedPreferences,
                SettingsActivity.this,
                sharedPreferences.getString("language", "ar"),
                sharedPreferences.getString("service_token", ""),
                String.valueOf(city_id),
                BaseFunctions.booleanToStringNumber(alert_active),
                BaseFunctions.languageToNumberConverter(language),
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json !=null){
                            Toast.makeText(SettingsActivity.this, getResources().getString(R.string.settings_success), Toast.LENGTH_SHORT).show();
                            editor.putInt("settings_country_id",country_id);
                            editor.putInt("settings_city_id",city_id);
                            editor.putString("language",language);
                            editor.putBoolean("settings_alerts",alert_active);
                            editor.commit();
                            startActivity(new Intent(SettingsActivity.this,SplashActivity.class));
                            finish();
                        }else {
                            Toast.makeText(SettingsActivity.this, getResources().getString(R.string.error_occurred), Toast.LENGTH_SHORT).show();
                            restoreViews();
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callAPI();
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });
    }
}
