package com.scit.sawwah.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Environment;
import androidx.annotation.NonNull;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.scit.sawwah.APIClasses.ComplaintsAPIsClass;
import com.scit.sawwah.R;
import com.scit.sawwah.interfaces.IFailure;
import com.scit.sawwah.interfaces.IResponse;
import com.scit.sawwah.tools.BaseActivity;
import com.soundcloud.android.crop.Crop;

import java.io.File;
import java.io.IOException;

public class ComplaintsAndSuggestionsActivity extends BaseActivity {
    private RelativeLayout toolbar,rv_send_btn;
    private ImageButton btn_back;
    private TextView tv_title;
    private ImageView iv_image;
    private EditText edt_title,edt_details;
    private static final String TEMP_PHOTO_FILE = "temporary_holder.jpg";
    private String imagePath = "";
    private Uri imageUri = null;
    private boolean image_selected = false;
    private LinearLayout root;

    @Override
    public void set_layout() {
        setContentView(R.layout.activity_complaints_and_suggestions);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        tv_title.setText(getResources().getString(R.string.complaints_title));
    }

    @Override
    public void init_views() {
        toolbar = findViewById(R.id.complaints_toolbar);
        btn_back = toolbar.findViewById(R.id.toolbar_back_btn);
        tv_title = toolbar.findViewById(R.id.toolbar_title);
        edt_title = findViewById(R.id.complaints_title_txt);
        edt_details = findViewById(R.id.complaints_details_txt);
        iv_image = findViewById(R.id.complaints_image);
        rv_send_btn = findViewById(R.id.complaints_send_btn);
        root = findViewById(R.id.complaints_layout);
    }

    @Override
    public void init_events() {
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        iv_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityCompat.requestPermissions(ComplaintsAndSuggestionsActivity.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        1);
            }
        });
        rv_send_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt_title.getText().toString().equals("")){
                    Toast.makeText(ComplaintsAndSuggestionsActivity.this, getResources().getString(R.string.complaints_no_title), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (edt_details.getText().toString().equals("")){
                    Toast.makeText(ComplaintsAndSuggestionsActivity.this, getResources().getString(R.string.complaints_no_content), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!image_selected){
                    Toast.makeText(ComplaintsAndSuggestionsActivity.this, getResources().getString(R.string.complaints_no_image), Toast.LENGTH_SHORT).show();
                    return;
                }
                callAPI();
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case 1:
            {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    Crop.pickImage(ComplaintsAndSuggestionsActivity.this);
                }else {
                    Toast.makeText(this, getResources().getString(R.string.add_photos_permission_denied), Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }

    private Uri getTempUri() {
        return Uri.fromFile(getTempFile());
    }


    private File getTempFile() {
        if (isSDCARDMounted()) {

            File f = new File(Environment.getExternalStorageDirectory(),TEMP_PHOTO_FILE);
            try {
                f.createNewFile();
            } catch (IOException e) {

            }
            return f;
        } else {
            return null;
        }
    }

    private boolean isSDCARDMounted(){
        String status = Environment.getExternalStorageState();
        if (status.equals(Environment.MEDIA_MOUNTED))
            return true;
        return false;
    }

    protected void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK){
            if (requestCode == Crop.REQUEST_PICK){
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Uri source_uri = data.getData();
                        Uri destination_uri = Uri.fromFile(getTempFile());
                        Crop.of(source_uri,destination_uri).withAspect(16,9).start(ComplaintsAndSuggestionsActivity.this);
                        String filePath= Environment.getExternalStorageDirectory()
                                + "/temporary_holder.jpg";
                        imagePath = filePath;
                        image_selected = true;
                    }
                });
            }else if (requestCode == Crop.REQUEST_CROP){
                handle_crop(resultCode,data);
            }
        }
    }

    private void handle_crop(int code, final Intent result){
        if (code == RESULT_OK){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    iv_image.setImageDrawable(null);
                    iv_image.setImageURI(Crop.getOutput(result));
                    String filePath= Environment.getExternalStorageDirectory()
                            + "/temporary_holder.jpg";
                    imagePath = filePath;
                    image_selected = true;
                }
            });

        }else if (code == Crop.RESULT_ERROR){
            Toast.makeText(this, "An error occurred", Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    public void set_fragment_place() {

    }

    private void callAPI(){
        ComplaintsAPIsClass.add(sharedPreferences,
                ComplaintsAndSuggestionsActivity.this,
                sharedPreferences.getString("language", "ar"),
                sharedPreferences.getString("service_token", ""),
                imagePath,
                sharedPreferences.getString("account_name", ""),
                sharedPreferences.getString("account_user_name", ""),
                edt_title.getText().toString(),
                edt_details.getText().toString(),
                "0",
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null){
                            Toast.makeText(ComplaintsAndSuggestionsActivity.this, getResources().getString(R.string.complaints_success), Toast.LENGTH_SHORT).show();
                            finish();
                        }else {
                            Toast.makeText(ComplaintsAndSuggestionsActivity.this, getResources().getString(R.string.error_occurred), Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callAPI();
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });
    }
}
