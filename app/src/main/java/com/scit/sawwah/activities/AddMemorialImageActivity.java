package com.scit.sawwah.activities;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.scit.sawwah.APIClasses.MemorialAPIsClass;
import com.scit.sawwah.R;
import com.scit.sawwah.interfaces.IFailure;
import com.scit.sawwah.interfaces.IResponse;
import com.scit.sawwah.tools.BaseActivity;

public class AddMemorialImageActivity extends BaseActivity {
    private RelativeLayout toolbar;
    private ImageButton btn_back;
    private TextView tv_title;
    private ImageView image;
    private EditText edt_comment;
    private LinearLayout btn_add;
    private String selected_image_path = "";
    private Intent myIntent;
    private LinearLayout root;

    @Override
    public void set_layout() {
        setContentView(R.layout.activity_add_memorial_image);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        tv_title.setText(getResources().getString(R.string.add_memo_title));
        if (getIntent().getStringExtra("type").equals("customer")){
            edt_comment.setVisibility(View.GONE);
        }
    }

    @Override
    public void init_views() {
        toolbar = findViewById(R.id.add_memo_image_toolbar);
        btn_back = toolbar.findViewById(R.id.toolbar_back_btn);
        tv_title = toolbar.findViewById(R.id.toolbar_title);
        image = findViewById(R.id.add_memo_image_image);
        edt_comment = findViewById(R.id.add_memo_image_desc_txt);
        btn_add = findViewById(R.id.add_memo_image_add_btn);
        myIntent = getIntent();
        root = findViewById(R.id.add_memo_image_layout);
    }

    @Override
    public void init_events() {
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                String[] mimeTypes = {"image/jpeg", "image/png"};
                intent.putExtra(Intent.EXTRA_MIME_TYPES,mimeTypes);
                startActivityForResult(intent,1);
            }
        });
        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selected_image_path.equals("")){
                    Toast.makeText(AddMemorialImageActivity.this, getResources().getString(R.string.add_memo_no_image), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (edt_comment.getText().toString().equals("")){
                    if (getIntent().getStringExtra("type").equals("facility")){
                        Toast.makeText(AddMemorialImageActivity.this, getResources().getString(R.string.add_memo_no_desc), Toast.LENGTH_SHORT).show();
                        return;
                    }
                }
                if (myIntent.getStringExtra("type").equals("facility")){
                    callAPI();
                }else {
                    callCustomerAPI();
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Result code is RESULT_OK only if the user selects an Image
        if (resultCode == Activity.RESULT_OK)
            switch (requestCode) {
                case 1:
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                    cursor.moveToFirst();
                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String imgDecodableString = cursor.getString(columnIndex);
                    cursor.close();
                    image.setImageBitmap(BitmapFactory.decodeFile(imgDecodableString));
                    selected_image_path = imgDecodableString;
                    break;

            }
    }

    @Override
    public void set_fragment_place() {

    }

    private void callAPI(){
        MemorialAPIsClass.addImage(sharedPreferences,
                AddMemorialImageActivity.this,
                sharedPreferences.getString("language", "ar"),
                sharedPreferences.getString("service_token", ""),
                String.valueOf(myIntent.getIntExtra("institution_id", 0)),
                edt_comment.getText().toString(),
                selected_image_path,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null){
                            Toast.makeText(AddMemorialImageActivity.this, getResources().getString(R.string.add_memo_success), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent();
                            intent.putExtra("key","value");
                            setResult(RESULT_OK,intent);
                            finish();
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callAPI();
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });
    }

    private void callCustomerAPI(){
        MemorialAPIsClass.addCustomerImage(sharedPreferences,
                AddMemorialImageActivity.this,
                sharedPreferences.getString("language", "ar"),
                sharedPreferences.getString("service_token", ""),
                selected_image_path,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null){
                            Toast.makeText(AddMemorialImageActivity.this, getResources().getString(R.string.add_memo_success), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent();
                            intent.putExtra("key","value");
                            setResult(RESULT_OK,intent);
                            finish();
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callAPI();
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });
    }
}
