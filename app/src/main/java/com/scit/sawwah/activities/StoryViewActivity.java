package com.scit.sawwah.activities;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.LinearLayoutCompat;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.scit.sawwah.APIClasses.AccountsAPIsClass;
import com.scit.sawwah.Models.MemorialImageObject;
import com.scit.sawwah.Models.ReactionTypeObject;
import com.scit.sawwah.R;
import com.scit.sawwah.dialoges.ReactDialog;
import com.scit.sawwah.interfaces.IFailure;
import com.scit.sawwah.interfaces.IMove;
import com.scit.sawwah.interfaces.IResponse;
import com.scit.sawwah.tools.BaseActivity;
import com.scit.sawwah.tools.BaseFunctions;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import jp.shts.android.storiesprogressview.StoriesProgressView;

public class StoryViewActivity extends BaseActivity implements StoriesProgressView.StoriesListener{

    private int counter = 0;
    private long pressTime = 0L;
    private long limit = 500L;
    private StoriesProgressView spv_progress;
    private ImageView img_image;
    private CircleImageView img_icon;
    private TextView tv_name;
    private List<String> images;
    private List<String> storyIds;
    private List<MemorialImageObject> listOfMemorial;
    private String userId;
    private View v_reverse,v_skip;
    private TextView tv_date;
    private Intent myIntent;
    private int position = 0;
    private int images_size = 0;
    private List<ReactionTypeObject> list_reactons;
    private RelativeLayout lyt_react;
    private TextView tv_react;
    private ImageView img_react;
    private RelativeLayout root;
    private int current_image_id = 0;
    private ReactDialog reactDialog;

    private View.OnTouchListener onTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            switch (event.getAction()){
                case MotionEvent.ACTION_DOWN:{
                    pressTime = System.currentTimeMillis();
                    spv_progress.pause();
                    return false;
                }
                case MotionEvent.ACTION_UP:{
                    long now = System.currentTimeMillis();
                    spv_progress.resume();
                    return limit < now - pressTime;
                }
            }
            return false;
        }
    };

    @Override
    public void set_layout() {
        setContentView(R.layout.activity_story_view);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        if (Build.VERSION.SDK_INT < 16){
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
        else {
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        }
        getWindow().setLayout(LinearLayoutCompat.LayoutParams.MATCH_PARENT,
                LinearLayoutCompat.LayoutParams.MATCH_PARENT);
        //getWindow().getAttributes().windowAnimations = R.style.SlideAnimation;
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        init_list_images();
    }

    @Override
    public void init_views() {
        spv_progress = findViewById(R.id.story_progress);
        img_image = findViewById(R.id.story_image);
        img_icon = findViewById(R.id.story_icon);
        tv_name = findViewById(R.id.story_name);
        v_reverse = findViewById(R.id.story_reverse);
        v_skip = findViewById(R.id.story_skip);
        tv_date = findViewById(R.id.story_date);
        lyt_react = findViewById(R.id.story_react_layout);
        tv_react = findViewById(R.id.story_react_txt);
        img_react = findViewById(R.id.story_react_image);
        myIntent = getIntent();
        root = findViewById(R.id.story_layout);

    }

    @Override
    public void init_events() {
        v_reverse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spv_progress.startStories(0);
                position--;
                if (position >= 0){
                    BaseFunctions.setGlideImage(StoryViewActivity.this,img_image,images.get(position));
                    BaseFunctions.setGlideImage(StoryViewActivity.this,img_icon,listOfMemorial.get(position).getFacility_image());
                    tv_name.setText(listOfMemorial.get(position).getFacility_name());
                    tv_date.setText(BaseFunctions.dateExtractor(listOfMemorial.get(position).getCreated_at()));
                    current_image_id = listOfMemorial.get(position).getId();
                    int reaction = listOfMemorial.get(position).getUser_reaction();
                    if (reaction== -1){
                        img_react.setVisibility(View.GONE);
                        tv_react.setVisibility(View.VISIBLE);
                    }else {
                        if (list_reactons.size() > 0){
                            for (ReactionTypeObject rto : list_reactons){
                                if (rto.getId() == reaction){
                                    tv_react.setVisibility(View.GONE);
                                    img_react.setVisibility(View.VISIBLE);
                                    BaseFunctions.setGlideImage(StoryViewActivity.this,img_react,rto.getImage_icon());
                                    break;
                                }
                            }
                        }
                    }
                }else {
                    spv_progress.destroy();
                    finish();
                }
            }
        });
        v_reverse.setOnTouchListener(onTouchListener);
        v_skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spv_progress.startStories(0);
                position++;
                if (position<=images_size-1){
                    BaseFunctions.setGlideImage(StoryViewActivity.this,img_image,images.get(position));
                    BaseFunctions.setGlideImage(StoryViewActivity.this,img_icon,listOfMemorial.get(position).getFacility_image());
                    tv_name.setText(listOfMemorial.get(position).getFacility_name());
                    tv_date.setText(BaseFunctions.dateExtractor(listOfMemorial.get(position).getCreated_at()));
                    current_image_id = listOfMemorial.get(position).getId();
                    int reaction = listOfMemorial.get(position).getUser_reaction();
                    if (reaction== -1){
                        img_react.setVisibility(View.GONE);
                        tv_react.setVisibility(View.VISIBLE);
                    }else {
                        if (list_reactons.size() > 0){
                            for (ReactionTypeObject rto : list_reactons){
                                if (rto.getId() == reaction){
                                    tv_react.setVisibility(View.GONE);
                                    img_react.setVisibility(View.VISIBLE);
                                    BaseFunctions.setGlideImage(StoryViewActivity.this,img_react,rto.getImage_icon());
                                    break;
                                }
                            }
                        }
                    }
                }else {
                    spv_progress.destroy();
                    finish();
                }
            }
        });
        v_skip.setOnTouchListener(onTouchListener);

        lyt_react.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spv_progress.pause();
                reactDialog = new ReactDialog(StoryViewActivity.this, new IMove() {
                    @Override
                    public void move(int position) {
                        int react_id = list_reactons.get(position).getId();
                        callReactAPI(react_id,current_image_id);
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        spv_progress.resume();
                    }
                });
                reactDialog.show();
            }
        });
    }

    @Override
    public void set_fragment_place() {

    }



    @Override
    public void onNext() {
        /*
        spv_progress.startStories(0);
        position++;
        if (position<=images_size-1){
            BaseFunctions.setGlideImage(StoryViewActivity.this,img_image,images.get(position));
            BaseFunctions.setGlideImage(StoryViewActivity.this,img_icon,listOfMemorial.get(position).getFacility_image());
            tv_name.setText(listOfMemorial.get(position).getFacility_name());
        }else {
            spv_progress.destroy();
            finish();
        }
        **/
    }

    @Override
    public void onPrev() {
        /*
        spv_progress.startStories(0);
        position--;
        if (position >= 0){
            BaseFunctions.setGlideImage(StoryViewActivity.this,img_image,images.get(position));
            BaseFunctions.setGlideImage(StoryViewActivity.this,img_icon,listOfMemorial.get(position).getFacility_image());
            tv_name.setText(listOfMemorial.get(position).getFacility_name());
        }else {
            spv_progress.destroy();
            finish();
        }
        **/
    }

    @Override
    public void onComplete() {
        spv_progress.startStories(0);
        position++;
        if (position<=images_size-1){
            BaseFunctions.setGlideImage(StoryViewActivity.this,img_image,images.get(position));
            BaseFunctions.setGlideImage(StoryViewActivity.this,img_icon,listOfMemorial.get(position).getFacility_image());
            tv_name.setText(listOfMemorial.get(position).getFacility_name());
            tv_date.setText(BaseFunctions.dateExtractor(listOfMemorial.get(position).getCreated_at()));
            current_image_id = listOfMemorial.get(position).getId();
            int reaction = listOfMemorial.get(position).getUser_reaction();
            if (reaction== -1){
                img_react.setVisibility(View.GONE);
                tv_react.setVisibility(View.VISIBLE);
            }else {
                if (list_reactons.size() > 0){
                    for (ReactionTypeObject rto : list_reactons){
                        if (rto.getId() == reaction){
                            tv_react.setVisibility(View.GONE);
                            img_react.setVisibility(View.VISIBLE);
                            BaseFunctions.setGlideImage(StoryViewActivity.this,img_react,rto.getImage_icon());
                            break;
                        }
                    }
                }
            }
        }else {
            spv_progress.destroy();
            finish();
        }
    }

    @Override
    protected void onDestroy() {
        spv_progress.destroy();
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        spv_progress.pause();
        super.onPause();
    }

    @Override
    protected void onResume() {
        spv_progress.resume();
        super.onResume();
    }

    private void init_list_images(){
        init_reactions_list();
        images = new ArrayList<>();
        listOfMemorial = new ArrayList<>();
        String images_json = myIntent.getStringExtra("images");
        MemorialImageObject[] images_array = new Gson().fromJson(images_json,MemorialImageObject[].class);
        if (images_array.length > 0){
            for (MemorialImageObject mio : images_array){
                images.add(mio.getImage_icon());
                listOfMemorial.add(mio);
            }
        }
        images_size = images.size();
        spv_progress.setStoriesCount(1);
        spv_progress.setStoryDuration(20000L);
        spv_progress.setStoriesListener(StoryViewActivity.this);
        position = myIntent.getIntExtra("position",0);
        spv_progress.startStories(0);
        BaseFunctions.setGlideImage(StoryViewActivity.this,img_image,images.get(position));
        BaseFunctions.setGlideImage(StoryViewActivity.this,img_icon,images_array[position].getFacility_image());
        tv_name.setText(images_array[position].getFacility_name());
        tv_date.setText(BaseFunctions.dateExtractor(myIntent.getStringExtra("date")));
        current_image_id = images_array[position].getId();
        int reaction = myIntent.getIntExtra("user_react",0);
        if (reaction== -1){
            img_react.setVisibility(View.GONE);
            tv_react.setVisibility(View.VISIBLE);
        }else {
            if (list_reactons.size() > 0){
                for (ReactionTypeObject rto : list_reactons){
                    if (rto.getId() == reaction){
                        tv_react.setVisibility(View.GONE);
                        img_react.setVisibility(View.VISIBLE);
                        BaseFunctions.setGlideImage(StoryViewActivity.this,img_react,rto.getImage_icon());
                        break;
                    }
                }
            }
        }
    }

    private void init_reactions_list(){
        list_reactons = new ArrayList<>();
        ReactionTypeObject[] array_reactions = new Gson().fromJson(sharedPreferences.getString("reactions","[]"),ReactionTypeObject[].class);
        if (array_reactions.length > 0){
            for (ReactionTypeObject rto : array_reactions){
                list_reactons.add(rto);
            }
        }
    }

    private void callReactAPI(final int reaction_id, final int memorial_image_id){
        spv_progress.pause();
        AccountsAPIsClass.reactOnImage(sharedPreferences,
                StoryViewActivity.this,
                sharedPreferences.getString("language", "ar"),
                sharedPreferences.getString("service_token", ""),
                reaction_id,
                memorial_image_id,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null){
                            tv_react.setVisibility(View.GONE);
                            for (ReactionTypeObject rto :list_reactons){
                                if (rto.getId()==reaction_id){
                                    BaseFunctions.setGlideImage(StoryViewActivity.this,img_react,rto.getImage_icon());
                                    img_react.setVisibility(View.VISIBLE);
                                    reactDialog.cancel();
                                    break;
                                }
                            }
                        }else {
                            reactDialog.cancel();
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callReactAPI(reaction_id,memorial_image_id);
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });
    }

}
