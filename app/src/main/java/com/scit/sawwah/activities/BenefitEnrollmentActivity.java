package com.scit.sawwah.activities;

import android.content.Intent;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.scit.sawwah.APIClasses.CardAPIsClass;
import com.scit.sawwah.Models.InstitutionObject;
import com.scit.sawwah.R;
import com.scit.sawwah.interfaces.IFailure;
import com.scit.sawwah.interfaces.IResponse;
import com.scit.sawwah.tools.BaseActivity;

public class BenefitEnrollmentActivity extends BaseActivity {
    private RelativeLayout toolbar;
    private ImageButton btn_back;
    private TextView btn_barcode;
    private Button btn_confirm;
    private EditText edt_card_number;
    private Intent myIntent;
    private LinearLayout root;

    @Override
    public void set_layout() {
        setContentView(R.layout.activity_benefit_enrollment);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {

    }

    @Override
    public void init_views() {
        toolbar = findViewById(R.id.benefit_enroll_toolbar);
        btn_back = toolbar.findViewById(R.id.toolbar_back_btn);
        btn_barcode = findViewById(R.id.benefit_enroll_barcode);
        btn_confirm = findViewById(R.id.benefit_enroll_confirm);
        edt_card_number = findViewById(R.id.benefit_enrollment_number);
        myIntent = getIntent();
        root = findViewById(R.id.benefit_enroll_layout);
    }

    @Override
    public void init_events() {
        btn_barcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IntentIntegrator integrator = new IntentIntegrator(BenefitEnrollmentActivity.this);
                integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
                integrator.setPrompt("Scan");
                integrator.setCameraId(0);
                integrator.setBeepEnabled(true);
                integrator.setBarcodeImageEnabled(false);
                integrator.initiateScan();
            }
        });
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btn_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt_card_number.getText().toString().equals("")){
                    Toast.makeText(BenefitEnrollmentActivity.this, getResources().getString(R.string.benefit_enrollment_no_number), Toast.LENGTH_SHORT).show();
                    return;
                }
                InstitutionObject[] insts_array = new Gson().fromJson(sharedPreferences.getString("my_insts","[]"),InstitutionObject[].class);
                if (insts_array.length == 0){
                    Toast.makeText(BenefitEnrollmentActivity.this, getResources().getString(R.string.benefit_enrollment_no_insts), Toast.LENGTH_SHORT).show();
                    return;
                }

                    callAPI(insts_array[0].getId());

            }
        });
    }

    @Override
    public void set_fragment_place() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null){
            if (result.getContents() == null){
                Toast.makeText(this, getResources().getString(R.string.benefit_enrollment_cancel_scanner), Toast.LENGTH_SHORT).show();
            }else {
                edt_card_number.setText(result.getContents());
            }
        }else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void callAPI(final int id){
        CardAPIsClass.add_benefit(sharedPreferences,
                BenefitEnrollmentActivity.this,
                sharedPreferences.getString("language", "ar"),
                sharedPreferences.getString("service_token", ""),
                edt_card_number.getText().toString(),
                String.valueOf(id),
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json != null){
                            Toast.makeText(BenefitEnrollmentActivity.this, getResources().getString(R.string.benefit_enrollment_success), Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callAPI(id);
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });
    }
}
