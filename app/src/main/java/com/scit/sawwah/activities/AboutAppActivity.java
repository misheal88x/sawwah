package com.scit.sawwah.activities;

import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.scit.sawwah.R;
import com.scit.sawwah.tools.BaseActivity;
import com.scit.sawwah.tools.BaseFunctions;

public class AboutAppActivity extends BaseActivity {
    private ImageButton btn_back;
    private ImageView btn_facebook;
    @Override
    public void set_layout() {
        setContentView(R.layout.activity_about_app);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {

    }

    @Override
    public void init_views() {
        btn_back = findViewById(R.id.about_back_btn);
        btn_facebook = findViewById(R.id.btn_facebook);
    }

    @Override
    public void init_events() {
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btn_facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BaseFunctions.openFacebook(AboutAppActivity.this,"https://www.facebook.com/Sawwaah");
            }
        });
    }

    @Override
    public void set_fragment_place() {

    }
}
