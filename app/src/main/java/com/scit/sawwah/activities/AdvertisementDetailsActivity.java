package com.scit.sawwah.activities;

import android.content.Intent;
import android.net.Uri;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.glide.slider.library.svg.GlideApp;
import com.google.gson.Gson;
import com.scit.sawwah.APIClasses.AdvertisementsAPIsClass;
import com.scit.sawwah.APIClasses.FavouriteAPIsClass;
import com.scit.sawwah.Models.AdvertisementDetailsResponse;
import com.scit.sawwah.Models.AdvertisementObject;
import com.scit.sawwah.R;
import com.scit.sawwah.adapters.SimilarAdvertisementsAdapter;
import com.scit.sawwah.interfaces.IFailure;
import com.scit.sawwah.interfaces.IMove;
import com.scit.sawwah.interfaces.IResponse;
import com.scit.sawwah.tools.BaseActivity;
import com.scit.sawwah.tools.BaseFunctions;
import com.scit.sawwah.tools.BaseRetrofit;

import java.util.ArrayList;
import java.util.List;

public class AdvertisementDetailsActivity extends BaseActivity {
    private RelativeLayout toolbar;
    private ImageButton btn_back;
    private TextView tv_title,tv_ad_title,tv_inst_name,tv_desc,tv_start_date,tv_end_date,tv_lovers,tv_viewers,tv_similar;
    private ImageView img_inst_icon,img_love,img_share;
    private RecyclerView rv_similar;
    private List<AdvertisementObject> list_similar;
    private SimilarAdvertisementsAdapter similar_adapter;
    private LinearLayoutManager similar_layout_manager;
    private LinearLayout root;
    private String id = "";
    private boolean loved = false;
    private int ads_id = 0;
    private Intent myIntent;
    private int lovers = 0;
    private CallbackManager callbackManager;
    private ShareDialog shareDialog;


    @Override
    public void set_layout() {
        setContentView(R.layout.activity_advertisement_details);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        root.setVisibility(View.GONE);
        tv_title.setText(getResources().getString(R.string.advertisement_details_title));
        //Similar Advertisements
        list_similar = new ArrayList<>();
        similar_adapter = new SimilarAdvertisementsAdapter(AdvertisementDetailsActivity.this, list_similar,new IMove() {
            @Override
            public void move(int position) {

            }
        });
        similar_layout_manager = new LinearLayoutManager(AdvertisementDetailsActivity.this,LinearLayoutManager.HORIZONTAL,false);
        rv_similar.setLayoutManager(similar_layout_manager);
        rv_similar.setAdapter(similar_adapter);
        callAPI();
    }

    @Override
    public void init_views() {
        toolbar = findViewById(R.id.advertisement_toolbar);
        btn_back = toolbar.findViewById(R.id.toolbar_back_btn);
        tv_title = toolbar.findViewById(R.id.toolbar_title);
        rv_similar = findViewById(R.id.advertisement_details_similar_recycler);
        tv_ad_title = findViewById(R.id.advertisement_details_title_txt);
        tv_inst_name = findViewById(R.id.advertisement_details_institution_name_txt);
        tv_desc = findViewById(R.id.advertisement_details_desc);
        tv_start_date = findViewById(R.id.advertisement_details_start_date);
        tv_end_date = findViewById(R.id.advertisement_details_end_date);
        tv_lovers = findViewById(R.id.advertisement_details_lovers);
        tv_viewers = findViewById(R.id.advertisement_details_viewers);
        img_inst_icon = findViewById(R.id.advertisement_details_int_icon);
        img_love = findViewById(R.id.advertisement_details_love);
        img_share = findViewById(R.id.advertisement_details_share);
        root = findViewById(R.id.advertisement_details_layout);
        tv_similar = findViewById(R.id.advertisement_details_similar_text);
        myIntent =getIntent();
    }

    @Override
    public void init_events() {
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        img_love.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (loved == false){
                    toggle_item(String.valueOf(ads_id),"3","1");
                }else {
                    toggle_item(String.valueOf(ads_id),"3","0");
                }
            }
        });
        img_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FacebookSdk.sdkInitialize(AdvertisementDetailsActivity.this.getApplicationContext());
                callbackManager = CallbackManager.Factory.create();
                shareDialog = new ShareDialog(AdvertisementDetailsActivity.this);
                shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
                    @Override
                    public void onSuccess(Sharer.Result result) {
                        Toast.makeText(AdvertisementDetailsActivity.this, "Posted successfully", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onCancel() {
                        Toast.makeText(AdvertisementDetailsActivity.this, "Posted canceled", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(FacebookException error) {
                        Toast.makeText(AdvertisementDetailsActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
                ShareLinkContent linkContent = new ShareLinkContent.Builder().setQuote(getResources().getString(R.string.facebook_post_ads)).
                        setContentUrl(Uri.parse(BaseRetrofit.BASE_ALI+"posters/"+String.valueOf(ads_id))).build();
                if (shareDialog.canShow(ShareLinkContent.class)){
                    shareDialog.show(linkContent);
                }else {
                    Toast.makeText(AdvertisementDetailsActivity.this, "can't show", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void set_fragment_place() {

    }
    private void callAPI(){
        AdvertisementsAPIsClass.details(sharedPreferences,
                AdvertisementDetailsActivity.this,
                sharedPreferences.getString("language", "ar"),
                sharedPreferences.getString("service_token", ""),
                0,
                0,
                String.valueOf(myIntent.getIntExtra("advertisement_id",0)),
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json != null){
                            String json1 = new Gson().toJson(json);
                            AdvertisementDetailsResponse success = new Gson().fromJson(json1,AdvertisementDetailsResponse.class);
                            if (success.getPoster()!=null){
                                //Advertisement id
                                ads_id = success.getPoster().getId();
                                //Advertisement title
                                if (success.getPoster().getTitle()!=null){
                                    tv_ad_title.setText(success.getPoster().getTitle());
                                }
                                //Institution name
                                if (success.getPoster().getFacility_name()!=null){
                                    tv_inst_name.setText(success.getPoster().getFacility_name());
                                }
                                //Description
                                if (success.getPoster().getDetails()!=null){
                                    tv_desc.setText(success.getPoster().getDetails());
                                }
                                //Start date
                                if (success.getPoster().getStart_date()!=null){
                                    tv_start_date.setText(BaseFunctions.dateExtractor(success.getPoster().getStart_date()));
                                }
                                //End Date
                                if (success.getPoster().getEnd_date()!=null){
                                    tv_end_date.setText(BaseFunctions.dateExtractor(success.getPoster().getEnd_date()));
                                }
                                //Lovers
                                lovers = success.getPoster().getFavourite_count();
                                tv_lovers.setText(String.valueOf(lovers));
                                //Love
                                if (!success.getPoster().getMarked_as_favourite()){
                                    img_love.setImageResource(R.drawable.ic_grey_heart);
                                }else {
                                    img_love.setImageResource(R.drawable.ic_heart);
                                }
                                //Viewers
                                tv_viewers.setText(String.valueOf(success.getPoster().getVisit_views()));
                                //Institution icon
                                if (success.getPoster().getFacility_image()!=null){
                                    try{
                                        GlideApp.with(AdvertisementDetailsActivity.this).load(success.getPoster().getFacility_image())
                                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                                .centerCrop().into(img_inst_icon);
                                    }catch (Exception e){}
                                }
                                //Love
                                if (success.getPoster().getMarked_as_favourite()!=null){
                                    if (success.getPoster().getMarked_as_favourite()){
                                        img_love.setImageResource(R.drawable.ic_heart);
                                    }else {
                                        img_love.setImageResource(R.drawable.ic_grey_heart);
                                    }
                                }
                            }
                            //Similar institutions
                            if (success.getRelated_posters()!=null){
                                if (success.getRelated_posters().size()>0){
                                    for (AdvertisementObject ao : success.getRelated_posters()){
                                        list_similar.add(ao);
                                        similar_adapter.notifyDataSetChanged();
                                    }
                                }else {
                                    tv_similar.setVisibility(View.GONE);
                                    rv_similar.setVisibility(View.GONE);
                                }
                            }
                            root.setVisibility(View.VISIBLE);
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callAPI();
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });
    }

    private void toggle_item(String id, String type, final String state){
        FavouriteAPIsClass.toggle_item(sharedPreferences,
                AdvertisementDetailsActivity.this,
                sharedPreferences.getString("language", "ar"),
                sharedPreferences.getString("service_token", ""),
                id,
                type,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json !=null){
                            if (state.equals("0")){
                                lovers--;
                                tv_lovers.setText(String.valueOf(lovers));
                                img_love.setImageResource(R.drawable.ic_grey_heart);
                                Toast.makeText(AdvertisementDetailsActivity.this,getResources().getString(R.string.favourite_ads_remover), Toast.LENGTH_SHORT).show();
                            }else {
                                lovers++;
                                tv_lovers.setText(String.valueOf(lovers));
                                img_love.setImageResource(R.drawable.ic_heart);
                                Toast.makeText(AdvertisementDetailsActivity.this, getResources().getString(R.string.favourite_ads_add), Toast.LENGTH_SHORT).show();
                            }
                        }else {
                            Toast.makeText(AdvertisementDetailsActivity.this, getResources().getString(R.string.error_occurred), Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Toast.makeText(AdvertisementDetailsActivity.this,getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                    }
                });
    }

}
