package com.scit.sawwah.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Environment;
import androidx.annotation.NonNull;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;
import com.scit.sawwah.APIClasses.InstitutionsAPIsClass;
import com.scit.sawwah.Models.CategoryObject;
import com.scit.sawwah.Models.CityObject;
import com.scit.sawwah.Models.ContactInfoObject;
import com.scit.sawwah.Models.CountryObject;
import com.scit.sawwah.Models.InstitutionObject;
import com.scit.sawwah.R;
import com.scit.sawwah.adapters.CustomSpinnerAdapter;
import com.scit.sawwah.adapters.RegisterCategoriesAdapter;
import com.scit.sawwah.adapters.RegisterCategoriesRowsAdapter;
import com.scit.sawwah.interfaces.IFailure;
import com.scit.sawwah.interfaces.IMove;
import com.scit.sawwah.interfaces.IResponse;
import com.scit.sawwah.tools.BaseActivity;
import com.soundcloud.android.crop.Crop;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class RegisterInstitutionActivity extends BaseActivity {
    private RelativeLayout toolbar;
    private ImageButton btn_back;
    private RecyclerView categories_recycler;
    private List<CategoryObject> categories_list;
    private List<CategoryObject> input_categories_list;
    private RegisterCategoriesAdapter categories_adapter;
    private GridLayoutManager categories_layout_manager;
    private Spinner countries_spinner,cities_spinner;
    private CustomSpinnerAdapter countries_adapter,cities_adapter;
    private List<CountryObject> countries_list;
    private List<CityObject> cities_list;
    private List<String> countries_list_string,cities_list_string;
    private ImageView map;
    private LinearLayout btn_next;
    private TextView tv_choose_category;
    private SimpleRatingBar rb_rate;
    private EditText edt_name,edt_desc,edt_address,edt_phone,edt_land_line;
    private CircleImageView img_icon;
    private RelativeLayout lyt_cover;
    private ImageView img_cover;
    private String choosed_city_id = "",choosed_country_id = "";
    private static final String TEMP_PHOTO_FILE1 = "temporary_holder1.jpg";
    private static final String TEMP_PHOTO_FILE2 = "temporary_holder2.jpg";
    private String iconPath = "",coverPath = "";
    private Uri iconUri = null,coverUri = null;
    private boolean icon_chosen = false;
    private boolean cover_chosen = false;
    private int image_clicked = -1;
    private String choosed_lat = "33.524412";
    private String choosed_lng = "36.318375";
    private String choosed_rate = "";
    private LinearLayout root;


    @Override
    public void set_layout() {
        setContentView(R.layout.activity_register_institution);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        init_recycler();
        init_spinner();
    }

    @Override
    public void init_views() {
        toolbar = findViewById(R.id.register_inst_toolbar);
        btn_back = toolbar.findViewById(R.id.toolbar_back_btn);
        categories_recycler = findViewById(R.id.register_inst_recycler);
        countries_spinner = findViewById(R.id.register_inst_country_spinner);
        cities_spinner = findViewById(R.id.register_inst_city_spinner);
        map = findViewById(R.id.register_inst_map);
        btn_next = findViewById(R.id.register_inst_next_btn);
        tv_choose_category = findViewById(R.id.register_inst_category_arrow);
        rb_rate = findViewById(R.id.register_inst_rate);
        edt_name = findViewById(R.id.register_inst_name);
        edt_desc = findViewById(R.id.register_inst_desc);
        edt_address = findViewById(R.id.register_inst_address);
        img_icon = findViewById(R.id.register_inst_icon);
        lyt_cover = findViewById(R.id.register_inst_cover_layout);
        img_cover = findViewById(R.id.register_inst_cover_img);
        root = findViewById(R.id.register_inst_layout);
        edt_phone = findViewById(R.id.register_inst_phone_number);
        edt_land_line = findViewById(R.id.register_inst_land_line);
    }

    @Override
    public void init_events() {
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(RegisterInstitutionActivity.this, MapActivity.class);
                i.putExtra("type","without_input");
                startActivityForResult(i, 1);
            }
        });
        tv_choose_category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder=new AlertDialog.Builder(RegisterInstitutionActivity.this);
                View view=getLayoutInflater().inflate(R.layout.dialog_register_categories,null);
                input_categories_list = new ArrayList<>();
                CategoryObject[] input_category_array = new Gson().fromJson(sharedPreferences.getString("categories","[]"),CategoryObject[].class);
                if (input_category_array.length>0){
                    for (CategoryObject co : input_category_array){
                        input_categories_list.add(co);
                    }
                }
                RecyclerView recycler = view.findViewById(R.id.register_categories_dialog_recycler);
                builder.setView(view);
                final AlertDialog dialog = builder.create();
                LinearLayoutManager layoutManager = new LinearLayoutManager(RegisterInstitutionActivity.this,LinearLayoutManager.VERTICAL,false);
                RegisterCategoriesRowsAdapter adapter = new RegisterCategoriesRowsAdapter(RegisterInstitutionActivity.this, input_categories_list, new IMove() {
                    @Override
                    public void move(int position) {
                        if (categories_list.size()==0){
                            categories_list.add(input_categories_list.get(position));
                            categories_adapter.notifyDataSetChanged();
                            dialog.cancel();
                        }else {
                            Toast.makeText(RegisterInstitutionActivity.this, getResources().getString(R.string.register_inst_no_more_category), Toast.LENGTH_SHORT).show();
                            dialog.cancel();
                        }
                    }
                });
                recycler.setLayoutManager(layoutManager);
                recycler.setAdapter(adapter);
                dialog.show();
            }
        });
        countries_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int i, long id) {
                long pos = parent.getItemIdAtPosition(i);
                int position = Integer.valueOf(String.valueOf(pos));
                choosed_country_id = String.valueOf(countries_list.get(position).getId());
                CountryObject co = countries_list.get(position);
                if (co.getCities()!=null){
                    if (co.getCities().size() > 0){
                        cities_list.clear();
                        cities_list_string.clear();
                        cities_adapter.notifyDataSetChanged();
                        for (CityObject coo : co.getCities()){
                            cities_list.add(coo);
                            cities_list_string.add(coo.getName());
                            cities_adapter.notifyDataSetChanged();
                        }
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        cities_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int i, long id) {
                long pos = parent.getItemIdAtPosition(i);
                int position = Integer.valueOf(String.valueOf(pos));
                choosed_city_id = String.valueOf(cities_list.get(position).getId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        img_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityCompat.requestPermissions(RegisterInstitutionActivity.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        1);
                image_clicked = 1;
            }
        });
        lyt_cover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityCompat.requestPermissions(RegisterInstitutionActivity.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        1);
                image_clicked = 2;
            }
        });
        rb_rate.setOnRatingBarChangeListener(new SimpleRatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(SimpleRatingBar simpleRatingBar, float rating, boolean fromUser) {
                choosed_rate = String.valueOf(rating);
            }
        });
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(categories_list.size() == 0){
                    Toast.makeText(RegisterInstitutionActivity.this, getResources().getString(R.string.register_inst_no_category), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (choosed_country_id.equals("")){
                    Toast.makeText(RegisterInstitutionActivity.this, getResources().getString(R.string.register_inst_no_country), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (choosed_city_id.equals("")){
                    Toast.makeText(RegisterInstitutionActivity.this, getResources().getString(R.string.register_inst_no_city), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (choosed_rate.equals("")){
                    Toast.makeText(RegisterInstitutionActivity.this, getResources().getString(R.string.register_inst_no_rate), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (edt_name.getText().toString().equals("")){
                    Toast.makeText(RegisterInstitutionActivity.this, getResources().getString(R.string.register_inst_no_name), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (edt_desc.getText().toString().equals("")){
                    Toast.makeText(RegisterInstitutionActivity.this, getResources().getString(R.string.register_inst_no_desc), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (edt_address.getText().toString().equals("")){
                    Toast.makeText(RegisterInstitutionActivity.this, getResources().getString(R.string.register_inst_no_address), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!icon_chosen){
                    Toast.makeText(RegisterInstitutionActivity.this, getResources().getString(R.string.register_inst_no_icon), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!cover_chosen){
                    Toast.makeText(RegisterInstitutionActivity.this, getResources().getString(R.string.register_inst_no_cover), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (choosed_lat.equals("")||choosed_lng.equals("")){
                    Toast.makeText(RegisterInstitutionActivity.this, getResources().getString(R.string.register_inst_no_location), Toast.LENGTH_SHORT).show();
                    return;
                }
                callAPI();

            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case 1:
            {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    Crop.pickImage(RegisterInstitutionActivity.this);
                }else {
                    Toast.makeText(this, getResources().getString(R.string.add_photos_permission_denied), Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }

    private Uri getTempUri() {
        if (image_clicked == 1){
            return Uri.fromFile(getTempFile(TEMP_PHOTO_FILE1));
        }else {
            return Uri.fromFile(getTempFile(TEMP_PHOTO_FILE2));
        }
    }

    private File getTempFile(String temp) {
        if (isSDCARDMounted()) {

            File f = new File(Environment.getExternalStorageDirectory(),temp);
            try {
                f.createNewFile();
            } catch (IOException e) {

            }
            return f;
        } else {
            return null;
        }
    }

    private boolean isSDCARDMounted(){
        String status = Environment.getExternalStorageState();
        if (status.equals(Environment.MEDIA_MOUNTED))
            return true;
        return false;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {
        if (requestCode == 1){
            if(resultCode == Activity.RESULT_OK){
                choosed_lat=data.getStringExtra("latitude");
                choosed_lng = data.getStringExtra("longitude");
            }
        }
        if (resultCode == RESULT_OK){
            if (requestCode == Crop.REQUEST_PICK){
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Uri source_uri = data.getData();
                        if (image_clicked == 1){
                            Uri destination_uri = Uri.fromFile(getTempFile(TEMP_PHOTO_FILE1));
                            Crop.of(source_uri,destination_uri).asSquare().start(RegisterInstitutionActivity.this);
                            //img_personal_image.setImageURI(Crop.getOutput(data));
                            String filePath= Environment.getExternalStorageDirectory()
                                    +"/"+ TEMP_PHOTO_FILE1;
                            iconPath = filePath;
                            icon_chosen = true;
                        }else {
                            Uri destination_uri = Uri.fromFile(getTempFile(TEMP_PHOTO_FILE2));
                            Crop.of(source_uri,destination_uri).withAspect(16,9).start(RegisterInstitutionActivity.this);
                            //img_personal_image.setImageURI(Crop.getOutput(data));
                            String filePath= Environment.getExternalStorageDirectory()
                                    + "/"+TEMP_PHOTO_FILE2;
                            coverPath = filePath;
                            cover_chosen = true;
                        }

                    }
                });
            }else if (requestCode == Crop.REQUEST_CROP){
                handle_crop(resultCode,data);
            }
        }
    }

    private void handle_crop(int code, final Intent result){
        if (code == RESULT_OK){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (image_clicked == 1){
                        img_icon.setImageDrawable(null);
                        img_icon.setImageURI(Crop.getOutput(result));
                        String filePath= Environment.getExternalStorageDirectory()
                                + "/"+TEMP_PHOTO_FILE1;
                        iconPath = filePath;
                        icon_chosen = true;
                    }else {
                        img_cover.setImageDrawable(null);
                        img_cover.setImageURI(Crop.getOutput(result));
                        String filePath= Environment.getExternalStorageDirectory()
                                +"/"+ TEMP_PHOTO_FILE2;
                        coverPath = filePath;
                        cover_chosen = true;
                    }

                }
            });

        }else if (code == Crop.RESULT_ERROR){
            Toast.makeText(this, "An error occurred", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void set_fragment_place() {

    }

    private void init_spinner(){
        countries_list = new ArrayList<>();
        cities_list = new ArrayList<>();
        countries_list_string = new ArrayList<>();
        cities_list_string = new ArrayList<>();

        CountryObject[] countries_array = new Gson().fromJson(sharedPreferences.getString("countries","[]"),CountryObject[].class);
        if (countries_array.length > 0){
            for (CountryObject co : countries_array){
                countries_list.add(co);
                countries_list_string.add(co.getName());
            }
        }

        countries_adapter = new CustomSpinnerAdapter(RegisterInstitutionActivity.this,R.layout.spinner_white_arrow_item,countries_list_string);
        countries_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        countries_spinner.setAdapter(countries_adapter);

        cities_adapter = new CustomSpinnerAdapter(RegisterInstitutionActivity.this,R.layout.spinner_white_arrow_item,cities_list_string);
        cities_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cities_spinner.setAdapter(cities_adapter);
    }

    private void init_recycler(){
        categories_list = new ArrayList<>();
        categories_adapter = new RegisterCategoriesAdapter(RegisterInstitutionActivity.this, categories_list, new IMove() {
            @Override
            public void move(int position) {
                //categories_list.remove(position);
            }
        });
        categories_layout_manager = new GridLayoutManager(RegisterInstitutionActivity.this,2);
        categories_recycler.setLayoutManager(categories_layout_manager);
        categories_recycler.setAdapter(categories_adapter);
    }

    private void callAPI(){
        String phonetxt = edt_phone.getText().toString();
        String landtxt = edt_land_line.getText().toString();
        ContactInfoObject cio = new ContactInfoObject();
        if (phonetxt.equals("")){
            phonetxt = "0";
        }
        if (landtxt.equals("")){
            landtxt = "0";
        }
        cio.setMobile_number(phonetxt);
        cio.setPhone_number(landtxt);
        InstitutionsAPIsClass.add(sharedPreferences,
                RegisterInstitutionActivity.this,
                sharedPreferences.getString("language", "ar"),
                sharedPreferences.getString("service_token", ""),
                String.valueOf(categories_list.get(0).getId()),
                choosed_city_id,
                choosed_lat,
                choosed_lng,
                edt_name.getText().toString(),
                edt_address.getText().toString(),
                edt_desc.getText().toString(),
                choosed_rate,
                iconPath,
                coverPath,
                new Gson().toJson(cio),
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null){
                            String json1 = new Gson().toJson(json);
                            InstitutionObject success = new Gson().fromJson(json1,InstitutionObject.class);
                            List<InstitutionObject> list = new ArrayList<>();
                            list.add(success);
                            String list_json = new Gson().toJson(list);
                            editor.putString("my_insts",list_json);
                            editor.commit();
                            Toast.makeText(RegisterInstitutionActivity.this, getResources().getString(R.string.register_inst_added_success), Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(RegisterInstitutionActivity.this,ThanksActivity.class));
                            finish();
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callAPI();
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });
    }
}
