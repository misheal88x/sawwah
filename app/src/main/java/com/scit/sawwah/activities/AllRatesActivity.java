package com.scit.sawwah.activities;

import android.content.Context;
import android.content.Intent;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.scit.sawwah.APIClasses.InstitutionsAPIsClass;
import com.scit.sawwah.Models.RateObject;
import com.scit.sawwah.Models.RatesResponse;
import com.scit.sawwah.R;
import com.scit.sawwah.adapters.AllUsersRatesAdapter;
import com.scit.sawwah.interfaces.IFailure;
import com.scit.sawwah.interfaces.IMove;
import com.scit.sawwah.interfaces.IResponse;
import com.scit.sawwah.tools.BaseActivity;
import com.scit.sawwah.tools.EndlessRecyclerViewScrollListener;

import java.util.ArrayList;
import java.util.List;

public class AllRatesActivity extends BaseActivity {
    private RelativeLayout toolbar;
    private TextView tv_title;
    private ImageButton btn_back;
    private RecyclerView recyclerView;
    private List<RateObject> list_rates;
    private AllUsersRatesAdapter adapter;
    private LinearLayoutManager layoutManager;
    private RelativeLayout root;
    private String id = "";
    private int currentPage = 1;
    private Intent myIntent;
    private LinearLayout empty_layout;

    @Override
    public void set_layout() {
        setContentView(R.layout.activity_all_rates);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        tv_title.setText(getResources().getString(R.string.all_rates_title));
        init_recycler();
        callAPI(currentPage,0);
    }

    @Override
    public void init_views() {
        toolbar = findViewById(R.id.all_rates_toolbar);
        tv_title = toolbar.findViewById(R.id.toolbar_title);
        btn_back = toolbar.findViewById(R.id.toolbar_back_btn);
        recyclerView = findViewById(R.id.all_rates_recycler);
        root = findViewById(R.id.all_rates_layout);
        empty_layout = findViewById(R.id.empty_layout);
        myIntent = getIntent();
        id = String.valueOf(myIntent.getIntExtra("institution_id",0));
    }

    @Override
    public void init_events() {
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void set_fragment_place() {

    }

    private void init_recycler(){
        list_rates = new ArrayList<>();
        adapter = new AllUsersRatesAdapter(AllRatesActivity.this, list_rates,new IMove() {
            @Override
            public void move(int position) {

            }
        });
        layoutManager = new LinearLayoutManager(AllRatesActivity.this,LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        recyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if (list_rates.size()>=20){
                    Snackbar.make(root,getResources().getString(R.string.get_more),Snackbar.LENGTH_SHORT).show();
                    currentPage++;
                    callAPI(currentPage,1);
                }
            }
        });
    }
    private void callAPI(final int page, final int type){
        InstitutionsAPIsClass.getRatings(sharedPreferences,
                AllRatesActivity.this,
                sharedPreferences.getString("language","ar"),
                sharedPreferences.getString("service_token",""),
                0,
                0,
                id,
                page,
                type,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json !=null){
                            String json1 = new Gson().toJson(json);
                            RatesResponse success = new Gson().fromJson(json1,RatesResponse.class);
                            if (success.getData()!=null){
                                if (success.getData().size() > 0){
                                    empty_layout.setVisibility(View.GONE);
                                    for (RateObject oo : success.getData()){
                                        list_rates.add(oo);
                                        adapter.notifyDataSetChanged();
                                    }
                                    if (type == 0){
                                        runAnimation(recyclerView,0,adapter);
                                    }
                                }else {
                                    if (type == 1){
                                        Snackbar.make(root,getResources().getString(R.string.no_more),Snackbar.LENGTH_SHORT).show();
                                    }else {
                                        empty_layout.setVisibility(View.VISIBLE);
                                        Toast.makeText(AllRatesActivity.this, getResources().getString(R.string.no_data), Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callAPI(page,type);
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });
    }
    public void runAnimation(RecyclerView recyclerView,int type,AllUsersRatesAdapter adapter){
        Context context = recyclerView.getContext();
        LayoutAnimationController controller = null;
        if (type == 0){ // Fall down animation
            controller = AnimationUtils.loadLayoutAnimation(context,R.anim.layout_fall_down_vertical);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutAnimation(controller);
            recyclerView.getAdapter().notifyDataSetChanged();
            recyclerView.scheduleLayoutAnimation();
        }
    }
}
