package com.scit.sawwah.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Environment;
import androidx.annotation.NonNull;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.scit.sawwah.APIClasses.InstitutionsAPIsClass;
import com.scit.sawwah.R;
import com.scit.sawwah.adapters.CustomSpinnerAdapter;
import com.scit.sawwah.interfaces.IFailure;
import com.scit.sawwah.interfaces.IResponse;
import com.scit.sawwah.tools.BaseActivity;
import com.scit.sawwah.tools.BaseFunctions;
import com.soundcloud.android.crop.Crop;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AddSocialActivity extends BaseActivity {

    private RelativeLayout toolbar;
    private ImageButton btn_back;
    private TextView tv_title;
    private ImageView image;
    private EditText edt_name,edt_link;
    private LinearLayout btn_add;
    private String selected_image_path = "";
    private Intent myIntent;
    private LinearLayout root;
    private Spinner type_spinner;
    private List<String> type_list_string;
    private List<SocialType> type_list;
    private CustomSpinnerAdapter type_adapter;
    private int selected_type = -1;
    private static final String TEMP_PHOTO_FILE = "temporary_holder.jpg";

    @Override
    public void set_layout() {
        setContentView(R.layout.activity_add_social);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        tv_title.setText(getResources().getString(R.string.add_social_title));
        init_spinner();
    }

    @Override
    public void init_views() {
        toolbar = findViewById(R.id.add_social_toolbar);
        btn_back = toolbar.findViewById(R.id.toolbar_back_btn);
        tv_title = toolbar.findViewById(R.id.toolbar_title);
        image = findViewById(R.id.add_social_image);
        type_spinner = findViewById(R.id.add_social_type_spinner);
        btn_add = findViewById(R.id.add_social_add_btn);
        myIntent = getIntent();
        root = findViewById(R.id.add_social_layout);
        edt_name = findViewById(R.id.add_social_name_txt);
        edt_link = findViewById(R.id.add_social_link_txt);
    }

    @Override
    public void init_events() {
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityCompat.requestPermissions(AddSocialActivity.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        1);
            }
        });
        type_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int i, long id) {
                int position = BaseFunctions.getSpinnerPosition(parent,i);
                selected_type = type_list.get(position).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selected_image_path.equals("")){
                    Toast.makeText(AddSocialActivity.this, getResources().getString(R.string.add_social_no_image), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(edt_name.getText().toString())){
                    Toast.makeText(AddSocialActivity.this, getResources().getString(R.string.add_social_no_name), Toast.LENGTH_SHORT).show();
                    edt_name.requestFocus();
                    return;
                }
                if (TextUtils.isEmpty(edt_link.getText().toString())){
                    Toast.makeText(AddSocialActivity.this, getResources().getString(R.string.add_social_no_link), Toast.LENGTH_SHORT).show();
                    edt_name.requestFocus();
                    return;
                }
                if (selected_type == -1){
                    Toast.makeText(AddSocialActivity.this, getResources().getString(R.string.add_social_no_type), Toast.LENGTH_SHORT).show();
                    return;
                }
                callAPI();
            }
        });
    }

    @Override
    public void set_fragment_place() {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case 1:
            {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    Crop.pickImage(AddSocialActivity.this);
                }else {
                    Toast.makeText(this, getResources().getString(R.string.add_photos_permission_denied), Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }

    private File getTempFile() {
        if (isSDCARDMounted()) {

            File f = new File(Environment.getExternalStorageDirectory(),TEMP_PHOTO_FILE);
            try {
                f.createNewFile();
            } catch (IOException e) {

            }
            return f;
        } else {
            return null;
        }
    }

    private boolean isSDCARDMounted(){
        String status = Environment.getExternalStorageState();
        if (status.equals(Environment.MEDIA_MOUNTED))
            return true;
        return false;
    }

    protected void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK){
            if (requestCode == Crop.REQUEST_PICK){
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Uri source_uri = data.getData();
                        Uri destination_uri = Uri.fromFile(getTempFile());
                        Crop.of(source_uri,destination_uri).asSquare().start(AddSocialActivity.this);
                        //img_personal_image.setImageURI(Crop.getOutput(data));
                        String filePath= Environment.getExternalStorageDirectory()
                                + "/temporary_holder.jpg";
                        selected_image_path = filePath;
                    }
                });
            }else if (requestCode == Crop.REQUEST_CROP){
                handle_crop(resultCode,data);
            }
        }
    }

    private void handle_crop(int code, final Intent result){
        if (code == RESULT_OK){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    image.setImageDrawable(null);
                    image.setImageURI(Crop.getOutput(result));
                    String filePath= Environment.getExternalStorageDirectory()
                            + "/temporary_holder.jpg";
                    selected_image_path = filePath;
                }
            });

        }else if (code == Crop.RESULT_ERROR){
            Toast.makeText(this, "An error occurred", Toast.LENGTH_SHORT).show();
        }
    }
    private void init_list() {
        type_list = new ArrayList<>();
        type_list_string = new ArrayList<>();
        SocialType st0 = new SocialType(0,getResources().getString(R.string.add_social_website));
        SocialType st1 = new SocialType(1,getResources().getString(R.string.add_social_mobile));
        SocialType st2 = new SocialType(2,getResources().getString(R.string.add_social_email));
        SocialType st3 = new SocialType(3,getResources().getString(R.string.add_social_facebook));
        SocialType st4 = new SocialType(4,getResources().getString(R.string.add_social_twitter));
        type_list.add(st0);
        type_list_string.add(st0.getName());
        type_list.add(st1);
        type_list_string.add(st1.getName());
        type_list.add(st2);
        type_list_string.add(st2.getName());
        type_list.add(st3);
        type_list_string.add(st3.getName());
        type_list.add(st4);
        type_list_string.add(st4.getName());
    }
    private void init_spinner(){
        init_list();
        type_adapter = new CustomSpinnerAdapter(AddSocialActivity.this,R.layout.new_transparent_spinner_item,type_list_string);
        type_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        type_spinner.setAdapter(type_adapter);
    }

    private void callAPI(){
        InstitutionsAPIsClass.addSocial(
                sharedPreferences,
                AddSocialActivity.this,
                sharedPreferences.getString("language", "ar"),
                sharedPreferences.getString("service_token", ""),
                edt_name.getText().toString(),
                edt_link.getText().toString(),
                String.valueOf(selected_type),
                String.valueOf(myIntent.getIntExtra("facility_id", 0)),
                selected_image_path,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null){
                            Intent intent = new Intent();
                            intent.putExtra("key","value");
                            setResult(RESULT_OK,intent);
                            finish();
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callAPI();
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                }
        );
    }

    class SocialType {
        private int id = 0;
        private String name = "";

        public SocialType() { }

        public SocialType(int id, String name) {
            this.id = id;
            this.name = name;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
