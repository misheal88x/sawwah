package com.scit.sawwah.activities;

import android.content.Context;
import android.content.Intent;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.scit.sawwah.APIClasses.CardAPIsClass;
import com.scit.sawwah.Models.BeneficiariesResponse;
import com.scit.sawwah.Models.BeneficiaryObject;
import com.scit.sawwah.R;
import com.scit.sawwah.adapters.BeneficiariesAdapter;
import com.scit.sawwah.interfaces.IFailure;
import com.scit.sawwah.interfaces.IMove;
import com.scit.sawwah.interfaces.IResponse;
import com.scit.sawwah.tools.BaseActivity;
import com.scit.sawwah.tools.EndlessRecyclerViewScrollListener;

import java.util.ArrayList;
import java.util.List;

public class BeneficiariesActivity extends BaseActivity {
    private RelativeLayout toolbar;
    private ImageButton btn_back;
    private TextView tv_title;
    private RecyclerView rv_benes;
    private List<BeneficiaryObject> list_benes;
    private BeneficiariesAdapter adapter;
    private LinearLayoutManager layoutManager;
    private RelativeLayout root;
    private Intent myIntent;
    private int currentPage = 1;
    private LinearLayout empty_layout;

    @Override
    public void set_layout() {
        setContentView(R.layout.activity_beneficiaries);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        tv_title.setText(getResources().getString(R.string.beneficiaries_title));
        init_recycler();
        callAPI(0,currentPage);
    }

    @Override
    public void init_views() {
        toolbar = findViewById(R.id.beneficiaries_toolbar);
        btn_back = toolbar.findViewById(R.id.toolbar_back_btn);
        tv_title = toolbar.findViewById(R.id.toolbar_title);
        rv_benes = findViewById(R.id.beneficiaries_recycler);
        root = findViewById(R.id.beneficiaries_layout);
        empty_layout = findViewById(R.id.empty_layout);
        myIntent = getIntent();
    }

    @Override
    public void init_events() {
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void set_fragment_place() {

    }

    private void init_recycler(){
        list_benes = new ArrayList<>();
        adapter = new BeneficiariesAdapter(BeneficiariesActivity.this,list_benes, new IMove() {
            @Override
            public void move(int position) {

            }
        });
        layoutManager = new LinearLayoutManager(BeneficiariesActivity.this,LinearLayoutManager.VERTICAL,false);
        rv_benes.setLayoutManager(layoutManager);
        rv_benes.setAdapter(adapter);
        rv_benes.addOnScrollListener(new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if (list_benes.size()>=20){
                    Snackbar.make(root,getResources().getString(R.string.get_more),Snackbar.LENGTH_SHORT).show();
                    currentPage++;
                    callAPI(1,currentPage);
                }
            }
        });
    }

    private void callAPI(final int type, final int page){
        CardAPIsClass.get_benefits(sharedPreferences,
                BeneficiariesActivity.this,
                sharedPreferences.getString("language", "ar"),
                sharedPreferences.getString("service_token", ""),
                String.valueOf(myIntent.getIntExtra("institution_id",0)),
                type,
                page,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json !=null){
                            String json1 = new Gson().toJson(json);
                            BeneficiariesResponse success = new Gson().fromJson(json1,BeneficiariesResponse.class);
                            if (success.getData()!=null){
                                if (success.getData().size() > 0){
                                    empty_layout.setVisibility(View.GONE);
                                    for (BeneficiaryObject bo : success.getData()){
                                        list_benes.add(bo);
                                        adapter.notifyDataSetChanged();
                                    }
                                    if (type == 0){
                                        runAnimation(rv_benes,0,adapter);
                                    }
                                }else {
                                    if (type == 1){
                                        Snackbar.make(root,getResources().getString(R.string.no_more),Snackbar.LENGTH_SHORT).show();
                                    }else {
                                        empty_layout.setVisibility(View.VISIBLE);
                                        Toast.makeText(BeneficiariesActivity.this, getResources().getString(R.string.no_data), Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callAPI(type,page);
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });
    }
    public void runAnimation(RecyclerView recyclerView,int type,BeneficiariesAdapter adapter){
        Context context = recyclerView.getContext();
        LayoutAnimationController controller = null;
        if (type == 0){ // Fall down animation
            controller = AnimationUtils.loadLayoutAnimation(context,R.anim.layout_fall_down_vertical);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutAnimation(controller);
            recyclerView.getAdapter().notifyDataSetChanged();
            recyclerView.scheduleLayoutAnimation();
        }
    }
}
