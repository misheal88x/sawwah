package com.scit.sawwah.activities;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.widget.NestedScrollView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.gson.Gson;
import com.scit.sawwah.APIClasses.AdvertisementsAPIsClass;
import com.scit.sawwah.APIClasses.OffersAPIsClass;
import com.scit.sawwah.Models.AdvertisementObject;
import com.scit.sawwah.Models.AdvertisementsResponse;
import com.scit.sawwah.Models.OfferObject;
import com.scit.sawwah.Models.OffersResponse;
import com.scit.sawwah.R;
import com.scit.sawwah.adapters.AllAdvertisementsAdapter;
import com.scit.sawwah.adapters.PreferredOffersAdapter;
import com.scit.sawwah.interfaces.IFailure;
import com.scit.sawwah.interfaces.IMove;
import com.scit.sawwah.interfaces.IResponse;
import com.scit.sawwah.tools.BaseActivity;
import com.scit.sawwah.tools.BaseRetrofit;

import java.util.ArrayList;
import java.util.List;

public class OffersAndAdvertisementsActivity extends BaseActivity {
    private RelativeLayout toolbar;
    private TextView tv_title;
    private ImageButton btn_back;
    private Button btn_all_offers,btn_all_ads;
    private RecyclerView rv_offers,rv_ads;
    private List<AdvertisementObject> list_ads;
    private List<OfferObject> list_offers;
    private PreferredOffersAdapter offers_adapter;
    private AllAdvertisementsAdapter ads_adapter;
    private LinearLayoutManager offers_layout_manager,ads_layout_manager;
    private RelativeLayout root;
    private RelativeLayout lyt_offers,lyt_ads;
    private NestedScrollView scrollView;
    private int currentPage = 1;
    private LinearLayout empty_layout;
    private CallbackManager callbackManager;
    private ShareDialog shareDialog;

    @Override
    public void set_layout() {
        setContentView(R.layout.activity_offers_and_advertisements);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        tv_title.setText(getResources().getString(R.string.offers_and_ads_title));
        init_recyclers();
        callOffersAPI();
    }

    @Override
    public void init_views() {
        toolbar = findViewById(R.id.offers_and_ads_toolbar);
        tv_title = toolbar.findViewById(R.id.toolbar_title);
        btn_back = toolbar.findViewById(R.id.toolbar_back_btn);
        btn_all_offers = findViewById(R.id.offers_and_ads_all_offered);
        btn_all_ads = findViewById(R.id.offers_and_ads_all_ads);
        rv_offers = findViewById(R.id.offers_and_ads_offers_recycler);
        rv_ads = findViewById(R.id.offers_and_ads_ads_recycler);
        root = findViewById(R.id.offers_and_ads_layout);
        lyt_offers = findViewById(R.id.offer_and_ads_offers_layout);
        lyt_ads = findViewById(R.id.offers_and_ads_ads_layout);
        scrollView = findViewById(R.id.offers_and_ads_scrollview);
        scrollView.setVisibility(View.GONE);
        empty_layout = findViewById(R.id.empty_layout);
    }

    @Override
    public void init_events() {
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btn_all_offers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OffersAndAdvertisementsActivity.this,AllOffersActivity.class);
                intent.putExtra("filter","-1");
                intent.putExtra("with",false);
                startActivity(intent);
            }
        });
        btn_all_ads.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OffersAndAdvertisementsActivity.this,AllAdvertisementsActivity.class);
                intent.putExtra("filter","-1");
                intent.putExtra("with",false);
                startActivity(intent);
            }
        });

    }

    @Override
    public void set_fragment_place() {

    }

    private void init_recyclers(){
        list_ads = new ArrayList<>();
        list_offers = new ArrayList<>();
        offers_adapter = new PreferredOffersAdapter(OffersAndAdvertisementsActivity.this, list_offers,new IMove() {
            @Override
            public void move(int position) {

            }
        });
        offers_layout_manager = new LinearLayoutManager(OffersAndAdvertisementsActivity.this,LinearLayoutManager.HORIZONTAL,false);
        rv_offers.setLayoutManager(offers_layout_manager);
        rv_offers.setAdapter(offers_adapter);

        ads_adapter = new AllAdvertisementsAdapter(OffersAndAdvertisementsActivity.this,list_ads ,new IMove() {
            @Override
            public void move(int position) {
                FacebookSdk.sdkInitialize(OffersAndAdvertisementsActivity.this.getApplicationContext());
                callbackManager = CallbackManager.Factory.create();
                shareDialog = new ShareDialog(OffersAndAdvertisementsActivity.this);
                shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
                    @Override
                    public void onSuccess(Sharer.Result result) {
                        Toast.makeText(OffersAndAdvertisementsActivity.this, "Posted successfully", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onCancel() {
                        Toast.makeText(OffersAndAdvertisementsActivity.this, "Posted canceled", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(FacebookException error) {
                        Toast.makeText(OffersAndAdvertisementsActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
                ShareLinkContent linkContent = new ShareLinkContent.Builder().setQuote(getResources().getString(R.string.facebook_post_ads)).
                        setContentUrl(Uri.parse(BaseRetrofit.BASE_ALI+"posters/"+list_ads.get(position).getId())).build();
                if (shareDialog.canShow(ShareLinkContent.class)){
                    shareDialog.show(linkContent);
                }else {
                    Toast.makeText(OffersAndAdvertisementsActivity.this, "can't show", Toast.LENGTH_SHORT).show();
                }
            }
        });
        ads_layout_manager = new LinearLayoutManager(OffersAndAdvertisementsActivity.this,LinearLayoutManager.VERTICAL,false);
        rv_ads.setLayoutManager(ads_layout_manager);
        rv_ads.setAdapter(ads_adapter);
    }

    private void callOffersAPI(){
        OffersAPIsClass.getAllOffers(sharedPreferences,
                OffersAndAdvertisementsActivity.this,
                sharedPreferences.getString("language", "ar"),
                sharedPreferences.getString("service_token", ""),
                0,
                0,
                "-1",
                1,
                0,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json !=null){
                            String json1 = new Gson().toJson(json);
                            OffersResponse success = new Gson().fromJson(json1,OffersResponse.class);
                            if (success.getData()!=null){
                                if (success.getData().size()>0){
                                    empty_layout.setVisibility(View.GONE);
                                    for (OfferObject oo : success.getData()){
                                        list_offers.add(oo);
                                        offers_adapter.notifyDataSetChanged();
                                    }
                                    runAnimationOffers(rv_offers,0,offers_adapter);
                                }else {
                                    lyt_offers.setVisibility(View.GONE);
                                    rv_offers.setVisibility(View.GONE);
                                }
                            }else {
                                lyt_offers.setVisibility(View.GONE);
                                rv_offers.setVisibility(View.GONE);
                            }
                        }
                        callAdsAPI();
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callOffersAPI();
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });
    }

    private void callAdsAPI(){
        AdvertisementsAPIsClass.getAllAds(sharedPreferences,
                OffersAndAdvertisementsActivity.this,
                sharedPreferences.getString("service_token", ""),
                sharedPreferences.getString("language", "ar"),
                0,
                0,
                "-1",
                0,
                1,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json !=null){
                            String json1 = new Gson().toJson(json);
                            AdvertisementsResponse success = new Gson().fromJson(json1,AdvertisementsResponse.class);
                            if (success.getData()!=null){
                                if (success.getData().size()>0){
                                    empty_layout.setVisibility(View.GONE);
                                    for (AdvertisementObject oo : success.getData()){
                                        list_ads.add(oo);
                                        ads_adapter.notifyDataSetChanged();
                                    }
                                    runAnimationAdvertisements(rv_ads,0,ads_adapter);
                                }else {
                                    lyt_ads.setVisibility(View.GONE);
                                    rv_ads.setVisibility(View.GONE);
                                }
                            }else {
                                lyt_ads.setVisibility(View.GONE);
                                rv_ads.setVisibility(View.GONE);
                            }
                            scrollView.setVisibility(View.VISIBLE);
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callAdsAPI();
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });
    }

    public void runAnimationOffers(RecyclerView recyclerView,int type,PreferredOffersAdapter adapter){
        Context context = recyclerView.getContext();
        LayoutAnimationController controller = null;
        if (type == 0){ // Fall down animation
            controller = AnimationUtils.loadLayoutAnimation(context,R.anim.layout_fall_down);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutAnimation(controller);
            recyclerView.getAdapter().notifyDataSetChanged();
            recyclerView.scheduleLayoutAnimation();
        }
    }
    public void runAnimationAdvertisements(RecyclerView recyclerView,int type,AllAdvertisementsAdapter adapter){
        Context context = recyclerView.getContext();
        LayoutAnimationController controller = null;
        if (type == 0){ // Fall down animation
            controller = AnimationUtils.loadLayoutAnimation(context,R.anim.layout_fall_down_vertical);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutAnimation(controller);
            recyclerView.getAdapter().notifyDataSetChanged();
            recyclerView.scheduleLayoutAnimation();
        }
    }
}
