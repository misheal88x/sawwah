package com.scit.sawwah.activities;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.gson.Gson;
import com.scit.sawwah.APIClasses.CategoriesAPIsClass;
import com.scit.sawwah.Models.InstitutionObject;
import com.scit.sawwah.R;
import com.scit.sawwah.adapters.CategoryInstitutionsAdapter;
import com.scit.sawwah.dialoges.SortDialog;
import com.scit.sawwah.interfaces.IFailure;
import com.scit.sawwah.interfaces.IMove;
import com.scit.sawwah.interfaces.IResponse;
import com.scit.sawwah.interfaces.OnSortTypeChoose;
import com.scit.sawwah.tools.BaseActivity;
import com.scit.sawwah.tools.BaseRetrofit;

import java.util.ArrayList;
import java.util.List;

public class CategoryInstitutionsActivity extends BaseActivity {
    private RelativeLayout toolbar;
    private ImageButton btn_back;
    private TextView tv_title;
    private RecyclerView rv_institutions;
    private List<InstitutionObject> list_institutions;
    private LinearLayoutManager layoutManager;
    private CategoryInstitutionsAdapter adapter;
    private FloatingActionButton fab_sorting;
    private RelativeLayout root;
    private int currentPage = 1;
    private String chosen_order = "distance,desc";
    private Intent myIntent;
    private String id = "";
    private LinearLayout empty_layout;
    private CallbackManager callbackManager;
    private ShareDialog shareDialog;

    @Override
    public void set_layout() {
        setContentView(R.layout.activity_category_institutions);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        tv_title.setText(getResources().getString(R.string.category_institutions_title));
        list_institutions = new ArrayList<>();
        adapter = new CategoryInstitutionsAdapter(CategoryInstitutionsActivity.this, list_institutions,new IMove() {
            @Override
            public void move(int position) {
                FacebookSdk.sdkInitialize(CategoryInstitutionsActivity.this.getApplicationContext());
                callbackManager = CallbackManager.Factory.create();
                shareDialog = new ShareDialog(CategoryInstitutionsActivity.this);
                shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
                    @Override
                    public void onSuccess(Sharer.Result result) {
                        Toast.makeText(CategoryInstitutionsActivity.this, "Posted successfully", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onCancel() {
                        Toast.makeText(CategoryInstitutionsActivity.this, "Posted canceled", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(FacebookException error) {
                        Toast.makeText(CategoryInstitutionsActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
                ShareLinkContent linkContent = new ShareLinkContent.Builder().setQuote(getResources().getString(R.string.facebook_post_inst)).
                        setContentUrl(Uri.parse(BaseRetrofit.BASE_ALI+"facilities/"+String.valueOf(list_institutions.get(position).getId()))).build();
                if (shareDialog.canShow(ShareLinkContent.class)){
                    shareDialog.show(linkContent);
                }else {
                    Toast.makeText(CategoryInstitutionsActivity.this, "can't show", Toast.LENGTH_SHORT).show();
                }
            }
        });
        layoutManager = new LinearLayoutManager(CategoryInstitutionsActivity.this,LinearLayoutManager.VERTICAL,false);
        rv_institutions.setLayoutManager(layoutManager);
        rv_institutions.setAdapter(adapter);
        callAPI(chosen_order);
    }

    @Override
    public void init_views() {
        toolbar = findViewById(R.id.category_institutions_toolbar);
        btn_back = toolbar.findViewById(R.id.toolbar_back_btn);
        tv_title = toolbar.findViewById(R.id.toolbar_title);
        rv_institutions = findViewById(R.id.category_institutions_recycler);
        fab_sorting = findViewById(R.id.category_institutions_options);
        root = findViewById(R.id.category_institutions_layout);
        empty_layout = findViewById(R.id.empty_layout);
        myIntent = getIntent();
    }

    @Override
    public void init_events() {
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        fab_sorting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SortDialog dialog = new SortDialog(CategoryInstitutionsActivity.this, new OnSortTypeChoose() {
                    @Override
                    public void sort(String sort_type) {
                        switch (sort_type){
                            case "az" : {
                                list_institutions.clear();
                                adapter = new CategoryInstitutionsAdapter(CategoryInstitutionsActivity.this, list_institutions, new IMove() {
                                    @Override
                                    public void move(int position) {
                                        FacebookSdk.sdkInitialize(CategoryInstitutionsActivity.this.getApplicationContext());
                                        callbackManager = CallbackManager.Factory.create();
                                        shareDialog = new ShareDialog(CategoryInstitutionsActivity.this);
                                        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
                                            @Override
                                            public void onSuccess(Sharer.Result result) {
                                                Toast.makeText(CategoryInstitutionsActivity.this, "Posted successfully", Toast.LENGTH_SHORT).show();
                                            }

                                            @Override
                                            public void onCancel() {
                                                Toast.makeText(CategoryInstitutionsActivity.this, "Posted canceled", Toast.LENGTH_SHORT).show();
                                            }

                                            @Override
                                            public void onError(FacebookException error) {
                                                Toast.makeText(CategoryInstitutionsActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                        ShareLinkContent linkContent = new ShareLinkContent.Builder().setQuote(getResources().getString(R.string.facebook_post_inst)).
                                                setContentUrl(Uri.parse(BaseRetrofit.BASE_ALI+"facilities/"+String.valueOf(list_institutions.get(position).getId()))).build();
                                        if (shareDialog.canShow(ShareLinkContent.class)){
                                            shareDialog.show(linkContent);
                                        }else {
                                            Toast.makeText(CategoryInstitutionsActivity.this, "can't show", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });
                                rv_institutions.setAdapter(adapter);
                                chosen_order = "name,desc";
                                currentPage = 1;
                                callAPI(chosen_order);} break;
                            case "visit" : {
                                list_institutions.clear();
                                adapter = new CategoryInstitutionsAdapter(CategoryInstitutionsActivity.this, list_institutions, new IMove() {
                                    @Override
                                    public void move(int position) {
                                        FacebookSdk.sdkInitialize(CategoryInstitutionsActivity.this.getApplicationContext());
                                        callbackManager = CallbackManager.Factory.create();
                                        shareDialog = new ShareDialog(CategoryInstitutionsActivity.this);
                                        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
                                            @Override
                                            public void onSuccess(Sharer.Result result) {
                                                Toast.makeText(CategoryInstitutionsActivity.this, "Posted successfully", Toast.LENGTH_SHORT).show();
                                            }

                                            @Override
                                            public void onCancel() {
                                                Toast.makeText(CategoryInstitutionsActivity.this, "Posted canceled", Toast.LENGTH_SHORT).show();
                                            }

                                            @Override
                                            public void onError(FacebookException error) {
                                                Toast.makeText(CategoryInstitutionsActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                        ShareLinkContent linkContent = new ShareLinkContent.Builder().setQuote(getResources().getString(R.string.facebook_post_inst)).
                                                setContentUrl(Uri.parse(BaseRetrofit.BASE_ALI+"facilities/"+String.valueOf(list_institutions.get(position).getId()))).build();
                                        if (shareDialog.canShow(ShareLinkContent.class)){
                                            shareDialog.show(linkContent);
                                        }else {
                                            Toast.makeText(CategoryInstitutionsActivity.this, "can't show", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });
                                rv_institutions.setAdapter(adapter);
                                chosen_order = "visit_views,desc";
                                currentPage = 1;
                                callAPI(chosen_order);
                            } break;
                            case "rate" : {
                                list_institutions.clear();
                                adapter = new CategoryInstitutionsAdapter(CategoryInstitutionsActivity.this, list_institutions, new IMove() {
                                    @Override
                                    public void move(int position) {
                                        FacebookSdk.sdkInitialize(CategoryInstitutionsActivity.this.getApplicationContext());
                                        callbackManager = CallbackManager.Factory.create();
                                        shareDialog = new ShareDialog(CategoryInstitutionsActivity.this);
                                        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
                                            @Override
                                            public void onSuccess(Sharer.Result result) {
                                                Toast.makeText(CategoryInstitutionsActivity.this, "Posted successfully", Toast.LENGTH_SHORT).show();
                                            }

                                            @Override
                                            public void onCancel() {
                                                Toast.makeText(CategoryInstitutionsActivity.this, "Posted canceled", Toast.LENGTH_SHORT).show();
                                            }

                                            @Override
                                            public void onError(FacebookException error) {
                                                Toast.makeText(CategoryInstitutionsActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                        ShareLinkContent linkContent = new ShareLinkContent.Builder().setQuote(getResources().getString(R.string.facebook_post_inst)).
                                                setContentUrl(Uri.parse(BaseRetrofit.BASE_ALI+"facilities/"+String.valueOf(list_institutions.get(position).getId()))).build();
                                        if (shareDialog.canShow(ShareLinkContent.class)){
                                            shareDialog.show(linkContent);
                                        }else {
                                            Toast.makeText(CategoryInstitutionsActivity.this, "can't show", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });
                                rv_institutions.setAdapter(adapter);
                                chosen_order = "rating,desc";
                                currentPage = 1;
                                callAPI(chosen_order);} break;
                            case "near" : {
                                list_institutions.clear();
                                adapter = new CategoryInstitutionsAdapter(CategoryInstitutionsActivity.this, list_institutions, new IMove() {
                                    @Override
                                    public void move(int position) {
                                        FacebookSdk.sdkInitialize(CategoryInstitutionsActivity.this.getApplicationContext());
                                        callbackManager = CallbackManager.Factory.create();
                                        shareDialog = new ShareDialog(CategoryInstitutionsActivity.this);
                                        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
                                            @Override
                                            public void onSuccess(Sharer.Result result) {
                                                Toast.makeText(CategoryInstitutionsActivity.this, "Posted successfully", Toast.LENGTH_SHORT).show();
                                            }

                                            @Override
                                            public void onCancel() {
                                                Toast.makeText(CategoryInstitutionsActivity.this, "Posted canceled", Toast.LENGTH_SHORT).show();
                                            }

                                            @Override
                                            public void onError(FacebookException error) {
                                                Toast.makeText(CategoryInstitutionsActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                        ShareLinkContent linkContent = new ShareLinkContent.Builder().setQuote(getResources().getString(R.string.facebook_post_inst)).
                                                setContentUrl(Uri.parse(BaseRetrofit.BASE_ALI+"facilities/"+String.valueOf(list_institutions.get(position).getId()))).build();
                                        if (shareDialog.canShow(ShareLinkContent.class)){
                                            shareDialog.show(linkContent);
                                        }else {
                                            Toast.makeText(CategoryInstitutionsActivity.this, "can't show", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });
                                rv_institutions.setAdapter(adapter);
                                chosen_order = "distance,desc";
                                currentPage = 1;
                                callAPI(chosen_order);
                            } break;
                            case "chosen" : {
                                list_institutions.clear();
                                adapter = new CategoryInstitutionsAdapter(CategoryInstitutionsActivity.this, list_institutions, new IMove() {
                                    @Override
                                    public void move(int position) {
                                        FacebookSdk.sdkInitialize(CategoryInstitutionsActivity.this.getApplicationContext());
                                        callbackManager = CallbackManager.Factory.create();
                                        shareDialog = new ShareDialog(CategoryInstitutionsActivity.this);
                                        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
                                            @Override
                                            public void onSuccess(Sharer.Result result) {
                                                Toast.makeText(CategoryInstitutionsActivity.this, "Posted successfully", Toast.LENGTH_SHORT).show();
                                            }

                                            @Override
                                            public void onCancel() {
                                                Toast.makeText(CategoryInstitutionsActivity.this, "Posted canceled", Toast.LENGTH_SHORT).show();
                                            }

                                            @Override
                                            public void onError(FacebookException error) {
                                                Toast.makeText(CategoryInstitutionsActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                        ShareLinkContent linkContent = new ShareLinkContent.Builder().setQuote(getResources().getString(R.string.facebook_post_inst)).
                                                setContentUrl(Uri.parse(BaseRetrofit.BASE_ALI+"facilities/"+String.valueOf(list_institutions.get(position).getId()))).build();
                                        if (shareDialog.canShow(ShareLinkContent.class)){
                                            shareDialog.show(linkContent);
                                        }else {
                                            Toast.makeText(CategoryInstitutionsActivity.this, "can't show", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });
                                rv_institutions.setAdapter(adapter);
                                chosen_order = "favourite_count,desc";
                                currentPage = 1;
                                callAPI(chosen_order);
                            } break;
                        }
                    }
                });
                dialog.show();
            }
        });
    }

    @Override
    public void set_fragment_place() {

    }

    private void callAPI(final String order){
        CategoriesAPIsClass.getFacilities(sharedPreferences,
                CategoryInstitutionsActivity.this,
                sharedPreferences.getString("language", "ar"),
                sharedPreferences.getString("service_token", ""),
                String.valueOf(myIntent.getIntExtra("category_id",0)),
                order,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json !=null){
                            String json1 = new Gson().toJson(json);
                            InstitutionObject[] success = new Gson().fromJson(json1,InstitutionObject[].class);
                            if (success.length > 0){
                                empty_layout.setVisibility(View.GONE);
                                    for (InstitutionObject oo : success){
                                        list_institutions.add(oo);
                                        adapter.notifyDataSetChanged();
                                    }
                                    runAnimation(rv_institutions,0,adapter);
                                }else {
                                empty_layout.setVisibility(View.VISIBLE);
                                Toast.makeText(CategoryInstitutionsActivity.this, getResources().getString(R.string.no_data), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callAPI(order);
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });
    }
    public void runAnimation(RecyclerView recyclerView,int type,CategoryInstitutionsAdapter adapter){
        Context context = recyclerView.getContext();
        LayoutAnimationController controller = null;
        if (type == 0){ // Fall down animation
            controller = AnimationUtils.loadLayoutAnimation(context,R.anim.layout_fall_down_vertical);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutAnimation(controller);
            recyclerView.getAdapter().notifyDataSetChanged();
            recyclerView.scheduleLayoutAnimation();
        }
    }
}
