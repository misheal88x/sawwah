package com.scit.sawwah.activities;

import android.content.Intent;
import android.graphics.Color;

import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.google.gson.Gson;
import com.scit.sawwah.APIClasses.InstitutionsAPIsClass;
import com.scit.sawwah.Models.AdvertisementObject;
import com.scit.sawwah.Models.ContactObject;
import com.scit.sawwah.Models.InstitutionObject;
import com.scit.sawwah.Models.MemorialImageObject;
import com.scit.sawwah.Models.OfferObject;
import com.scit.sawwah.R;
import com.scit.sawwah.fragments.EditInstAccountFragment;
import com.scit.sawwah.fragments.EditInstAdvertisementsFragment;
import com.scit.sawwah.fragments.EditInstImagesFragment;
import com.scit.sawwah.fragments.EditInstOffersFragment;
import com.scit.sawwah.fragments.EditInstSocialsFragment;
import com.scit.sawwah.interfaces.IFailure;
import com.scit.sawwah.interfaces.IResponse;
import com.scit.sawwah.tools.BaseActivity;

import java.util.ArrayList;
import java.util.List;

public class UpdateInstitutionDetailsActivity extends BaseActivity {
    private RelativeLayout toolbar;
    private ImageButton btn_back;
    private Button btn_account,btn_offers,btn_advertisements,btn_socials,btn_images;
    private ImageView account_arrow,offers_arrow,advertisements_arrow,socials_arrow,images_arrow;
    private RelativeLayout root;
    private LinearLayout lyt_tools,lyt_arrows;
    private Intent myIntent;
    private String id = "";
    @Override
    public void set_layout() {
        setContentView(R.layout.activity_update_institution_details);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        callAPI();
    }

    @Override
    public void init_views() {
        toolbar = findViewById(R.id.update_inst_toolbar);
        btn_back = toolbar.findViewById(R.id.toolbar_back_btn);

        btn_account =findViewById(R.id.update_inst_details_account);
        btn_offers =findViewById(R.id.update_inst_details_offers);
        btn_advertisements =findViewById(R.id.update_inst_details_ads);
        btn_socials =findViewById(R.id.update_inst_details_socials);
        btn_images =findViewById(R.id.update_inst_details_images);

        account_arrow = findViewById(R.id.update_inst_details_account_arrow);
        offers_arrow = findViewById(R.id.update_inst_details_offers_arrow);
        advertisements_arrow = findViewById(R.id.update_inst_details_ads_arrow);
        socials_arrow = findViewById(R.id.update_inst_details_socials_arrow);
        images_arrow = findViewById(R.id.update_inst_details_images_arrow);
        root = findViewById(R.id.update_inst_details_layout);
        lyt_tools = findViewById(R.id.update_inst_tools);
        lyt_arrows = findViewById(R.id.update_inst_arrows_layout);
        lyt_tools.setVisibility(View.GONE);
        lyt_arrows.setVisibility(View.GONE);
        myIntent = getIntent();
        id = String.valueOf(myIntent.getIntExtra("institution_id",0));
    }

    @Override
    public void init_events() {
        btn_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_click(1);
            }
        });
        btn_offers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_click(2);
            }
        });
        btn_advertisements.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_click(3);
            }
        });
        btn_socials.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_click(4);
            }
        });
        btn_images.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_click(5);
            }
        });

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void set_fragment_place() {
        this.fragment_place = (ViewGroup) findViewById(R.id.fragment_place);
    }

    private void btn_click(int index){
        switch (index){
            //Account
            case 1 : {
                //Icon
                btn_account.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_edit_inst_grey_account, 0, 0);
                btn_offers.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_edit_inst_red_offers, 0, 0);
                btn_advertisements.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_edit_inst_red_advertisement, 0, 0);
                btn_socials.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_edit_inst_red_socials, 0, 0);
                btn_images.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_edit_inst_red_images, 0, 0);
                //Arrow
                account_arrow.setImageResource(R.drawable.ic_tab_arrow);
                account_arrow.setColorFilter(Color.argb(255, 255, 255, 255));
                offers_arrow.setImageResource(0);
                advertisements_arrow.setImageResource(0);
                socials_arrow.setImageResource(0);
                images_arrow.setImageResource(0);
                open_fragment(new EditInstAccountFragment());
            }break;
            //Offers
            case 2 : {
                //Icon
                btn_account.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_edit_inst_red_account, 0, 0);
                btn_offers.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_edit_inst_grey_offers, 0, 0);
                btn_advertisements.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_edit_inst_red_advertisement, 0, 0);
                btn_socials.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_edit_inst_red_socials, 0, 0);
                btn_images.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_edit_inst_red_images, 0, 0);
                //Arrow
                account_arrow.setImageResource(0);
                advertisements_arrow.setImageResource(0);
                offers_arrow.setImageResource(R.drawable.ic_tab_arrow);
                offers_arrow.setColorFilter(Color.argb(255, 255, 255, 255));
                socials_arrow.setImageResource(0);
                images_arrow.setImageResource(0);
                open_fragment(new EditInstOffersFragment());
            }break;
            //Advertisements
            case 3 : {
                //Icon
                btn_account.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_edit_inst_red_account, 0, 0);
                btn_offers.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_edit_inst_red_offers, 0, 0);
                btn_advertisements.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_edit_inst_grey_advertisement, 0, 0);
                btn_socials.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_edit_inst_red_socials, 0, 0);
                btn_images.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_edit_inst_red_images, 0, 0);
                //Arrow
                account_arrow.setImageResource(0);
                offers_arrow.setImageResource(0);
                advertisements_arrow.setImageResource(R.drawable.ic_tab_arrow);
                advertisements_arrow.setColorFilter(Color.argb(255, 255, 255, 255));
                socials_arrow.setImageResource(0);
                images_arrow.setImageResource(0);
                open_fragment(new EditInstAdvertisementsFragment());
            }break;
            //Socials
            case 4 : {
                //Icon
                btn_account.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_edit_inst_red_account, 0, 0);
                btn_offers.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_edit_inst_red_offers, 0, 0);
                btn_advertisements.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_edit_inst_red_advertisement, 0, 0);
                btn_socials.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_edit_inst_grey_socials, 0, 0);
                btn_images.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_edit_inst_red_images, 0, 0);
                //Arrow
                account_arrow.setImageResource(0);
                offers_arrow.setImageResource(0);
                socials_arrow.setImageResource(R.drawable.ic_tab_arrow);
                socials_arrow.setColorFilter(Color.argb(255, 255, 255, 255));
                advertisements_arrow.setImageResource(0);
                images_arrow.setImageResource(0);
                open_fragment(new EditInstSocialsFragment());
            }break;
            //Images
            case 5 : {
                //Icon
                btn_account.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_edit_inst_red_account, 0, 0);
                btn_offers.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_edit_inst_red_offers, 0, 0);
                btn_advertisements.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_edit_inst_red_advertisement, 0, 0);
                btn_socials.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_edit_inst_red_socials, 0, 0);
                btn_images.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_edit_inst_grey_images, 0, 0);
                //Arrow
                account_arrow.setImageResource(0);
                offers_arrow.setImageResource(0);
                images_arrow.setImageResource(R.drawable.ic_tab_arrow);
                images_arrow.setColorFilter(Color.argb(255, 255, 255, 255));
                advertisements_arrow.setImageResource(0);
                socials_arrow.setImageResource(0);
                open_fragment(new EditInstImagesFragment());
            }break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void callAPI(){
        InstitutionsAPIsClass.details(sharedPreferences,
                UpdateInstitutionDetailsActivity.this,
                sharedPreferences.getString("language", "ar"),
                sharedPreferences.getString("service_token", ""),
                id,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json !=null){
                            String json1 = new Gson().toJson(json);
                            InstitutionObject success = new Gson().fromJson(json1,InstitutionObject.class);

                            //Institution Object
                            editor.putString("update_inst_institution_object",new Gson().toJson(success));
                            editor.putInt("update_inst_id",success.getId());
                            //Offers
                            if (success.getOffers()!=null){
                                if (success.getOffers().size()>0){
                                    List<OfferObject> list = new ArrayList<>();
                                    for (OfferObject oo : success.getOffers()){
                                        list.add(oo);
                                    }
                                    String oo_json = new Gson().toJson(list);
                                    editor.putString("update_inst_offers",oo_json);
                                }else {
                                    editor.putString("update_inst_offers","[]");
                                }
                            }else {
                                editor.putString("update_inst_offers","[]");
                            }
                            //Advertisements
                            if (success.getPosters()!=null){
                                if (success.getPosters().size()>0){
                                    List<AdvertisementObject> list = new ArrayList<>();
                                    for (AdvertisementObject ao : success.getPosters()){
                                        list.add(ao);
                                    }
                                    String ao_json = new Gson().toJson(list);
                                    editor.putString("update_inst_ads",ao_json);
                                }else {
                                    editor.putString("update_inst_ads","[]");
                                }
                            }else {
                                editor.putString("update_inst_ads","[]");
                            }
                            //Memorial images
                            if (success.getMemorial_images()!=null){
                                if (success.getMemorial_images().size()>0){
                                    List<MemorialImageObject> list = new ArrayList<>();
                                    for (MemorialImageObject meo : success.getMemorial_images()){
                                        list.add(meo);
                                    }
                                    String meo_json = new Gson().toJson(list);
                                    editor.putString("update_inst_images",meo_json);
                                }else {
                                    editor.putString("update_inst_images","[]");
                                }
                            }else {
                                editor.putString("update_inst_images","[]");
                            }

                            //Contacts
                            if (success.getContacts()!=null){
                                if (success.getContacts().size()>0){
                                    List<ContactObject> list = new ArrayList<>();
                                    for (ContactObject co : success.getContacts()){
                                        list.add(co);
                                    }
                                    String co_json = new Gson().toJson(list);
                                    editor.putString("update_inst_contacts",co_json);
                                }else {
                                    editor.putString("update_inst_contacts","[]");
                                }
                            }else {
                                editor.putString("update_inst_contacts","[]");
                            }
                            //ContactInfo
                            if (success.getContact_info()!=null){
                                String cio_json = new Gson().toJson(success.getContact_info());
                                editor.putString("contact_info_object",cio_json);
                            }else {
                                editor.putString("contact_info_object","{}");
                            }
                            editor.commit();
                            lyt_tools.setVisibility(View.VISIBLE);
                            lyt_arrows.setVisibility(View.VISIBLE);
                            btn_click(1);
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callAPI();
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });
    }
}
