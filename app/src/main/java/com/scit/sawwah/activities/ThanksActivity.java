package com.scit.sawwah.activities;

import android.content.Intent;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.scit.sawwah.MainActivity;
import com.scit.sawwah.R;
import com.scit.sawwah.tools.BaseActivity;

public class ThanksActivity extends BaseActivity {
    private RelativeLayout toolbar;
    private ImageButton btn_back;

    @Override
    public void set_layout() {
        setContentView(R.layout.activity_thanks);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        btn_back.setVisibility(View.GONE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(ThanksActivity.this, MainActivity.class));
                finish();
            }
        },3000);
    }

    @Override
    public void init_views() {
        toolbar = findViewById(R.id.thanks_toolbar);
        btn_back = toolbar.findViewById(R.id.toolbar_back_btn);
    }

    @Override
    public void init_events() {

    }

    @Override
    public void set_fragment_place() {

    }

    @Override
    public void onBackPressed() {
        return;
    }
}
