package com.scit.sawwah.activities;

import android.content.Intent;
import android.graphics.Color;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.rm.rmswitch.RMSwitch;
import com.scit.sawwah.APIClasses.AccountsAPIsClass;
import com.scit.sawwah.Models.InstitutionObject;
import com.scit.sawwah.Models.PricePackageObject;
import com.scit.sawwah.Models.ProfileResponse;
import com.scit.sawwah.R;
import com.scit.sawwah.adapters.AccountOwnerSettingsInstsAdapter;
import com.scit.sawwah.adapters.AccountOwnerSettingsPackagesAdapter;
import com.scit.sawwah.interfaces.IFailure;
import com.scit.sawwah.interfaces.IMove;
import com.scit.sawwah.interfaces.IResponse;
import com.scit.sawwah.tools.BaseActivity;
import com.scit.sawwah.tools.BaseFunctions;

import java.util.ArrayList;
import java.util.List;

public class AccountOwnerSettingsActivity extends BaseActivity {
    private RelativeLayout toolbar;
    private ImageButton btn_back;
    private RMSwitch active_switch;
    private RecyclerView institutions_recycler,packages_recycler;
    private List<InstitutionObject> institutions_list;
    private List<PricePackageObject> packages_list;
    private AccountOwnerSettingsInstsAdapter institutions_adapter;
    private AccountOwnerSettingsPackagesAdapter packages_adapter;
    private LinearLayoutManager institutions_layout_manager,packages_layout_manager;
    private RelativeLayout btn_show,btn_beneficiaries,btn_edit_password,btn_add_adv,btn_add_offer,btn_enrollment,btn_souvenir;
    private Button btn_manage_packages;
    private TextView tv_user_name,tv_insts_txt,tv_active_packages_txt,tv_num_images,tv_memorial;
    private LinearLayout lyt_memorial;
    private ImageView img_personal_image;
    private String packages_json= "",active_packages_json = "";
    private ScrollView scrollView;
    private LinearLayout root;


    @Override
    public void set_layout() {
        setContentView(R.layout.activity_account_owner_settings);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        setActiveSwitchColors();
        init_institutions_recycler();
        init_packages_recycler();
        restore_data();
        callProfileAPI();
    }

    @Override
    public void init_views() {
        toolbar = findViewById(R.id.account_owner_settings_toolbar);
        btn_back = toolbar.findViewById(R.id.toolbar_back_btn);
        active_switch = findViewById(R.id.account_owner_settings_active_switch);
        institutions_recycler = findViewById(R.id.account_owner_settings_inst_recycler);
        packages_recycler = findViewById(R.id.account_owner_settings_active_packages_recycler);
        btn_show = findViewById(R.id.account_owner_settings_show_btn);
        btn_beneficiaries = findViewById(R.id.account_owner_settings_beneficiaries_btn);
        btn_edit_password = findViewById(R.id.account_owner_settings_password_btn);
        btn_add_adv = findViewById(R.id.account_owner_settings_advertisement_btn);
        btn_add_offer = findViewById(R.id.account_owner_settings_offer_btn);
        btn_enrollment = findViewById(R.id.account_owner_settings_benefit_btn);
        btn_manage_packages = findViewById(R.id.account_owner_settings_manage_packages);
        btn_souvenir = findViewById(R.id.account_owner_settings_souvenir_btn);
        tv_user_name = findViewById(R.id.account_owner_settings_user_name);
        img_personal_image = findViewById(R.id.account_owner_settings_personal_image);
        tv_insts_txt = findViewById(R.id.account_owner_settings_institutions_txt);
        tv_active_packages_txt = findViewById(R.id.account_owner_settings_active_packages_txt);
        tv_num_images = findViewById(R.id.account_owner_settings_num_images);
        tv_memorial = findViewById(R.id.account_owner_settings_memorial_txt);
        lyt_memorial = findViewById(R.id.account_owner_settings_memorial_layout);
        scrollView = findViewById(R.id.account_owner_settings_scrollview);
        root = findViewById(R.id.account_owner_settings_layout);
        scrollView.setVisibility(View.GONE);
        active_switch.setClickable(false);
    }

    @Override
    public void init_events() {
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btn_show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AccountOwnerSettingsActivity.this,MemorialImagesActivity.class);
                intent.putExtra("type","from_owner_settings");
                startActivity(intent);
            }
        });
        btn_beneficiaries.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AccountOwnerSettingsActivity.this,BeneficiariesActivity.class));
            }
        });
        btn_edit_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AccountOwnerSettingsActivity.this,EditPasswordActivity.class));
            }
        });
        btn_add_adv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AccountOwnerSettingsActivity.this,AddAdvertiseActivity.class));
            }
        });
        btn_add_offer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AccountOwnerSettingsActivity.this,AddOfferActivity.class));
            }
        });
        btn_enrollment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AccountOwnerSettingsActivity.this,BenefitEnrollmentActivity.class));
            }
        });
        btn_manage_packages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AccountOwnerSettingsActivity.this,ManagePricesPackagesActivity.class);
                intent.putExtra("packages",packages_json);
                intent.putExtra("active_packages",active_packages_json);
                startActivity(intent);
            }
        });
        btn_souvenir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AccountOwnerSettingsActivity.this,SouvenirManagementActivity.class));
            }
        });

    }

    @Override
    public void set_fragment_place() {

    }

    private void setActiveSwitchColors() {
        active_switch.setSwitchBkgCheckedColor(Color.WHITE);
        active_switch.setSwitchBkgNotCheckedColor(Color.WHITE);
        active_switch.setSwitchToggleCheckedColor(getResources().getColor(R.color.colorPrimary));
        active_switch.setSwitchToggleNotCheckedColor(getResources().getColor(R.color.colorPrimary));
    }

    private void init_institutions_recycler(){
        institutions_list = new ArrayList<>();
        institutions_adapter = new AccountOwnerSettingsInstsAdapter(AccountOwnerSettingsActivity.this,institutions_list, new IMove() {
            @Override
            public void move(int position) {
                startActivity(new Intent(AccountOwnerSettingsActivity.this,UpdateInstitutionDetailsActivity.class));
            }
        });
        institutions_layout_manager = new LinearLayoutManager(AccountOwnerSettingsActivity.this,LinearLayoutManager.HORIZONTAL,false);
        institutions_recycler.setLayoutManager(institutions_layout_manager);
        institutions_recycler.setAdapter(institutions_adapter);
    }

    private void init_packages_recycler(){
        packages_list = new ArrayList<>();
        packages_adapter = new AccountOwnerSettingsPackagesAdapter(AccountOwnerSettingsActivity.this, packages_list,new IMove() {
            @Override
            public void move(int position) {

            }
        });
        packages_layout_manager = new LinearLayoutManager(AccountOwnerSettingsActivity.this,LinearLayoutManager.HORIZONTAL,false);
        packages_recycler.setLayoutManager(packages_layout_manager);
        packages_recycler.setAdapter(packages_adapter);
    }

    private void restore_data(){
        //UserName
        tv_user_name.setText(sharedPreferences.getString("account_name",""));
        //Status
        if (sharedPreferences.getInt("account_status",0)==1||sharedPreferences.getInt("account_status",0)==2){
            active_switch.setChecked(true);
        }else {
            active_switch.setChecked(false);
        }
        //Personal Image
        BaseFunctions.setGlideImage(AccountOwnerSettingsActivity.this,img_personal_image,sharedPreferences.getString("account_image",""));
        //Institutions
        InstitutionObject[] insts_array = new Gson().fromJson(sharedPreferences.getString("my_insts","[]"),InstitutionObject[].class);
        if (insts_array.length>0){
            for (InstitutionObject io : insts_array){
                institutions_list.add(io);
                institutions_adapter.notifyDataSetChanged();
            }
        }else {
            tv_insts_txt.setVisibility(View.GONE);
            institutions_recycler.setVisibility(View.GONE);
        }
    }

    private void callProfileAPI(){
        AccountsAPIsClass.profile(sharedPreferences,
                AccountOwnerSettingsActivity.this,
                sharedPreferences.getString("language", "ar"),
                sharedPreferences.getString("service_token", ""),
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json != null){
                            String json1 = new Gson().toJson(json);
                            ProfileResponse success = new Gson().fromJson(json1,ProfileResponse.class);
                            //Packages
                            packages_json = new Gson().toJson(success.getPackages());
                            //Active Packages
                            if (success.getActive_packages()!=null){
                                if (success.getActive_packages().size()>0){
                                    active_packages_json = new Gson().toJson(success.getActive_packages());
                                    for (PricePackageObject ppo : success.getActive_packages()){
                                        packages_list.add(ppo);
                                        packages_adapter.notifyDataSetChanged();
                                    }
                                }else {
                                    tv_active_packages_txt.setVisibility(View.GONE);
                                    packages_recycler.setVisibility(View.GONE);
                                }
                            }else {
                                tv_active_packages_txt.setVisibility(View.GONE);
                                packages_recycler.setVisibility(View.GONE);
                            }
                            //Memorial Images
                            if (success.getMemporial_images()!=null){
                                if (success.getMemporial_images().size() > 0){
                                    tv_num_images.setText(" "+String.valueOf(success.getMemporial_images().size())+" ");
                                }else {
                                    btn_souvenir.setVisibility(View.GONE);
                                    tv_memorial.setVisibility(View.GONE);
                                    lyt_memorial.setVisibility(View.GONE);
                                    btn_show.setVisibility(View.GONE);
                                }
                            }else {
                                btn_souvenir.setVisibility(View.GONE);
                                tv_memorial.setVisibility(View.GONE);
                                lyt_memorial.setVisibility(View.GONE);
                                btn_show.setVisibility(View.GONE);
                            }
                        }else {
                            tv_active_packages_txt.setVisibility(View.GONE);
                            packages_recycler.setVisibility(View.GONE);
                            btn_souvenir.setVisibility(View.GONE);
                            tv_memorial.setVisibility(View.GONE);
                            lyt_memorial.setVisibility(View.GONE);
                            btn_show.setVisibility(View.GONE);
                        }
                        scrollView.setVisibility(View.VISIBLE);
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callProfileAPI();
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });
    }

}
