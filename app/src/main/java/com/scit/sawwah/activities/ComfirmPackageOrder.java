package com.scit.sawwah.activities;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.scit.sawwah.APIClasses.PackagesAPIsClass;
import com.scit.sawwah.Models.PaymentTypeObject;
import com.scit.sawwah.R;
import com.scit.sawwah.adapters.CustomSpinnerAdapter;
import com.scit.sawwah.interfaces.IFailure;
import com.scit.sawwah.interfaces.IResponse;
import com.scit.sawwah.tools.BaseActivity;
import com.scit.sawwah.tools.BaseFunctions;

import java.util.ArrayList;
import java.util.List;

public class ComfirmPackageOrder extends BaseActivity {
    private RelativeLayout toolbar;
    private ImageButton btn_back;
    private TextView tv_title;
    private Spinner payment_spinner;
    private List<String> payment_list_string;
    private List<PaymentTypeObject> payment_list;
    private CustomSpinnerAdapter payment_adapter;
    private ImageView img_image;
    private String selected_image_path = "";
    private String selected_payment_type_id = "";
    private Intent myIntent;
    private EditText edt_number;
    private LinearLayout root;
    private RelativeLayout btn_request;

    @Override
    public void set_layout() {
        setContentView(R.layout.activity_comfirm_package_order);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        tv_title.setText(getResources().getString(R.string.confirm_package_order_title));
        init_spinner();
    }

    @Override
    public void init_views() {
        toolbar = findViewById(R.id.confirm_package_toolbar);
        btn_back = toolbar.findViewById(R.id.toolbar_back_btn);
        tv_title = toolbar.findViewById(R.id.toolbar_title);
        payment_spinner = findViewById(R.id.confirm_package_payment_spinner);
        img_image = findViewById(R.id.confirm_package_image);
        edt_number = findViewById(R.id.confirm_package_contact);
        myIntent = getIntent();
        root = findViewById(R.id.confirm_package_layout);
        btn_request = findViewById(R.id.confirm_package_request_btn);
    }

    @Override
    public void init_events() {
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        img_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                String[] mimeTypes = {"image/jpeg", "image/png"};
                intent.putExtra(Intent.EXTRA_MIME_TYPES,mimeTypes);
                startActivityForResult(intent,1);

            }
        });
        payment_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int i, long id) {
                int position = BaseFunctions.getSpinnerPosition(parent,i);
                selected_payment_type_id = String.valueOf(payment_list.get(position).getId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        btn_request.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt_number.getText().toString().equals("")){
                    Toast.makeText(ComfirmPackageOrder.this, getResources().getString(R.string.confirm_package_order_no_number), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (selected_payment_type_id.equals("")){
                    Toast.makeText(ComfirmPackageOrder.this, getResources().getString(R.string.confirm_package_order_no_payment), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (selected_image_path.equals("")){
                    Toast.makeText(ComfirmPackageOrder.this, getResources().getString(R.string.confirm_package_order_no_image), Toast.LENGTH_SHORT).show();
                    return;
                }
                callRequestAPI();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Result code is RESULT_OK only if the user selects an Image
        if (resultCode == Activity.RESULT_OK)
            switch (requestCode) {
                case 1:
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                    cursor.moveToFirst();
                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String imgDecodableString = cursor.getString(columnIndex);
                    cursor.close();
                    img_image.setImageBitmap(BitmapFactory.decodeFile(imgDecodableString));
                    selected_image_path = imgDecodableString;
                    break;

            }
    }

    @Override
    public void set_fragment_place() {

    }

    private void init_list() {
        payment_list = new ArrayList<>();
        payment_list_string = new ArrayList<>();
        PaymentTypeObject[] payment_array = new Gson().fromJson(sharedPreferences.getString("payments_types","[]"),PaymentTypeObject[].class);
        if (payment_array.length > 0){
            for (PaymentTypeObject pto : payment_array){
                payment_list.add(pto);
                payment_list_string.add(pto.getType());
            }
        }
    }
    private void init_spinner(){
        init_list();
        payment_adapter = new CustomSpinnerAdapter(ComfirmPackageOrder.this,R.layout.new_transparent_spinner_item,payment_list_string);
        payment_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        payment_spinner.setAdapter(payment_adapter);
    }

    private void callRequestAPI(){
        PackagesAPIsClass.request(sharedPreferences,
                ComfirmPackageOrder.this,
                sharedPreferences.getString("language", "ar"),
                sharedPreferences.getString("service_token", ""),
                String.valueOf(myIntent.getIntExtra("package_id", 0)),
                selected_payment_type_id,
                edt_number.getText().toString(),
                selected_image_path,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json != null){
                            Toast.makeText(ComfirmPackageOrder.this, getResources().getString(R.string.confirm_package_order_success), Toast.LENGTH_LONG).show();
                            finish();
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callRequestAPI();
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });
    }
}
