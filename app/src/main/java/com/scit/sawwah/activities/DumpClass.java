package com.scit.sawwah.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.scit.sawwah.MainActivity;

/**
 * Created by Misheal on 9/9/2019.
 */

public class DumpClass {
    private int need_to_login = 0;
    private int user_type = 0;
    private int is_profile_completed = 0;
    private int social_register = 0;
    private Context context;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    private void login(){
        //Need to login
        if (need_to_login == 1){
            context.startActivity(new Intent(context,LoginActivity.class));
        }else {
            //App new installed
            if (sharedPreferences.getString("access_token","").equals("")){
                context.startActivity(new Intent(context,LoginActivity.class));
            }else {
                //Profile Completed
                if (is_profile_completed == 1){
                    if (user_type == 1){
                        editor.putString("account_type","customer");
                    }else if (user_type == 2){
                        editor.putString("account_type","owner");
                    }
                    editor.commit();
                    context.startActivity(new Intent(context,MainActivity.class));
                }else {
                    //User type
                    if (user_type == 0){
                        if (social_register == 0){
                            context.startActivity(new Intent(context,ChooseAccountTypeActivity.class));
                        }else {
                            context.startActivity(new Intent(context,RegisterActivity.class));
                        }
                    }else if (user_type == 1){
                        context.startActivity(new Intent(context,MainActivity.class));
                    }else if (user_type == 2){
                        context.startActivity(new Intent(context,RegisterInstitutionActivity.class));
                    }
                }
            }
        }
    }
}
