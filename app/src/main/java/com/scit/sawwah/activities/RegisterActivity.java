package com.scit.sawwah.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Environment;
import androidx.annotation.NonNull;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.Gson;
import com.scit.sawwah.APIClasses.BasicAPIsClass;
import com.scit.sawwah.Models.DeviceSettingsObject;
import com.scit.sawwah.Models.RegisterResponse;
import com.scit.sawwah.R;
import com.scit.sawwah.adapters.CustomSpinnerAdapter;
import com.scit.sawwah.interfaces.IFailure;
import com.scit.sawwah.interfaces.IResponse;
import com.scit.sawwah.tools.BaseActivity;
import com.scit.sawwah.tools.BaseFunctions;
import com.soundcloud.android.crop.Crop;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class RegisterActivity extends BaseActivity {
    private RelativeLayout toolbar;
    private ImageButton btn_back;
    private LinearLayout btn_create_account;
    private Spinner type_spinner;
    private CustomSpinnerAdapter type_adapter;
    private List<String> type_list;
    private CircleImageView img_personal_image;
    private EditText edt_name,edt_username,edt_password,edt_confirm;
    private ImageView img_password_eye,img_confirm_eye;
    private LinearLayout btn_policy;
    private int password_eye_state = 0,confirm_eye_state = 0;
    private Boolean image_chosen = false;
    private Boolean policy_confirmed = false;
    private String selected_type = "";
    private static final String TEMP_PHOTO_FILE = "temporary_holder.jpg";
    private String imagePath = "";
    private Uri imageUri = null;
    private LinearLayout root;
    private ImageView img_like;


    @Override
    public void set_layout() {
        setContentView(R.layout.activity_register);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        init_spinner();
    }

    @Override
    public void init_views() {
        toolbar = findViewById(R.id.register_toolbar);
        btn_back = toolbar.findViewById(R.id.toolbar_back_btn);
        type_spinner = findViewById(R.id.register_account_type_spinner);
        btn_create_account = findViewById(R.id.register_create_account);
        img_personal_image = findViewById(R.id.register_personal_image);
        edt_username = findViewById(R.id.register_email_or_phone_edt);
        edt_name = findViewById(R.id.register_full_name);
        edt_password = findViewById(R.id.register_password_edt);
        edt_confirm = findViewById(R.id.register_confirm_password_edt);
        img_password_eye = findViewById(R.id.register_password_icon);
        img_confirm_eye = findViewById(R.id.register_confirm_password_icon);
        btn_policy = findViewById(R.id.register_policy);
        root = findViewById(R.id.register_layout);
        img_like = findViewById(R.id.register_like);
    }

    @Override
    public void init_events() {
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        type_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int i, long id) {
                long pos = parent.getItemIdAtPosition(i);
                int position = Integer.valueOf(String.valueOf(pos));
                if (position == 0){
                    selected_type = "2";
                }else {
                    selected_type = "1";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        img_personal_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityCompat.requestPermissions(RegisterActivity.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        1);
            }
        });

        img_password_eye.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //1 : text
                //InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD : textPassword
                if (password_eye_state == 0){
                    password_eye_state = 1;
                    img_password_eye.setImageResource(R.drawable.ic_white_eye);
                    edt_password.setInputType(1);
                }else {
                    password_eye_state = 0;
                    img_password_eye.setImageResource(R.drawable.ic_white_hidden_eye);
                    edt_password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                }
            }
        });
        img_confirm_eye.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //1 : text
                //InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD : textPassword
                if (confirm_eye_state == 0){
                    confirm_eye_state = 1;
                    img_confirm_eye.setImageResource(R.drawable.ic_white_eye);
                    edt_confirm.setInputType(1);
                }else {
                    confirm_eye_state = 0;
                    img_confirm_eye.setImageResource(R.drawable.ic_white_hidden_eye);
                    edt_confirm.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                }
            }
        });

        btn_policy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                policy_confirmed = true;
                img_like.setImageResource(R.drawable.ic_blue_like);
                img_like.startAnimation(AnimationUtils.loadAnimation(RegisterActivity.this,R.anim.on_click_animation_reversed));
                Toast.makeText(RegisterActivity.this, getResources().getString(R.string.register_policy_confirmed), Toast.LENGTH_SHORT).show();
            }
        });
        btn_create_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!image_chosen){
                    Toast.makeText(RegisterActivity.this, getResources().getString(R.string.register_no_personal_image), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (edt_username.getText().toString().equals("")){
                    Toast.makeText(RegisterActivity.this, getResources().getString(R.string.register_no_phone_number), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (edt_username.getText().toString().length() < 10){
                    Toast.makeText(RegisterActivity.this, getResources().getString(R.string.register_error_mobile), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!BaseFunctions.subString(edt_username.getText().toString(),0,2).equals("09")){
                    Toast.makeText(RegisterActivity.this, getResources().getString(R.string.register_error_mobile2), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (edt_name.getText().toString().equals("")){
                    Toast.makeText(RegisterActivity.this, getResources().getString(R.string.register_no_full_name), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (edt_password.getText().toString().equals("")){
                    Toast.makeText(RegisterActivity.this, getResources().getString(R.string.register_no_password), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (edt_confirm.getText().toString().equals("")){
                    Toast.makeText(RegisterActivity.this, getResources().getString(R.string.register_no_confirm), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!edt_password.getText().toString().equals(edt_confirm.getText().toString())){
                    Toast.makeText(RegisterActivity.this, getResources().getString(R.string.register_not_match), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (selected_type.equals("")){
                    Toast.makeText(RegisterActivity.this, getResources().getString(R.string.register_no_type), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!policy_confirmed){
                    Toast.makeText(RegisterActivity.this, getResources().getString(R.string.register_no_policy), Toast.LENGTH_SHORT).show();
                    return;
                }
                callAPI();
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case 1:
            {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    Crop.pickImage(RegisterActivity.this);
                }else {
                    Toast.makeText(this, getResources().getString(R.string.add_photos_permission_denied), Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }

    private Uri getTempUri() {
        return Uri.fromFile(getTempFile());
    }


    private File getTempFile() {
        if (isSDCARDMounted()) {

            File f = new File(Environment.getExternalStorageDirectory(),TEMP_PHOTO_FILE);
            try {
                f.createNewFile();
            } catch (IOException e) {

            }
            return f;
        } else {
            return null;
        }
    }

    private boolean isSDCARDMounted(){
        String status = Environment.getExternalStorageState();
        if (status.equals(Environment.MEDIA_MOUNTED))
            return true;
        return false;
    }

    protected void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK){
            if (requestCode == Crop.REQUEST_PICK){
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Uri source_uri = data.getData();
                        Uri destination_uri = Uri.fromFile(getTempFile());
                        Crop.of(source_uri,destination_uri).asSquare().start(RegisterActivity.this);
                        //img_personal_image.setImageURI(Crop.getOutput(data));
                        String filePath= Environment.getExternalStorageDirectory()
                                + "/temporary_holder.jpg";
                        imagePath = filePath;
                        image_chosen = true;
                    }
                });
            }else if (requestCode == Crop.REQUEST_CROP){
                handle_crop(resultCode,data);
            }
        }
    }

    private void handle_crop(int code, final Intent result){
        if (code == RESULT_OK){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    img_personal_image.setImageDrawable(null);
                    img_personal_image.setImageURI(Crop.getOutput(result));
                    String filePath= Environment.getExternalStorageDirectory()
                            + "/temporary_holder.jpg";
                    imagePath = filePath;
                    image_chosen = true;
                }
            });

        }else if (code == Crop.RESULT_ERROR){
            Toast.makeText(this, "An error occurred", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void set_fragment_place() {

    }

    private void init_list(){
        type_list = new ArrayList<>();
        type_list.add(getResources().getString(R.string.register_institution_owner));
        type_list.add(getResources().getString(R.string.register_beneficiary));
    }

    private void init_spinner(){
        init_list();
        type_adapter = new CustomSpinnerAdapter(RegisterActivity.this,R.layout.spinner_white_arrow_item,type_list);
        type_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        type_spinner.setAdapter(type_adapter);
    }

    private void callAPI(){
        BasicAPIsClass.register(sharedPreferences,
                RegisterActivity.this,
                sharedPreferences.getString("language", "ar"),
                sharedPreferences.getString("service_token", ""),
                edt_name.getText().toString(),
                edt_username.getText().toString(),
                edt_password.getText().toString(),
                selected_type,
                imagePath, new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null){
                            String json1 = new Gson().toJson(json);
                            RegisterResponse success = new Gson().fromJson(json1,RegisterResponse.class);
                            //Id
                            editor.putInt("account_id",success.getId());
                            //Service Token
                            editor.putString("service_token",success.getService_token());
                            //Image
                            if (success.getImage()!=null){
                                editor.putString("account_image",success.getImage());
                            }
                            //name
                            if (success.getName()!=null){
                                editor.putString("account_name",success.getName());
                            }
                            //User name
                            if (success.getUsername()!=null){
                                editor.putString("account_user_name",success.getUsername());
                            }
                            //Account type
                            if (success.getAccount_type_id()!= 0){
                                if (success.getAccount_type_id() == 1){
                                    editor.putString("account_type","customer");
                                }else {
                                    editor.putString("account_type","owner");
                                }
                            }
                            //Account status
                            editor.putInt("account_status",success.getStatus());
                            //Access Token
                            if (success.getAccess_token()!=null){
                                editor.putString("access_token",success.getAccess_token());
                            }
                            //Device Settings
                            if (success.getDevice_setting()!=null){
                                DeviceSettingsObject dso = success.getDevice_setting();
                                //City Id
                                editor.putInt("settings_city_id",dso.getCity_id());
                                //Country Id
                                editor.putInt("settings_country_id",dso.getCity_id());
                                //Language
                                editor.putString("language", BaseFunctions.numberToLanguageConverter(String.valueOf(dso.getView_language_id())));
                                //Alert
                                editor.putBoolean("settings_alerts",BaseFunctions.stringNumberToBoolean(String.valueOf(dso.getAllow_global_notification())));
                                editor.commit();
                            }
                            if (success.getAccount_settings()!=null){
                                editor.putInt("account_status",success.getAccount_settings().getAccount_status());
                            }
                            editor.putBoolean("is_visitor",false);
                            editor.commit();
                            Toast.makeText(RegisterActivity.this, getResources().getString(R.string.register_success), Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(RegisterActivity.this,ConfirmRegisterActivity.class));
                            finish();
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callAPI();
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });
    }
}
