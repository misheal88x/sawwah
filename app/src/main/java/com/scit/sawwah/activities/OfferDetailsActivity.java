package com.scit.sawwah.activities;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.gson.Gson;
import com.scit.sawwah.APIClasses.FavouriteAPIsClass;
import com.scit.sawwah.APIClasses.OffersAPIsClass;
import com.scit.sawwah.Models.OfferObject;
import com.scit.sawwah.R;
import com.scit.sawwah.adapters.HomeOffersAdapter;
import com.scit.sawwah.interfaces.IFailure;
import com.scit.sawwah.interfaces.IMove;
import com.scit.sawwah.interfaces.IResponse;
import com.scit.sawwah.tools.BaseActivity;
import com.scit.sawwah.tools.BaseFunctions;
import com.scit.sawwah.tools.BaseRetrofit;

import java.util.ArrayList;
import java.util.List;

public class OfferDetailsActivity extends BaseActivity {
    private RelativeLayout toolbar;
    private ImageButton btn_back;
    private TextView tv_title;
    private RecyclerView rv_similar;
    private List<OfferObject> list_offers;
    private HomeOffersAdapter similar_adapter;
    private LinearLayoutManager similar_layout_manager;
    private boolean loved = false;
    private int offer_id = 0;
    private LinearLayout root;
    private ScrollView scrollView;
    private Intent myIntent;
    private int lovers = 0;
    private TextView tv_offer_title,tv_inst_name,tv_desc,tv_start_date,tv_end_date,tv_lovers,tv_viewers,tv_similar;
    private ImageView img_inst_icon,img_image,img_love,img_share;
    private CallbackManager callbackManager;
    private ShareDialog shareDialog;

    @Override
    public void set_layout() {
        setContentView(R.layout.activity_offer_details);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        tv_title.setText(getResources().getString(R.string.offer_details_title));
        //Similar Offers
        init_list();
        similar_adapter = new HomeOffersAdapter(OfferDetailsActivity.this, list_offers,new IMove() {
            @Override
            public void move(int position) {

            }
        });
        similar_layout_manager = new LinearLayoutManager(OfferDetailsActivity.this,LinearLayoutManager.HORIZONTAL,false);
        rv_similar.setLayoutManager(similar_layout_manager);
        rv_similar.setAdapter(similar_adapter);
        callAPI(String.valueOf(myIntent.getIntExtra("offer_id",0)));
    }

    @Override
    public void init_views() {
        toolbar = findViewById(R.id.offer_toolbar);
        btn_back = toolbar.findViewById(R.id.toolbar_back_btn);
        tv_title = toolbar.findViewById(R.id.toolbar_title);
        rv_similar = findViewById(R.id.offer_details_similar_recycler);
        root = findViewById(R.id.offer_details_layout);
        tv_offer_title = findViewById(R.id.offer_details_title_txt);
        tv_inst_name = findViewById(R.id.offer_details_institution_name_txt);
        tv_desc = findViewById(R.id.offer_details_desc);
        tv_start_date = findViewById(R.id.offer_details_start_date);
        tv_end_date = findViewById(R.id.offer_details_end_date);
        tv_lovers = findViewById(R.id.offer_details_lovers);
        tv_viewers = findViewById(R.id.offer_details_viewers);
        tv_similar = findViewById(R.id.offer_details_similar_txt);
        img_inst_icon = findViewById(R.id.offer_details_inst_icon);
        img_image = findViewById(R.id.offer_details_image);
        img_love = findViewById(R.id.offer_details_love);
        img_share = findViewById(R.id.offer_details_share);
        scrollView = findViewById(R.id.offer_details_scrollview);
        myIntent = getIntent();
        scrollView.setVisibility(View.GONE);
    }

    @Override
    public void init_events() {
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        img_love.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (loved == false){
                    toggle_item(String.valueOf(offer_id),"2","1");
                }else {
                    toggle_item(String.valueOf(offer_id),"2","0");
                }
            }
        });
        img_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FacebookSdk.sdkInitialize(OfferDetailsActivity.this.getApplicationContext());
                callbackManager = CallbackManager.Factory.create();
                shareDialog = new ShareDialog(OfferDetailsActivity.this);
                shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
                    @Override
                    public void onSuccess(Sharer.Result result) {
                        Toast.makeText(OfferDetailsActivity.this, "Posted successfully", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onCancel() {
                        Toast.makeText(OfferDetailsActivity.this, "Posted canceled", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(FacebookException error) {
                        Toast.makeText(OfferDetailsActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
                ShareLinkContent linkContent = new ShareLinkContent.Builder().setQuote(getResources().getString(R.string.facebook_post_offer)).
                        setContentUrl(Uri.parse(BaseRetrofit.BASE_ALI+"offers/"+String.valueOf(offer_id))).build();
                if (shareDialog.canShow(ShareLinkContent.class)){
                    shareDialog.show(linkContent);
                }else {
                    Toast.makeText(OfferDetailsActivity.this, "can't show", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void set_fragment_place() {

    }

    private void init_list(){
        list_offers = new ArrayList<>();
    }

    private void callAPI(final String id){
        OffersAPIsClass.get_details(sharedPreferences,
                OfferDetailsActivity.this,
                sharedPreferences.getString("language", "ar"),
                sharedPreferences.getString("service_token", ""),
                id,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json != null){
                            String json1 = new Gson().toJson(json);
                            OfferObject success = new Gson().fromJson(json1,OfferObject.class);
                            //ID
                            offer_id = success.getId();
                            //Offer title
                            if (success.getTitle() != null){
                                tv_offer_title.setText(success.getTitle());
                            }
                            //Institution title
                            if (success.getFacility_name()!=null){
                                tv_inst_name.setText(success.getFacility_name());
                            }
                            //Institution icon
                            if (success.getFacility_image()!=null){
                                BaseFunctions.setGlideImage(OfferDetailsActivity.this,img_inst_icon,success.getFacility_image());
                            }
                            //Offer image
                            if (success.getImage()!=null){
                                BaseFunctions.setGlideImage(OfferDetailsActivity.this,img_image,success.getImage());
                            }
                            //Description
                            tv_desc.setText(success.getDetails());
                            //Start date
                            if (success.getStart_date()!=null){
                                tv_start_date.setText(BaseFunctions.dateExtractor(success.getStart_date()));
                            }
                            //End Date
                            if (success.getEnd_date()!=null){
                                tv_end_date.setText(BaseFunctions.dateExtractor(success.getEnd_date()));
                            }
                            //Love
                            loved = success.isMarked_as_favourite();
                            if (success.isMarked_as_favourite()){
                                img_love.setImageResource(R.drawable.ic_heart);
                            }else {
                                img_love.setImageResource(R.drawable.ic_grey_heart);
                            }
                            //Lovers
                            lovers = success.getFavourite_count();
                            tv_lovers.setText(String.valueOf(success.getFavourite_count()));
                            //Viewers
                            tv_viewers.setText(String.valueOf(success.getVisit_views()));
                            //Similar Offers
                            if (success.getSimilar_offers()!=null){
                                if (success.getSimilar_offers().size() > 0){
                                    for (OfferObject oo : success.getSimilar_offers()){
                                        list_offers.add(oo);
                                        similar_adapter.notifyDataSetChanged();
                                    }
                                    runAnimation(rv_similar,0,similar_adapter);
                                }else {
                                    tv_similar.setVisibility(View.GONE);
                                }
                            }
                            tv_similar.setVisibility(View.GONE);
                            scrollView.setVisibility(View.VISIBLE);
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callAPI(id);
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });
    }

    private void toggle_item(String id, String type, final String state){
        FavouriteAPIsClass.toggle_item(sharedPreferences,
                OfferDetailsActivity.this,
                sharedPreferences.getString("language", "ar"),
                sharedPreferences.getString("service_token", ""),
                id,
                type,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json !=null){
                            if (state.equals("0")){
                                lovers--;
                                tv_lovers.setText(String.valueOf(lovers));
                                img_love.setImageResource(R.drawable.ic_grey_heart);
                                Toast.makeText(OfferDetailsActivity.this, getResources().getString(R.string.favourite_offer_remover), Toast.LENGTH_SHORT).show();
                            }else {
                                lovers++;
                                tv_lovers.setText(String.valueOf(lovers));
                                img_love.setImageResource(R.drawable.ic_heart);
                                Toast.makeText(OfferDetailsActivity.this, getResources().getString(R.string.favourite_offer_add), Toast.LENGTH_SHORT).show();
                            }
                        }else {
                            Toast.makeText(OfferDetailsActivity.this, getResources().getString(R.string.error_occurred), Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Toast.makeText(OfferDetailsActivity.this, getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    public void runAnimation(RecyclerView recyclerView,int type,HomeOffersAdapter adapter){
        Context context = recyclerView.getContext();
        LayoutAnimationController controller = null;
        if (type == 0){ // Fall down animation
            controller = AnimationUtils.loadLayoutAnimation(context,R.anim.layout_fall_down);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutAnimation(controller);
            recyclerView.getAdapter().notifyDataSetChanged();
            recyclerView.scheduleLayoutAnimation();
        }
    }
}
