package com.scit.sawwah.activities;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.scit.sawwah.R;
import com.scit.sawwah.fragments.AccountAlbumFragment;
import com.scit.sawwah.fragments.AccountMyCardFragment;
import com.scit.sawwah.fragments.AccountStatisticsFragment;
import com.scit.sawwah.fragments.AccountUserInfoFragment;
import com.scit.sawwah.tools.BaseActivity;

public class AccountSettingsActivity extends BaseActivity {
    private RelativeLayout toolbar;
    private ImageButton btn_back;
    private TextView tv_title;
    private Button btn_account,btn_statistics,btn_album,btn_card;
    private ImageView arr_account,arr_statistics,arr_album,arr_card;

    @Override
    public void set_layout() {
        setContentView(R.layout.activity_account_settings);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        tabClicked(1);
    }

    @Override
    public void init_views() {
        toolbar = findViewById(R.id.account_settings_toolbar);
        btn_back = toolbar.findViewById(R.id.toolbar_back_btn);
        tv_title = toolbar.findViewById(R.id.toolbar_title);

        btn_account = findViewById(R.id.account_settings_account_btn);
        btn_statistics = findViewById(R.id.account_settings_statistics_btn);
        btn_album = findViewById(R.id.account_settings_album_btn);
        btn_card = findViewById(R.id.account_settings_card_btn);

        arr_account = findViewById(R.id.account_settings_account_arrow);
        arr_statistics = findViewById(R.id.account_settings_result_arrow);
        arr_album = findViewById(R.id.account_settings_album_arrow);
        arr_card = findViewById(R.id.account_settings_card_arrow);
    }

    @Override
    public void init_events() {
        btn_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tabClicked(1);
            }
        });
        btn_statistics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tabClicked(2);
            }
        });
        btn_album.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tabClicked(3);
            }
        });
        btn_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tabClicked(4);
            }
        });
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void set_fragment_place() {
        this.fragment_place = (ViewGroup) findViewById(R.id.fragment_place);
    }

    private void tabClicked(int i){
        switch (i){
            //Account
            case 1:{
                btn_account.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_account_account_on, 0, 0);
                btn_statistics.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_account_result_off, 0, 0);
                btn_album.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_account_album_off, 0, 0);
                btn_card.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_account_card_off, 0, 0);

                btn_account.setTextColor(getResources().getColor(R.color.tabchecked));
                btn_statistics.setTextColor(getResources().getColor(R.color.white));
                btn_account.setTextColor(getResources().getColor(R.color.white));
                btn_account.setTextColor(getResources().getColor(R.color.white));


                arr_account.setVisibility(View.VISIBLE);
                arr_album.setVisibility(View.GONE);
                arr_card.setVisibility(View.GONE);
                arr_statistics.setVisibility(View.GONE);

                open_fragment(new AccountUserInfoFragment());
            }break;
            //StatisticsAPIs
            case 2:{
                btn_account.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_account_account_off, 0, 0);
                btn_statistics.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_account_result_on, 0, 0);
                btn_album.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_account_album_off, 0, 0);
                btn_card.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_account_card_off, 0, 0);

                btn_account.setTextColor(getResources().getColor(R.color.white));
                btn_statistics.setTextColor(getResources().getColor(R.color.tabchecked));
                btn_account.setTextColor(getResources().getColor(R.color.white));
                btn_account.setTextColor(getResources().getColor(R.color.white));

                arr_account.setVisibility(View.GONE);
                arr_album.setVisibility(View.GONE);
                arr_card.setVisibility(View.GONE);
                arr_statistics.setVisibility(View.VISIBLE);

                open_fragment(new AccountStatisticsFragment());
            }break;
            //Album
            case 3:{
                btn_account.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_account_account_off, 0, 0);
                btn_statistics.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_account_result_off, 0, 0);
                btn_album.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_account_album_on, 0, 0);
                btn_card.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_account_card_off, 0, 0);

                btn_account.setTextColor(getResources().getColor(R.color.white));
                btn_statistics.setTextColor(getResources().getColor(R.color.white));
                btn_account.setTextColor(getResources().getColor(R.color.tabchecked));
                btn_account.setTextColor(getResources().getColor(R.color.white));

                arr_account.setVisibility(View.GONE);
                arr_album.setVisibility(View.VISIBLE);
                arr_card.setVisibility(View.GONE);
                arr_statistics.setVisibility(View.GONE);

                open_fragment(new AccountAlbumFragment());
            }break;
            //Card
            case 4:{
                btn_account.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_account_account_off, 0, 0);
                btn_statistics.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_account_result_off, 0, 0);
                btn_album.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_account_album_off, 0, 0);
                btn_card.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_account_card_on, 0, 0);

                btn_account.setTextColor(getResources().getColor(R.color.white));
                btn_statistics.setTextColor(getResources().getColor(R.color.white));
                btn_account.setTextColor(getResources().getColor(R.color.white));
                btn_account.setTextColor(getResources().getColor(R.color.tabchecked));

                arr_account.setVisibility(View.GONE);
                arr_album.setVisibility(View.GONE);
                arr_card.setVisibility(View.VISIBLE);
                arr_statistics.setVisibility(View.GONE);

                open_fragment(new AccountMyCardFragment());
            }break;
        }
    }


}
