package com.scit.sawwah.activities;

import android.content.Context;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.scit.sawwah.APIClasses.AccountsAPIsClass;
import com.scit.sawwah.Models.NotificationObject;
import com.scit.sawwah.Models.NotificationsResponse;
import com.scit.sawwah.R;
import com.scit.sawwah.adapters.NotificationsAdapter;
import com.scit.sawwah.interfaces.IFailure;
import com.scit.sawwah.interfaces.IMove;
import com.scit.sawwah.interfaces.IResponse;
import com.scit.sawwah.tools.BaseActivity;
import com.scit.sawwah.tools.EndlessRecyclerViewScrollListener;

import java.util.ArrayList;
import java.util.List;

public class NotificationsActivity extends BaseActivity {
    private RelativeLayout toolbar;
    private ImageButton btn_back;
    private TextView tv_title;
    private List<NotificationObject> list_notifications;
    private RecyclerView rv_notifications;
    private NotificationsAdapter adapter;
    private LinearLayoutManager layoutManager;
    private RelativeLayout root;
    private int currentPage = 1;
    private LinearLayout empty_layout;

    @Override
    public void set_layout() {
        setContentView(R.layout.activity_notifications);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        tv_title.setText(getResources().getString(R.string.notifications));
        list_notifications = new ArrayList<>();
        adapter = new NotificationsAdapter(NotificationsActivity.this, list_notifications,new IMove() {
            @Override
            public void move(int position) {

            }
        });
        layoutManager = new LinearLayoutManager(NotificationsActivity.this,LinearLayoutManager.VERTICAL,false);
        rv_notifications.setLayoutManager(layoutManager);
        rv_notifications.setAdapter(adapter);
        rv_notifications.addOnScrollListener(new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if (list_notifications.size()>=20){
                    Snackbar.make(root,getResources().getString(R.string.get_more),Snackbar.LENGTH_SHORT).show();
                    currentPage++;
                    callAPI(1,currentPage);
                }
            }
        });
        callAPI(0,currentPage);
    }

    @Override
    public void init_views() {
        toolbar = findViewById(R.id.notifications_toolbar);
        btn_back = toolbar.findViewById(R.id.toolbar_back_btn);
        tv_title = toolbar.findViewById(R.id.toolbar_title);
        rv_notifications = findViewById(R.id.notifications_recycler);
        root = findViewById(R.id.notifications_layout);
        empty_layout = findViewById(R.id.empty_layout);
    }

    @Override
    public void init_events() {
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void set_fragment_place() {

    }
    private void callAPI(final int type, final int page){
        AccountsAPIsClass.getNotifications(sharedPreferences,
                NotificationsActivity.this,
                sharedPreferences.getString("language", "ar"),
                sharedPreferences.getString("service_token", ""),
                type,
                page,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json !=null){
                            String json1 = new Gson().toJson(json);
                            NotificationsResponse success = new Gson().fromJson(json1,NotificationsResponse.class);
                            if (success.getData()!=null){
                                if (success.getData().size() > 0){
                                    empty_layout.setVisibility(View.GONE);
                                    for (NotificationObject oo : success.getData()){
                                        list_notifications.add(oo);
                                        adapter.notifyDataSetChanged();
                                    }
                                    if (type == 0){
                                        runAnimation(rv_notifications,0,adapter);
                                    }
                                }else {
                                    if (type == 1){
                                        Snackbar.make(root,getResources().getString(R.string.no_more),Snackbar.LENGTH_SHORT).show();
                                    }else {
                                        empty_layout.setVisibility(View.VISIBLE);
                                        Toast.makeText(NotificationsActivity.this, getResources().getString(R.string.no_data), Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callAPI(type,page);
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });
    }

    public void runAnimation(RecyclerView recyclerView,int type,NotificationsAdapter adapter){
        Context context = recyclerView.getContext();
        LayoutAnimationController controller = null;
        if (type == 0){ // Fall down animation
            controller = AnimationUtils.loadLayoutAnimation(context,R.anim.layout_fall_down_vertical);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutAnimation(controller);
            recyclerView.getAdapter().notifyDataSetChanged();
            recyclerView.scheduleLayoutAnimation();
        }
    }

}
