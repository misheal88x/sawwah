package com.scit.sawwah.activities;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.maps.android.ui.IconGenerator;
import com.scit.sawwah.APIClasses.SearchAPIsClass;
import com.scit.sawwah.Models.InstitutionObject;
import com.scit.sawwah.Models.SearchResponse;
import com.scit.sawwah.R;
import com.scit.sawwah.interfaces.IFailure;
import com.scit.sawwah.interfaces.IResponse;
import com.scit.sawwah.tools.BaseActivity;

import java.util.ArrayList;
import java.util.List;

public class SearchResultsMapActivity extends BaseActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener{

    private GoogleMap map;
    private String currentLatitude = "", currentLongitude = "";
    private FloatingActionButton location_fab;
    private Marker marker;
    private List<InstitutionObject> list_of_facilities;
    private List<MarkerOptions> list_of_markers;
    private RelativeLayout root;
    //---------------------------------------------------------------------------
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    PendingResult<LocationSettingsResult> result;
    final static int REQUEST_LOCATION = 199;
    private Intent myIntent;
    //---------------------------------------------------------------------------
    @Override
    public void set_layout() {
        setContentView(R.layout.activity_search_results_map);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        initMap();
        list_of_facilities = new ArrayList<>();
        list_of_markers = new ArrayList<>();
        callSearchAPI(0,1,0);
    }

    @Override
    public void init_views() {
        root = findViewById(R.id.search_results_map_layout);
        location_fab = findViewById(R.id.map_location_fab);
        myIntent = getIntent();
    }

    @Override
    public void init_events() {
        location_fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(SearchResultsMapActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(SearchResultsMapActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                Location currentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                if (currentLocation !=null){
                    currentLatitude = String.valueOf(currentLocation.getLatitude());
                    currentLongitude = String.valueOf(currentLocation.getLongitude());
                    //map.addMarker(new MarkerOptions().position(new LatLng(currentLocation.getLatitude(),currentLocation.getLongitude())).title(getResources().getString(R.string.map_my_location))).setDraggable(true);
                    map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()),15.0f));
                }
            }
        });
    }

    @Override
    public void set_fragment_place() {

    }

    private void initMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.search_results_map_fragment);
        mapFragment.getMapAsync(SearchResultsMapActivity.this);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);

        result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                //final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS: {
                        if (ActivityCompat.checkSelfPermission(SearchResultsMapActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(SearchResultsMapActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            return;
                        }
                        Location currentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                        if (currentLocation != null) {
                            currentLatitude = String.valueOf(currentLocation.getLatitude());
                            currentLongitude = String.valueOf(currentLocation.getLongitude());
                            map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()), 15.0f));
                        }
                    }
                    break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(
                                    SearchResultsMapActivity.this,
                                    REQUEST_LOCATION);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        //...
                        break;
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("onActivityResult()", Integer.toString(resultCode));

        //final LocationSettingsStates states = LocationSettingsStates.fromIntent(data);
        switch (requestCode) {
            case REQUEST_LOCATION:
                switch (resultCode) {
                    case Activity.RESULT_OK: {
                        // All required changes were successfully made
                        Toast.makeText(SearchResultsMapActivity.this, getResources().getString(R.string.gps_on), Toast.LENGTH_LONG).show();
                        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            return;
                        }
                        Location currentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                        if (currentLocation !=null){
                            currentLatitude = String.valueOf(currentLocation.getLatitude());
                            currentLongitude = String.valueOf(currentLocation.getLongitude());
                            //map.addMarker(new MarkerOptions().position(new LatLng(currentLocation.getLatitude(),currentLocation.getLongitude())).title(getResources().getString(R.string.map_my_location))).setDraggable(true);
                            map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()), 15.0f));
                        }
                        break;
                    }
                    case Activity.RESULT_CANCELED:
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder(SearchResultsMapActivity.this);
                        builder.setMessage(getResources().getString(R.string.gps_off)).setCancelable(false).
                                setPositiveButton(getResources().getString(R.string.no_internet_ok), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        finish();
                                    }
                                }).show();
                        break;
                    }
                    default:
                    {
                        break;
                    }
                }
                break;
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.map = googleMap;
        mGoogleApiClient = new GoogleApiClient.Builder(SearchResultsMapActivity.this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(SearchResultsMapActivity.this)
                .addOnConnectionFailedListener(SearchResultsMapActivity.this).build();
        mGoogleApiClient.connect();
        this.map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                for (InstitutionObject soo : list_of_facilities){
                    if (soo.getLat() ==marker.getPosition().latitude&&
                            soo.getLng()==marker.getPosition().longitude) {
                        Intent intent = new Intent(SearchResultsMapActivity.this,ProfileActivity.class);
                        intent.putExtra("institution_id",soo.getId());
                        startActivity(intent);
                    }
                }
                return true;
            }
        });
    }


    private void callSearchAPI(final int type , final int page, final int section){
        SearchAPIsClass.search(sharedPreferences,
                SearchResultsMapActivity.this,
                sharedPreferences.getString("language", "ar"),
                sharedPreferences.getString("service_token", ""),
                sharedPreferences.getString("my_lat","0"),
                sharedPreferences.getString("my_lng","0"),
                myIntent.getStringExtra("city_id"),
                "all",
                myIntent.getStringExtra("country_id"),
                myIntent.getStringExtra("category_id"),
                myIntent.getStringExtra("rating"),
                String.valueOf(myIntent.getIntExtra("radius",-1)),
                "",
                type,
                page,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json !=null){
                            String json1 = new Gson().toJson(json);
                            SearchResponse success = new Gson().fromJson(json1,SearchResponse.class);
                            if (type == 0){
                                //Facilities
                                if (success.getFacilities()!=null){
                                    if (success.getFacilities()!=null){
                                        if (success.getFacilities().size()>0){
                                            IconGenerator iconFactory = new IconGenerator(SearchResultsMapActivity.this);
                                            iconFactory.setColor(getResources().getColor(R.color.colorPrimary));
                                            iconFactory.setTextAppearance(R.style.CustomTitleWhite);
                                            float sumLat = 0;
                                            float sumLng = 0;
                                            for (InstitutionObject io : success.getFacilities()){
                                                sumLat+=io.getLat();
                                                sumLng+=io.getLng();
                                                String text = io.getName();
                                                LatLng pos = new LatLng(Double.valueOf(io.getLat()),Double.valueOf(io.getLng()));
                                                MarkerOptions markerOptions = new MarkerOptions().
                                                        icon(BitmapDescriptorFactory.fromBitmap(iconFactory.makeIcon(text))).
                                                        position(pos).
                                                        anchor(iconFactory.getAnchorU(), iconFactory.getAnchorV());
                                                if (map != null){
                                                    map.addMarker(markerOptions);
                                                    list_of_facilities.add(io);
                                                    list_of_markers.add(markerOptions);
                                                }
                                            }
                                            //map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(sumLat/success.getFacilities().getData().size(),sumLng/success.getFacilities().getData().size() ), 15.0f));
                                        }else {
                                            Toast.makeText(SearchResultsMapActivity.this, getResources().getString(R.string.advanced_search_no_results), Toast.LENGTH_SHORT).show();
                                        }
                                    }else {
                                        Toast.makeText(SearchResultsMapActivity.this, getResources().getString(R.string.advanced_search_no_results), Toast.LENGTH_SHORT).show();
                                    }
                                }else {
                                    Toast.makeText(SearchResultsMapActivity.this, getResources().getString(R.string.advanced_search_no_results), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callSearchAPI(type,page,section);
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });

    }

}
