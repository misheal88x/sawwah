package com.scit.sawwah.activities;

import android.content.Context;
import android.content.Intent;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.scit.sawwah.APIClasses.SearchAPIsClass;
import com.scit.sawwah.Models.AdvertisementObject;
import com.scit.sawwah.Models.InstitutionObject;
import com.scit.sawwah.Models.OfferObject;
import com.scit.sawwah.Models.SearchResponse;
import com.scit.sawwah.R;
import com.scit.sawwah.adapters.SearchAdvertisementsAdapter;
import com.scit.sawwah.adapters.SearchInstitutionsAdapter;
import com.scit.sawwah.adapters.SearchOffersAdapter;
import com.scit.sawwah.interfaces.IFailure;
import com.scit.sawwah.interfaces.IMove;
import com.scit.sawwah.interfaces.IResponse;
import com.scit.sawwah.tools.BaseActivity;

import java.util.ArrayList;
import java.util.List;

public class SearchResultsActivity extends BaseActivity {
    private RelativeLayout toolbar;
    private ImageButton btn_back;
    private TextView tv_title;
    private RecyclerView rv_offers,rv_advertisements,rv_institutions;
    private List<InstitutionObject> list_insts;
    private List<OfferObject> list_offers;
    private List<AdvertisementObject> list_ads;
    private LinearLayoutManager offersLayoutManager,advertisementsLayoutManager,institutionsLayoutManager;
    private SearchOffersAdapter offersAdapter;
    private SearchAdvertisementsAdapter advertisementsAdapter;
    private SearchInstitutionsAdapter institutionsAdapter;
    private Button btn_all_offers,btn_all_advertisements,btn_all_insts;
    private RelativeLayout root;
    private RelativeLayout lyt_offers,lyt_ads,lyt_inst;
    private Intent myIntent;
    private ScrollView scrollView;
    private String search_type = "",word = "";
    private int currentOffersPage = 1;
    private int currentAdsPage = 1;
    private int currentInstPage = 1;
    private boolean paginateOffers = true;
    private boolean paginateAds = true;
    private boolean paginateInst = true;
    private LinearLayout empty_layout;

    @Override
    public void set_layout() {
        setContentView(R.layout.activity_search_results);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        tv_title.setText(getResources().getString(R.string.search_results_title));
        //Offers
        list_offers = new ArrayList<>();
        offersAdapter = new SearchOffersAdapter(SearchResultsActivity.this, list_offers,new IMove() {
            @Override
            public void move(int position) {

            }
        });
        offersLayoutManager = new LinearLayoutManager(SearchResultsActivity.this,LinearLayoutManager.HORIZONTAL,false);
        rv_offers.setLayoutManager(offersLayoutManager);
        rv_offers.setAdapter(offersAdapter);
        rv_offers.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (list_offers.size()>20){
                    LinearLayoutManager layoutManager=LinearLayoutManager.class.cast(recyclerView.getLayoutManager());
                    int totalItemCount = layoutManager.getItemCount();
                    int lastVisible = layoutManager.findLastVisibleItemPosition();

                    boolean endHasBeenReached = lastVisible + 1 >= totalItemCount;
                    if (totalItemCount > 0 && endHasBeenReached) {
                        if (paginateOffers){
                            currentOffersPage++;
                            if (search_type.equals("quick")){
                                searchWithWord(1,currentOffersPage,1);
                            }else if (search_type.equals("advanced")){
                                callSearchAPI(1,currentOffersPage,1);
                            }
                            paginateOffers = false;
                        }

                    }
                }
            }
        });

        //Advertisements
        list_ads = new ArrayList<>();
        advertisementsLayoutManager = new LinearLayoutManager(SearchResultsActivity.this,LinearLayoutManager.HORIZONTAL,false);
        advertisementsAdapter = new SearchAdvertisementsAdapter(SearchResultsActivity.this, list_ads,new IMove() {
            @Override
            public void move(int position) {

            }
        });
        rv_advertisements.setLayoutManager(advertisementsLayoutManager);
        rv_advertisements.setAdapter(advertisementsAdapter);
        rv_advertisements.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (list_ads.size()>20){
                    LinearLayoutManager layoutManager=LinearLayoutManager.class.cast(recyclerView.getLayoutManager());
                    int totalItemCount = layoutManager.getItemCount();
                    int lastVisible = layoutManager.findLastVisibleItemPosition();

                    boolean endHasBeenReached = lastVisible + 1 >= totalItemCount;
                    if (totalItemCount > 0 && endHasBeenReached) {
                        if (paginateAds){
                            currentAdsPage++;
                            if (search_type.equals("quick")){
                                searchWithWord(1,currentAdsPage,2);
                            }else if (search_type.equals("advanced")){
                                callSearchAPI(1,currentAdsPage,2);
                            }
                            paginateAds = false;
                        }

                    }
                }
            }
        });

        //Institutions
        list_insts = new ArrayList<>();
        institutionsLayoutManager = new LinearLayoutManager(SearchResultsActivity.this,LinearLayoutManager.HORIZONTAL,false);
        institutionsAdapter = new SearchInstitutionsAdapter(SearchResultsActivity.this,list_insts, new IMove() {
            @Override
            public void move(int position) {

            }
        });
        rv_institutions.setLayoutManager(institutionsLayoutManager);
        rv_institutions.setAdapter(institutionsAdapter);
        rv_institutions.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (list_insts.size()>20){
                    LinearLayoutManager layoutManager=LinearLayoutManager.class.cast(recyclerView.getLayoutManager());
                    int totalItemCount = layoutManager.getItemCount();
                    int lastVisible = layoutManager.findLastVisibleItemPosition();

                    boolean endHasBeenReached = lastVisible + 1 >= totalItemCount;
                    if (totalItemCount > 0 && endHasBeenReached) {
                        if (paginateInst){
                            currentInstPage++;
                            if (search_type.equals("quick")){
                                searchWithWord(1,currentInstPage,0);
                            }else if (search_type.equals("advanced")){
                                callSearchAPI(1,currentInstPage,0);
                            }
                            paginateInst = false;
                        }
                    }
                }
            }
        });
        if (search_type.equals("quick")){
            searchWithWord(0,1,0);
        }else if (search_type.equals("advanced")){
            callSearchAPI(0,1,0);
        }

    }

    @Override
    public void init_views() {
        toolbar = findViewById(R.id.search_results_toolbar);
        btn_back = toolbar.findViewById(R.id.toolbar_back_btn);
        tv_title = toolbar.findViewById(R.id.toolbar_title);
        rv_offers = findViewById(R.id.search_results_offers_recycler);
        rv_advertisements = findViewById(R.id.search_results_advertisements_recycler);
        rv_institutions = findViewById(R.id.search_results_institutions_recycler);
        btn_all_offers = findViewById(R.id.search_results_show_all_offers);
        btn_all_advertisements = findViewById(R.id.search_results_show_all_advertisements);
        btn_all_insts = findViewById(R.id.search_results_inst_btn);
        root = findViewById(R.id.search_results_layout);
        lyt_offers = findViewById(R.id.search_results_offers_layout);
        lyt_ads = findViewById(R.id.search_results_ads_layout);
        lyt_inst = findViewById(R.id.search_results_offers_layout);
        myIntent = getIntent();
        search_type = myIntent.getStringExtra("type");
        if (search_type.equals("quick")){
            word = myIntent.getStringExtra("word");
        }
        scrollView = findViewById(R.id.search_results_scrollview);
        scrollView.setVisibility(View.GONE);
        empty_layout = findViewById(R.id.empty_layout);
    }

    @Override
    public void init_events() {
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btn_all_offers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SearchResultsActivity.this,AllOffersActivity.class);
                intent.putExtra("filter","-1");
                intent.putExtra("with",false);
                startActivity(intent);
            }
        });
        btn_all_advertisements.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SearchResultsActivity.this,AllAdvertisementsActivity.class);
                intent.putExtra("filter","-1");
                intent.putExtra("with",false);
                startActivity(intent);
            }
        });
        btn_all_insts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SearchResultsActivity.this,AllInstitutionsActivity.class));
            }
        });
    }

    @Override
    public void set_fragment_place() {

    }

    private void searchWithWord(final int type , final int page, final int section){
        SearchAPIsClass.searchByWord(sharedPreferences,
                SearchResultsActivity.this,
                sharedPreferences.getString("language", "ar"),
                sharedPreferences.getString("service_token", ""),
                word,
                type,
                page,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json !=null){
                            String json1 = new Gson().toJson(json);
                            SearchResponse success = new Gson().fromJson(json1,SearchResponse.class);
                            if (type == 0){
                                //Facilities
                                if (success.getFacilities()!=null){
                                    if (success.getFacilities()!=null){
                                        if (success.getFacilities().size()>0){
                                            empty_layout.setVisibility(View.GONE);
                                            for (InstitutionObject io : success.getFacilities()){
                                                list_insts.add(io);
                                                institutionsAdapter.notifyDataSetChanged();
                                            }
                                        }else {
                                            lyt_inst.setVisibility(View.GONE);
                                            rv_institutions.setVisibility(View.GONE);
                                        }
                                    }else {
                                        lyt_inst.setVisibility(View.GONE);
                                        rv_institutions.setVisibility(View.GONE);
                                    }
                                }else {
                                    lyt_inst.setVisibility(View.GONE);
                                    rv_institutions.setVisibility(View.GONE);
                                }

                                //Offers
                                if (success.getOffers()!=null){
                                    if (success.getOffers().getData()!=null){
                                        if (success.getOffers().getData().size()>0){
                                            empty_layout.setVisibility(View.GONE);
                                            for (OfferObject io : success.getOffers().getData()){
                                                list_offers.add(io);
                                                offersAdapter.notifyDataSetChanged();
                                            }
                                        }else {
                                            lyt_offers.setVisibility(View.GONE);
                                            rv_offers.setVisibility(View.GONE);
                                        }
                                    }else {
                                        lyt_offers.setVisibility(View.GONE);
                                        rv_offers.setVisibility(View.GONE);
                                    }
                                }else {
                                    lyt_offers.setVisibility(View.GONE);
                                    rv_offers.setVisibility(View.GONE);
                                }
                                //Ads
                                if (success.getPosters()!=null){
                                    if (success.getPosters().getData()!=null){
                                        if (success.getPosters().getData().size()>0){
                                            empty_layout.setVisibility(View.GONE);
                                            for (AdvertisementObject io : success.getPosters().getData()){
                                                list_ads.add(io);
                                                advertisementsAdapter.notifyDataSetChanged();
                                            }
                                        }else {
                                            lyt_ads.setVisibility(View.GONE);
                                            rv_advertisements.setVisibility(View.GONE);
                                        }
                                    }else {
                                        lyt_ads.setVisibility(View.GONE);
                                        rv_advertisements.setVisibility(View.GONE);
                                    }
                                }else {
                                    lyt_ads.setVisibility(View.GONE);
                                    rv_advertisements.setVisibility(View.GONE);
                                }
                                scrollView.setVisibility(View.VISIBLE);
                            }else {
                                //Facilities
                                if (section == 0){
                                    if (success.getFacilities()!=null){
                                        if (success.getFacilities()!=null){
                                            if (success.getFacilities().size()>0){
                                                for (InstitutionObject io : success.getFacilities()){
                                                    list_insts.add(io);
                                                    institutionsAdapter.notifyDataSetChanged();
                                                }
                                                paginateInst = true;
                                            }else {
                                                Snackbar.make(root,getResources().getString(R.string.search_results_no_more_inst),Snackbar.LENGTH_SHORT).show();
                                                paginateInst = false;
                                            }
                                        }else {
                                            Snackbar.make(root,getResources().getString(R.string.search_results_no_more_inst),Snackbar.LENGTH_SHORT).show();
                                            paginateInst = false;
                                        }
                                    }else {
                                        Snackbar.make(root,getResources().getString(R.string.search_results_no_more_inst),Snackbar.LENGTH_SHORT).show();
                                        paginateInst = false;
                                    }
                                }
                                //Offers
                                else if (section == 1){
                                    if (success.getOffers()!=null){
                                        if (success.getOffers().getData()!=null){
                                            if (success.getOffers().getData().size()>0){
                                                for (OfferObject io : success.getOffers().getData()){
                                                    list_offers.add(io);
                                                    offersAdapter.notifyDataSetChanged();
                                                }
                                                paginateOffers = true;
                                            }else {
                                                Snackbar.make(root,getResources().getString(R.string.search_results_no_more_offers),Snackbar.LENGTH_SHORT).show();
                                                paginateOffers = false;
                                            }
                                        }else {
                                            Snackbar.make(root,getResources().getString(R.string.search_results_no_more_offers),Snackbar.LENGTH_SHORT).show();
                                            paginateOffers = false;
                                        }
                                    }else {
                                        Snackbar.make(root,getResources().getString(R.string.search_results_no_more_offers),Snackbar.LENGTH_SHORT).show();
                                        paginateOffers = false;
                                    }
                                }
                                //Advertisements
                                else if (section == 2){
                                    if (success.getPosters()!=null){
                                        if (success.getPosters().getData()!=null){
                                            if (success.getPosters().getData().size()>0){
                                                for (AdvertisementObject io : success.getPosters().getData()){
                                                    list_ads.add(io);
                                                    advertisementsAdapter.notifyDataSetChanged();
                                                }
                                                paginateAds = true;
                                            }else {
                                                Snackbar.make(root,getResources().getString(R.string.search_results_no_more_ads),Snackbar.LENGTH_SHORT).show();
                                                paginateAds = false;
                                            }
                                        }else {
                                            Snackbar.make(root,getResources().getString(R.string.search_results_no_more_ads),Snackbar.LENGTH_SHORT).show();
                                            paginateAds = false;
                                        }
                                    }else {
                                        Snackbar.make(root,getResources().getString(R.string.search_results_no_more_ads),Snackbar.LENGTH_SHORT).show();
                                        paginateAds = false;
                                    }
                                }
                            }
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        searchWithWord(type,page,section);
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });

    }

    private void callSearchAPI(final int type , final int page, final int section){
        Log.i("my_lat", "getFacilities: "+sharedPreferences.getString("my_lat","0"));
        SearchAPIsClass.search(sharedPreferences,
                SearchResultsActivity.this,
                sharedPreferences.getString("language", "ar"),
                sharedPreferences.getString("service_token", ""),
                sharedPreferences.getString("my_lat","0"),
                sharedPreferences.getString("my_lng","0"),
                myIntent.getStringExtra("city_id"),
                "all",
                myIntent.getStringExtra("country_id"),
                myIntent.getStringExtra("category_id"),
                myIntent.getStringExtra("rating"),
                String.valueOf(myIntent.getIntExtra("radius",-1)),
                "",
                type,
                page,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json !=null){
                            String json1 = new Gson().toJson(json);
                            SearchResponse success = new Gson().fromJson(json1,SearchResponse.class);
                            if (type == 0){
                                //Facilities
                                if (success.getFacilities()!=null){
                                    if (success.getFacilities()!=null){
                                        if (success.getFacilities().size()>0){
                                            empty_layout.setVisibility(View.GONE);
                                            for (InstitutionObject io : success.getFacilities()){
                                                list_insts.add(io);
                                                institutionsAdapter.notifyDataSetChanged();
                                            }
                                            if (type == 0){
                                                runAnimationInstitutions(rv_institutions,0,institutionsAdapter);
                                            }
                                        }else {
                                            lyt_inst.setVisibility(View.GONE);
                                            rv_institutions.setVisibility(View.GONE);
                                        }
                                    }else {
                                        lyt_inst.setVisibility(View.GONE);
                                        rv_institutions.setVisibility(View.GONE);
                                    }
                                }else {
                                    lyt_inst.setVisibility(View.GONE);
                                    rv_institutions.setVisibility(View.GONE);
                                }

                                //Offers
                                if (success.getOffers()!=null){
                                    if (success.getOffers().getData()!=null){
                                        if (success.getOffers().getData().size()>0){
                                            empty_layout.setVisibility(View.GONE);
                                            for (OfferObject io : success.getOffers().getData()){
                                                list_offers.add(io);
                                                offersAdapter.notifyDataSetChanged();
                                            }
                                            if (type == 0){
                                                runAnimationOffers(rv_offers,0,offersAdapter);
                                            }
                                        }else {
                                            lyt_offers.setVisibility(View.GONE);
                                            rv_offers.setVisibility(View.GONE);
                                        }
                                    }else {
                                        lyt_offers.setVisibility(View.GONE);
                                        rv_offers.setVisibility(View.GONE);
                                    }
                                }else {
                                    lyt_offers.setVisibility(View.GONE);
                                    rv_offers.setVisibility(View.GONE);
                                }
                                //Ads
                                if (success.getPosters()!=null){
                                    if (success.getPosters().getData()!=null){
                                        if (success.getPosters().getData().size()>0){
                                            empty_layout.setVisibility(View.GONE);
                                            for (AdvertisementObject io : success.getPosters().getData()){
                                                list_ads.add(io);
                                                advertisementsAdapter.notifyDataSetChanged();
                                            }
                                            if (type == 0){
                                                runAnimationAdvertisements(rv_advertisements,0,advertisementsAdapter);
                                            }
                                        }else {
                                            lyt_ads.setVisibility(View.GONE);
                                            rv_advertisements.setVisibility(View.GONE);
                                        }
                                    }else {
                                        lyt_ads.setVisibility(View.GONE);
                                        rv_advertisements.setVisibility(View.GONE);
                                    }
                                }else {
                                    lyt_ads.setVisibility(View.GONE);
                                    rv_advertisements.setVisibility(View.GONE);
                                }
                                scrollView.setVisibility(View.VISIBLE);
                            }else {
                                //Facilities
                                if (section == 0){
                                    if (success.getFacilities()!=null){
                                        if (success.getFacilities()!=null){
                                            if (success.getFacilities().size()>0){
                                                for (InstitutionObject io : success.getFacilities()){
                                                    list_insts.add(io);
                                                    institutionsAdapter.notifyDataSetChanged();
                                                }
                                                paginateInst = true;
                                            }else {
                                                Snackbar.make(root,getResources().getString(R.string.search_results_no_more_inst),Snackbar.LENGTH_SHORT).show();
                                                paginateInst = false;
                                            }
                                        }else {
                                            Snackbar.make(root,getResources().getString(R.string.search_results_no_more_inst),Snackbar.LENGTH_SHORT).show();
                                            paginateInst = false;
                                        }
                                    }else {
                                        Snackbar.make(root,getResources().getString(R.string.search_results_no_more_inst),Snackbar.LENGTH_SHORT).show();
                                        paginateInst = false;
                                    }
                                }
                                //Offers
                                else if (section == 1){
                                    if (success.getOffers()!=null){
                                        if (success.getOffers().getData()!=null){
                                            if (success.getOffers().getData().size()>0){
                                                for (OfferObject io : success.getOffers().getData()){
                                                    list_offers.add(io);
                                                    offersAdapter.notifyDataSetChanged();
                                                }
                                                paginateOffers = true;
                                            }else {
                                                Snackbar.make(root,getResources().getString(R.string.search_results_no_more_offers),Snackbar.LENGTH_SHORT).show();
                                                paginateOffers = false;
                                            }
                                        }else {
                                            Snackbar.make(root,getResources().getString(R.string.search_results_no_more_offers),Snackbar.LENGTH_SHORT).show();
                                            paginateOffers = false;
                                        }
                                    }else {
                                        Snackbar.make(root,getResources().getString(R.string.search_results_no_more_offers),Snackbar.LENGTH_SHORT).show();
                                        paginateOffers = false;
                                    }
                                }
                                //Advertisements
                                else if (section == 2){
                                    if (success.getPosters()!=null){
                                        if (success.getPosters().getData()!=null){
                                            if (success.getPosters().getData().size()>0){
                                                for (AdvertisementObject io : success.getPosters().getData()){
                                                    list_ads.add(io);
                                                    advertisementsAdapter.notifyDataSetChanged();
                                                }
                                                paginateAds = true;
                                            }else {
                                                Snackbar.make(root,getResources().getString(R.string.search_results_no_more_ads),Snackbar.LENGTH_SHORT).show();
                                                paginateAds = false;
                                            }
                                        }else {
                                            Snackbar.make(root,getResources().getString(R.string.search_results_no_more_ads),Snackbar.LENGTH_SHORT).show();
                                            paginateAds = false;
                                        }
                                    }else {
                                        Snackbar.make(root,getResources().getString(R.string.search_results_no_more_ads),Snackbar.LENGTH_SHORT).show();
                                        paginateAds = false;
                                    }
                                }
                            }
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callSearchAPI(type,page,section);
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });

    }

    public void runAnimationOffers(RecyclerView recyclerView,int type,SearchOffersAdapter adapter){
        Context context = recyclerView.getContext();
        LayoutAnimationController controller = null;
        if (type == 0){ // Fall down animation
            controller = AnimationUtils.loadLayoutAnimation(context,R.anim.layout_fall_down);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutAnimation(controller);
            recyclerView.getAdapter().notifyDataSetChanged();
            recyclerView.scheduleLayoutAnimation();
        }
    }
    public void runAnimationAdvertisements(RecyclerView recyclerView,int type,SearchAdvertisementsAdapter adapter){
        Context context = recyclerView.getContext();
        LayoutAnimationController controller = null;
        if (type == 0){ // Fall down animation
            controller = AnimationUtils.loadLayoutAnimation(context,R.anim.layout_fall_down);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutAnimation(controller);
            recyclerView.getAdapter().notifyDataSetChanged();
            recyclerView.scheduleLayoutAnimation();
        }
    }
    public void runAnimationInstitutions(RecyclerView recyclerView,int type,SearchInstitutionsAdapter adapter){
        Context context = recyclerView.getContext();
        LayoutAnimationController controller = null;
        if (type == 0){ // Fall down animation
            controller = AnimationUtils.loadLayoutAnimation(context,R.anim.layout_fall_down);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutAnimation(controller);
            recyclerView.getAdapter().notifyDataSetChanged();
            recyclerView.scheduleLayoutAnimation();
        }
    }
}
