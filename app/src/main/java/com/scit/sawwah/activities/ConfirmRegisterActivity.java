package com.scit.sawwah.activities;

import android.content.Intent;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.scit.sawwah.APIClasses.BasicAPIsClass;
import com.scit.sawwah.R;
import com.scit.sawwah.interfaces.IFailure;
import com.scit.sawwah.interfaces.IResponse;
import com.scit.sawwah.tools.BaseActivity;

public class ConfirmRegisterActivity extends BaseActivity {
    private RelativeLayout toolbar;
    private ImageButton btn_back;
    private LinearLayout btn_confirm;
    private EditText edt_code;
    private LinearLayout root,btn_resend_code;

    @Override
    public void set_layout() {
        setContentView(R.layout.activity_confirm_register);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {

    }

    @Override
    public void init_views() {
        toolbar = findViewById(R.id.confirm_register_toolbar);
        btn_back = toolbar.findViewById(R.id.toolbar_back_btn);
        btn_confirm = findViewById(R.id.confirm_register_confirm_btn);
        edt_code = findViewById(R.id.confirm_register_ver_code_edt);
        root = findViewById(R.id.confirm_register_layout);
        btn_resend_code = findViewById(R.id.confirm_register_resend_btn);
    }

    @Override
    public void init_events() {
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btn_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt_code.getText().toString().equals("")){
                    Toast.makeText(ConfirmRegisterActivity.this, getResources().getString(R.string.confirm_register_no_code), Toast.LENGTH_SHORT).show();
                    return;
                }
                callAPI();
            }
        });
        btn_resend_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callResendCodeAPI();
            }
        });
    }

    @Override
    public void set_fragment_place() {

    }

    private void callAPI(){
        BasicAPIsClass.activeAccount(sharedPreferences,
                ConfirmRegisterActivity.this,
                sharedPreferences.getString("language", "ar"),
                sharedPreferences.getString("service_token", ""),
                edt_code.getText().toString(),
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json != null){
                            Toast.makeText(ConfirmRegisterActivity.this, getResources().getString(R.string.confirm_register_success), Toast.LENGTH_SHORT).show();
                            if (sharedPreferences.getString("account_type","customer").equals("customer")){
                                startActivity(new Intent(ConfirmRegisterActivity.this,ThanksActivity.class));
                                finish();
                            }else {
                                startActivity(new Intent(ConfirmRegisterActivity.this,RegisterInstitutionActivity.class));
                                finish();
                            }
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callAPI();
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });
    }

    private void callResendCodeAPI(){
        BasicAPIsClass.resendCode(sharedPreferences,
                ConfirmRegisterActivity.this,
                sharedPreferences.getString("language", "ar"),
                sharedPreferences.getString("service_token", ""),
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json != null){
                            Snackbar.make(root,getResources().getString(R.string.confirm_register_message),Snackbar.LENGTH_SHORT).show();
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callResendCodeAPI();
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });
    }
}
