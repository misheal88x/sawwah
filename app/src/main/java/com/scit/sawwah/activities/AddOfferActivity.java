package com.scit.sawwah.activities;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import androidx.annotation.NonNull;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.DialogFragment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.scit.sawwah.APIClasses.AccountsAPIsClass;
import com.scit.sawwah.APIClasses.OffersAPIsClass;
import com.scit.sawwah.Models.InstitutionObject;
import com.scit.sawwah.Models.PricePackageObject;
import com.scit.sawwah.Models.ProfileResponse;
import com.scit.sawwah.R;
import com.scit.sawwah.adapters.CustomSpinnerAdapter;
import com.scit.sawwah.fragments.DatePickerFragment;
import com.scit.sawwah.interfaces.IFailure;
import com.scit.sawwah.interfaces.IResponse;
import com.scit.sawwah.tools.BaseActivity;
import com.scit.sawwah.tools.BaseFunctions;
import com.soundcloud.android.crop.Crop;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AddOfferActivity extends BaseActivity implements DatePickerDialog.OnDateSetListener{
    private RelativeLayout toolbar,rl_add_btn;
    private ImageButton btn_back;
    private TextView tv_title;
    private ImageView img_image;
    private EditText edt_title,edt_details;
    private Spinner package_spinner,institution_spinner;
    private List<InstitutionObject> list_insts;
    private List<PricePackageObject> list_packages;
    private CustomSpinnerAdapter package_adapter,institution_adapter;
    private List<String> package_list_string,institution_list_string;
    private List<PricePackageObject> package_list;
    private List<InstitutionObject> institution_list;
    private TextView tv_start_date,tv_end_date;
    private int clicked_date = 0;
    private String chosen_start_date = "",chosen_end_date = "";
    private boolean image_selected = false;
    private int selected_inst_id = 0,selected_package_id = 0;
    private static final String TEMP_PHOTO_FILE = "temporary_holder.jpg";
    private String imagePath = "";
    private Uri imageUri = null;
    private LinearLayout root;
    private int tries = 0;
    private int num_offers = 0;


    @Override
    public void set_layout() {
        setContentView(R.layout.activity_add_offer);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        init_spinners();
        tv_title.setText(getResources().getString(R.string.add_offer_title));
    }

    @Override
    public void init_views() {
        toolbar = findViewById(R.id.add_offer_toolbar);
        rl_add_btn = findViewById(R.id.add_offer_add_btn);
        btn_back = toolbar.findViewById(R.id.toolbar_back_btn);
        tv_title = toolbar.findViewById(R.id.toolbar_title);
        img_image = findViewById(R.id.add_offer_image);
        edt_title = findViewById(R.id.add_offer_title);
        edt_details = findViewById(R.id.add_offer_details);
        package_spinner = findViewById(R.id.add_offer_package_spinner);
        institution_spinner = findViewById(R.id.add_offer_institution_spinner);
        tv_start_date = findViewById(R.id.add_offer_start_date_txt);
        tv_end_date = findViewById(R.id.add_offer_end_date_txt);
        root = findViewById(R.id.add_offer_layout);
    }

    @Override
    public void init_events() {
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        package_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int pos = BaseFunctions.getSpinnerPosition(parent,position);
                if (list_packages.size()>0){
                    PricePackageObject ppo = list_packages.get(pos);
                    selected_package_id = ppo.getId();
                    if (ppo.getType() == 1){
                        num_offers = ppo.getOp_offer_num();
                    }else if (ppo.getType() == 2){
                        num_offers = ppo.getAop_offer_num();
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        institution_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int pos = BaseFunctions.getSpinnerPosition(parent,position);
                if (institution_list.size()>0){
                    InstitutionObject io = institution_list.get(pos);
                    selected_inst_id = io.getId();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        tv_start_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clicked_date = 1;
                DialogFragment datePicker = new DatePickerFragment();
                datePicker.show(getSupportFragmentManager(),"date picker");
            }
        });
        tv_end_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clicked_date = 2;
                DialogFragment datePicker = new DatePickerFragment();
                datePicker.show(getSupportFragmentManager(),"date picker");
            }
        });
        img_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityCompat.requestPermissions(AddOfferActivity.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        1);
            }
        });
        rl_add_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt_title.getText().toString().equals("")){
                    Toast.makeText(AddOfferActivity.this, getResources().getString(R.string.add_offer_no_title), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (edt_details.getText().toString().equals("")){
                    Toast.makeText(AddOfferActivity.this, getResources().getString(R.string.add_offer_no_details), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!image_selected){
                    Toast.makeText(AddOfferActivity.this, getResources().getString(R.string.add_offer_no_image), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (selected_package_id == 0){
                    Toast.makeText(AddOfferActivity.this, getResources().getString(R.string.add_offer_no_package), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (chosen_start_date.equals("")){
                    Toast.makeText(AddOfferActivity.this, getResources().getString(R.string.add_offer_no_start_date), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (chosen_end_date.equals("")){
                    Toast.makeText(AddOfferActivity.this, getResources().getString(R.string.add_offer_no_end_date), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (selected_inst_id == 0){
                    Toast.makeText(AddOfferActivity.this, getResources().getString(R.string.add_offer_no_inst), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (num_offers == 0){
                    Toast.makeText(AddOfferActivity.this, getResources().getString(R.string.add_offer_not_enough_offers), Toast.LENGTH_SHORT).show();
                    return;
                }
                callAPI();
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case 1:
            {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    Crop.pickImage(AddOfferActivity.this);
                }else {
                    Toast.makeText(this, getResources().getString(R.string.add_photos_permission_denied), Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }

    private Uri getTempUri() {
        return Uri.fromFile(getTempFile());
    }


    private File getTempFile() {
        if (isSDCARDMounted()) {

            File f = new File(Environment.getExternalStorageDirectory(),TEMP_PHOTO_FILE);
            try {
                f.createNewFile();
            } catch (IOException e) {

            }
            return f;
        } else {
            return null;
        }
    }

    private boolean isSDCARDMounted(){
        String status = Environment.getExternalStorageState();
        if (status.equals(Environment.MEDIA_MOUNTED))
            return true;
        return false;
    }

    protected void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK){
            if (requestCode == Crop.REQUEST_PICK){
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Uri source_uri = data.getData();
                        Uri destination_uri = Uri.fromFile(getTempFile());
                        Crop.of(source_uri,destination_uri).asSquare().start(AddOfferActivity.this);
                        //img_personal_image.setImageURI(Crop.getOutput(data));
                        String filePath= Environment.getExternalStorageDirectory()
                                + "/temporary_holder.jpg";
                        imagePath = filePath;
                        image_selected = true;
                    }
                });
            }else if (requestCode == Crop.REQUEST_CROP){
                handle_crop(resultCode,data);
            }
        }
    }

    private void handle_crop(int code, final Intent result){
        if (code == RESULT_OK){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    img_image.setImageDrawable(null);
                    img_image.setImageURI(Crop.getOutput(result));
                    String filePath= Environment.getExternalStorageDirectory()
                            + "/temporary_holder.jpg";
                    imagePath = filePath;
                    image_selected = true;
                }
            });

        }else if (code == Crop.RESULT_ERROR){
            Toast.makeText(this, "An error occurred", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
        String year_str = new String();
        String month_str = new String();
        String day_str = new String();
        year_str = String.valueOf(year);
        if (month+1<10){
            month_str = "0"+String.valueOf(month+1);
        }else {
            month_str = String.valueOf(month+1);
        }
        if (day<10){
            day_str = "0"+String.valueOf(day);
        }else {
            day_str = String.valueOf(day);
        }
        String desDate = year_str+"-"+month_str+"-"+day_str;
        if (clicked_date == 1){
            chosen_start_date = desDate;
            tv_start_date.setText(desDate);
        }else {
            chosen_end_date = desDate;
            tv_end_date.setText(desDate);
        }
    }

    @Override
    public void set_fragment_place() {

    }


    private void init_spinners() {

        package_list = new ArrayList<>();
        institution_list = new ArrayList<>();
        package_list_string = new ArrayList<>();
        institution_list_string = new ArrayList<>();

        /*
        PricePackageObject[] package_array = new Gson().fromJson(sharedPreferences.getString("my_packages","[]"),PricePackageObject[].class);
        if (package_array.length > 0){
            for (PricePackageObject ppo : package_array){
                package_list.add(ppo);
                package_list_string.add(ppo.getName());
            }
        }
        **/
        InstitutionObject[] insts_array = new Gson().fromJson(sharedPreferences.getString("my_insts","[]"),InstitutionObject[].class);
        if (insts_array.length > 0){
            for (InstitutionObject io : insts_array){
                institution_list.add(io);
                institution_list_string.add(io.getName());
            }
        }




        package_adapter = new CustomSpinnerAdapter(getApplicationContext(), R.layout.new_spinner_item, package_list_string);
        institution_adapter = new CustomSpinnerAdapter(getApplicationContext(), R.layout.new_spinner_item, institution_list_string);



        package_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        institution_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


        package_spinner.setAdapter(package_adapter);
        institution_spinner.setAdapter(institution_adapter);

        callProfileAPI();
    }

    private void callAPI(){
        OffersAPIsClass.addOffer(sharedPreferences,
                AddOfferActivity.this,
                sharedPreferences.getString("language", "ar"),
                sharedPreferences.getString("service_token", ""),
                edt_title.getText().toString(),
                edt_details.getText().toString(),
                String.valueOf(selected_package_id),
                String.valueOf(selected_inst_id),
                chosen_start_date,
                chosen_end_date,
                imagePath,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json != null){
                            Toast.makeText(AddOfferActivity.this, getResources().getString(R.string.add_offer_success), Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callAPI();
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });
    }

    private void callProfileAPI(){
        AccountsAPIsClass.profile(sharedPreferences,
                AddOfferActivity.this,
                sharedPreferences.getString("language", "ar"),
                sharedPreferences.getString("service_token", ""),
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json != null){
                            String json1 = new Gson().toJson(json);
                            ProfileResponse success = new Gson().fromJson(json1,ProfileResponse.class);
                            //Active Packages
                            if (success.getActive_packages()!=null){
                                if (success.getActive_packages().size()>0){
                                    for (PricePackageObject ppo : success.getActive_packages()){
                                        if (ppo.getType()==1||ppo.getType()==2){
                                            package_list.add(ppo);
                                            package_list_string.add(ppo.getName());
                                            package_adapter.notifyDataSetChanged();
                                        }
                                    }
                                    if (package_list.size()==0){
                                        Toast.makeText(AddOfferActivity.this, getResources().getString(R.string.prices_packages_no_packages), Toast.LENGTH_SHORT).show();
                                    }
                                }else {
                                    Toast.makeText(AddOfferActivity.this, getResources().getString(R.string.prices_packages_no_packages), Toast.LENGTH_SHORT).show();
                                }
                            }else {
                                Toast.makeText(AddOfferActivity.this, getResources().getString(R.string.prices_packages_no_packages), Toast.LENGTH_SHORT).show();
                            }
                        }

                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        if (tries<5){
                            tries++;
                            callProfileAPI();
                        }
                        /*
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callProfileAPI();
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                                **/
                    }
                });
    }
}
