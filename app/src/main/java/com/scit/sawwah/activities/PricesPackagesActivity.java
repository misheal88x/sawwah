package com.scit.sawwah.activities;

import com.google.android.material.snackbar.Snackbar;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.scit.sawwah.APIClasses.PackagesAPIsClass;
import com.scit.sawwah.Models.PricePackageObject;
import com.scit.sawwah.R;
import com.scit.sawwah.adapters.CustomSpinnerAdapter;
import com.scit.sawwah.adapters.PricesPackagesAdapter;
import com.scit.sawwah.interfaces.IFailure;
import com.scit.sawwah.interfaces.IResponse;
import com.scit.sawwah.tools.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import me.relex.circleindicator.CircleIndicator;

public class PricesPackagesActivity extends BaseActivity {
    private RelativeLayout toolbar;
    private TextView tv_title;
    private ImageButton btn_back;
    private Spinner categories_spinner;
    private List<String> categories_list;
    private CustomSpinnerAdapter categories_adapter;
    private ViewPager viewPager;
    private CircleIndicator indicator;
    private List<PricePackageObject> list_packages;
    private PricesPackagesAdapter packagesAdapter;
    private ScrollView scrollView;
    private RelativeLayout root;
    private String filter = "-1";
    private LinearLayout empty_layout;


    @Override
    public void set_layout() {
        setContentView(R.layout.activity_prices_packages);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        tv_title.setText(getResources().getString(R.string.prices_packages_title));
        init_spinner();
        init_view_pager();
        scrollView.setVisibility(View.GONE);
    }

    @Override
    public void init_views() {
        toolbar = findViewById(R.id.prices_toolbar);
        tv_title = toolbar.findViewById(R.id.toolbar_title);
        btn_back = toolbar.findViewById(R.id.toolbar_back_btn);
        categories_spinner = findViewById(R.id.prices_packages_category_spinner);
        viewPager = findViewById(R.id.prices_packages_view_pager);
        indicator = findViewById(R.id.prices_packages_indicator);
        scrollView = findViewById(R.id.prices_packages_scrollview);
        root = findViewById(R.id.prices_packages_layout);
        empty_layout = findViewById(R.id.empty_layout);
    }

    @Override
    public void init_events() {
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        categories_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int i, long id) {
                long pos = parent.getItemIdAtPosition(i);
                int position = Integer.valueOf(String.valueOf(pos));
                switch (position){
                    //All
                    case 0 : {
                        list_packages.clear();
                        packagesAdapter.notifyDataSetChanged();
                        packagesAdapter = new PricesPackagesAdapter(PricesPackagesActivity.this,list_packages);
                        callAPI("-1");
                    }break;
                    //Offer
                    case 1 : {
                        list_packages.clear();
                        packagesAdapter.notifyDataSetChanged();
                        packagesAdapter = new PricesPackagesAdapter(PricesPackagesActivity.this,list_packages);
                        callAPI("1");
                    }break;
                    //Ads
                    case 2 : {
                        list_packages.clear();
                        packagesAdapter.notifyDataSetChanged();
                        packagesAdapter = new PricesPackagesAdapter(PricesPackagesActivity.this,list_packages);
                        callAPI("0");
                    }break;
                    //Offers + Ads
                    case 3 : {
                        list_packages.clear();
                        packagesAdapter.notifyDataSetChanged();
                        packagesAdapter = new PricesPackagesAdapter(PricesPackagesActivity.this,list_packages);
                        callAPI("2");
                    }break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void set_fragment_place() {

    }

    private void init_spinner(){
        categories_list = new ArrayList<>();
        categories_list.add(getResources().getString(R.string.prices_packages_all));
        categories_list.add(getResources().getString(R.string.prices_packages_offers));
        categories_list.add(getResources().getString(R.string.prices_packages_ads));
        categories_list.add(getResources().getString(R.string.prices_packages_offers_and_ads));
        categories_adapter = new CustomSpinnerAdapter(PricesPackagesActivity.this,R.layout.spinner_white_arrow_item,categories_list);
        categories_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        categories_spinner.setAdapter(categories_adapter);
    }

    private void init_view_pager(){
        list_packages = new ArrayList<>();
        packagesAdapter = new PricesPackagesAdapter(PricesPackagesActivity.this,list_packages);
        viewPager.setAdapter(packagesAdapter);
        indicator.setViewPager(viewPager);
    }

    private void callAPI(final String filter){
        PackagesAPIsClass.get_all_packages(sharedPreferences,
                PricesPackagesActivity.this,
                sharedPreferences.getString("language", "ar"),
                sharedPreferences.getString("service_token", ""),
                filter,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json !=null){
                            String json1 = new Gson().toJson(json);
                            PricePackageObject[] success = new Gson().fromJson(json1,PricePackageObject[].class);
                            if (success.length > 0){
                                empty_layout.setVisibility(View.GONE);
                                for (PricePackageObject ppo : success){
                                    list_packages.add(ppo);
                                    //packagesAdapter.notifyDataSetChanged();
                                }
                                packagesAdapter = new PricesPackagesAdapter(PricesPackagesActivity.this,list_packages);
                                viewPager.setAdapter(packagesAdapter);
                                indicator.setViewPager(viewPager);
                                scrollView.setVisibility(View.VISIBLE);
                            }else {
                                Toast.makeText(PricesPackagesActivity.this, getResources().getString(R.string.prices_packages_no_packages), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callAPI(filter);
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });
    }
}
