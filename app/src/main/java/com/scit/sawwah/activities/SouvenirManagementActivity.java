package com.scit.sawwah.activities;

import android.content.Context;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.scit.sawwah.APIClasses.MemorialAPIsClass;
import com.scit.sawwah.Models.MemorialImageObject;
import com.scit.sawwah.Models.MemorialImagesResponse;
import com.scit.sawwah.R;
import com.scit.sawwah.adapters.AccountAlbumAdapter;
import com.scit.sawwah.interfaces.IFailure;
import com.scit.sawwah.interfaces.IMove;
import com.scit.sawwah.interfaces.IResponse;
import com.scit.sawwah.tools.BaseActivity;
import com.scit.sawwah.tools.EndlessRecyclerViewGridScrollListener;

import java.util.ArrayList;
import java.util.List;

public class SouvenirManagementActivity extends BaseActivity {
    private RelativeLayout toolbar;
    private ImageButton btn_back;
    private TextView tv_title;
    private RecyclerView rv_images;
    private List<MemorialImageObject> list_images;
    private AccountAlbumAdapter adapter;
    private GridLayoutManager layoutManager;
    private RelativeLayout root;
    private LinearLayout empty_layout;
    private int currentPage = 1;

    @Override
    public void set_layout() {
        setContentView(R.layout.activity_souvenir_management);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        tv_title.setText(getResources().getString(R.string.souvenir_management_title));
        init_recycler();
    }

    @Override
    public void init_views() {
        toolbar = findViewById(R.id.souvenir_manage_toolbar);
        btn_back = toolbar.findViewById(R.id.toolbar_back_btn);
        tv_title = toolbar.findViewById(R.id.toolbar_title);
        rv_images = findViewById(R.id.souvenir_manage_recycler);
        root = findViewById(R.id.souvenir_manage_layout);
        empty_layout = findViewById(R.id.empty_layout);
    }

    @Override
    public void init_events() {
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void set_fragment_place() {

    }

    private void init_recycler(){
        list_images = new ArrayList<>();
        adapter = new AccountAlbumAdapter(SouvenirManagementActivity.this, 1,list_images, new IMove() {
            @Override
            public void move(int position) {

            }
        });
        layoutManager = new GridLayoutManager(SouvenirManagementActivity.this,3);
        rv_images.setLayoutManager(layoutManager);
        rv_images.setAdapter(adapter);
        rv_images.addOnScrollListener(new EndlessRecyclerViewGridScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if (list_images.size()>=20){
                    Snackbar.make(root,getResources().getString(R.string.get_more),Snackbar.LENGTH_SHORT).show();
                    currentPage++;
                    callAPI(currentPage,1,String.valueOf(sharedPreferences.getInt("account_id",0)));
                }
            }
        });
        callAPI(currentPage,0,String.valueOf(sharedPreferences.getInt("account_id",0)));
    }

    private void callAPI(final int page, final int type, final String filter){
        MemorialAPIsClass.getImages(sharedPreferences,
                SouvenirManagementActivity.this,
                sharedPreferences.getString("language", "ar"),
                sharedPreferences.getString("service_token", ""),
                filter,
                type,
                page,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json !=null){
                            String json1 = new Gson().toJson(json);
                            MemorialImagesResponse success = new Gson().fromJson(json1,MemorialImagesResponse.class);
                            if (success.getData()!=null){
                                if (success.getData().size() > 0){
                                    empty_layout.setVisibility(View.GONE);
                                    for (MemorialImageObject mio : success.getData()){
                                        list_images.add(mio);
                                        adapter.notifyDataSetChanged();
                                    }
                                    if (type == 0){
                                        runAnimation(rv_images,0,adapter);
                                    }
                                }else {
                                    if (type == 1){
                                        Snackbar.make(root,getResources().getString(R.string.no_more),Snackbar.LENGTH_SHORT).show();
                                    }else {
                                        Toast.makeText(SouvenirManagementActivity.this, getResources().getString(R.string.no_data), Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callAPI(page,type,filter);
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });
    }
    public void runAnimation(RecyclerView recyclerView,int type,AccountAlbumAdapter adapter){
        Context context = recyclerView.getContext();
        LayoutAnimationController controller = null;
        if (type == 0){ // Fall down animation
            controller = AnimationUtils.loadLayoutAnimation(context,R.anim.layout_fall_down_vertical);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutAnimation(controller);
            recyclerView.getAdapter().notifyDataSetChanged();
            recyclerView.scheduleLayoutAnimation();
        }
    }
}
