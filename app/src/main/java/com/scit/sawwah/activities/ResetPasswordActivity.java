package com.scit.sawwah.activities;

import android.content.Intent;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.scit.sawwah.APIClasses.AccountsAPIsClass;
import com.scit.sawwah.R;
import com.scit.sawwah.interfaces.IFailure;
import com.scit.sawwah.interfaces.IResponse;
import com.scit.sawwah.tools.BaseActivity;

public class ResetPasswordActivity extends BaseActivity {
    private RelativeLayout toolbar;
    private ImageButton btn_back;
    private LinearLayout btn_confirm;
    private EditText edt_code,edt_new,edt_confirm;
    private ImageView img_new_eye,img_confirm_eye;
    private LinearLayout root;
    private boolean code_requested = false;
    private int shots_count = 2;
    private Intent myIntent;
    private int password_eye_state = 0,confirm_eye_state = 0;

    @Override
    public void set_layout() {
        setContentView(R.layout.activity_reset_password);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {

    }

    @Override
    public void init_views() {
        toolbar = findViewById(R.id.reset_password_toolbar);
        btn_back = toolbar.findViewById(R.id.toolbar_back_btn);
        btn_confirm = findViewById(R.id.reset_password_confirm_btn);
        edt_code = findViewById(R.id.reset_password_code_edt);
        edt_new = findViewById(R.id.reset_password_new_password_edt);
        edt_confirm = findViewById(R.id.reset_password_confirm_new_password_edt);
        img_new_eye = findViewById(R.id.reset_password_new_password_eye);
        img_confirm_eye = findViewById(R.id.reset_password_confirm_eye);
        root = findViewById(R.id.reset_password_layout);
        myIntent = getIntent();
    }

    @Override
    public void init_events() {
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btn_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt_code.getText().toString().equals("")){
                    if (shots_count>1){
                        Toast.makeText(ResetPasswordActivity.this, getResources().getString(R.string.reset_password_no_code), Toast.LENGTH_SHORT).show();
                        shots_count--;
                        return;
                    }else {
                        Snackbar.make(root, getResources().getString(R.string.reset_password_no_code_received), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.reset_password_request_code), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callRequestAPI();
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                        return;
                    }
                }
                if (edt_new.getText().toString().equals("")){
                    Toast.makeText(ResetPasswordActivity.this, getResources().getString(R.string.reset_password_no_new), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (edt_confirm.getText().toString().equals("")){
                    Toast.makeText(ResetPasswordActivity.this, getResources().getString(R.string.reset_password_no_confirm), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!edt_confirm.getText().toString().equals(edt_new.getText().toString())){
                    Toast.makeText(ResetPasswordActivity.this, getResources().getString(R.string.reset_password_no_match), Toast.LENGTH_SHORT).show();
                    return;
                }
                callResetAPI();
            }
        });
        img_new_eye.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (password_eye_state == 0){
                    password_eye_state = 1;
                    img_new_eye.setImageResource(R.drawable.ic_white_eye);
                    edt_new.setInputType(1);
                }else {
                    password_eye_state = 0;
                    img_new_eye.setImageResource(R.drawable.ic_white_hidden_eye);
                    edt_new.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                }
            }
        });
        img_confirm_eye.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (password_eye_state == 0){
                    password_eye_state = 1;
                    img_confirm_eye.setImageResource(R.drawable.ic_white_eye);
                    edt_confirm.setInputType(1);
                }else {
                    password_eye_state = 0;
                    img_confirm_eye.setImageResource(R.drawable.ic_white_hidden_eye);
                    edt_confirm.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                }
            }
        });
    }

    @Override
    public void set_fragment_place() {

    }

    private void callRequestAPI(){
        AccountsAPIsClass.resetPasswordRequest(sharedPreferences,
                ResetPasswordActivity.this,
                sharedPreferences.getString("language", "ar"),
                sharedPreferences.getString("service_token", ""),
                myIntent.getStringExtra("phone_number"),
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json != null){
                            Snackbar.make(root,getResources().getString(R.string.reset_password_wait),Snackbar.LENGTH_SHORT).show();
                            code_requested = true;
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callRequestAPI();
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });
    }

    private void callResetAPI(){
        AccountsAPIsClass.resetPassword(sharedPreferences,
                ResetPasswordActivity.this,
                sharedPreferences.getString("language", "ar"),
                sharedPreferences.getString("service_token", ""),
                edt_code.getText().toString(),
                edt_new.getText().toString(),
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json != null){
                            Toast.makeText(ResetPasswordActivity.this, getResources().getString(R.string.reset_password_success), Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(ResetPasswordActivity.this,LoginActivity.class));
                            finish();
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callResetAPI();
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });
    }
}
