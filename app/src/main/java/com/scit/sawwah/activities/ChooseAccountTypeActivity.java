package com.scit.sawwah.activities;

import android.content.Intent;
import android.provider.Settings;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.scit.sawwah.APIClasses.BasicAPIsClass;
import com.scit.sawwah.Models.AccountSettingsObject;
import com.scit.sawwah.Models.DeviceSettingsObject;
import com.scit.sawwah.Models.LoginResponse;
import com.scit.sawwah.R;
import com.scit.sawwah.interfaces.IFailure;
import com.scit.sawwah.interfaces.IResponse;
import com.scit.sawwah.tools.BaseActivity;
import com.scit.sawwah.tools.BaseFunctions;

public class ChooseAccountTypeActivity extends BaseActivity {
    private RelativeLayout toolbar;
    private ImageButton btn_back;
    private RelativeLayout btn_next,btn_customer_type,btn_owner_type;
    private String type = "";
    private LinearLayout root;
    private String selected_type = "";
    private Intent myIntent;
    private ImageView img_bene,img_owner;
    private ImageView img_loader;

    @Override
    public void set_layout() {
        setContentView(R.layout.activity_choose_account_type);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {

    }

    @Override
    public void init_views() {
        toolbar = findViewById(R.id.choose_account_type_toolbar);
        btn_back = toolbar.findViewById(R.id.toolbar_back_btn);
        btn_next = findViewById(R.id.choose_account_type_next_btn);
        btn_owner_type = findViewById(R.id.choose_account_type_owner_type_btn);
        img_owner = findViewById(R.id.choose_account_type_owner_type_icon);
        btn_customer_type = findViewById(R.id.choose_account_type_customer_type_btn);
        img_bene = findViewById(R.id.choose_account_type_customer_type_icon);
        root = findViewById(R.id.choose_account_type_layout);
        img_loader = findViewById(R.id.choose_account_type_loader_img);
        BaseFunctions.setGlideDrawableImage(ChooseAccountTypeActivity.this,img_loader,R.drawable.loading_indicator);
        myIntent = getIntent();
    }

    @Override
    public void init_events() {
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selected_type.equals("")){
                    Toast.makeText(ChooseAccountTypeActivity.this, getResources().getString(R.string.choose_account_type_no_type), Toast.LENGTH_SHORT).show();
                    return;
                }else {
                    if (myIntent.getStringExtra("type").equals("facebook")){
                       callFacebookAPI(selected_type,myIntent.getStringExtra("image"),myIntent.getStringExtra("token"));
                    }else if (myIntent.getStringExtra("type").equals("google")){
                        callGoogleAPI(selected_type,myIntent.getStringExtra("image"),myIntent.getStringExtra("token"));
                    }
                }
            }
        });
        btn_owner_type.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(ChooseAccountTypeActivity.this,R.anim.on_click_animation));
                img_owner.setImageResource(R.drawable.ic_red_left_arrow);
                img_bene.setImageResource(R.drawable.ic_white_left_arrow);
                selected_type = "2";
            }
        });
        btn_customer_type.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(ChooseAccountTypeActivity.this,R.anim.on_click_animation));
                img_owner.setImageResource(R.drawable.ic_white_left_arrow);
                img_bene.setImageResource(R.drawable.ic_red_left_arrow);
                selected_type = "1";
            }
        });
    }

    @Override
    public void set_fragment_place() {

    }

    private void callFacebookAPI(final String account_type, final String image, final String access_token){
        img_loader.setVisibility(View.VISIBLE);
        String android_id = Settings.Secure.getString(ChooseAccountTypeActivity.this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        BasicAPIsClass.facebookRegister(sharedPreferences,
                ChooseAccountTypeActivity.this,
                sharedPreferences.getString("language", "ar"),
                sharedPreferences.getString("service_token", ""),
                android_id,
                account_type,
                image,
                access_token,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json != null){
                            String json1 = new Gson().toJson(json);
                            LoginResponse success = new Gson().fromJson(json1,LoginResponse.class);
                            //Id
                            editor.putInt("account_id",success.getId());
                            //Service token
                            editor.putString("service_token",success.getService_token());
                            //Image
                            if (success.getImage()!=null){
                                editor.putString("account_image",success.getImage());
                            }
                            //name
                            if (success.getName()!=null){
                                editor.putString("account_name",success.getName());
                            }
                            //User name
                            if (success.getUsername()!=null){
                                editor.putString("account_user_name",success.getUsername());
                            }
                            //Account type
                            if (success.getAccount_type_id()!= 0){
                                if (success.getAccount_type_id() == 1){
                                    editor.putString("account_type","customer");
                                }else {
                                    editor.putString("account_type","owner");
                                }
                            }
                            //Account status
                            editor.putInt("account_status",success.getStatus());
                            //Access Token
                            if (success.getAccess_token()!=null){
                                editor.putString("access_token",success.getAccess_token());
                            }
                            //Device Settings
                            if (success.getDevice_setting()!=null){
                                DeviceSettingsObject dso = success.getDevice_setting();
                                //City Id
                                editor.putInt("settings_city_id",dso.getCity_id());
                                //Country Id
                                editor.putInt("settings_country_id",dso.getCity_id());
                                //Language
                                editor.putString("language", BaseFunctions.numberToLanguageConverter(String.valueOf(dso.getView_language_id())));
                                //Alert
                                editor.putBoolean("settings_alerts",BaseFunctions.stringNumberToBoolean(String.valueOf(dso.getAllow_global_notification())));
                                editor.commit();
                            }
                            //My Institutions
                            if (success.getMy_facilities()!=null){
                                if (success.getMy_facilities().size() > 0){
                                    String insts_json = new Gson().toJson(success.getMy_facilities());
                                    editor.putString("my_insts",insts_json);
                                }
                            }
                            //My Packages
                            if(success.getMy_packages()!=null){
                                if (success.getMy_facilities().size()>0){
                                    String packages_json = new Gson().toJson(success.getMy_packages());
                                    editor.putString("my_packages",packages_json);
                                }
                            }
                            //Account Settings
                            if (success.getAccount_settings()!=null){
                                AccountSettingsObject aso = success.getAccount_settings();
                                editor.putInt("account_status",success.getAccount_settings().getAccount_status());
                                /*
                                //Profile Completed
                                if (aso.getIs_profile_completed() == 1){
                                    if (aso.getUser_type() == 2){
                                        editor.putString("account_type","customer");
                                    }else if (aso.getUser_type() == 1){
                                        editor.putString("account_type","owner");
                                    }
                                    editor.putInt("account_status",aso.getAccount_status());
                                    editor.commit();
                                    startActivity(new Intent(LoginActivity.this,MainActivity.class));
                                }else {
                                    //User type
                                    if (aso.getUser_type() == 0){
                                        if (aso.getCreated_as()!=1){
                                            startActivity(new Intent(LoginActivity.this,ChooseAccountTypeActivity.class));
                                        }else {
                                            startActivity(new Intent(LoginActivity.this,RegisterActivity.class));
                                        }
                                    }else if (aso.getUser_type() == 2){
                                        startActivity(new Intent(LoginActivity.this,MainActivity.class));
                                    }else if (aso.getUser_type() == 1){
                                        startActivity(new Intent(LoginActivity.this,RegisterInstitutionActivity.class));
                                    }
                                }
                                **/
                            }
                            editor.commit();
                            if (selected_type.equals("2")){
                                editor.putBoolean("is_visitor",false);
                                editor.commit();
                                startActivity(new Intent(ChooseAccountTypeActivity.this, RegisterInstitutionActivity.class));
                                finish();
                            }else {
                                editor.putBoolean("is_visitor",false);
                                editor.commit();
                                startActivity(new Intent(ChooseAccountTypeActivity.this, ThanksActivity.class));
                                finish();
                            }
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        img_loader.setVisibility(View.GONE);
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callFacebookAPI(account_type,image,access_token);
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });
    }
    private void callGoogleAPI(final String account_type, final String image, final String access_token){
        img_loader.setVisibility(View.VISIBLE);
        String android_id = Settings.Secure.getString(ChooseAccountTypeActivity.this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        BasicAPIsClass.googleRegister(sharedPreferences,
                ChooseAccountTypeActivity.this,
                sharedPreferences.getString("language", "ar"),
                sharedPreferences.getString("service_token", ""),
                android_id,
                account_type,
                image,
                access_token,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json != null){
                            String json1 = new Gson().toJson(json);
                            LoginResponse success = new Gson().fromJson(json1,LoginResponse.class);
                            //Id
                            editor.putInt("account_id",success.getId());
                            //Service token
                            editor.putString("service_token",success.getService_token());
                            //Image
                            if (success.getImage()!=null){
                                editor.putString("account_image",success.getImage());
                            }
                            //name
                            if (success.getName()!=null){
                                editor.putString("account_name",success.getName());
                            }
                            //User name
                            if (success.getUsername()!=null){
                                editor.putString("account_user_name",success.getUsername());
                            }
                            //Account type
                            if (success.getAccount_type_id()!= 0){
                                if (success.getAccount_type_id() == 1){
                                    editor.putString("account_type","customer");
                                }else {
                                    editor.putString("account_type","owner");
                                }
                            }
                            //Account status
                            editor.putInt("account_status",success.getStatus());
                            //Access Token
                            if (success.getAccess_token()!=null){
                                editor.putString("access_token",success.getAccess_token());
                            }
                            //Device Settings
                            if (success.getDevice_setting()!=null){
                                DeviceSettingsObject dso = success.getDevice_setting();
                                //City Id
                                editor.putInt("settings_city_id",dso.getCity_id());
                                //Country Id
                                editor.putInt("settings_country_id",dso.getCity_id());
                                //Language
                                editor.putString("language", BaseFunctions.numberToLanguageConverter(String.valueOf(dso.getView_language_id())));
                                //Alert
                                editor.putBoolean("settings_alerts",BaseFunctions.stringNumberToBoolean(String.valueOf(dso.getAllow_global_notification())));
                                editor.commit();
                            }
                            //My Institutions
                            if (success.getMy_facilities()!=null){
                                if (success.getMy_facilities().size() > 0){
                                    String insts_json = new Gson().toJson(success.getMy_facilities());
                                    editor.putString("my_insts",insts_json);
                                }
                            }
                            //My Packages
                            if(success.getMy_packages()!=null){
                                if (success.getMy_facilities().size()>0){
                                    String packages_json = new Gson().toJson(success.getMy_packages());
                                    editor.putString("my_packages",packages_json);
                                }
                            }
                            //Account Settings
                            if (success.getAccount_settings()!=null){
                                AccountSettingsObject aso = success.getAccount_settings();
                                editor.putInt("account_status",success.getAccount_settings().getAccount_status());
                                /*
                                //Profile Completed
                                if (aso.getIs_profile_completed() == 1){
                                    if (aso.getUser_type() == 2){
                                        editor.putString("account_type","customer");
                                    }else if (aso.getUser_type() == 1){
                                        editor.putString("account_type","owner");
                                    }
                                    editor.putInt("account_status",aso.getAccount_status());
                                    editor.commit();
                                    startActivity(new Intent(LoginActivity.this,MainActivity.class));
                                }else {
                                    //User type
                                    if (aso.getUser_type() == 0){
                                        if (aso.getCreated_as()!=1){
                                            startActivity(new Intent(LoginActivity.this,ChooseAccountTypeActivity.class));
                                        }else {
                                            startActivity(new Intent(LoginActivity.this,RegisterActivity.class));
                                        }
                                    }else if (aso.getUser_type() == 2){
                                        startActivity(new Intent(LoginActivity.this,MainActivity.class));
                                    }else if (aso.getUser_type() == 1){
                                        startActivity(new Intent(LoginActivity.this,RegisterInstitutionActivity.class));
                                    }
                                }
                                **/
                            }

                            editor.commit();
                            if (selected_type.equals("2")){
                                editor.putBoolean("is_visitor",false);
                                editor.commit();
                                startActivity(new Intent(ChooseAccountTypeActivity.this, RegisterInstitutionActivity.class));
                                finish();
                            }else {
                                editor.putBoolean("is_visitor",false);
                                editor.commit();
                                startActivity(new Intent(ChooseAccountTypeActivity.this, ThanksActivity.class));
                                finish();
                            }
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        img_loader.setVisibility(View.GONE);
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callFacebookAPI(account_type,image,access_token);
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });
    }
}
