package com.scit.sawwah.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.scit.sawwah.Models.NotificationObject;
import com.scit.sawwah.R;
import com.scit.sawwah.interfaces.IMove;
import com.scit.sawwah.tools.BaseFunctions;

import java.util.List;


public class NotificationsAdapter extends  RecyclerView.Adapter<NotificationsAdapter.ViewHolder> {
    private List<NotificationObject> list;
    private Context context;
    private IMove iMove;


    public NotificationsAdapter( Context context,List<NotificationObject> list,IMove iMove) {
        this.context = context;
        this.iMove = iMove;
        this.list = list;

    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView img_image,img_bell;
        private TextView tv_text,tv_time;

        public ViewHolder(View view) {
            super(view);
            img_image = view.findViewById(R.id.item_notification_image);
            img_bell = view.findViewById(R.id.item_notification_bell);
            tv_text = view.findViewById(R.id.item_notification_text);
            tv_time = view.findViewById(R.id.item_notification_time);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_notification, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        NotificationObject no = list.get(position);
        BaseFunctions.setGlideImage(context,holder.img_image,no.getImage());
        holder.img_bell.setImageResource(R.drawable.ic_colored_bell);
        holder.tv_text.setText(no.getPayload());
        holder.tv_time.setText(BaseFunctions.dateExtractor(no.getCreated_at()));

    }

}