package com.scit.sawwah.adapters;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.scit.sawwah.Models.ContactObject;
import com.scit.sawwah.R;
import com.scit.sawwah.interfaces.IMove;
import com.scit.sawwah.tools.BaseFunctions;

import java.util.List;

/**
 * Created by Misheal on 8/28/2019.
 */

public class EditInstSocialsAdapter extends  RecyclerView.Adapter<EditInstSocialsAdapter.ViewHolder> {
    private List<ContactObject> list;
    //private String[] titles = new String[]{"Touristic hotel","Touristic hotel","Touristic hotel"};
    //private String[] urls = new String[]{"facebook.com/Montajaa","instagram.com/Montajaa","twitter.com/Montajaa"};
    //private int[] images = new int[]{R.drawable.ic_facebook,R.drawable.ic_instagram,R.drawable.ic_twetter};
    private Context context;
    private IMove iMove;


    public EditInstSocialsAdapter( Context context,List<ContactObject> list,IMove iMove) {
        this.context = context;
        this.iMove = iMove;
        this.list = list;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_title;
        private TextView tv_url;
        private ImageView img_icon;


        public ViewHolder(View view) {
            super(view);
            tv_title = view.findViewById(R.id.item_edit_inst_socials_title);
            tv_url = view.findViewById(R.id.item_edit_inst_socials_url);
            img_icon = view.findViewById(R.id.item_edit_inst_socials_icon);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_edit_inst_socials, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        ContactObject co = list.get(position);
        holder.tv_title.setText(co.getName());
        holder.tv_url.setText(co.getValue());
        BaseFunctions.setGlideImage(context,holder.img_icon,co.getIcon());
    }
}
