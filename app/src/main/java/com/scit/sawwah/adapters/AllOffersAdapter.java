package com.scit.sawwah.adapters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.scit.sawwah.APIClasses.FavouriteAPIsClass;
import com.scit.sawwah.Models.OfferObject;
import com.scit.sawwah.R;
import com.scit.sawwah.activities.OfferDetailsActivity;
import com.scit.sawwah.dialoges.VisitorDialog;
import com.scit.sawwah.interfaces.IFailure;
import com.scit.sawwah.interfaces.IMove;
import com.scit.sawwah.interfaces.IResponse;
import com.scit.sawwah.tools.BaseFunctions;

import java.util.List;

/**
 * Created by Misheal on 8/17/2018.
 */

public class AllOffersAdapter extends  RecyclerView.Adapter<AllOffersAdapter.ViewHolder> {
    private List<OfferObject> list;
    private Context context;
    private IMove iMove;
    private int lovers = 0;
    private SharedPreferences sharedPreferences;


    public AllOffersAdapter( Context context,List<OfferObject> list,IMove iMove) {
        this.context = context;
        this.iMove = iMove;
        this.list = list;
        this.sharedPreferences = context.getSharedPreferences("Main",Context.MODE_PRIVATE);
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_offer_title;
        private TextView tv_inst_name;
        private ImageView img_icon;
        private ImageView img_image;
        private TextView tv_desc;
        private ImageView img_love;
        private TextView tv_num_lovers;
        private ImageView img_share;
        private LinearLayout layout;

        public ViewHolder(View view) {
            super(view);
            tv_offer_title = view.findViewById(R.id.item_all_offer_title_txt);
            tv_inst_name = view.findViewById(R.id.item_all_offer_institution_name_txt);
            img_icon = view.findViewById(R.id.item_all_offer_icon);
            img_image = view.findViewById(R.id.item_all_offer_image);
            tv_desc = view.findViewById(R.id.item_all_offer_description);
            img_love = view.findViewById(R.id.item_all_offer_love);
            tv_num_lovers = view.findViewById(R.id.item_all_offer_num_lovers);
            img_share = view.findViewById(R.id.item_all_offer_share);
            layout = view.findViewById(R.id.item_all_offer_layout);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_all_offer, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final OfferObject oo = list.get(position);
        holder.tv_offer_title.setText(oo.getTitle());
        holder.tv_inst_name.setText(oo.getFacility_name());
        BaseFunctions.setGlideImage(context,holder.img_icon,oo.getFacility_image());
        BaseFunctions.setGlideImage(context,holder.img_image,oo.getImage());
        holder.tv_desc.setText(oo.getDetails());
        lovers = oo.getFavourite_count();
        holder.tv_num_lovers.setText(String.valueOf(oo.getFavourite_count()));
        if (oo.isMarked_as_favourite() == false){
            holder.img_love.setImageResource(R.drawable.ic_grey_heart);
        }else {
            holder.img_love.setImageResource(R.drawable.ic_heart);
        }
        holder.img_love.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (oo.isMarked_as_favourite() == false){
                    toggle_item(holder,String.valueOf(oo.getId()),"2","1");
                }else {
                    toggle_item(holder,String.valueOf(oo.getId()),"2","0");
                }

            }
        });
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sharedPreferences.getBoolean("is_visitor",false)){
                    VisitorDialog dialog = new VisitorDialog(context);
                    dialog.show();
                }else{
                    Intent intent = new Intent(context, OfferDetailsActivity.class);
                    intent.putExtra("offer_id",oo.getId());
                    context.startActivity(intent);
                }

            }
        });
        holder.img_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iMove.move(position);
            }
        });
    }

    private void toggle_item(final ViewHolder holder, String id, String type, final String state){
        FavouriteAPIsClass.toggle_item(sharedPreferences,
                context,
                sharedPreferences.getString("language", "ar"),
                sharedPreferences.getString("service_token", ""),
                id,
                type,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json !=null){
                            if (state.equals("0")){
                                lovers--;
                                holder.tv_num_lovers.setText(String.valueOf(lovers));
                                holder.img_love.setImageResource(R.drawable.ic_grey_heart);
                                Toast.makeText(context, context.getResources().getString(R.string.favourite_offer_remover), Toast.LENGTH_SHORT).show();
                            }else {
                                lovers++;
                                holder.tv_num_lovers.setText(String.valueOf(lovers));
                                holder.img_love.setImageResource(R.drawable.ic_heart);
                                Toast.makeText(context, context.getResources().getString(R.string.favourite_offer_add), Toast.LENGTH_SHORT).show();
                            }
                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.error_occurred), Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                    }
                });
    }
}
