package com.scit.sawwah.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.scit.sawwah.Models.CategoryObject;
import com.scit.sawwah.R;
import com.scit.sawwah.interfaces.IMove;

import java.util.List;

/**
 * Created by Misheal on 8/19/2019.
 */

public class RegisterCategoriesAdapter extends  RecyclerView.Adapter<RegisterCategoriesAdapter.ViewHolder> {
    private List<CategoryObject> list;
    private Context context;
    private IMove iMove;


    public RegisterCategoriesAdapter( Context context,List<CategoryObject> list,IMove iMove) {
        this.context = context;
        this.iMove = iMove;
        this.list = list;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_title;
        private ImageView img_remove;
        public ViewHolder(View view) {
            super(view);
            tv_title = view.findViewById(R.id.item_register_category_txt);
            img_remove = view.findViewById(R.id.item_register_category_remove);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_register_category, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        holder.tv_title.setText(list.get(position).getName());
        holder.img_remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                list.remove(position);
                notifyDataSetChanged();
                iMove.move(position);
            }
        });
    }
}
