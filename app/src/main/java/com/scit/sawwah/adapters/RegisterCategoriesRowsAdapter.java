package com.scit.sawwah.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RadioButton;

import com.scit.sawwah.Models.CategoryObject;
import com.scit.sawwah.R;
import com.scit.sawwah.interfaces.IMove;

import java.util.List;

/**
 * Created by Misheal on 8/19/2019.
 */

public class RegisterCategoriesRowsAdapter extends  RecyclerView.Adapter<RegisterCategoriesRowsAdapter.ViewHolder> {
    private List<CategoryObject> list;
    private Context context;
    private IMove iMove;


    public RegisterCategoriesRowsAdapter( Context context,List<CategoryObject> list,IMove iMove) {
        this.context = context;
        this.iMove = iMove;
        this.list = list;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private RadioButton tv_radio;

        public ViewHolder(View view) {
            super(view);
            tv_radio = view.findViewById(R.id.register_category_row_radio);

        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_register_category_row, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        holder.tv_radio.setText(list.get(position).getName());
        holder.tv_radio.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    iMove.move(position);
                }
            }
        });
    }
}
