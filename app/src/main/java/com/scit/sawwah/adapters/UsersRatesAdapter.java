package com.scit.sawwah.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.iarcuschin.simpleratingbar.SimpleRatingBar;
import com.scit.sawwah.Models.RateObject;
import com.scit.sawwah.R;
import com.scit.sawwah.interfaces.IMove;

import java.util.List;

/**
 * Created by Misheal on 8/27/2019.
 */

public class UsersRatesAdapter extends  RecyclerView.Adapter<UsersRatesAdapter.ViewHolder>{
    private List<RateObject> list ;
    private Context context;
    private IMove iMove;


    public UsersRatesAdapter( Context context,List<RateObject> list,IMove iMove) {
        this.context = context;
        this.iMove = iMove;
        this.list = list;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_text;
        private SimpleRatingBar rb_rate;

        public ViewHolder(View view) {
            super(view);
            tv_text = view.findViewById(R.id.item_user_rate_text);
            rb_rate = view.findViewById(R.id.item_user_rate_rate);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_user_rate, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        RateObject ro = list.get(position);
        holder.tv_text.setText(ro.getNote());
        holder.rb_rate.setRating(ro.getRate_value());
    }
}
