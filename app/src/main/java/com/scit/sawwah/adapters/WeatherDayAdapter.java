package com.scit.sawwah.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.scit.sawwah.Models.WeatherListObject;
import com.scit.sawwah.R;
import com.scit.sawwah.interfaces.IMove;
import com.scit.sawwah.tools.BaseFunctions;

import java.util.List;

/**
 * Created by Misheal on 8/29/2019.
 */

public class WeatherDayAdapter extends  RecyclerView.Adapter<WeatherDayAdapter.ViewHolder> {
    private List<WeatherListObject> list;
    private Context context;
    private IMove iMove;


    public WeatherDayAdapter( Context context,List<WeatherListObject> list,IMove iMove) {
        this.context = context;
        this.iMove = iMove;
        this.list = list;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_day;
        private TextView tv_weather;
        private ImageView iv_image;

        public ViewHolder(View view) {
            super(view);
            tv_day = view.findViewById(R.id.item_weather_day_day);
            tv_weather = view.findViewById(R.id.item_weather_day_weather);
            iv_image = view.findViewById(R.id.item_weather_day_icon);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_weather_day, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        WeatherListObject wlo = list.get(position);
        if (wlo.getDt_txt()!=null){
            holder.tv_day.setText(BaseFunctions.dateToDayConverter(BaseFunctions.stringToDateConverter(BaseFunctions.dateExtractor(wlo.getDt_txt()))));
        }
        if (wlo.getWeather()!=null){
            if (wlo.getWeather().size()>0){
                if (wlo.getWeather().get(0).getMain()!=null){
                    switch (wlo.getWeather().get(0).getMain()){
                        //Sunny
                        case "Clear" : {
                            holder.tv_weather.setText(context.getResources().getString(R.string.weather_sunny));
                            holder.iv_image.setImageResource(R.drawable.ic_sunny);
                        }break;
                        //Rainy
                        case "Drizzle" :
                        case "Rain" : {
                            holder.tv_weather.setText(context.getResources().getString(R.string.weather_rainy));
                            holder.iv_image.setImageResource(R.drawable.ic_weather_raining);
                        }break;
                        //Cloudy
                        case "Mist" :
                        case "Smoke" :
                        case "Haze" :
                        case "Dust" :
                        case "Fog" :
                        case "Sand" :
                        case "Ash" :
                        case "Clouds" :{
                            holder.tv_weather.setText(context.getResources().getString(R.string.weather_cloudy));
                            holder.iv_image.setImageResource(R.drawable.ic_cloudy);
                        }break;
                        //Snowy
                        case "Snow" : {
                            holder.tv_weather.setText(context.getResources().getString(R.string.weather_snowy));
                            holder.iv_image.setImageResource(R.drawable.ic_snowy);
                        }break;
                        //Stormy
                        case "Thunderstorm" :
                        case "Squall" :
                        case "Tornado" :{
                            holder.tv_weather.setText(context.getResources().getString(R.string.weather_stormy));
                            holder.iv_image.setImageResource(R.drawable.ic_stormy);
                        }break;
                    }
                }
            }
        }
    }
}
