package com.scit.sawwah.adapters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.glide.slider.library.svg.GlideApp;
import com.scit.sawwah.Models.OfferObject;
import com.scit.sawwah.R;
import com.scit.sawwah.activities.OfferDetailsActivity;
import com.scit.sawwah.dialoges.VisitorDialog;
import com.scit.sawwah.interfaces.IMove;

import java.util.List;

/**
 * Created by Misheal on 8/6/2019.
 */

public class HomeOffersAdapter extends  RecyclerView.Adapter<HomeOffersAdapter.ViewHolder> {
    private List<OfferObject> list;
    private Context context;
    private IMove iMove;
    private SharedPreferences sharedPreferences;


    public HomeOffersAdapter( Context context,List<OfferObject> list,IMove iMove) {
        this.context = context;
        this.iMove = iMove;
        this.list = list;
        this.sharedPreferences = context.getSharedPreferences("Main", Context.MODE_PRIVATE);
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_title;
        private ImageView iv_image;
        private RelativeLayout layout;

        public ViewHolder(View view) {
            super(view);
            tv_title = view.findViewById(R.id.item_home_offer_title);
            iv_image = view.findViewById(R.id.item_home_offer_image);
            layout = view.findViewById(R.id.item_home_offer_layout);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_home_offer, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final OfferObject oo = list.get(position);
        holder.tv_title.setText(oo.getTitle());
        try{
            GlideApp.with(context).load(oo.getImage())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .centerCrop().into(holder.iv_image);
        }catch (Exception e){}
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //iMove.move(position);
                if (sharedPreferences.getBoolean("is_visitor",false)){
                    VisitorDialog dialog = new VisitorDialog(context);
                    dialog.show();
                }else {
                    Intent intent = new Intent(context,OfferDetailsActivity.class);
                    intent.putExtra("offer_id",oo.getId());
                    context.startActivity(intent);
                }

            }
        });
    }
}
