package com.scit.sawwah.adapters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.scit.sawwah.Models.AdvertisementObject;
import com.scit.sawwah.R;
import com.scit.sawwah.activities.AdvertisementDetailsActivity;
import com.scit.sawwah.dialoges.VisitorDialog;
import com.scit.sawwah.interfaces.IMove;

import java.util.List;

/**
 * Created by Misheal on 8/6/2019.
 */

public class HomeAdvertisementsAdapter extends  RecyclerView.Adapter<HomeAdvertisementsAdapter.ViewHolder> {
    private List<AdvertisementObject> list;
    private Context context;
    private IMove iMove;
    private SharedPreferences sharedPreferences;


    public HomeAdvertisementsAdapter( Context context,List<AdvertisementObject> list,IMove iMove) {
        this.context = context;
        this.iMove = iMove;
        this.list = list;
        this.sharedPreferences = context.getSharedPreferences("Main",Context.MODE_PRIVATE);
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_title;
        private TextView tv_text;
        private LinearLayout lyt_layout;

        public ViewHolder(View view) {
            super(view);
            tv_title = view.findViewById(R.id.item_home_ad_title);
            tv_text = view.findViewById(R.id.item_home_ad_text);
            lyt_layout = view.findViewById(R.id.item_home_ad_layout);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_home_advertisement, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final AdvertisementObject ao = list.get(position);
        if (ao.getTitle()!=null){
            holder.tv_title.setText(ao.getTitle());
        }
        if (ao.getDetails()!=null){
            holder.tv_text.setText(ao.getDetails());
        }

        holder.lyt_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sharedPreferences.getBoolean("is_visitor",false)){
                    VisitorDialog dialog = new VisitorDialog(context);
                    dialog.show();
                }else {
                    Intent intent = new Intent(context, AdvertisementDetailsActivity.class);
                    intent.putExtra("advertisement_id",ao.getId());
                    context.startActivity(intent);
                }
            }
        });
    }
}
