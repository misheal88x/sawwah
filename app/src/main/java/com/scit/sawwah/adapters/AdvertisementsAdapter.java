package com.scit.sawwah.adapters;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.scit.sawwah.Models.AdvertisementObject;
import com.scit.sawwah.R;
import com.scit.sawwah.activities.AdvertisementDetailsActivity;
import com.scit.sawwah.interfaces.IMove;
import com.scit.sawwah.tools.BaseFunctions;

import java.util.List;


public class AdvertisementsAdapter extends  RecyclerView.Adapter<AdvertisementsAdapter.ViewHolder> {
    private List<AdvertisementObject> list;
    private Context context;
    private IMove iMove;


    public AdvertisementsAdapter( Context context,List<AdvertisementObject> list,IMove iMove) {
        this.context = context;
        this.iMove = iMove;
        this.list = list;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_title;
        private TextView tv_desc;
        private LinearLayout layout;

        public ViewHolder(View view) {
            super(view);
            tv_title = view.findViewById(R.id.adv_title);
            tv_desc = view.findViewById(R.id.adv_content);
            layout = view.findViewById(R.id.item_ads_layout);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_advertise, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final AdvertisementObject ao = list.get(position);
        holder.tv_title.setText(ao.getTitle()+" "+ BaseFunctions.dateExtractor(ao.getStart_date()));
        holder.tv_desc.setText(ao.getDetails());
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AdvertisementDetailsActivity.class);
                intent.putExtra("advertisement_id",ao.getId());
                context.startActivity(intent);
            }
        });
    }
}