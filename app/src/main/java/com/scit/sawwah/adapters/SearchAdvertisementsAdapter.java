package com.scit.sawwah.adapters;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.scit.sawwah.Models.AdvertisementObject;
import com.scit.sawwah.R;
import com.scit.sawwah.activities.AdvertisementDetailsActivity;
import com.scit.sawwah.interfaces.IMove;
import com.scit.sawwah.tools.BaseFunctions;

import java.util.List;

/**
 * Created by Misheal on 8/13/2019.
 */

public class SearchAdvertisementsAdapter extends RecyclerView.Adapter<SearchAdvertisementsAdapter.ViewHolder> {
    private List<AdvertisementObject> list;
    private String title = "Advertisement title";
    private int[] images = new int[]{R.drawable.offerone,R.drawable.offertwo,R.drawable.offertwo};
    private Context context;
    private IMove iMove;


    public SearchAdvertisementsAdapter( Context context,List<AdvertisementObject> list,IMove iMove) {
        this.context = context;
        this.iMove = iMove;
        this.list = list;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_title;
        private ImageView iv_image;
        private CardView layout;

        public ViewHolder(View view) {
            super(view);
            tv_title = view.findViewById(R.id.item_search_offer_name);
            iv_image = view.findViewById(R.id.item_search_offer_image);
            layout = view.findViewById(R.id.item_search_offer_layout);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_search_offer, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final AdvertisementObject ao = list.get(position);
        if (ao.getTitle()!=null){
            holder.tv_title.setText(ao.getTitle());
        }
        if (ao.getFacility_image()!=null){
            BaseFunctions.setGlideImage(context,holder.iv_image,ao.getFacility_image());
        }
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AdvertisementDetailsActivity.class);
                intent.putExtra("advertisement_id",ao.getId());
                context.startActivity(intent);
            }
        });
    }
}
