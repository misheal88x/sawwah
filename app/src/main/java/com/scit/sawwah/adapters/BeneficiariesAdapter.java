package com.scit.sawwah.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.scit.sawwah.Models.BeneficiaryObject;
import com.scit.sawwah.R;
import com.scit.sawwah.interfaces.IMove;
import com.scit.sawwah.tools.BaseFunctions;

import java.util.List;

/**
 * Created by Misheal on 8/25/2019.
 */

public class BeneficiariesAdapter extends  RecyclerView.Adapter<BeneficiariesAdapter.ViewHolder> {
    private List<BeneficiaryObject> list;
    private Context context;
    private IMove iMove;


    public BeneficiariesAdapter( Context context,List<BeneficiaryObject> list,IMove iMove) {
        this.context = context;
        this.iMove = iMove;
        this.list = list;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_name;
        private TextView tv_date;
        private ImageView iv_image;

        public ViewHolder(View view) {
            super(view);
            tv_name = view.findViewById(R.id.item_beneficiary_name);
            tv_date = view.findViewById(R.id.item_beneficiary_date);
            iv_image = view.findViewById(R.id.item_beneficiary_image);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_beneficiary, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        BeneficiaryObject bo = list.get(position);
        if (bo.getUser_name()!=null){
            holder.tv_name.setText(bo.getScan_time());
        }
        if (bo.getScan_time()!=null){
            holder.tv_date.setText(BaseFunctions.dateExtractor(bo.getScan_time()));
        }
        if (bo.getUser_image()!=null){
            BaseFunctions.setGlideImage(context,holder.iv_image,bo.getUser_image());
        }
    }
}
