package com.scit.sawwah.adapters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.scit.sawwah.Models.OfferObject;
import com.scit.sawwah.R;
import com.scit.sawwah.activities.OfferDetailsActivity;
import com.scit.sawwah.dialoges.VisitorDialog;
import com.scit.sawwah.interfaces.IMove;
import com.scit.sawwah.tools.BaseFunctions;

import java.util.List;

/**
 * Created by Misheal on 8/22/2019.
 */

public class PreferredOffersAdapter extends RecyclerView.Adapter<PreferredOffersAdapter.ViewHolder> {
    private List<OfferObject> list;
    private String title = "Offer title";
    private int[] images = new int[]{R.drawable.offerone,R.drawable.offertwo,R.drawable.offertwo};
    private Context context;
    private IMove iMove;
    private SharedPreferences sharedPreferences;


    public PreferredOffersAdapter( Context context,List<OfferObject> list,IMove iMove) {
        this.context = context;
        this.iMove = iMove;
        this.list = list;
        this.sharedPreferences = context.getSharedPreferences("Main",Context.MODE_PRIVATE);

    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_title;
        private ImageView iv_image;
        private CardView layout;


        public ViewHolder(View view) {
            super(view);
            tv_title = view.findViewById(R.id.item_search_offer_name);
            iv_image = view.findViewById(R.id.item_search_offer_image);
            layout = view.findViewById(R.id.item_search_offer_layout);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_search_offer, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        holder.tv_title.setText(list.get(position).getTitle());
        BaseFunctions.setGlideImage(context,holder.iv_image,list.get(position).getImage());
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sharedPreferences.getBoolean("is_visitor",false)){
                    VisitorDialog dialog = new VisitorDialog(context);
                    dialog.show();
                }else {
                    Intent intent = new Intent(context, OfferDetailsActivity.class);
                    intent.putExtra("offer_id",list.get(position).getId());
                    context.startActivity(intent);
                }
            }
        });
    }
}
