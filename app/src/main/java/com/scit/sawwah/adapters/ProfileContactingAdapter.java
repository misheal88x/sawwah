package com.scit.sawwah.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.scit.sawwah.Models.ContactObject;
import com.scit.sawwah.R;
import com.scit.sawwah.interfaces.IMove;
import com.scit.sawwah.tools.BaseFunctions;

import java.util.List;

/**
 * Created by Misheal on 9/30/2019.
 */

public class ProfileContactingAdapter extends  RecyclerView.Adapter<ProfileContactingAdapter.ViewHolder> {
    private List<ContactObject> list;
    private Context context;
    private IMove iMove;


    public ProfileContactingAdapter( Context context,List<ContactObject> list,IMove iMove) {
        this.context = context;
        this.iMove = iMove;
        this.list = list;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView iv_image;

        public ViewHolder(View view) {
            super(view);
            iv_image = view.findViewById(R.id.item_profile_contacting_image);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_profile_contacting, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final ContactObject co = list.get(position);
        BaseFunctions.setGlideImage(context,holder.iv_image,co.getIcon());
        holder.iv_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (co.getType()){
                    //URL
                    case 0 : {
                        BaseFunctions.openBrowser(context,co.getValue());
                    }break;
                    //Mobile
                    case 1 : {
                        BaseFunctions.openDialer(context,co.getValue());
                    }break;
                    //Email
                    case 2 : {
                        BaseFunctions.openEmail(context,co.getValue());
                    }break;
                    //Facebook
                    case 3 : {
                        BaseFunctions.openFacebook(context,co.getValue());
                    }break;
                    //Twitter
                    case 4 : {
                        BaseFunctions.openTwitter(context,co.getValue());
                    }break;
                }
            }
        });
    }
}
