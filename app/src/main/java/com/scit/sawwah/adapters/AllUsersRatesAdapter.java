package com.scit.sawwah.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.iarcuschin.simpleratingbar.SimpleRatingBar;
import com.scit.sawwah.Models.RateObject;
import com.scit.sawwah.R;
import com.scit.sawwah.interfaces.IMove;
import com.scit.sawwah.tools.BaseFunctions;

import java.util.List;

/**
 * Created by Misheal on 8/28/2019.
 */

public class AllUsersRatesAdapter extends  RecyclerView.Adapter<AllUsersRatesAdapter.ViewHolder>{
    private List<RateObject> list;
    private Context context;
    private IMove iMove;


    public AllUsersRatesAdapter( Context context,List<RateObject> list,IMove iMove) {
        this.context = context;
        this.iMove = iMove;
        this.list = list;

    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_name;
        private TextView tv_comment;
        private TextView tv_date;
        private SimpleRatingBar rb_rate;

        public ViewHolder(View view) {
            super(view);
            tv_name = view.findViewById(R.id.item_rate_row_name);
            tv_comment = view.findViewById(R.id.item_rate_row_comment);
            tv_date = view.findViewById(R.id.item_rate_row_date);
            rb_rate = view.findViewById(R.id.item_rate_row_rate);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_user_rate_row, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        RateObject ro = list.get(position);
        holder.tv_name.setText(ro.getUser_name());
        holder.tv_comment.setText(ro.getNote());
        holder.tv_date.setText(BaseFunctions.dateExtractor(ro.getCreated_at()));
        holder.rb_rate.setRating(ro.getRate_value());
    }
}
