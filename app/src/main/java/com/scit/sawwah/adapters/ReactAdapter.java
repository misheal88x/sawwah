package com.scit.sawwah.adapters;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.scit.sawwah.Models.ReactionTypeObject;
import com.scit.sawwah.R;
import com.scit.sawwah.interfaces.IMove;
import com.scit.sawwah.tools.BaseFunctions;

import java.util.List;

/**
 * Created by Misheal on 9/14/2019.
 */

public class ReactAdapter extends  RecyclerView.Adapter<ReactAdapter.ViewHolder> {
    private List<ReactionTypeObject> list;
    private Context context;
    private IMove iMove;


    public ReactAdapter( Context context,List<ReactionTypeObject> list,IMove iMove) {
        this.context = context;
        this.iMove = iMove;
        this.list = list;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_title;
        private ImageView iv_image;
        private LinearLayout layout;

        public ViewHolder(View view) {
            super(view);
            tv_title = view.findViewById(R.id.item_react_txt);
            iv_image = view.findViewById(R.id.item_react_image);
            layout = view.findViewById(R.id.item_react_layout);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_react, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final ReactionTypeObject rto = list.get(position);
        holder.tv_title.setText(rto.getTitle());
        BaseFunctions.setGlideImage(context,holder.iv_image,rto.getImage_icon());
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iMove.move(position);
            }
        });
    }
}
