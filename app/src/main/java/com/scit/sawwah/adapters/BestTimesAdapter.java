package com.scit.sawwah.adapters;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.scit.sawwah.Models.MemorialImageObject;
import com.scit.sawwah.R;
import com.scit.sawwah.interfaces.IMove;
import com.scit.sawwah.tools.BaseFunctions;

import java.util.List;


public class BestTimesAdapter extends  RecyclerView.Adapter<BestTimesAdapter.ViewHolder> {
    private List<MemorialImageObject> list;
    private Context context;
    private IMove iMove;


    public BestTimesAdapter( Context context,List<MemorialImageObject> list,IMove iMove) {
        this.context = context;
        this.iMove = iMove;
        this.list = list;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_title;
        private ImageView iv_image;
        private TextView tv_date;


        public ViewHolder(View view) {
            super(view);
            tv_title = view.findViewById(R.id.best_time_desc);
            iv_image = view.findViewById(R.id.best_time_pic);
            tv_date = view.findViewById(R.id.best_time_date);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_best_times, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        MemorialImageObject mio  = list.get(position);
        if (mio.getImage_icon()!=null){
            BaseFunctions.setGlideImage(context,holder.iv_image,mio.getImage_icon());
        }
        if (mio.getFacility_name()!=null){
            holder.tv_title.setText(mio.getFacility_name());
        }
        if (mio.getCreated_at()!=null){
            holder.tv_date.setText(BaseFunctions.dateExtractor(mio.getCreated_at()));
        }
    }
}