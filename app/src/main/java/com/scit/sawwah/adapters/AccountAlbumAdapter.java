package com.scit.sawwah.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.glide.slider.library.svg.GlideApp;
import com.scit.sawwah.Models.MemorialImageObject;
import com.scit.sawwah.R;
import com.scit.sawwah.dialoges.ViewImageDialog;
import com.scit.sawwah.interfaces.IMove;

import java.util.List;

/**
 * Created by Misheal on 8/10/2019.
 */

public class AccountAlbumAdapter extends  RecyclerView.Adapter<AccountAlbumAdapter.ViewHolder>{
    private List<MemorialImageObject> list;
    private int type = 0;
    private Context context;
    private SharedPreferences sharedPreferences;
    private IMove iMove;


    public AccountAlbumAdapter( Context context,int type,List<MemorialImageObject> list,IMove iMove) {
        this.context = context;
        this.iMove = iMove;
        this.type = type;
        this.list = list;
        this.sharedPreferences = context.getSharedPreferences("Main",Context.MODE_PRIVATE);
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView iv_image;
        private ImageView img_edit;

        public ViewHolder(View view) {
            super(view);
            iv_image = view.findViewById(R.id.item_account_album_image);
            img_edit = view.findViewById(R.id.item_account_album_edit);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_account_album, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        try{
            GlideApp.with(context).load(list.get(position).getImage_icon())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .centerCrop().into(holder.iv_image);
        }catch (Exception e){}
        if (list.get(position).getImage_icon()!=null){
            if (!list.get(position).getImage_icon().equals("")){
                holder.iv_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ViewImageDialog dialog = new ViewImageDialog(context,list.get(position).getImage_icon());
                        dialog.show();
                    }
                });
            }
        }
        holder.img_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage(context.getResources().getString(R.string.memorial_image_delete_message)).
                        setPositiveButton(context.getResources().getString(R.string.add_offer_yes), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                iMove.move(position);
                            }
                        }).setNegativeButton(context.getResources().getString(R.string.add_offer_no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).show();
            }
        });
        if (type == 0){
            holder.img_edit.setVisibility(View.GONE);
        }
    }
}
