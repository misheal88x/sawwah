package com.scit.sawwah.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.scit.sawwah.Models.SliderObject;
import com.scit.sawwah.R;
import com.scit.sawwah.interfaces.IMove;
import com.scit.sawwah.tools.BaseFunctions;

import java.util.List;

/**
 * Created by Misheal on 8/6/2019.
 */

public class HomeSpecialOffersAdapter extends  RecyclerView.Adapter<HomeSpecialOffersAdapter.ViewHolder> {
    private List<SliderObject> listOfSliders;
    private String[] offers = new String[]{"Discount three days on hotel reservation",
            "Discount three days on hotel reservation",
            "Discount three days on hotel reservation"};
    private String[] dscounts = new String[]{"50%","50%","50%"};
    private Context context;
    private IMove iMove;


    public HomeSpecialOffersAdapter( Context context,List<SliderObject> list,IMove iMove) {
        this.context = context;
        this.iMove = iMove;
        this.listOfSliders = list;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView iv_image;
        private TextView tv_text;
        private TextView tv_discount;

        public ViewHolder(View view) {
            super(view);
            iv_image = view.findViewById(R.id.item_home_special_offer_image);
            tv_text = view.findViewById(R.id.item_home_special_offer_text);
            tv_discount = view.findViewById(R.id.item_home_special_offer_discount);
        }
    }

    @Override
    public int getItemCount() {
        return listOfSliders.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_home_special_offer, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        SliderObject so = listOfSliders.get(position);
        if (so.getVlaue()!=null){
            BaseFunctions.setGlideImage(context,holder.iv_image,so.getVlaue());
        }
        if (so.getData()!=null){
            holder.tv_text.setText(so.getData());
        }
        holder.tv_discount.setText(String.valueOf(so.getDiscount())+"%");
    }
}
