package com.scit.sawwah.adapters;

import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.scit.sawwah.Models.InstitutionObject;
import com.scit.sawwah.R;
import com.scit.sawwah.activities.ProfileActivity;
import com.scit.sawwah.interfaces.IMove;
import com.scit.sawwah.tools.BaseFunctions;

import java.util.List;

/**
 * Created by Misheal on 8/13/2019.
 */

public class SearchInstitutionsAdapter extends RecyclerView.Adapter<SearchInstitutionsAdapter.ViewHolder> {
    private List<InstitutionObject> list;
    private int[] images = new int[]{R.drawable.offerone};
    private Context context;
    private IMove iMove;


    public SearchInstitutionsAdapter( Context context,List<InstitutionObject> list,IMove iMove) {
        this.context = context;
        this.iMove = iMove;
        this.list = list;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView iv_image;
        private CardView layout;

        public ViewHolder(View view) {
            super(view);
            iv_image = view.findViewById(R.id.item_search_institution_image);
            layout = view.findViewById(R.id.item_search_institution_layout);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_search_institution, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        //todo it should be getCover but in profileActivity it is image_icon
        BaseFunctions.setGlideImage(context,holder.iv_image,list.get(position).getImage_icon());
        holder.iv_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ProfileActivity.class);
                intent.putExtra("institution_id",list.get(position).getId());
                context.startActivity(intent);
            }
        });
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,ProfileActivity.class);
                intent.putExtra("institution_id",list.get(position).getId());
                context.startActivity(intent);
            }
        });
    }
}
