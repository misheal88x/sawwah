package com.scit.sawwah.adapters;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.scit.sawwah.Models.OfferObject;
import com.scit.sawwah.R;
import com.scit.sawwah.activities.OfferDetailsActivity;
import com.scit.sawwah.interfaces.IMove;
import com.scit.sawwah.tools.BaseFunctions;

import java.util.List;


public class FavoriteOffersAdapter extends RecyclerView.Adapter<FavoriteOffersAdapter.ViewHolder> {
    private List<OfferObject> list;
    private Context context;
    private IMove iMove;


    public FavoriteOffersAdapter( Context context,List<OfferObject> list,IMove iMove) {
        this.context = context;
        this.iMove = iMove;
        this.list = list;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_name;
        private ImageView iv_image;
        private LinearLayout layout;

        public ViewHolder(View view) {
            super(view);
            tv_name = view.findViewById(R.id.offer_desc);
            iv_image = view.findViewById(R.id.offer_pic);
            layout = view.findViewById(R.id.item_favoutite_offer_layout);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_favorite, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final OfferObject oo = list.get(position);
        if (oo.getTitle()!=null){
            holder.tv_name.setText(oo.getTitle());
        }
        if (oo.getImage()!=null){
            BaseFunctions.setGlideImage(context,holder.iv_image,oo.getImage());
        }
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, OfferDetailsActivity.class);
                intent.putExtra("offer_id",oo.getId());
                context.startActivity(intent);
            }
        });
    }
}