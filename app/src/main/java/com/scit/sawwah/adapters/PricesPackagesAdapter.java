package com.scit.sawwah.adapters;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.scit.sawwah.Models.PricePackageObject;
import com.scit.sawwah.R;
import com.scit.sawwah.activities.ComfirmPackageOrder;
import com.scit.sawwah.tools.BaseFunctions;

import java.util.List;

/**
 * Created by Misheal on 8/20/2019.
 */

public class PricesPackagesAdapter extends PagerAdapter {
    //Looks like SpecialAdvertisementsAdapter
    private Context context;
    private LayoutInflater layoutInflater;
    private List<PricePackageObject> list;
    TextView tv_name,tv_num_offers,tv_num_ads,tv_price,tv_during,tv_offer_during,tv_ads_during;
    private ImageView img_icon;
    RelativeLayout btn_confirm;
    private LinearLayout lyt_offers_txt_layout,lyt_ads_txt_layout;
    private boolean doNotifyDataSetChangedOnce = false;


    public PricesPackagesAdapter(Context context,List<PricePackageObject> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(final ViewGroup container, int position) {
        layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.item_price_package,null);
        init_views(view);
        final PricePackageObject ppo = list.get(position);
        if (ppo.getName() != null){
            tv_name.setText(ppo.getName());
        }
        if (ppo.getImage_icon()!=null){
            BaseFunctions.setGlideImage(context,img_icon,ppo.getImage_icon());
        }
        tv_price.setText(ppo.getCost()+" "+context.getResources().getString(R.string.prices_packages_pound));
        tv_during.setText(String.valueOf(ppo.getDuration())+" "+context.getResources().getString(R.string.prices_packages_days));
        switch (ppo.getType()){
            //Ads
            case 0 : {
                //lyt_offers_txt_layout.setVisibility(View.GONE);
                //tv_num_offers.setVisibility(View.GONE);
                //tv_offer_during.setVisibility(View.GONE);
                tv_num_offers.setText("0");
                tv_offer_during.setText("0 "+context.getResources().getString(R.string.prices_packages_days));
                tv_num_ads.setText(String.valueOf(ppo.getAp_ads_num()));
                tv_ads_during.setText(String.valueOf(ppo.getAp_ads_duration())+" "+context.getResources().getString(R.string.prices_packages_days));
            }break;
            //Offers
            case 1 : {
                //lyt_ads_txt_layout.setVisibility(View.GONE);
                //tv_num_ads.setVisibility(View.GONE);
                //tv_ads_during.setVisibility(View.GONE);
                tv_num_ads.setText("0");
                tv_ads_during.setText("0 "+context.getResources().getString(R.string.prices_packages_days));
                tv_num_offers.setText(String.valueOf(ppo.getOp_offer_num()));
                tv_offer_during.setText(String.valueOf(ppo.getOp_offer_duration())+" "+context.getResources().getString(R.string.prices_packages_days));
            }break;
            //Ads + Offers
            case 2 : {
                tv_num_ads.setText(String.valueOf(ppo.getAop_ads_num()));
                tv_ads_during.setText(String.valueOf(ppo.getAop_ads_duration())+" "+context.getResources().getString(R.string.prices_packages_days));
                tv_num_offers.setText(String.valueOf(ppo.getAop_offer_num()));
                tv_offer_during.setText(String.valueOf(ppo.getIaop_offer_durationd())+" "+context.getResources().getString(R.string.prices_packages_days));
            }break;
        }
        btn_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,ComfirmPackageOrder.class);
                intent.putExtra("package_id",ppo.getId());
                context.startActivity(intent);
            }
        });
        ViewPager viewPager = (ViewPager)container;
        viewPager.addView(view,0);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ViewPager viewPager = (ViewPager) container;
        View view = (View) object;
        viewPager.removeView(view);
    }

    private void init_views(View view){
        tv_name = view.findViewById(R.id.item_price_package_name);
        tv_num_offers = view.findViewById(R.id.item_price_package_num_offers);
        tv_num_ads = view.findViewById(R.id.item_price_package_num_ads);
        tv_price = view.findViewById(R.id.item_price_package_price);
        tv_during = view.findViewById(R.id.item_price_package_duration);
        tv_offer_during = view.findViewById(R.id.item_price_package_duration_offers);
        tv_ads_during = view.findViewById(R.id.item_price_package_duration_ads);
        img_icon = view.findViewById(R.id.item_price_package_image);
        lyt_offers_txt_layout = view.findViewById(R.id.item_price_package_offers_txt_layout);
        lyt_ads_txt_layout = view.findViewById(R.id.item_price_package_ads_txt_layout);
        btn_confirm = view.findViewById(R.id.item_price_offer_order_btn);
    }
}
