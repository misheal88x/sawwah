package com.scit.sawwah.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.scit.sawwah.Models.PricePackageObject;
import com.scit.sawwah.R;
import com.scit.sawwah.interfaces.IMove;

import java.util.List;

/**
 * Created by Misheal on 8/20/2019.
 */

public class AccountOwnerSettingsPackagesAdapter extends  RecyclerView.Adapter<AccountOwnerSettingsPackagesAdapter.ViewHolder> {
    private Context context;
    private IMove iMove;
    private List<PricePackageObject> list;

    public AccountOwnerSettingsPackagesAdapter( Context context,List<PricePackageObject> list,IMove iMove) {
        this.context = context;
        this.iMove = iMove;
        this.list = list;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_title;

        public ViewHolder(View view) {
            super(view);
            tv_title = view.findViewById(R.id.item_account_owner_settings_package_name);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_account_owner_settings_package, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        holder.tv_title.setText(list.get(position).getName());
    }
}
