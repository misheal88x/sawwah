package com.scit.sawwah.adapters;

import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.scit.sawwah.R;
import com.scit.sawwah.activities.ActivePricePackageActivity;

/**
 * Created by Misheal on 8/25/2019.
 */

public class PricesPackagesManageSubAdapter extends  RecyclerView.Adapter<PricesPackagesManageSubAdapter.ViewHolder> {
    private String title = "Gold package";
    private String[] status = new String[]{"Active","Not active","Active"};
    private Context context;

    public PricesPackagesManageSubAdapter( Context context) {
        this.context = context;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_name,tv_status,tv_renew;


        public ViewHolder(View view) {
            super(view);
            tv_name = view.findViewById(R.id.item_sub_manage_prices_packages_name);
            tv_status = view.findViewById(R.id.item_sub_manage_prices_packages_status);
            tv_renew = view.findViewById(R.id.item_sub_manage_prices_packages_renew);
        }
    }

    @Override
    public int getItemCount() {
        return 3;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_sub_manage_prices_packages, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        holder.tv_name.setText(title);
        holder.tv_status.setText(status[position]);
        holder.tv_renew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, ActivePricePackageActivity.class));
            }
        });
    }


}
