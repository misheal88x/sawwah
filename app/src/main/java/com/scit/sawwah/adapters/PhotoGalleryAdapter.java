package com.scit.sawwah.adapters;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.scit.sawwah.Models.ImageObject;
import com.scit.sawwah.R;
import com.scit.sawwah.interfaces.IMove;
import com.scit.sawwah.tools.BaseFunctions;

import java.util.List;

/**
 * Created by Misheal on 8/20/2018.
 */

public class PhotoGalleryAdapter extends  RecyclerView.Adapter<PhotoGalleryAdapter.ViewHolder> {
    private List<ImageObject> list;
    private Context context;
    private IMove iMove;


    public PhotoGalleryAdapter( Context context,List<ImageObject> list,IMove iMove) {
        this.context = context;
        this.iMove = iMove;
        this.list = list;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView iv_image;
        public ViewHolder(View view) {
            super(view);
            iv_image = view.findViewById(R.id.item_photo_gallery_image);

        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_photo_gallery, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        ImageObject oo = list.get(position);
        if (oo.getSrc()!=null){
            BaseFunctions.setGlideImage(context,holder.iv_image,oo.getSrc());
        }
    }
}
