package com.scit.sawwah.adapters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.glide.slider.library.svg.GlideApp;
import com.scit.sawwah.APIClasses.FavouriteAPIsClass;
import com.scit.sawwah.Models.AdvertisementObject;
import com.scit.sawwah.R;
import com.scit.sawwah.activities.AdvertisementDetailsActivity;
import com.scit.sawwah.dialoges.VisitorDialog;
import com.scit.sawwah.interfaces.IFailure;
import com.scit.sawwah.interfaces.IMove;
import com.scit.sawwah.interfaces.IResponse;

import java.util.List;

/**
 * Created by Misheal on 8/15/2019.
 */

public class AllAdvertisementsAdapter extends  RecyclerView.Adapter<AllAdvertisementsAdapter.ViewHolder> {
    private List<AdvertisementObject> list;
    private Context context;
    private IMove iMove;
    private int lovers = 0;
    private SharedPreferences sharedPreferences;


    public AllAdvertisementsAdapter( Context context,List<AdvertisementObject> list,IMove iMove) {
        this.context = context;
        this.iMove = iMove;
        this.list = list;
        this.sharedPreferences = context.getSharedPreferences("Main",Context.MODE_PRIVATE);
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_title;
        private TextView tv_inst_name;
        private ImageView img_icon;
        private TextView tv_desc;
        private ImageView img_love;
        private TextView tv_num_lovers;
        private ImageView img_share;
        private LinearLayout layout;

        public ViewHolder(View view) {
            super(view);
            tv_title = view.findViewById(R.id.item_all_advertisement_title_txt);
            tv_inst_name = view.findViewById(R.id.item_all_advertisement_institution_name_txt);
            img_icon = view.findViewById(R.id.item_all_advertisement_icon);
            tv_desc = view.findViewById(R.id.item_all_advertisement_desc);
            img_love = view.findViewById(R.id.item_all_advertisement_love);
            tv_num_lovers = view.findViewById(R.id.item_all_advertisement_lovers);
            img_share = view.findViewById(R.id.item_all_advertisement_share);
            layout = view.findViewById(R.id.item_all_advertisement_layout);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_all_advertisement, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final AdvertisementObject ao  = list.get(position);
        holder.tv_title.setText(ao.getTitle());
        holder.tv_inst_name.setText(ao.getFacility_name());
        try{
            GlideApp.with(context).load(ao.getFacility_image())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .centerCrop().into(holder.img_icon);
        }catch (Exception e){}
        holder.tv_desc.setText(ao.getDetails());
        if (ao.getMarked_as_favourite()==false){
            holder.img_love.setImageResource(R.drawable.ic_grey_heart);
        }else {
            holder.img_love.setImageResource(R.drawable.ic_heart);
        }
        lovers = ao.getFavourite_count();
        holder.tv_num_lovers.setText(String.valueOf(ao.getFavourite_count()));
        holder.img_love.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ao.getMarked_as_favourite() == false){
                    toggle_item(holder,String.valueOf(ao.getId()),"3","1");
                }else {
                    toggle_item(holder,String.valueOf(ao.getId()),"3","0");
                }


            }
        });
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sharedPreferences.getBoolean("is_visitor",false)){
                    VisitorDialog dialog = new VisitorDialog(context);
                    dialog.show();
                }else {
                    Intent intent = new Intent(context, AdvertisementDetailsActivity.class);
                    intent.putExtra("advertisement_id",ao.getId());
                    context.startActivity(intent);
                }
            }
        });
        holder.img_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iMove.move(position);
            }
        });
    }

    private void toggle_item(final ViewHolder holder, String id, String type, final String state){
        FavouriteAPIsClass.toggle_item(sharedPreferences,
                context,
                sharedPreferences.getString("language", "ar"),
                sharedPreferences.getString("service_token", ""),
                id,
                type,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json !=null){
                            if (state.equals("0")){
                                holder.img_love.setImageResource(R.drawable.ic_grey_heart);
                                lovers--;
                                holder.tv_num_lovers.setText(String.valueOf(lovers));
                                Toast.makeText(context, context.getResources().getString(R.string.favourite_ads_remover), Toast.LENGTH_SHORT).show();
                            }else {
                                holder.img_love.setImageResource(R.drawable.ic_heart);
                                lovers++;
                                holder.tv_num_lovers.setText(String.valueOf(lovers));
                                Toast.makeText(context, context.getResources().getString(R.string.favourite_ads_add), Toast.LENGTH_SHORT).show();
                            }
                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.error_occurred), Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                    }
                });
    }
}
