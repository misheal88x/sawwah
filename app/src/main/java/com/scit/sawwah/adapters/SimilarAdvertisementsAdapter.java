package com.scit.sawwah.adapters;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.scit.sawwah.Models.AdvertisementObject;
import com.scit.sawwah.R;
import com.scit.sawwah.activities.AdvertisementDetailsActivity;
import com.scit.sawwah.interfaces.IMove;

import java.util.List;

/**
 * Created by Misheal on 8/8/2019.
 */

public class SimilarAdvertisementsAdapter extends  RecyclerView.Adapter<SimilarAdvertisementsAdapter.ViewHolder> {
    private List<AdvertisementObject> list;
    //private String title = "Institution name";
    //private String details = "This text is a sample could be replaced in the same area....This text is a sample could be replaced in the same area....";
    private Context context;
    private IMove iMove;


    public SimilarAdvertisementsAdapter( Context context,List<AdvertisementObject> list,IMove iMove) {
        this.context = context;
        this.iMove = iMove;
        this.list = list;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_name,tv_details;
        private LinearLayout layout;

        public ViewHolder(View view) {
            super(view);
            tv_name = view.findViewById(R.id.item_similar_advertisement_name);
            tv_details = view.findViewById(R.id.item_similar_advertisement_details);
            layout = view.findViewById(R.id.item_similar_advertisement_layout);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_similar_advertisement, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final AdvertisementObject ao = list.get(position);
        holder.tv_name.setText(ao.getTitle());
        holder.tv_details.setText(ao.getDetails());
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(context, AdvertisementDetailsActivity.class);
                intent.putExtra("advetisement_id",ao.getId());
                context.startActivity(intent);
            }
        });
    }
}
