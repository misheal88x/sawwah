package com.scit.sawwah.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.glide.slider.library.svg.GlideApp;
import com.scit.sawwah.Models.CategoryObject;
import com.scit.sawwah.R;
import com.scit.sawwah.interfaces.IMove;

import java.util.List;

/**
 * Created by Misheal on 9/5/2019.
 */

public class InstitutionsCategoriesAdapter extends  RecyclerView.Adapter<InstitutionsCategoriesAdapter.ViewHolder> {
    private List<CategoryObject> list;
    private Context context;
    private IMove iMove;


    public InstitutionsCategoriesAdapter( Context context,List<CategoryObject> list,IMove iMove) {
        this.context = context;
        this.iMove = iMove;
        this.list = list;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView iv_image;
        private TextView tv_text;
        private LinearLayout layout;

        public ViewHolder(View view) {
            super(view);
            iv_image = view.findViewById(R.id.item_inst_category_icon);
            tv_text = view.findViewById(R.id.item_inst_category_name);
            layout = view.findViewById(R.id.item_inst_category_layout);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_inst_category, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        CategoryObject co = list.get(position);
        try{
            GlideApp.with(context).load(co.getImage_icon())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .centerCrop().into(holder.iv_image);
        }catch (Exception e){}
        holder.tv_text.setText(co.getName());
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iMove.move(position);
            }
        });
    }
}
