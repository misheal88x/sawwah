package com.scit.sawwah.adapters;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.scit.sawwah.Models.PricePackageObject;
import com.scit.sawwah.R;
import com.scit.sawwah.activities.ActivePricePackageActivity;

import java.util.List;

/**
 * Created by Misheal on 8/25/2019.
 */

public class PricesPackagesManageAdapter extends PagerAdapter {

    private Context context;
    private LayoutInflater layoutInflater;
    private List<PricePackageObject> list;
    private LinearLayout package1_layout;
    private TextView package1_name,package1_status,package1_renew;
    private LinearLayout package2_layout;
    private TextView package2_name,package2_status,package2_renew;
    private LinearLayout package3_layout;
    private TextView package3_name,package3_status,package3_renew;

    public PricesPackagesManageAdapter(Context context,List<PricePackageObject> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        int x = list.size()/3;
        int y = 0;
        if (list.size()%3 >0){
            y = 1;
        }
        return x+y;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(final ViewGroup container, int position) {
        layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.item_manage_prices_packages,null);
        init_views(view);
        set_data(position);

        ViewPager viewPager = (ViewPager)container;
        viewPager.addView(view,0);
        return view;
    }


    private void init_views(View view) {
        package1_layout = view.findViewById(R.id.item_price_package_manage_pack1_layout);
        package1_name = view.findViewById(R.id.item_price_package_manage_pack1_name);
        package1_status = view.findViewById(R.id.item_price_package_manage_pack1_status);
        package1_renew = view.findViewById(R.id.item_price_package_manage_pack1_renew);

        package2_layout = view.findViewById(R.id.item_price_package_manage_pack2_layout);
        package2_name = view.findViewById(R.id.item_price_package_manage_pack2_name);
        package2_status = view.findViewById(R.id.item_price_package_manage_pack2_status);
        package2_renew = view.findViewById(R.id.item_price_package_manage_pack2_renew);

        package3_layout = view.findViewById(R.id.item_price_package_manage_pack3_layout);
        package3_name = view.findViewById(R.id.item_price_package_manage_pack3_name);
        package3_status = view.findViewById(R.id.item_price_package_manage_pack3_status);
        package3_renew = view.findViewById(R.id.item_price_package_manage_pack3_renew);
    }

    private void set_data(int position){
        int x = list.size()/3;
        int y = 0;
        if (list.size()%3 >0){
            y = 1;
        }
        if (position == x+y-1){
            if (list.size()%3==1){
                package2_layout.setVisibility(View.INVISIBLE);
                package3_layout.setVisibility(View.INVISIBLE);
            }else if (list.size()%3==2){
                package3_layout.setVisibility(View.INVISIBLE);
            }
        }
        PricePackageObject package1 = new PricePackageObject();
        PricePackageObject package2 = new PricePackageObject();
        PricePackageObject package3 = new PricePackageObject();
            try {
                package1 = list.get((position*3)+0);
            package1_name.setText(package1.getName());
            switch (package1.getStatus()){
                case -1 : {
                    package1_status.setText(context.getResources().getString(R.string.prices_packages_refused));
                }break;
                case 0 : {
                    package1_status.setText(context.getResources().getString(R.string.prices_packages_waiting));
                }break;
                case 1 : {
                    package1_status.setText(context.getResources().getString(R.string.prices_packages_wait_for_money));
                }break;
                case 2 : {
                    package1_status.setText(context.getResources().getString(R.string.prices_packages_active));
                }break;
            }
        }catch (Exception e){}
            try {
                package2 = list.get((position*3)+1);
                package2_name.setText(package2.getName());
                switch (package2.getStatus()){
                    case -1 : {
                        package2_status.setText(context.getResources().getString(R.string.prices_packages_refused));
                    }break;
                    case 0 : {
                        package2_status.setText(context.getResources().getString(R.string.prices_packages_waiting));
                    }break;
                    case 1 : {
                        package2_status.setText(context.getResources().getString(R.string.prices_packages_wait_for_money));
                    }break;
                    case 2 : {
                        package2_status.setText(context.getResources().getString(R.string.prices_packages_active));
                    }break;
                }
            }catch (Exception e){}
            try {
                package3 = list.get((position*3)+2);
                package3_name.setText(package3.getName());
                switch (package3.getStatus()){
                    case -1 : {
                        package3_status.setText(context.getResources().getString(R.string.prices_packages_refused));
                    }break;
                    case 0 : {
                        package3_status.setText(context.getResources().getString(R.string.prices_packages_waiting));
                    }break;
                    case 1 : {
                        package3_status.setText(context.getResources().getString(R.string.prices_packages_wait_for_money));
                    }break;
                    case 2 : {
                        package3_status.setText(context.getResources().getString(R.string.prices_packages_active));
                    }break;
                }
            }catch (Exception e){}

        final PricePackageObject finalPackage1 = package1;
        final PricePackageObject finalPackage2 = package2;
        final PricePackageObject finalPackage3 = package3;
        package1_renew.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (finalPackage1.getStatus()!=2){
                        Intent intent = new Intent(context, ActivePricePackageActivity.class);
                        intent.putExtra("package_id", finalPackage1.getId());
                        intent.putExtra("package_name", finalPackage1.getName());
                        intent.putExtra("package_status", finalPackage1.getStatus());
                        context.startActivity(intent);
                    }else {
                        Toast.makeText(context, context.getResources().getString(R.string.confirm_package_order_no_renew), Toast.LENGTH_SHORT).show();
                    }
                }
            });
            package2_renew.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (finalPackage2.getStatus()!=2){
                        Intent intent = new Intent(context, ActivePricePackageActivity.class);
                        intent.putExtra("package_id",finalPackage2.getId());
                        intent.putExtra("package_name",finalPackage2.getName());
                        intent.putExtra("package_status",finalPackage2.getStatus());
                        context.startActivity(intent);
                    }else {
                        Toast.makeText(context, context.getResources().getString(R.string.confirm_package_order_no_renew), Toast.LENGTH_SHORT).show();
                    }
                }
            });
            package3_renew.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (finalPackage3.getStatus()!=2){
                        Intent intent = new Intent(context, ActivePricePackageActivity.class);
                        intent.putExtra("package_id",finalPackage3.getId());
                        intent.putExtra("package_name",finalPackage3.getName());
                        intent.putExtra("package_status",finalPackage3.getStatus());
                        context.startActivity(intent);
                    }else {
                        Toast.makeText(context, context.getResources().getString(R.string.confirm_package_order_no_renew), Toast.LENGTH_SHORT).show();
                    }
                }
            });
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ViewPager viewPager = (ViewPager) container;
        View view = (View) object;
        viewPager.removeView(view);
    }
}
