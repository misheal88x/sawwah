package com.scit.sawwah.adapters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.glide.slider.library.svg.GlideApp;
import com.google.gson.Gson;
import com.scit.sawwah.Models.MemorialImageObject;
import com.scit.sawwah.Models.ReactionTypeObject;
import com.scit.sawwah.R;
import com.scit.sawwah.activities.StoryViewActivity;
import com.scit.sawwah.dialoges.VisitorDialog;
import com.scit.sawwah.interfaces.IMove;
import com.scit.sawwah.tools.BaseFunctions;

import java.util.List;

/**
 * Created by Misheal on 8/6/2019.
 */

public class HomeMemorialPhotosAdapter extends  RecyclerView.Adapter<HomeMemorialPhotosAdapter.ViewHolder>{
    private List<MemorialImageObject> listOfImages;
    private Context context;
    private IMove iMove;
    private SharedPreferences sharedPreferences ;


    public HomeMemorialPhotosAdapter( Context context,List<MemorialImageObject> list,IMove iMove) {
        this.context = context;
        this.iMove = iMove;
        this.listOfImages = list;
        this.sharedPreferences = context.getSharedPreferences("Main",Context.MODE_PRIVATE);
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_name;
        private TextView tv_date;
        private ImageView iv_image;
        private ImageView iv_emoji;
        private LinearLayout layout;

        public ViewHolder(View view) {
            super(view);
            tv_name = view.findViewById(R.id.item_home_memoral_name);
            tv_date = view.findViewById(R.id.item_home_memoral_date);
            iv_image = view.findViewById(R.id.item_home_memoral_image);
            iv_emoji = view.findViewById(R.id.item_home_memoral_emoji);
            layout = view.findViewById(R.id.item_home_memoral_layout);
        }
    }

    @Override
    public int getItemCount() {
        return listOfImages.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_home_memoral, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final MemorialImageObject meo = listOfImages.get(position);
        if (meo.getFacility_name()!=null&&!meo.getFacility_name().equals("")){
            holder.tv_name.setText(meo.getFacility_name());
        }else {
            holder.tv_name.setText(meo.getOwner_name());
        }

        holder.tv_date.setText(BaseFunctions.dateExtractor(meo.getCreated_at()));
        try{
            GlideApp.with(context).load(meo.getImage_icon())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .centerCrop().into(holder.iv_image);
        }catch (Exception e){}
        ReactionTypeObject[] reactions_array = new Gson().fromJson(sharedPreferences.getString("reactions","[]"),ReactionTypeObject[].class);
        if (reactions_array.length > 0){
            for (ReactionTypeObject rto : reactions_array){
                if (meo.getUser_reaction() == rto.getId()){
                    BaseFunctions.setGlideImage(context,holder.iv_emoji,rto.getImage_icon());
                    break;
                }
            }
        }
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sharedPreferences.getBoolean("is_visitor",false)){
                    VisitorDialog dialog = new VisitorDialog(context);
                    dialog.show();
                }else {
                    Intent intent = new Intent(context, StoryViewActivity.class);
                    String list_json = new Gson().toJson(listOfImages);
                    intent.putExtra("images",list_json);
                    intent.putExtra("position",position);
                    intent.putExtra("user_react",meo.getUser_reaction());
                    intent.putExtra("date",meo.getCreated_at());
                    context.startActivity(intent);
                }

            }
        });
    }
}
