package com.scit.sawwah.adapters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.glide.slider.library.svg.GlideApp;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;
import com.scit.sawwah.APIClasses.FavouriteAPIsClass;
import com.scit.sawwah.Models.InstitutionObject;
import com.scit.sawwah.R;
import com.scit.sawwah.activities.ProfileActivity;
import com.scit.sawwah.interfaces.IFailure;
import com.scit.sawwah.interfaces.IMove;
import com.scit.sawwah.interfaces.IResponse;

import java.util.List;

/**
 * Created by Misheal on 8/10/2019.
 */

public class CategoryInstitutionsAdapter extends  RecyclerView.Adapter<CategoryInstitutionsAdapter.ViewHolder> {
    private List<InstitutionObject> list;
    private Context context;
    private IMove iMove;
    private int lovers = 0,temp_lovers = 0;
    private SharedPreferences sharedPreferences;


    public CategoryInstitutionsAdapter( Context context,List<InstitutionObject> list,IMove iMove) {
        this.context = context;
        this.iMove = iMove;
        this.list = list;
        this.sharedPreferences = context.getSharedPreferences("Main",Context.MODE_PRIVATE);
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView iv_image;
        private TextView tv_name;
        private TextView tv_far;
        private ImageView img_heart;
        private TextView tv_num_people,tv_you_and,tv_add_to;
        private SimpleRatingBar rb_rate;
        private LinearLayout layout,btn_share;

        public ViewHolder(View view) {
            super(view);
            iv_image = view.findViewById(R.id.item_institution_row_image);
            tv_name = view.findViewById(R.id.item_institution_row_name);
            tv_far = view.findViewById(R.id.item_institution_row_far);
            tv_num_people = view.findViewById(R.id.item_institution_row_num_add_fav);
            rb_rate = view.findViewById(R.id.item_institution_row_rate);
            img_heart = view.findViewById(R.id.item_institution_row_heart);
            tv_you_and = view.findViewById(R.id.item_institution_row_you_and);
            tv_add_to = view.findViewById(R.id.item_institution_row_add_to_fav);
            layout = view.findViewById(R.id.item_institution_row_layout);
            btn_share = view.findViewById(R.id.item_institution_row_share);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_institution_row, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final InstitutionObject io = list.get(position);
        try{
            GlideApp.with(context).load(io.getImage_icon())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .centerCrop().into(holder.iv_image);
        }catch (Exception e){}
        holder.tv_name.setText(io.getName());
        if (Math.round(io.getDistance())>900){
            holder.tv_far.setText(context.getResources().getString(R.string.item_institution_row_far_from_you)+
                    " "+String.valueOf(Math.round(io.getDistance()/1000))+" "+context.getResources().getString(R.string.kilo_meter));
        }else {
            holder.tv_far.setText(context.getResources().getString(R.string.item_institution_row_far_from_you)+
                    " "+String.valueOf(Math.round(io.getDistance()))+" "+context.getResources().getString(R.string.meter));
        }
        lovers = io.getFavourite_count();
        temp_lovers = io.getFavourite_count();
        if (io.getMarked_as_favourite() == false){
            holder.tv_you_and.setVisibility(View.GONE);
            holder.img_heart.setImageResource(R.drawable.ic_grey_heart);
            holder.tv_num_people.setText(String.valueOf(io.getFavourite_count())+" ");
        }else {
            if (lovers >= 2){
                lovers--;
                holder.tv_num_people.setText(" "+String.valueOf(lovers)+" ");
            }else {
                holder.tv_num_people.setVisibility(View.GONE);
                holder.tv_you_and.setVisibility(View.GONE);
                holder.tv_add_to.setVisibility(View.GONE);
            }
            holder.img_heart.setImageResource(R.drawable.ic_heart);
        }
        holder.rb_rate.setRating(io.getRating());
        holder.img_heart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (io.getMarked_as_favourite() == false){
                    toggle_item(holder,String.valueOf(io.getId()),"1","1");
                }else {
                    toggle_item(holder,String.valueOf(io.getId()),"1","0");
                }

            }
        });
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,ProfileActivity.class);
                intent.putExtra("institution_id",io.getId());
                context.startActivity(intent);
            }
        });
        holder.btn_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iMove.move(position);
            }
        });
    }

    private void toggle_item(final ViewHolder holder, String id, String type, final String state){
        FavouriteAPIsClass.toggle_item(sharedPreferences,
                context,
                sharedPreferences.getString("language", "ar"),
                sharedPreferences.getString("service_token", ""),
                id,
                type,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json !=null){
                            if (state.equals("0")){
                                temp_lovers--;
                                holder.tv_num_people.setText(String.valueOf(temp_lovers));
                                holder.img_heart.setImageResource(R.drawable.ic_grey_heart);
                                holder.tv_you_and.setVisibility(View.GONE);
                                Toast.makeText(context, context.getResources().getString(R.string.favourite_inst_remover), Toast.LENGTH_SHORT).show();
                            }else {
                                temp_lovers++;
                                if (temp_lovers>=2){
                                    holder.tv_you_and.setVisibility(View.VISIBLE);
                                    holder.tv_num_people.setVisibility(View.VISIBLE);
                                    holder.tv_num_people.setText(String.valueOf(temp_lovers-1));
                                    holder.tv_add_to.setVisibility(View.VISIBLE);
                                }else {
                                    holder.tv_you_and.setVisibility(View.GONE);
                                    holder.tv_num_people.setVisibility(View.GONE);
                                    holder.tv_add_to.setVisibility(View.GONE);
                                }
                                holder.img_heart.setImageResource(R.drawable.ic_heart);
                                Toast.makeText(context, context.getResources().getString(R.string.favourite_inst_add), Toast.LENGTH_SHORT).show();
                            }
                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.error_occurred), Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                    }
                });
    }
}
