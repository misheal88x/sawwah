package com.scit.sawwah.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import androidx.annotation.NonNull;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.rm.rmswitch.RMSwitch;
import com.scit.sawwah.APIClasses.AdvertisementsAPIsClass;
import com.scit.sawwah.Models.AdvertisementObject;
import com.scit.sawwah.R;
import com.scit.sawwah.activities.AdvertisementDetailsActivity;
import com.scit.sawwah.interfaces.IFailure;
import com.scit.sawwah.interfaces.IMove;
import com.scit.sawwah.interfaces.IResponse;
import com.scit.sawwah.tools.BaseFunctions;

import java.util.List;

/**
 * Created by Misheal on 8/28/2019.
 */

public class EditInstAdvertisementsAdapter extends  RecyclerView.Adapter<EditInstAdvertisementsAdapter.ViewHolder> {
    private List<AdvertisementObject> list;
    private Context context;
    private IMove iMove;
    private SharedPreferences sharedPreferences;
    private RelativeLayout root;


    public EditInstAdvertisementsAdapter( Context context,List<AdvertisementObject> list,RelativeLayout root,IMove iMove) {
        this.context = context;
        this.iMove = iMove;
        this.list = list;
        this.root = root;
        this.sharedPreferences = context.getSharedPreferences("Main",Context.MODE_PRIVATE);
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_title;
        private TextView tv_inst_name;
        private ImageView tv_inst_icon;
        private TextView tv_desc;
        private TextView tv_start_date;
        private TextView tv_end_date;
        private LinearLayout btn_delete;
        private RMSwitch sw_show;
        private LinearLayout layout;

        public ViewHolder(View view) {
            super(view);
            tv_title = view.findViewById(R.id.item_edit_inst_ads_title_txt);
            tv_inst_name = view.findViewById(R.id.item_edit_inst_ads_institution_name_txt);
            tv_inst_icon = view.findViewById(R.id.item_edit_inst_ads_icon);
            tv_desc = view.findViewById(R.id.item_edit_inst_ads_desc);
            tv_start_date = view.findViewById(R.id.item_edit_inst_ads_start_date);
            tv_end_date = view.findViewById(R.id.item_edit_inst_ads_end_date);
            btn_delete = view.findViewById(R.id.item_edit_inst_ads_delete);
            sw_show = view.findViewById(R.id.item_edit_inst_ads_show_switch);
            sw_show.setSwitchBkgCheckedColor(context.getResources().getColor(R.color.colorTextSecobdary));
            sw_show.setSwitchToggleCheckedColor(Color.WHITE);
            sw_show.setSwitchToggleNotCheckedColor(Color.WHITE);
            layout = view.findViewById(R.id.item_edit_inst_ads_layout);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_edit_inst_ads, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final AdvertisementObject ao = list.get(position);

        holder.tv_title.setText(ao.getTitle());
        holder.tv_inst_name.setText(ao.getFacility_name());
        BaseFunctions.setGlideImage(context,holder.tv_inst_icon,ao.getFacility_image());
        holder.tv_desc.setText(ao.getDetails());
        holder.tv_start_date.setText(BaseFunctions.dateExtractor(ao.getStart_date()));
        holder.tv_end_date.setText(BaseFunctions.dateExtractor(ao.getEnd_date()));
        if (ao.getIs_hidden() == 0){
            holder.sw_show.setChecked(true);
        }else {
            holder.sw_show.setChecked(false);
        }
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AdvertisementDetailsActivity.class);
                intent.putExtra("advertisement_id",ao.getId());
                context.startActivity(intent);
            }
        });
        holder.btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage(context.getResources().getString(R.string.edit_inst_ads_delete_message)).
                        setPositiveButton(context.getResources().getString(R.string.add_offer_yes), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                callDeleteAPI(String.valueOf(ao.getId()),position);
                            }
                        }).setNegativeButton(context.getResources().getString(R.string.add_offer_no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).show();
            }
        });
        holder.sw_show.addSwitchObserver(new RMSwitch.RMSwitchObserver() {
            @Override
            public void onCheckStateChange(RMSwitch switchView, boolean isChecked) {
                if (isChecked){
                    callUpdateAPI(String.valueOf(ao.getId()),
                            String.valueOf(ao.getPackage_id()),
                            String.valueOf(ao.getFacility_id()),
                            ao.getTitle(),
                            ao.getStart_date(),
                            ao.getEnd_date(),
                            ao.getDetails(),
                            "0");
                }else {
                    callUpdateAPI(String.valueOf(ao.getId()),
                            String.valueOf(ao.getPackage_id()),
                            String.valueOf(ao.getFacility_id()),
                            ao.getTitle(),
                            ao.getStart_date(),
                            ao.getEnd_date(),
                            ao.getDetails(),
                            "1");
                }
            }
        });
    }

    private void callDeleteAPI(final String ad_id, final int position){
        AdvertisementsAPIsClass.deleteAd(sharedPreferences,
                context,
                sharedPreferences.getString("language", "ar"),
                sharedPreferences.getString("service_token", ""),
                ad_id,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json !=null){
                            Toast.makeText(context, context.getResources().getString(R.string.edit_inst_ads_delete_success), Toast.LENGTH_SHORT).show();
                            list.remove(position);
                            notifyDataSetChanged();
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, context.getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(context.getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callDeleteAPI(ad_id,position);
                                    }
                                }).setActionTextColor(context.getResources().getColor(R.color.white)).show();
                    }
                });
    }

    private void callUpdateAPI(final String ad_id, final String package_id, final String facility_id, final String title, final String start_date,
                               final String end_date, final String details, final String is_hidden){
        AdvertisementsAPIsClass.updateAdvertisement(sharedPreferences,
                context,
                sharedPreferences.getString("language", "ar"),
                sharedPreferences.getString("service_token", ""),
                ad_id,
                title,
                details,
                package_id,
                facility_id,
                start_date,
                end_date,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json !=null){
                            if (is_hidden.equals("0")){
                                Toast.makeText(context, context.getResources().getString(R.string.edit_inst_ads_shown), Toast.LENGTH_SHORT).show();
                            }else {
                                Toast.makeText(context, context.getResources().getString(R.string.edit_inst_ads_hidden), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, context.getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(context.getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callUpdateAPI(ad_id,package_id,facility_id,title,start_date,end_date,details,is_hidden);
                                    }
                                }).setActionTextColor(context.getResources().getColor(R.color.white)).show();
                    }
                });

    }
}
