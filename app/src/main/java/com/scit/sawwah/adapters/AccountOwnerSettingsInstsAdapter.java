package com.scit.sawwah.adapters;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.scit.sawwah.Models.InstitutionObject;
import com.scit.sawwah.R;
import com.scit.sawwah.activities.ProfileActivity;
import com.scit.sawwah.interfaces.IMove;
import com.scit.sawwah.tools.BaseFunctions;

import java.util.List;

/**
 * Created by Misheal on 8/20/2019.
 */

public class AccountOwnerSettingsInstsAdapter extends  RecyclerView.Adapter<AccountOwnerSettingsInstsAdapter.ViewHolder> {
    private List<InstitutionObject> list;
    private Context context;
    private IMove iMove;


    public AccountOwnerSettingsInstsAdapter( Context context,List<InstitutionObject> list,IMove iMove) {
        this.context = context;
        this.iMove = iMove;
        this.list = list;

    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_title;
        private ImageView img_image;
        private CardView layout;

        public ViewHolder(View view) {
            super(view);
            tv_title = view.findViewById(R.id.item_account_owner_settings_name);
            img_image = view.findViewById(R.id.item_account_owner_settings_image);
            layout = view.findViewById(R.id.item_account_owner_settings_layout);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_account_owner_settings_inst, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final InstitutionObject io = list.get(position);
        holder.tv_title.setText(io.getName());
        BaseFunctions.setGlideImage(context,holder.img_image,io.getImage_icon());
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ProfileActivity.class);
                intent.putExtra("institution_id",io.getId());
                context.startActivity(intent);
            }
        });
    }
}
