package com.scit.sawwah.interfaces;

public interface OnChooseCategory {

    void choose(String category_id);
}
