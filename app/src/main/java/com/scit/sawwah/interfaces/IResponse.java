package com.scit.sawwah.interfaces;

/**
 * Created by Misheal on 8/30/2019.
 */

public interface IResponse {
    void onResponse();
    void onResponse(Object json);
}
