package com.scit.sawwah.interfaces;

public interface IPhoto {

    void accept(int position);

    void reject(int position);

}
