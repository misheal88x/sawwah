package com.scit.sawwah.interfaces;

public interface OnSortTypeChoose {
    void sort(String sort_type);
}
