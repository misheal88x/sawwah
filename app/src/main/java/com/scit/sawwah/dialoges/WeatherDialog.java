package com.scit.sawwah.dialoges;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.scit.sawwah.APIClasses.WeatherAPIClass;
import com.scit.sawwah.Models.WeatherListObject;
import com.scit.sawwah.Models.WeatherResponse;
import com.scit.sawwah.R;
import com.scit.sawwah.adapters.WeatherDayAdapter;
import com.scit.sawwah.interfaces.IFailure;
import com.scit.sawwah.interfaces.IMove;
import com.scit.sawwah.interfaces.IResponse;
import com.scit.sawwah.tools.BaseFunctions;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Misheal on 8/15/2018.
 */

public class WeatherDialog extends AlertDialog {
    private Context context;
    private ImageView background;
    private List<WeatherListObject> days_list;
    private RecyclerView days_recycler;
    private RelativeLayout root;
    private WeatherDayAdapter days_adapter;
    private LinearLayoutManager days_layout_manager;
    private String lat = "";
    private String lng = "";
    private String city = "";
    private TextView tv_low_heat,tv_high_heat;
    private TextView tv_start_date,tv_end_date;
    private TextView tv_desc;
    private ImageView img_today_top_icon;
    private TextView tv_today_top_temp,tv_today_top_name,tv_today_top_number,tv_today_top_city;
    private TextView tv_today_bottom_temp,tv_today_bottom_name,tv_today_bottom_number,tv_today_bottom_city;
    private TextView tv_today_bottom_weather;
    private ImageView img_today_bottom_icon;
    public WeatherDialog(@NonNull Context context,String lat,String lng,String city) {
        super(context);
        this.context = context;
        this.lat = lat;
        this.lng = lng;
        this.city = city;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialoge_weather);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        if (Build.VERSION.SDK_INT < 16){
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
        else {
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        }
        //getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        getWindow().setLayout(LinearLayoutCompat.LayoutParams.MATCH_PARENT,
                LinearLayoutCompat.LayoutParams.MATCH_PARENT);
        getWindow().getAttributes().windowAnimations = R.style.SlideAnimation;
        init_views();
        days_list = new ArrayList<>();
        days_layout_manager = new LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false);
        days_adapter = new WeatherDayAdapter(context, days_list,new IMove() {
            @Override
            public void move(int position) {

            }
        });
        days_recycler.setLayoutManager(days_layout_manager);
        days_recycler.setAdapter(days_adapter);
        callAPI();
    }

    private void init_views(){
        root = findViewById(R.id.weather_layout);
        background = findViewById(R.id.weather_background);
        days_recycler = findViewById(R.id.weather_days_recycler);
        background.setImageResource(R.drawable.cloudy_backgroundt);

        tv_low_heat = findViewById(R.id.weather_low_heat);
        tv_high_heat = findViewById(R.id.weather_high_heat);
        tv_start_date = findViewById(R.id.weather_start_date);
        tv_end_date = findViewById(R.id.weather_end_date);
        tv_desc = findViewById(R.id.weather_desc);
        img_today_top_icon = findViewById(R.id.weather_today_top_icon);
        tv_today_top_temp = findViewById(R.id.weather_today_top_temp);
        tv_today_top_name = findViewById(R.id.weather_today_top_name);
        tv_today_top_number = findViewById(R.id.weather_today_top_number);
        tv_today_top_city = findViewById(R.id.weather_today_top_city);
        tv_today_bottom_temp = findViewById(R.id.weather_today_bottom_temp);
        tv_today_bottom_name = findViewById(R.id.weather_today_bottom_name);
        tv_today_bottom_number = findViewById(R.id.weather_today_bottom_number);
        tv_today_bottom_city = findViewById(R.id.weather_today_bottom_city);
        tv_today_bottom_weather = findViewById(R.id.weather_today_bottom_weather);
        img_today_bottom_icon = findViewById(R.id.weather_today_bottom_icon);
        root.setVisibility(View.GONE);
    }
    @Override
    public void dismiss() {
        super.dismiss();
    }

    @Override
    public void setOnDismissListener(OnDismissListener listener) {
        super.setOnDismissListener(listener);
    }

    private void callAPI(){
        WeatherAPIClass.getWeather(context,
                lat,
                lng,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json !=null){
                            String json1 = new Gson().toJson(json);
                            WeatherResponse success = new Gson().fromJson(json1,WeatherResponse.class);
                            if (success.getList().size()>0){
                                if (success.getList()!=null){
                                    if (success.getList().size()>0){
                                        days_list.add(success.getList().get(8));
                                        days_list.add(success.getList().get(16));
                                        days_list.add(success.getList().get(24));
                                        days_list.add(success.getList().get(32));
                                        if (success.getList().get(0)!=null){
                                            if (success.getList().get(0).getDt_txt()!=null){
                                                tv_start_date.setText(BaseFunctions.dateExtractor(success.getList().get(0).getDt_txt()));
                                            }
                                            if (success.getList().get(0).getMain()!=null){
                                                tv_low_heat.setText(String.valueOf(success.getList().get(0).getMain().getTemp_min()));
                                                tv_high_heat.setText(String.valueOf(success.getList().get(0).getMain().getTemp_max()));
                                                tv_today_top_temp.setText(String.valueOf(success.getList().get(0).getMain().getTemp()));
                                                tv_today_bottom_temp.setText(String.valueOf(success.getList().get(0).getMain().getTemp()));
                                            }
                                            if (success.getList().get(0).getWeather()!=null){
                                                if (success.getList().get(0).getWeather().size()>0){
                                                    if (success.getList().get(0).getWeather().get(0).getMain()!=null){
                                                        switch (success.getList().get(0).getWeather().get(0).getMain()){
                                                            //Sunny
                                                            case "Clear" : {
                                                                background.setImageResource(R.drawable.sunny_backgroundt);
                                                                tv_desc.setText(context.getResources().getString(R.string.weather_sunny_desc));
                                                                img_today_top_icon.setImageResource(R.drawable.ic_sunny);
                                                                img_today_bottom_icon.setImageResource(R.drawable.ic_sunny);
                                                                tv_today_bottom_weather.setText(context.getResources().getString(R.string.weather_sunny));
                                                            }break;
                                                            //Rainy
                                                            case "Drizzle" :
                                                            case "Rain" : {
                                                                background.setImageResource(R.drawable.rain_backgroundt);
                                                                tv_desc.setText(context.getResources().getString(R.string.weather_rainy_desc));
                                                                img_today_top_icon.setImageResource(R.drawable.ic_weather_raining);
                                                                img_today_bottom_icon.setImageResource(R.drawable.ic_weather_raining);
                                                                tv_today_bottom_weather.setText(context.getResources().getString(R.string.weather_rainy));
                                                            }break;
                                                            //Cloudy
                                                            case "Mist" :
                                                            case "Smoke" :
                                                            case "Haze" :
                                                            case "Dust" :
                                                            case "Fog" :
                                                            case "Sand" :
                                                            case "Ash" :
                                                            case "Clouds" :{
                                                                background.setImageResource(R.drawable.cloudy_backgroundt);
                                                                tv_desc.setText(context.getResources().getString(R.string.weather_cloudy_desc));
                                                                img_today_top_icon.setImageResource(R.drawable.ic_cloudy);
                                                                img_today_bottom_icon.setImageResource(R.drawable.ic_cloudy);
                                                                tv_today_bottom_weather.setText(context.getResources().getString(R.string.weather_cloudy));
                                                            }break;
                                                            //Snowy
                                                            case "Snow" : {
                                                                background.setImageResource(R.drawable.snow_backgroundt);
                                                                tv_desc.setText(context.getResources().getString(R.string.weather_snowy_desc));
                                                                img_today_top_icon.setImageResource(R.drawable.ic_snowy);
                                                                img_today_bottom_icon.setImageResource(R.drawable.ic_snowy);
                                                                tv_today_bottom_weather.setText(context.getResources().getString(R.string.weather_snowy));
                                                            }break;
                                                            //Stormy
                                                            case "Thunderstorm" :
                                                            case "Squall" :
                                                            case "Tornado" :{
                                                                background.setImageResource(R.drawable.rain_backgroundt);
                                                                tv_desc.setText(context.getResources().getString(R.string.weather_stormy_desc));
                                                                img_today_top_icon.setImageResource(R.drawable.ic_stormy);
                                                                img_today_bottom_icon.setImageResource(R.drawable.ic_stormy);
                                                                tv_today_bottom_weather.setText(context.getResources().getString(R.string.weather_stormy));
                                                            }break;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        if (success.getList().get(39).getDt_txt()!=null){
                                            tv_end_date.setText(BaseFunctions.dateExtractor(success.getList().get(39).getDt_txt()));
                                        }
                                    }
                                }
                            }
                            tv_today_top_city.setText(city);
                            tv_today_bottom_city.setText(city);
                            tv_today_top_name.setText(BaseFunctions.dateToDayConverter(BaseFunctions.stringToDateConverter(BaseFunctions.todayDateString())));
                            tv_today_bottom_name.setText(BaseFunctions.dateToDayConverter(BaseFunctions.stringToDateConverter(BaseFunctions.todayDateString())));
                            String day_num = BaseFunctions.subString(BaseFunctions.todayDateString(),8,10);
                            switch (day_num){
                                case "01" : {
                                    tv_today_top_number.setText(day_num+"st");
                                    tv_today_bottom_number.setText(day_num+"st");
                                }break;
                                case "02" : {
                                    tv_today_top_number.setText(day_num+"nd");
                                    tv_today_bottom_number.setText(day_num+"nd");
                                }break;
                                case "03" : {
                                    tv_today_top_number.setText(day_num+"rd");
                                    tv_today_bottom_number.setText(day_num+"rd");
                                }default:{
                                    tv_today_top_number.setText(day_num+"th");
                                    tv_today_bottom_number.setText(day_num+"th");
                                }
                            }
                            root.setVisibility(View.VISIBLE);
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, context.getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(context.getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callAPI();
                                    }
                                }).setActionTextColor(context.getResources().getColor(R.color.white)).show();
                    }
                });
    }
}
