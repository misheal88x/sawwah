package com.scit.sawwah.dialoges;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.LinearLayoutCompat;
import android.view.View;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.RadioButton;

import com.scit.sawwah.R;
import com.scit.sawwah.interfaces.OnSortTypeChoose;

public class SortDialog extends AlertDialog {

    private Activity activity;
    private OnSortTypeChoose onSortTypeChoose;
    private RadioButton rb_sort_by_az, rb_sort_by_most_visited, rb_sort_by_top_rated, rb_sort_by_nearest,rb_sort_by_most_chosen;

    public SortDialog(@NonNull Context context, OnSortTypeChoose onSortTypeChoose) {
        super(context);
        this.onSortTypeChoose = onSortTypeChoose;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialoge_items_sort_by);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        //getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        if (Build.VERSION.SDK_INT < 16){
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
        else {
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        }
        //getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        getWindow().setLayout(LinearLayoutCompat.LayoutParams.MATCH_PARENT,
                LinearLayoutCompat.LayoutParams.MATCH_PARENT);
        getWindow().getAttributes().windowAnimations = R.style.SlideAnimation;

        rb_sort_by_az = findViewById(R.id.rb_sort_by_az);
        rb_sort_by_most_visited = findViewById(R.id.rb_sort_by_top_visited);
        rb_sort_by_top_rated = findViewById(R.id.rb_sort_by_top_rating);
        rb_sort_by_nearest = findViewById(R.id.item_institution_row_order_by_nearest_place);
        rb_sort_by_most_chosen = findViewById(R.id.rb_sort_by_most_chosen);

        rb_sort_by_az.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    onSortTypeChoose.sort("az");
                    dismiss();
                }
            }
        });
        rb_sort_by_most_visited.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    onSortTypeChoose.sort("visit");
                    dismiss();
                }
            }
        });
        rb_sort_by_top_rated.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    onSortTypeChoose.sort("rate");
                    dismiss();
                }
            }
        });
        rb_sort_by_nearest.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    onSortTypeChoose.sort("near");
                    dismiss();
                }
            }
        });
        rb_sort_by_most_chosen.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    onSortTypeChoose.sort("chosen");
                    dismiss();
                }
            }
        });

    }


    @Override
    public void dismiss() {
        super.dismiss();

    }

    @Override
    public void setOnDismissListener(OnDismissListener listener) {
        super.setOnDismissListener(listener);

    }

}
