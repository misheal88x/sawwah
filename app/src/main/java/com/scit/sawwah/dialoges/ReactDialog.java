package com.scit.sawwah.dialoges;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.WindowManager;

import com.google.gson.Gson;
import com.scit.sawwah.Models.ReactionTypeObject;
import com.scit.sawwah.R;
import com.scit.sawwah.adapters.ReactAdapter;
import com.scit.sawwah.interfaces.IFailure;
import com.scit.sawwah.interfaces.IMove;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Misheal on 9/14/2019.
 */

public class ReactDialog extends AlertDialog {
    private RecyclerView rv_reactions;
    private List<ReactionTypeObject> list_reactions;
    private GridLayoutManager layoutManager;
    private ReactAdapter adapter;
    private Context context;
    private SharedPreferences sharedPreferences;
    private IMove iMove;
    private IFailure iFailure;
    public ReactDialog(@NonNull Context context,IMove iMove,IFailure iFailure) {
        super(context);
        this.context = context;
        this.iMove = iMove;
        this.iFailure = iFailure;
        this.sharedPreferences = context.getSharedPreferences("Main",Context.MODE_PRIVATE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_react);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        //getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        if (Build.VERSION.SDK_INT < 16){
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
        else {
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        }
        //getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        getWindow().setLayout(LinearLayoutCompat.LayoutParams.MATCH_PARENT,
                LinearLayoutCompat.LayoutParams.MATCH_PARENT);
        getWindow().getAttributes().windowAnimations = R.style.FadeAnimation;
        init_views();
        init_dialog();
        init_events();
    }

    private void init_views(){
        rv_reactions = findViewById(R.id.dialog_react_recycler);
    }
    private void init_dialog(){
        init_recycler();
    }
    private void init_events(){

    }

    private void init_recycler(){
        list_reactions = new ArrayList<>();
        ReactionTypeObject[] array_reactions = new Gson().fromJson(sharedPreferences.getString("reactions","[]"),ReactionTypeObject[].class);
        if (array_reactions.length > 0){
            for (ReactionTypeObject rto : array_reactions){
                list_reactions.add(rto);
            }
        }
        adapter = new ReactAdapter(context, list_reactions, new IMove() {
            @Override
            public void move(int position) {
                iMove.move(position);
            }
        });
        layoutManager = new GridLayoutManager(context,3);
        rv_reactions.setLayoutManager(layoutManager);
        rv_reactions.setAdapter(adapter);
    }

    @Override
    public void dismiss() {
        super.dismiss();
        iFailure.onFailure();
    }
}
