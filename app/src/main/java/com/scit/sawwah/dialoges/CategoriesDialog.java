package com.scit.sawwah.dialoges;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.widget.NestedScrollView;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.scit.sawwah.APIClasses.CategoriesAPIsClass;
import com.scit.sawwah.Models.CategoryObject;
import com.scit.sawwah.Models.InstCategoriesResponse;
import com.scit.sawwah.R;
import com.scit.sawwah.activities.CategoryInstitutionsActivity;
import com.scit.sawwah.adapters.InstitutionsCategoriesAdapter;
import com.scit.sawwah.interfaces.IFailure;
import com.scit.sawwah.interfaces.IMove;
import com.scit.sawwah.interfaces.IResponse;
import com.scit.sawwah.tools.BaseFunctions;

import java.util.ArrayList;
import java.util.List;

public class CategoriesDialog extends AlertDialog {
    private Activity activity;
    private ImageView btn_close;
    private RecyclerView rv_categories;
    private List<CategoryObject> list_categories;
    private GridLayoutManager layout_manager_categories;
    private InstitutionsCategoriesAdapter adapter_categories;
    private Context context;
    private ImageView img_loading;
    private SharedPreferences sharedPreferences;
    private LinearLayout root;
    private NestedScrollView scrollView;
    int currentPage = 1;


    public CategoriesDialog(@NonNull Context context) {
        super(context);
        this.context = context;
        sharedPreferences = context.getSharedPreferences("Main",Context.MODE_PRIVATE);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialoge_categories);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        if (Build.VERSION.SDK_INT < 16){
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
        else {
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        }
        //getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        getWindow().setLayout(LinearLayoutCompat.LayoutParams.MATCH_PARENT,
                LinearLayoutCompat.LayoutParams.MATCH_PARENT);
        getWindow().getAttributes().windowAnimations = R.style.SlideAnimation;
        init_views();
        init_recycler();
        init_events();
        callAPI(0,currentPage);
    }


    @Override
    public void dismiss() {
        super.dismiss();
    }

    @Override
    public void setOnDismissListener(OnDismissListener listener) {
        super.setOnDismissListener(listener);
    }

    private void init_views(){
        btn_close = findViewById(R.id.btn_close);
        rv_categories = findViewById(R.id.categories_dialog_recycler);
        root = findViewById(R.id.categories_dialog_layout);
        scrollView = findViewById(R.id.categories_dialog_scrollview);
        img_loading = findViewById(R.id.categories_dialog_loading);
        BaseFunctions.setGlideDrawableImage(context,img_loading,R.drawable.loading_indicator);
    }

    private void init_events(){
        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    private void init_recycler(){
        list_categories = new ArrayList<>();
        adapter_categories = new InstitutionsCategoriesAdapter(context, list_categories, new IMove() {
            @Override
            public void move(int position) {
                Intent intent = new Intent(context, CategoryInstitutionsActivity.class);
                intent.putExtra("category_id",list_categories.get(position).getId());
                context.startActivity(intent);
            }
        });
        layout_manager_categories = new GridLayoutManager(context,3);
        rv_categories.setLayoutManager(layout_manager_categories);
        rv_categories.hasFixedSize();
        rv_categories.setAdapter(adapter_categories);
        scrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if(v.getChildAt(v.getChildCount() - 1) != null) {
                    if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                            scrollY > oldScrollY) {
                        if (list_categories.size()>=20){
                            currentPage++;
                            callAPI(1,currentPage);
                        }
                    }
                }
            }
        });
    }

    private void callAPI(final int type, final int page){
        if (type == 0){
            img_loading.setVisibility(View.VISIBLE);
        }
        CategoriesAPIsClass.getCategories(sharedPreferences,
                context,
                sharedPreferences.getString("language", "ar"),
                sharedPreferences.getString("service_token", ""),
                type,
                page,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json !=null){
                            String json1 = new Gson().toJson(json);
                            InstCategoriesResponse success = new Gson().fromJson(json1,InstCategoriesResponse.class);
                            if (success.getData()!=null){
                                img_loading.setVisibility(View.GONE);
                                if (success.getData().size()>0){
                                    for (CategoryObject co : success.getData()){
                                        list_categories.add(co);
                                        adapter_categories.notifyDataSetChanged();
                                    }
                                }else {
                                    if (type == 0){
                                        try {
                                            Toast.makeText(context, context.getResources().getString(R.string.no_data), Toast.LENGTH_SHORT).show();
                                        }catch (Exception e){
                                            Log.i("categories_exception", "onResponse: "+e.getMessage());
                                        }
                                    }else {
                                        Snackbar.make(root,context.getResources().getString(R.string.no_more),Snackbar.LENGTH_SHORT).show();
                                    }
                                }
                            }
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        img_loading.setVisibility(View.GONE);
                        Snackbar.make(root, context.getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(context.getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callAPI(type,page);
                                    }
                                }).setActionTextColor(context.getResources().getColor(R.color.white)).show();
                    }
                });
    }

    public void runAnimation(RecyclerView recyclerView,int type,InstitutionsCategoriesAdapter adapter){
        Context context = recyclerView.getContext();
        LayoutAnimationController controller = null;
        if (type == 0){ // Fall down animation
            controller = AnimationUtils.loadLayoutAnimation(context,R.anim.layout_fall_down_vertical);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutAnimation(controller);
            recyclerView.getAdapter().notifyDataSetChanged();
            recyclerView.scheduleLayoutAnimation();
        }
    }
}
