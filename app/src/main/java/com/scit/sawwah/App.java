package com.scit.sawwah;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import androidx.multidex.MultiDex;
import androidx.core.app.ActivityCompat;
import android.util.Log;

import com.google.firebase.FirebaseApp;
import com.scit.sawwah.activities.LoginActivity;
import com.scit.sawwah.tools.LocaleHelper;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by Misheal on 8/17/2018.
 */
public class App extends Application implements LocationListener {
    protected boolean gps_enabled, network_enabled;
    private LocationManager locationManager;
    private String provider;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    public static App instance;

    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseApp.initializeApp(this);
        instance = this;
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/STCRegular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        provider = LocationManager.GPS_PROVIDER;
        // getting GPS status
        gps_enabled = locationManager
                .isProviderEnabled(LocationManager.GPS_PROVIDER);
        // getting network status
        network_enabled = locationManager
                .isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.i("perno_called", "one :  " + "yes");
        }
        Log.i("peryes_called", "two :  "+"yes");
        //Location location = locationManager.getLastKnownLocation(provider);
        try {
            if (gps_enabled) {
                locationManager.requestLocationUpdates(
                        LocationManager.GPS_PROVIDER, 10000, 1,
                        this);
            }else if (network_enabled) {
                locationManager.requestLocationUpdates(
                        LocationManager.NETWORK_PROVIDER, 10000, 1,
                        this);
            }
        }catch (Exception e){}
        sharedPreferences = getSharedPreferences("Main",Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }
    public static void changeFont(){
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/STC-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());
    }
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base, "ar"));
        MultiDex.install(this);

    }

    public static App getInstance(){
        return instance;
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.i("test_location", "onLocationChanged: "+String.valueOf(location.getLatitude())+
                String.valueOf(location.getLongitude()));
        editor.putString("my_lat",String.valueOf(location.getLatitude()));
        editor.putString("my_lng",String.valueOf(location.getLongitude()));
        editor.commit();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
